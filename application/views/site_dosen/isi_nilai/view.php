<script type="text/javascript">
function konversi(id){
	var b_nilai = $("#b_nilai_"+id).val();
	//alert(b_nilai);
	if (b_nilai>94 && b_nilai<=100){
		//alert(b_nilai+" "+"A"+" "+"4.0");
		$("#nilai_"+id).val('4.0');
	}else if(b_nilai>89 && b_nilai<=94){
		// alert(b_nilai+" "+"A-"+" "+"3.7");
		$("#nilai_"+id).val('3.7');
	}else if(b_nilai>84 && b_nilai<=89){
		// alert(b_nilai+" "+"B+"+" "+"3.3");
		$("#nilai_"+id).val('3.3');
	}else if(b_nilai>79 && b_nilai<=84){
		// alert(b_nilai+" "+"B"+" "+"3.0");
		$("#nilai_"+id).val('3.0');
	}else if(b_nilai>74 && b_nilai<=79){
		// alert(b_nilai+" "+"B-"+" "+"2.7");
		$("#nilai_"+id).val('2.7');
	}else if(b_nilai>69 && b_nilai<=74){
		// alert(b_nilai+" "+"C+"+" "+"2.3");
		$("#nilai_"+id).val('2.3');
	}else if(b_nilai>64 && b_nilai<=69){
		// alert(b_nilai+" "+"C-"+" "+"2.0");
		$("#nilai_"+id).val('2.0');
	}else{
		alert("Bobot Nilai Harus Diatas 64");
	}
	simpan_nilai(id);	
}

function simpan_nilai(id){
	var nilai = $("#nilai_"+id).val();
	var b_nilai = $("#b_nilai_"+id).val();
	//alert(nilai+" "+b_nilai);
	var string = "id="+id+"&nilai="+nilai;

	//alert(string);
	$.ajax({
		type	: 'POST',
		url		: "<?php echo site_url(); ?>/site_dosen/isi_nilai/simpan_nilai",
		data	: string,
		cache	: false,
		success	: function(data){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: data,
				class_name: 'gritter-info'
			});
		}
	});
}
</script>
<table  class="table fpTable lcnp table-striped table-bordered table-hover dt-responsive">
    <thead>
        <tr>
            <th class="center">No</th>
            <th class="center">NIM</th>
            <th class="center">Nama</th>
            <th class="center">L/P</th>
			<th class="center">Kelas</th>
			<th class="center">Bobot Nilai</th>
            <th class="center">Nilai Akhir</th>
        </tr>
    </thead>
    <tbody>
    	<?php
		$i=1;
		foreach($data->result() as $dt){
			$data_mhs = $this->model_data->getInfoMhs($dt->nim);
			$nama = $data_mhs['nama_mhs'];
			$sex = $data_mhs['sex'];
			$kelas = $data_mhs['kelas'];
			$nilai = $dt->nilai_akhir;
		?>
        <tr>
        	<td class="center span1"><?php echo $i;?></td>
            <td ><?php echo $dt->nim;?>
           	<input type="hidden" name="nim_<?php echo $i;?>" id="nim_<?php echo $i;?>" value="<?php echo $dt->nim;?>" />
            </td>
            <td ><?php echo $nama;?></td>
            <td class="center"><?php echo $sex;?></td>
			<td class="center"><?php echo $kelas;?></td>
			<td class="center span2"> 
				<input type="text" name="b_nilai_<?php echo $dt->id_krs;?>" id="b_nilai_<?php echo $dt->id_krs;?>" onchange="konversi(<?php echo $dt->id_krs;?>)" placeholder="Isi Untuk Konversi Nilai Akhir" class="span3">
			</td>
            <td class="center span2">
            	<select name="nilai_<?php echo $dt->id_krs;?>" id="nilai_<?php echo $dt->id_krs;?>" class="span1" onchange="simpan_nilai('<?php echo $dt->id_krs;?>')">
	            	<option value="-">-</option>
	            	<!-- simpan_nilai('<?php echo $dt->id_krs;?>') -->

	            	<?php
					$data = $this->model_data->nilai();
					foreach($data as $dt){
						if($dt==$nilai){
							$select = "selected=true";
						}else{
							$select = '';
						}

					?>
			        <option value="<?php echo $dt;?>" <?php echo $select;?>><?php echo $dt;?></option>
					<?php
					
					}

					?>
            	</select>

            </td>
        </tr>
		<?php
		$i++;
		} ?>
    </tbody>
    <input type="hidden" name="jml_data" id="jml_data" value="<?php echo $i-1;?>" />
</table>