<div class="row-fluid">
    <div class="span12">
        <!--PAGE CONTENT BEGINS-->

        <div class="alert alert-block alert-warning">

            <i class="icon-ok green"></i>
            <?php?>
            Selamat datang <strong><?= @$_SESSION['nama_lengkap'];?></strong> di
            <strong class="green">
                Aplikasi <?php echo app_setting()['data_setting'][0]->nama_aplikasi;?>
                <small>(v1.1.0)</small>
            </strong>
            ,
            <?php echo app_setting()['data_setting'][0]->nama_pendek;?>  <?php echo app_setting()['data_setting'][0]->nama_instansi;?>
        </div>
</div>
</div>

<?= $this->load->view('site_dosen/view_info_dosen');?>
<br/>
<!-- <?= $this->load->view('site_mahasiswa/view_materi_kuliah');?> -->
<?= $this->load->view('site_dosen/view_materi_seminar');?>
