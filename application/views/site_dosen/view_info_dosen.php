<script type="text/javascript">
$(document).ready(function(){

});



</script>
<div class="row-fluid">
<div class="table-header dosen">
    INFO KAMPUS
</div>

<table  class="table fpTable lcnp table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="center" width="5%">No</th>
						<th class="center" width="15%">Tanggal</th>
						<th class="center" width="25%">Judul</th>
            <th class="center" width="40%">Info</th>
            <th class="center" width="15%">Pengirim</th>

        </tr>
    </thead>
    <tbody>
    	<?php
      $this->db->select('*');
      $this->db->where('jenis', 'DOSEN');
      $this->db->or_where('jenis', 'UMUM');
      $this->db->order_by('id','DESC');
      $data = $this->db->get('info');
		$i=1;
		foreach($data->result() as $dt){ ?>
        <tr>
        	<td class="center" width="5%"><?php echo $i++?></td>
            <td class="center" width="15%"><?php echo $dt->tgl;?></td>
            <td width="25%"><?php echo $dt->judul;?></td>
            <td width="40%"><?php echo $dt->info;?></td>
            <td class="center" width="15%"><?php echo $dt->pengirim;?></td>
        </tr>
		<?php } ?>
    </tbody>
</table>
</div>
