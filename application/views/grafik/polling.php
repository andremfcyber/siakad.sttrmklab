
<script type="text/javascript">
$(document).ready(function(){

	$("#view").click(function(){
		// console.log('testing');
		cari_data();
	});

	function cari_data(){
		var string = $("#my-form").serialize();
		// console.log('tet');
		// alert('test');

		if(!$("#th_ak").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Th Akademik tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#th_ak").focus();
			return false();
		}

    if(!$("#poll_jenis_id").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Jenis tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#poll_jenis_id").focus();
			return false();
		}

    if(!$("#poll_pertanyaan_id").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Pertanyaan tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#poll_pertanyaan_id").focus();
			return false();
		}

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url('grafik/getPolling'); ?>",
			data	: string,
			cache	: false,
			success	: function(data){
				$("#data").html(data);
			}
		});
	}

	$("#poll_jenis_id").change(function(){
		cari_pertanyaan();
		cari_object();
	});

	function cari_pertanyaan()
	{
		var string = {
			th_ak : $("#th_ak").val(),
			poll_jenis_id : $("#poll_jenis_id").val()
		};

		if(!$("#th_ak").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Th Akademik tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#th_ak").focus();
			return false();
		}

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url('lap_polling/cari_pertanyaan'); ?>",
			data	: string,
			cache	: false,
			success	: function(data){
				// console.log(data);
				$("#poll_pertanyaan_id").html(data);
			}
		});
	}

	function cari_object()
	{
		var string = {
			th_ak : $("#th_ak").val(),
			poll_jenis_id : $("#poll_jenis_id").val()
		};

		if(!$("#th_ak").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Th Akademik tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#th_ak").focus();
			return false();
		}

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url('lap_polling/cari_object'); ?>",
			data	: string,
			cache	: false,
			success	: function(data){
				// console.log(data);
				$("#object").html(data);
			}
		});
	}



});
</script>


<div class="widget-box ">
    <div class="widget-header">
        <h4 class="lighter smaller">
            <i class="icon-book blue"></i>
            <?php echo $judul;?>
        </h4>
    </div>

    <div class="widget-body">
    	<div class="widget-main no-padding">

            <form class="form-horizontal" name="my-form" id="my-form">
							<fieldset>
                    <div class="control-group">
                        <label class="control-label" for="form-field-1">Tahun Akademik</label>
                        <div class="controls">
                            <select name="th_ak" id="th_ak" class="span2">
                            	<option value="" selected="selected">-Pilih-</option>
                                <?php
																	$this->db->select('th_akademik');
																	$this->db->from('th_akademik');
																	$this->db->group_by('th_akademik');
																	$this->db->order_by('th_akademik','DESC');
																	$data = $this->db->get();
																	foreach($data->result() as $dt){
																	?>
                                	<option value="<?php echo $dt->th_akademik;?>"><?php echo $dt->th_akademik;?></option>
                                <?php } ?>
                                </select>
                        </div>
                    </div>

										<div class="control-group">
                        <label class="control-label" for="form-field-1">Jenis</label>
                        <div class="controls">
                            <select name="poll_jenis_id" id="poll_jenis_id" class="span2">
                            	<option value="" selected="selected">-Pilih-</option>
                                <?php
								$data = $this->db->get('poll_jenis');
								foreach($data->result() as $dt){
								?>
                                <option value="<?php echo $dt->id;?>"><?php echo $dt->nama;?></option>
								<?php } ?>
                             </select>
                        </div>
                    </div>

										<div class="control-group">
                        <label class="control-label" for="form-field-1">Pertanyaan</label>
                        <div class="controls">
                            <select name="poll_pertanyaan_id" id="poll_pertanyaan_id" class="span7">
                            	<option value="" selected="selected">-Pilih-</option>
                             </select>
                        </div>
                    </div>

										<div class="control-group">
                        <label class="control-label" for="form-field-1">Dosen / Karyawan</label>
                        <div class="controls">
                            <select name="object" id="object" class="span5">
                            	<option value="" selected="selected">-Pilih-</option>
                             </select>
                        </div>
                    </div>

									</fieldset>

            	<div class="form-actions center">
                     <button type="button" name="view" id="view" class="btn btn-mini btn-info">
                     <i class="icon-double-angle-right"></i> Lihat
                     </button>
           			</div>
           </form>
        </div> <!-- wg body -->
    </div> <!--wg-main-->
</div>

<div id="data"></div>
