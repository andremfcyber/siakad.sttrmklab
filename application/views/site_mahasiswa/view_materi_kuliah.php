<script type="text/javascript">
$(document).ready(function(){

});



</script>
<div class="row-fluid">
<div class="table-header <?php echo ($_SESSION['kd_prodi'] == '201' || $_SESSION['kd_prodi'] == '77101')?'mhs-teo':'mhs-pak';?>">
    MATERI KULIAH
</div>

<table  class="table fpTable lcnp table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="center">No</th>
						<th class="center">Mata Kuliah</th>
						<th class="center">Semester</th>
						<th class="center">Judul</th>
            <th class="center">File</th>
        </tr>
    </thead>
    <tbody>
    	<?php
      $nim = $_SESSION['username'];
      $currentSmt = $this->model_data->getCurrentSmt($nim);
      // echo $currentSmt;
      $this->db->order_by('kd_mk,id','DESC');
      $data = $this->db->get_where('materi_kuliah', array('smt' => $currentSmt));
		$i=1;
		foreach($data->result() as $dt){
			$mk = $this->model_data->cari_nama_mk($dt->kd_mk);
			?>

        <tr>
        	<td class="center span1"><?php echo $i++?></td>
            <td class="center"><?php echo $mk;?></td>
            <td class="center"><?php echo $dt->smt;?></td>
            <td ><?php echo $dt->judul;?></td>
            <td class="center">
							<a href="<?php echo base_url();?>assets/materi_kuliah/<?= $dt->file;?>" target="_blank">
							<?php echo $dt->file;?></a>
						</td>
        </tr>
		<?php } ?>
    </tbody>
</table>

</div>
