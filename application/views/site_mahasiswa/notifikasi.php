<ul class="nav ace-nav pull-right">
    <li class="light-green">
        <a data-toggle="dropdown" href="#" class="dropdown-toggle <?php echo ($_SESSION['kd_prodi'] == '201' || $_SESSION['kd_prodi'] == '77101')?'mhs-teo':'mhs-pak';?>">
        	<?php
			$u = @$_SESSION['username'];
			$foto = $this->model_data->cari_foto_mahasiswa($u);
        	if(!empty($foto)){
			?>
            	<img class="nav-user-photo" src="<?php echo base_url();?>assets/foto_mhs/<?php echo $foto;?>" alt="<?php echo $u;?>" />
            <?php }else{ ?>
            <img class="nav-user-photo" src="<?php echo base_url();?>assets/avatars/deddy.jpg" alt="Deddy Rusdiansyah" />
            <?php } ?>
            <span class="user-info">
                <small>Welcome,</small>
                <?php echo @$_SESSION['nama_lengkap'];?>
            </span>

            <i class="icon-caret-down"></i>
        </a>
        <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer">
            <li>
                <a href="<?php echo base_url();?>index.php/site_mahasiswa/profil">
                    <i class="icon-user"></i>
                    Edit Profile
                </a>
            </li>

            <li class="divider"></li>

            <li>
                <a href="<?php echo base_url();?>index.php/login/logout">
                    <i class="icon-off"></i>
                    Keluar
                </a>
            </li>
        </ul>
    </li>
</ul><!--/.ace-nav-->