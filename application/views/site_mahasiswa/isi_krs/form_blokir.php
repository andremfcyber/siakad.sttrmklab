<div class="alert alert-block alert-error">
<blockquote>
	<strong>Informasi !!!</strong>
    <p>Maaf. Pengisian <strong>KRS</strong> tidak bisa dilakukan. Silahkan <strong>Hubungi Bagian Keuangan</strong>.</p>

    <small>
        Bagian Keuangan.
    </small>
</blockquote>
</div>
