<script type="text/javascript">
$(document).ready(function(){

	$("#view").click(function(){
		cari_data();
	});

	function cari_data(){
		var smt = $("#smt").val();

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url(); ?>/site_mahasiswa/mata_kuliah/cari_data",
			data	: "smt="+smt,
			cache	: false,
			success	: function(data){
				$("#view_detail").html(data);
                setTimeout(function(){ reinitDatatable(); },50);

            }
        });
    }

    function reinitDatatable(){

        $(".fpTable").dataTable().fnDestroy();
        $(".fpTable").dataTable({
                                bSort: true,
                                bAutoWidth: true,
                                // scrollY:        300,
                                // scrollX:        true,
                                // scrollCollapse: true,
                                // paging:         false,
                                // fixedColumns:   true,
                                "iDisplayLength": 20, "aLengthMenu": [20,40,80,120], // can be removed for basic 10 items per page
                                "sPaginationType": "full_numbers",
                                "aoColumnDefs": [{"bSortable": false,
                                                 "aTargets": [ -1 , 0]}]});

    }


});
</script>

<div class="widget-box ">
    <div class="widget-header">
        <h4 class="lighter smaller">
            <i class="icon-desktop blue"></i>
            <?php echo $judul;?>
        </h4>
    </div>

    <div class="widget-body">
    	<div class="widget-main no-padding">

            <form class="form-horizontal" name="my-form" id="my-form" action="<?php echo base_url();?>index.php/lap_mata_kuliah/cetak" method="post">
							<fieldset>
                    <div class="control-group">
                        <label class="control-label" for="form-field-1">Semester</label>
                        <div class="controls">
                            <select name="smt" id="smt" class="span2">
                            	<option value="" selected="selected">-Pilih-</option>
                                <?php
								$data = $this->model_data->smt();
								foreach($data as $dt){
								?>
                                <option value="<?php echo $dt;?>"><?php echo $dt;?></option>
								<?php } ?>
                             </select>
                        </div>
                    </div>
									</fieldset>

            		<div class="form-actions center">
                     <button type="button" name="view" id="view" class="btn btn-mini btn-info">
                     <i class="icon-th"></i> Lihat Data
                     </button>
           				</div>
           </form>

           <?php
		  	echo  $this->session->flashdata('result_info');
		   ?>
        </div> <!-- wg body -->
    </div> <!--wg-main-->
</div>
<div id="view_detail"></div>
