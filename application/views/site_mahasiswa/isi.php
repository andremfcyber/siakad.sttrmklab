<div class="row-fluid">
    <div class="span12">
        <!--PAGE CONTENT BEGINS-->

        <div class="alert alert-block alert-warning">

            <i class="icon-ok green"></i>

            Selamat datang <strong><?=@$_SESSION['nama_lengkap'];?></strong> di
            <strong class="green">
                Aplikasi <?php echo  app_setting()['data_setting'][0]->nama_aplikasi;?>
                <small>(v1.1.0)</small>
            </strong>
            ,
            <?php echo app_setting()['data_setting'][0]->nama_pendek;?> <?php echo app_setting()['data_setting'][0]->nama_instansi;?>


        </div>

        <?php
          $nim = @$_SESSION['username'];
          $infoMhs = $this->model_data->getInfoMhs($nim);
          $nama_mhs = $infoMhs['nama_mhs'];
          $kelamin = $infoMhs['sex']=='L'?'Laki-laki':'Perempuan';
          $th_angkatan = $infoMhs['th_akademik'];
          $nama_prodi = $infoMhs['nama_prodi'];
          $kelas = $infoMhs['kelas'];
        ?>

        <div class="profile-user-info profile-user-info-striped">
								<div class="profile-info-row">
									<div class="profile-info-name"> NIM </div>
									<div class="profile-info-value">
										<span class="editable" id="username"><?php echo $nim;?></span>
									</div>
								</div>

								<div class="profile-info-row">
									<div class="profile-info-name"> Nama Lengkap </div>
									<div class="profile-info-value">
										<span class="editable" id="username"><?php echo $nama_mhs;?></span>
									</div>
								</div>

								<div class="profile-info-row">
									<div class="profile-info-name"> Kelamin </div>

									<div class="profile-info-value">
										<span class="editable" id="kelamin"><?php echo $kelamin;?></span>
									</div>
								</div>

                <div class="profile-info-row">
									<div class="profile-info-name"> Th Angkatan </div>

									<div class="profile-info-value">
										<span class="editable" id="signup"><?php echo $th_angkatan;?></span>
									</div>
								</div>

                <div class="profile-info-row">
									<div class="profile-info-name"> Program Studi </div>

									<div class="profile-info-value">
										<span class="editable" id="signup"><?php echo $nama_prodi;?></span>
									</div>
								</div>

                <div class="profile-info-row">
									<div class="profile-info-name"> Kelas </div>

									<div class="profile-info-value">
										<span class="editable" id="signup"><?php echo $kelas;?></span>
									</div>
								</div>

								<div class="profile-info-row">
									<div class="profile-info-name"> Th Akademik Aktif</div>

									<div class="profile-info-value">
										<span class="editable" id="signup"><?php echo $th_akademik = $this->model_global->getThAkademikAktif()['th_akademik'];?></span>
									</div>
								</div>

								<div class="profile-info-row">
									<div class="profile-info-name"> Semester Aktif </div>

									<div class="profile-info-value">
										<span class="editable" id="login"><?php echo $this->model_global->semester($nim,$th_akademik).' - '.$this->model_global->getThAkademikAktif()['semester'];?></span>
									</div>
								</div>


							</div>

                    <br/>
</div>
</div>
<?= $this->load->view('site_mahasiswa/view_info');?>
<br/>
<?= $this->load->view('site_mahasiswa/view_materi_kuliah');?>
<br/>
<?= $this->load->view('site_mahasiswa/view_materi_seminar');?>

