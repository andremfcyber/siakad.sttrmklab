<script type="text/javascript">
$(document).ready(function(){

});



</script>
<div class="row-fluid">
<div class="table-header <?php echo ($_SESSION['kd_prodi'] == '201' || $_SESSION['kd_prodi'] == '77101')?'mhs-teo':'mhs-pak';?>">
    MATERI SEMINAR
</div>

<table  class="table fpTable lcnp table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="center">No</th>
						<th class="center">Judul Seminar</th>
            <th class="center">File</th>
        </tr>
    </thead>
    <tbody>
    	<?php
      // echo $currentSmt;
      $data = $this->db->get('materi_seminar');
		$i=1;
		foreach($data->result() as $dt){
			?>

        <tr>
        	<td class="center span1"><?php echo $i++?></td>
            <td ><?php echo $dt->judul;?></td>
            <td class="center">
							<a href="<?php echo base_url();?>assets/materi_seminar/<?= $dt->file;?>" target="_blank">
							<?php echo $dt->file;?></a>
						</td>
        </tr>
		<?php } ?>
    </tbody>
</table>

</div>
