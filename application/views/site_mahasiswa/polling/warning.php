<div class="row-fluid">
	<div class="widget-box">
		<div class="widget-header widget-header-flat">
			<h4 class="smaller"><?php echo $judul;?></h4>
		</div>

		<div class="widget-body ">
			<div class="widget-main no-padding">
        <div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert">
						<i class="icon-remove"></i>
					</button>
					<strong>Informasi !</strong>
            Anda sudah mengisi Kuisioner.
					<br>
				</div>
			</div>
		</div>
	</div>
</div>
