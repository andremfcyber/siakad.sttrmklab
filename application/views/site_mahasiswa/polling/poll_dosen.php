<?php
$th_akademik = $this->model_global->getThAkademikAktif()['th_akademik'];
?>
<div class="row-fluid">
	<div class="widget-box">
		<div class="widget-header widget-header-flat">
                    <h4 class="smaller"><?php echo $sub_judul;?></h4>
		</div>

		<div class="widget-body ">
			<div class="widget-main no-padding">

        <?php
        $info = $this->session->flashdata('info');
        if(!empty($info)){
          ?>
          <div class="alert alert-danger"><?php echo $info;?></div>
          <?php
        }
        ?>

        <form class="form-horizontal" method="POST" action="<?php echo site_url('site_mahasiswa/poll_dosen/simpan');?>" onsubmit="return CekForm();">

          <fieldset>


          <div class="control-group">
              <label class="control-label" for="form-field-1">Nama Dosen</label>

              <div class="controls">
                  <select class="span5" name="kd_dosen" id="kd_dosen">
                    <option value="">-Pilih Dosen-</option>
                    <?php
                    foreach ($list_dosen->result() as $key => $value) {
                      ?>
                      <<option value="<?php echo $value->kd_dosen;?>"><?php echo $value->nama_dosen;?></option>
                      <?php
                    }
                     ?>
                  </select>
              </div>
          </div>

          </fieldset>

          <table class="table table-striped table-bordered table-hover">
					  <thead>
					    <tr>
					      <th class="span1">No</th>
                <th>Pertanyaan</th>
					    </tr>
					  </thead>
            <tbody>
              <?php
              $get = $this->model_polling->getDataPertanyaan($th_akademik,'DOSEN');
              if($get->num_rows() == 0){
                echo "MAAF, BELUM ADA PERTANYAAN KUISIONER";
              } else {
                $row = $get->row();
                $exp_pertanyaan = $row->poll_pertanyaan_id;
                $pertanyaan = explode(',',$exp_pertanyaan);
                $no=1;
                foreach($pertanyaan as $value)
                {
                  $infoPertanyaan = $this->model_polling->getInfoPertanyaan($value);
                  $tanya = $infoPertanyaan['pertanyaan'];
                  $exp_pilih = $infoPertanyaan['pilihan'];
                  $pilih = explode(',',$exp_pilih);
                  ?>
                  <tr>
                    <td class="center"><?php echo $no++;?></td>
                    <td><?php echo $tanya;?>
                      <input type="hidden" name="tanya_<?php echo $value;?>" value="<?php echo $value;?>">
                      <br/>
                      <?php
                      foreach($pilih as $pil)
                      {
                        ?>
                        <label>
                          <input type="radio" name="jawab_<?php echo $value;?>" value="<?php echo $pil;?>">
                          &nbsp;<span>&nbsp;<?php echo $pil;?></span>
                          <!-- &nbsp;<span class="lbl">&nbsp;<?php //echo $pil;?></span> -->
                        </label>
                        <?php
                      }
                       ?>
                    </td>
                  </tr>
                  <?php
                }
              }?>
            </tbody>
          </table>

                <div class="form-actions center">
                    <?php if($get->num_rows() > 0){
                        echo '<button type="submit" name="simpan" id="simpan"  class="btn btn-small btn-primary">
                                    Simpan
                                    <i class="icon-save icon-on-right bigger-110"></i>
                            </button>';
                    }?>
                    <a href="<?php echo site_url('site_mahasiswa/home');?>" class="btn btn-small btn-info">
                      Kembali
                      <i class="icon-undo icon-on-right bigger-110"></i>
                    </a>
                </div>
        </form>

			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

function CekForm()
{
  if(!$("#kd_dosen").val()){
    $.gritter.add({
      title: 'Peringatan..!!',
      text: 'Nama Dosen tidak boleh kosong',
      class_name: 'gritter-error'
    });
    $("#kd_dosen").focus();
    return false;
  }
}

</script>
