<script type="text/javascript">
$(document).ready(function(){

  var i =$(".pilihan").length;
  $("#add").click(function(){
    i++;
    $("#id_pilihan").append('<input type="text" name="pilihan[]" id="pilihan'+i+'" placeholder="pilihan" class="span6 pilihan" /><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn-small btn_remove"><i class="icon-trash icon-on-right bigger-110"></i></button>');
  });


  $(document).on('click','.btn_remove',function(){
    var button_id = $(this).attr("id");
    $("#pilihan"+button_id+"").remove();
    $("#"+button_id+"").remove();
    // console.log(button_id);
  });


});

function cekform()
{
  if(!$("#poll_jenis_id").val()){
    $.gritter.add({
      title: 'Peringatan..!!',
      text: 'Jenis tidak boleh kosong',
      class_name: 'gritter-error'
    });
    $("#poll_jenis_id").focus();
    return false;
  }

  if(!$("#pertanyaan").val()){
    $.gritter.add({
      title: 'Peringatan..!!',
      text: 'Pertanyaan tidak boleh kosong',
      class_name: 'gritter-error'
    });
    $("#pertanyaan").focus();
    return false;
  }

  // if(!$("#pilihan").val()){
  //   $.gritter.add({
  //     title: 'Peringatan..!!',
  //     text: 'Pilihan tidak boleh kosong',
  //     class_name: 'gritter-error'
  //   });
  //   $("#pilihan").focus();
  //   return false;
  // }
}

</script>

<?php
$info = $this->session->flashdata('info');
if(!empty($info)){
  ?>
  <div class="alert alert-info"><?php echo $info;?></div>
  <?php
}
 ?>

<div class="widget-box ">
    <div class="widget-header">
        <h4 class="lighter smaller">
            <i class="icon-book blue"></i>
            <?php echo $judul;?>
        </h4>
    </div>

    <div class="widget-body">
    	<div class="widget-main">
            <div class="row-fluid">
      <form class="form-horizontal" name="my-form" id="my-form" action="<?php echo site_url('poll_pertanyaan/simpan');?>" method="POST" onsubmit="return cekform();">
            	<input type="hidden" name="id" id="id" value="<?php echo $id;?>" />
                <div class="control-group">

                    <label class="control-label" for="form-field-1">Jenis</label>
                    <div class="controls">
                    	<select name="poll_jenis_id" id="poll_jenis_id">
                        	<option value="">-Pilih-</option>
                            <?php
              							$data = $this->db->get('poll_jenis');
              							foreach($data->result() as $dt){
                              if($poll_jenis_id==$dt->id)
                              {
                                $select = "selected='selected'";
                              }else{
                                $select = '';
                              }
              							?>
                            <option value="<?php echo $dt->id;?>" <?php echo $select;?> ><?php echo $dt->nama;?></option>
                            <?php
              							}
              							?>
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="form-field-1">Pertanyaan</label>

                    <div class="controls">
                        <input type="text" name="pertanyaan" id="pertanyaan" placeholder="pertanyaan" class="span9" value="<?php echo $pertanyaan;?>" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="form-field-1">Aktif</label>
                    <?php
                    if($aktif=='Y')
                    {
                      $value='Y';
                      $checkd = 'checked';
                    }elseif($aktif=='T')
                    {
                        $value='T';
                        $checkd = '';
                    }else{
                      $value='Y';
                      $checkd = 'checked';
                    }
                     ?>
                    <div class="controls">
                      <label>
                        <input type="checkbox" name="aktif" value="<?php echo $value;?>" <?php echo $checkd;?>>
                        <span class="lbl"> Ya</span>
                      </label>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="form-field-1">Pilihan</label>

                    <div class="controls">
                      <button type="button" name="add" id="add" class="btn btn-primary btn-small"><i class="icon-plus icon-on-right bigger-110"></i></button>

                        <div id="id_pilihan">
                          <?php
                          // echo $pilihan;
                          if($pilihan){
                            $pilihan = explode(',',$pilihan);
                            $i=1;
                            foreach($pilihan as $value){
                          ?>
        									<input type="text" name="pilihan[]" id="pilihan<?php echo $i;?>" placeholder="pilihan" class="span6 pilihan" value="<?php echo $value;?>" /><button type="button" name="remove" id="<?php echo $i;?>" class="btn btn-danger btn-small btn_remove"><i class="icon-trash icon-on-right bigger-110"></i></button>
                          <?php
                          $i++;
                        }
                        } ?>
                        </div>
                    </div>
                </div>



        </div>
    </div>

    <div class="modal-footer">
        <div class="pagination no-margin">
        <center>
        <button type="submit" name="simpan" id="simpan" class="btn btn-small btn-success">
            <i class="icon-save"></i>
            Simpan
        </button>
        <!-- <a href="<?php echo base_url();?>index.php/poll_pertanyaan/tambah" name="add" id="add" class="btn btn-small btn-info">
            <i class="icon-check"></i>
            Tambah
        </a> -->
        <a href="<?php echo base_url();?>index.php/poll_pertanyaan" class="btn btn-small btn-danger">
            <i class="icon-remove"></i>
            Close
        </a>
        </center>
		</div>
    </form>
    </div>
</div>
