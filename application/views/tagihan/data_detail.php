<table  class="table fpTable lcnp table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="center" rowspan="2">No</th>
            <th class="center" rowspan="2">Th Akademik</th>
            <th class="center" colspan="3">Tagihan</th>
            <th class="center" rowspan="2">Pembayaran</th>
            <th class="center" rowspan="2">Sisa</th>
        </tr>
        <tr>
            <th class="center">Kode</th>
            <th class="center">Nama</th>
            <th class="center">Jumlah</th>
        </tr>
    </thead>
    <tbody>
      <?php
		$i=1;
    $tota_sisa = 0;
		foreach($data as $dt){
      $infotagihan = $this->model_data->getJenisTagihan($dt->jenis_tagihan_id);
      $kodetagihan = $infotagihan['kode'];
      $namatagihan = $infotagihan['nama'];
      $pembayaran = $this->model_data->getJumlahPembayaran($dt->id);
      $sisa = $dt->jumlah - $pembayaran;
      // $tagihan = $this->model_data->tagihan($th_akademik_kode,$jenis_tagihan_id);
      // $smt =  $this->model_global->semester($dt->nim,$th_akademik_kode);
      // $status = $this->model_data->cekStatusTagihan($th_akademik_kode,$jenis_tagihan_id,$dt->nim);
      ?>
        <tr>
        	<td class="center span1"><?php echo $i++?></td>
            <td class="center"><?php echo $dt->th_akademik_kode;?></td>
            <td class="center"><?php echo $kodetagihan;?></td>
            <td><?php echo $namatagihan;?></td>
            <td style="text-align:right"><?php echo number_format($dt->jumlah);?></td>
            <td style="text-align:right"><?php echo number_format($pembayaran);?></td>
            <td style="text-align:right"><?php echo number_format($sisa);?></td>
        </tr>
		<?php
      $tota_sisa +=$sisa;
      } ?>
      <tr>
        <td colspan="6" class="center">
          <strong>Total Tagihan</strong>
        </td>
        <td style="text-align:right" class="red">
          <strong><?php echo number_format($tota_sisa);?></strong>
        </td>
      </tr>
    </tbody>
</table>
