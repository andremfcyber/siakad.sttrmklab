<script type="text/javascript">
  $(document).ready(function(){
      $(".chzn-select").chosen();

      $("#nim").change(function(){
        view_tagihan();
      });

      function view_tagihan()
      {
        var string = {};
        string.nim = $("#nim").val();
        $.ajax({
    			type	: 'POST',
    			url		: "<?php echo site_url('tagihan/cari_mhs'); ?>",
    			data	: string,
    			cache	: false,
          dataType : 'json',
    			success	: function(data){
            // console.log(data);
    				$("#th_angkatan").val(data.th_angkatan);
            $("#nama_mhs").val(data.nama_mhs);
            $("#kd_prodi").val(data.kd_prodi);
            $("#nama_prodi").val(data.nama_prodi);
            $("#kelas").val(data.kelas);
            list_tagihan();
            data_detail();
    			}
    		});
      }

      function list_tagihan()
      {
        var string = {};
        string.th_akademik_kode = $("#th_angkatan").val();
        // console.log(string);
        $.ajax({
    			type	: 'POST',
    			url		: "<?php echo site_url('generatetagihan/list_tagihan'); ?>",
    			data	: string,
    			cache	: false,
    			success	: function(data){
    				// $("#info_mhs").html(data);
            // console.log(data);
            $("#jenis_tagihan_id").html(data);
            $("#jumlah").val('');
            // $("#list_mhs").hide();
    			}
    		});
      }

      function data_detail()
      {
        var string = {};
        string.nim = $("#nim").val();
        // console.log(string);
        $.ajax({
    			type	: 'POST',
    			url		: "<?php echo site_url('tagihan/data_detail'); ?>",
    			data	: string,
    			cache	: false,
    			success	: function(data){
    				// $("#info_mhs").html(data);
            // console.log(data);
            $("#data_detail").html(data);
            // $("#list_mhs").hide();
    			}
    		});
      }

      $("#simpan").click(function(){

          if(!$("#th_akademik").val()){
      			$.gritter.add({
      				title: 'Peringatan..!!',
      				text: 'Tahun Akademik tidak boleh kosong',
      				class_name: 'gritter-error'
      			});

      			$("#th_akademik").focus();
      			return false();
      		}

          if(!$("#nim").val()){
      			$.gritter.add({
      				title: 'Peringatan..!!',
      				text: 'NIM tidak boleh kosong',
      				class_name: 'gritter-error'
      			});

      			$("#nim").focus();
      			return false();
      		}

          if(!$("#jenis_tagihan_id").val()){
      			$.gritter.add({
      				title: 'Peringatan..!!',
      				text: 'Jenis Tagihan tidak boleh kosong',
      				class_name: 'gritter-error'
      			});

      			$("#jenis_tagihan_id").focus();
      			return false();
      		}

          if(!$("#jumlah").val()){
      			$.gritter.add({
      				title: 'Peringatan..!!',
      				text: 'Jumlah Tagihan tidak boleh kosong',
      				class_name: 'gritter-error'
      			});

      			$("#jumlah").focus();
      			return false();
      		}


          var string = $("#form-entry").serialize();
          // console.log(string);

          $.ajax({
      			type	: 'POST',
      			url		: "<?php echo site_url('tagihan/simpan'); ?>",
      			data	: string,
      			cache	: false,
      			success	: function(data){
              $.gritter.add({
        				title: 'Info..!!',
        				text: data,
        				class_name: 'gritter-success'
        			});
              view_tagihan();
      			}
      		});
      });

      $("#jenis_tagihan_id").change(function(){
          var string = {};
          string.id = $("#jenis_tagihan_id").val();
          $.ajax({
              type	: 'POST',
              url		: "<?php echo site_url('tagihan/jumlah_tagihan'); ?>",
              data	: string,
              cache	: false,
              dataType : 'json',
              success	: function(data){
                // console.log(data);
                $("#jumlah").val(data.jumlah);
              }
            });
      });

  });
</script>
<div class="widget-box">
	<div class="widget-header">
		<h4><?php echo $judul;?></h4>

		<span class="widget-toolbar">
			<a href="#" data-action="collapse">
				<i class="icon-chevron-up"></i>
			</a>

			<a href="#" data-action="close">
				<i class="icon-remove"></i>
			</a>
		</span>
	</div>

	<div class="widget-body">
		<div class="widget-main no-padding">
      <?php
      $info = $this->session->flashdata('info');
      if(!empty($info)){
        echo $info;
      }
       ?>
       <form name="form-entry" id="form-entry" class="form-horizontal">
         <fieldset>
           <div class="control-group">
             <label class="control-label" for="form-field-1">Th Akademik Aktif</label>

             <div class="controls">
               <input type="text" name="th_akademik" id="th_akademik" class="span1" readonly="true" value="<?php echo $th_akademik_aktif;?>">
             </div>
           </div>

						<div class="control-group">
							<label class="control-label" for="form-field-1">NIM</label>

							<div class="controls">
								<select name="nim" id="nim" class="chzn-select" data-placeholder="Cari NIM ....">
                  <option value="">-Pilih-</option>
                  <?php
                  $list_nim = $this->db->where('status','Aktif')->get('mahasiswa')->result();
                  foreach($list_nim as $row){
                    ?>
                    <option value="<?php echo $row->nim;?>"><?php echo $row->nim;?></option>
                    <?php
                  }
                   ?>
                </select>
							</div>
						</div>

            <div class="control-group">
							<label class="control-label" for="form-field-1">Th Angkatan</label>

							<div class="controls">
                <input type="text" name="th_angkatan" id="th_angkatan" class="span2" readonly>
							</div>
						</div>

            <div class="control-group">
							<label class="control-label" for="form-field-1">Nama</label>

							<div class="controls">
                <input type="text" name="nama_mhs" id="nama_mhs" class="span5" readonly>
							</div>
						</div>

            <div class="control-group">
							<label class="control-label" for="form-field-1">Program Studi</label>

							<div class="controls">
                <input type="text" name="kd_prodi" id="kd_prodi" class="span1" readonly>
                <input type="text" name="nama_prodi" id="nama_prodi" class="span5" readonly>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="form-field-2">Kelas</label>

							<div class="controls">
                <input type="text" name="kelas" id="kelas" class="span1" readonly>
							</div>
						</div>

            <div class="control-group">
							<label class="control-label" for="form-field-2">Jenis Tagihan</label>

							<div class="controls">
                <select name="jenis_tagihan_id" id="jenis_tagihan_id" class="span2">

                </select>
							</div>
						</div>

            <div class="control-group">
							<label class="control-label" for="form-field-2">Jumlah</label>

							<div class="controls">
                <input type="number" name="jumlah" id="jumlah" class="span2">
							</div>
						</div>
            </fieldset>

            <div class="form-actions center">
							<button type="button" name="simpan" id="simpan" class="btn btn-small btn-success">
								Simpan
								<i class="icon-save icon-on-right bigger-110"></i>
							</button>
						</div>


      </form>

		</div>
	</div>
</div>

<div id="data_detail"></div>
