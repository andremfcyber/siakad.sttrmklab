<script type="text/javascript">
  $(document).ready(function(){
      $(".chzn-select").chosen();

      cari_data();
      
      function cari_data()
      {
        var string = {};
        string.th_akademik = $("#th_akademik").val();
        string.nim = $("#nim").val();
        $.ajax({
    			type	: 'POST',
    			url		: "<?php echo site_url('dispensasi/cari_data'); ?>",
    			data	: string,
    			cache	: false,
          dataType : 'json',
    			success	: function(data){
            // console.log(data);
    				$("#th_angkatan").val(data.th_angkatan);
            $("#nama_mhs").val(data.nama_mhs);
            $("#kd_prodi").val(data.kd_prodi);
            $("#nama_prodi").val(data.nama_prodi);
            $("#kelas").val(data.kelas);
            $("#alasan").val(data.alasan);
    			}
    		});
      }

      $("#nim").change(function(){
        view_tagihan();
      });

      function view_tagihan()
      {
        var string = {};
        string.nim = $("#nim").val();
        $.ajax({
    			type	: 'POST',
    			url		: "<?php echo site_url('dispensasi/cari_mhs'); ?>",
    			data	: string,
    			cache	: false,
          dataType : 'json',
    			success	: function(data){
            // console.log(data);
    				$("#th_angkatan").val(data.th_angkatan);
            $("#nama_mhs").val(data.nama_mhs);
            $("#kd_prodi").val(data.kd_prodi);
            $("#nama_prodi").val(data.nama_prodi);
            $("#kelas").val(data.kelas);
    			}
    		});
      }



      $("#simpan").click(function(){

          if(!$("#th_akademik").val()){
      			$.gritter.add({
      				title: 'Peringatan..!!',
      				text: 'Tahun Akademik tidak boleh kosong',
      				class_name: 'gritter-error'
      			});

      			$("#th_akademik").focus();
      			return false();
      		}

          if(!$("#nim").val()){
      			$.gritter.add({
      				title: 'Peringatan..!!',
      				text: 'NIM tidak boleh kosong',
      				class_name: 'gritter-error'
      			});

      			$("#nim").focus();
      			return false();
      		}



          if(!$("#alasan").val()){
      			$.gritter.add({
      				title: 'Peringatan..!!',
      				text: 'Alasan tidak boleh kosong',
      				class_name: 'gritter-error'
      			});

      			$("#alasan").focus();
      			return false();
      		}


          var string = $("#form-entry").serialize();
          // console.log(string);

          $.ajax({
      			type	: 'POST',
      			url		: "<?php echo site_url('dispensasi/simpan'); ?>",
      			data	: string,
      			cache	: false,
      			success	: function(data){
              $.gritter.add({
        				title: 'Info..!!',
        				text: data,
        				class_name: 'gritter-success'
        			});
              view_tagihan();
      			}
      		});
      });


  });
</script>
<div class="widget-box">
	<div class="widget-header">
		<h4><?php echo $judul;?></h4>

		<span class="widget-toolbar">
			<a href="#" data-action="collapse">
				<i class="icon-chevron-up"></i>
			</a>

			<a href="#" data-action="close">
				<i class="icon-remove"></i>
			</a>
		</span>
	</div>

	<div class="widget-body">
		<div class="widget-main no-padding">
      <?php
      $info = $this->session->flashdata('info');
      if(!empty($info)){
        echo $info;
      }
       ?>
       <form name="form-entry" id="form-entry" class="form-horizontal">
         <fieldset>
           <div class="control-group">
             <label class="control-label" for="form-field-1">Th Akademik Aktif</label>

             <div class="controls">
               <input type="text" name="th_akademik" id="th_akademik" class="span1" readonly="true" value="<?php echo $th_akademik_aktif;?>">
             </div>
           </div>

						<div class="control-group">
							<label class="control-label" for="form-field-1">NIM</label>

							<div class="controls">
								<select name="nim" id="nim" class="chzn-select" data-placeholder="Cari NIM ....">
                  <option value="">-Pilih-</option>
                  <?php
                  $list_nim = $this->db->where('status','Aktif')->get('mahasiswa')->result();
                  foreach($list_nim as $row){
                    if($row->nim==$nim){
                      $select = "selected='true'";
                    }else{
                      $select = '';
                    }
                    ?>
                    <option value="<?php echo $row->nim;?>" <?php echo $select;?> ><?php echo $row->nim;?></option>
                    <?php
                  }
                   ?>
                </select>
							</div>
						</div>

            <div class="control-group">
							<label class="control-label" for="form-field-1">Th Angkatan</label>

							<div class="controls">
                <input type="text" name="th_angkatan" id="th_angkatan" class="span2" readonly>
							</div>
						</div>

            <div class="control-group">
							<label class="control-label" for="form-field-1">Nama</label>

							<div class="controls">
                <input type="text" name="nama_mhs" id="nama_mhs" class="span5" readonly>
							</div>
						</div>

            <div class="control-group">
							<label class="control-label" for="form-field-1">Program Studi</label>

							<div class="controls">
                <input type="text" name="kd_prodi" id="kd_prodi" class="span1" readonly>
                <input type="text" name="nama_prodi" id="nama_prodi" class="span5" readonly>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="form-field-2">Kelas</label>

							<div class="controls">
                <input type="text" name="kelas" id="kelas" class="span1" readonly>
							</div>
						</div>


            <div class="control-group">
							<label class="control-label" for="form-field-2">Alasan</label>

							<div class="controls">
                <input type="text" name="alasan" id="alasan" class="span6">
							</div>
						</div>
            </fieldset>

            <div class="form-actions center">
							<button type="button" name="simpan" id="simpan" class="btn btn-small btn-success">
								Simpan
								<i class="icon-save icon-on-right bigger-110"></i>
							</button>
              <a href="<?php echo site_url('dispensasi');?>" class="btn btn-small btn-info">Tutup
                <i class="icon-undo icon-on-right bigger-110"></i>
              </a>
						</div>


      </form>

		</div>
	</div>
</div>
