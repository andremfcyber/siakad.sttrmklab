<!-- <pre>
    <?php print_r($data->result());?>
</pre> -->

<table  class="table fpTable lcnp table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="center" width="10">No</th>
            <th class="center span2">Th. Akademik</th>
            <th class="center span2">No. Pendaftaran</th>
            <th class="center">Nama</th>
            <th class="center" width="10">L/P</th>
            <th class="center">Program Studi</th>
        </tr>
    </thead>
    <tbody>
    	<?php 
        $th_akademik = $this->model_global->getThAkademikAktif()['th_akademik'];// $th_now.'/'.$th_next;
		$i=1;
		foreach($data->result() as $dt){ 
			$nama_prodi = $this->model_data->nama_jurusan($dt->prodi);
		?>
        <tr>
        	<td class="center"><?php echo $i++?></td>
            <td class="center"><?php echo $th_akademik;?></td>
            <td class="center">
			<a href="<?php echo base_url();?>index.php/maba/edit/<?php echo $dt->no_pendaftaran;?>">
			<?php echo $dt->no_pendaftaran;?></a>
            </td>
            <td ><?php echo $dt->nama;?></td>
            <td class="center" ><?php echo $dt->jk;?></td>
            <td class="center" ><?php echo $dt->prodi.'-'.$nama_prodi;?></td>
        </tr>
		<?php 
		} 
		?>
    </tbody>
</table>