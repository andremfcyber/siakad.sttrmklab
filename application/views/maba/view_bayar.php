<table  class="table fpTable lcnp table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="center">No</th>
            <th class="center">Th Akademik</th>
            <th class="center">Nomor</th>
            <th class="center">Tanggal</th>
            <th class="center">Jenis Tagihan</th>
            <th class="center">Semester</th>
            <th class="center">Jumlah</th>
            <th class="center">Keterangan</th>
        </tr>
    </thead>
    <tbody>
    	<?php
		$i=1;
    $total = 0;
		foreach($data->result() as $dt){
      $infoMhs = $this->model_data->getInfoMhs($dt->nim);
      $nama_mhs = $infoMhs['nama_mhs'];
      $sex = $infoMhs['sex'];
      $kelas = $infoMhs['kelas'];
      $nama_prodi = $infoMhs['nama_prodi'];

      $infoTagihan = $this->model_data->getJenisTagihan($dt->jenis_tagihan_id);
      $nama_tagihan  = $infoTagihan['nama'];
		?>
        <tr>
        	<td class="center span1"><?php echo $i++?></td>
            <td class="center"><?php echo $dt->th_akademik;?></td>
            <td class="center"><?php echo $dt->nomor;?></td>
            <td class="center"><?php echo $this->model_global->tgl_str($dt->tanggal);?></td>
            <td ><?php echo $nama_tagihan;?></td>
            <td class="center"><?php echo $dt->smt;?></td>
            <td style="text-align:right;"><?php echo number_format($dt->jumlah);?></td>
            <td ><?php echo $dt->keterangan;?></td>
        </tr>
		<?php
        $total +=$dt->jumlah;
      } ?>
      <tr>
        <td colspan="6" class="center">
          TOTAL PEMBAYARAN
        </td>
        <td style="text-align:right;">
            <?php echo number_format($total);?>
        </td>
      </tr>
    </tbody>
</table>
