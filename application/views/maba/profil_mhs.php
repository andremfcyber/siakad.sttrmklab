<script type="text/javascript">
$(document).ready(function(){
	$("#nim").focus();

	$('.date-picker').datepicker().next().on(ace.click_event, function(){
		$(this).prev().focus();
	});

	$("#simpan").click(function(){

		var string = $("#my-form").serialize();

//		if(!$("#nim").val()){
//			$.gritter.add({
//				title: 'Peringatan..!!',
//				text: 'NIM tidak boleh kosong',
//				class_name: 'gritter-error'
//			});
//
//			$("#kd_prodi").focus();
//			return false();
//		}

		if(!$("#kd_prodi").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Kode Prodi tidak boleh kosong',
				class_name: 'gritter-error'
			});

			$("#kd_prodi").focus();
			return false();
		}

		if(!$("#nama_lengkap").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Nama Lengkap tidak boleh kosong',
				class_name: 'gritter-error'
			});

			$("#nama_lengkap").focus();
			return false();
		}

		if(!$("#tgl_lahir").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Tanggal Lahir tidak boleh kosong',
				class_name: 'gritter-error'
			});

			$("#tgl_lahir").focus();
			return false();
		}
		if (!$.isNumeric( $("#telp").val() )){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'No Telpon harus numeric',
				class_name: 'gritter-error'
			});

			$("#telp").focus();
			return false();
		}

		if ($("#telp").val().length < 10){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'No Telpon minimal 10 digit',
				class_name: 'gritter-error'
			});

			$("#telp").focus();
			return false();
		}

		if(!$("#tgl_masuk").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Tanggal Masuk tidak boleh kosong',
				class_name: 'gritter-error'
			});

			$("#tgl_masuk").focus();
			return false();
		}

		if(!$("#ktp").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'KTP tidak boleh kosong',
				class_name: 'gritter-error'
			});

			$("#ktp").focus();
			return false();
		}

		if(!$("#kelas").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Kelas tidak boleh kosong',
				class_name: 'gritter-error'
			});

			$("#kelas").focus();
			return false();
		}

		if(!$("#email").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Email tidak boleh kosong',
				class_name: 'gritter-error'
			});

			$("#email").focus();
			return false();
		}

		if( !validateEmail($("#email").val())) {
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'invalid email address',
				class_name: 'gritter-error'
			});

			/* do stuff here */
			$("#email").focus();
			return false();
		}


		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url(); ?>/maba/simpan",
			data	: string,
			cache	: false,
			success	: function(data){
				$.gritter.add({
					title: 'Info..!!',
					text: data,
					class_name: 'gritter-info'
				});

//				$('#my-form')[0].reset();

			}
		});

	});

});

function validateEmail(email) {
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	return emailReg.test( email );
}
</script>
<div class="row-fluid">
    <div class="span3 center">
        <span class="profile-picture">
        	<?php
			if(empty($foto)){
			?>
            <img class="editable" id="avatar2" src="<?php echo base_url();?>assets/foto_mhs/no_foto.jpg" />
			<?php }else{ ?>
            <img class="editable" id="avatar2" src="<?php echo base_url();?>assets/foto_mhs/<?php echo $foto;?>" width="100" />
            <?php } ?>
        </span>

        <div class="space space-4"></div>
        <a href="#" class="btn btn-small btn-block btn-primary">
            <i class="icon-envelope-alt"></i>
            Kirim Pesan
        </a>
    </div><!--/span-->

    <div class="span9">
        <h4 class="blue">
            <span class="label label-purple arrowed-in-right">
                <i class="icon-circle smaller-80"></i>
                <?php echo $status_mhs;?>
            </span>
        </h4>
		<form name="my-form" id="my-form">
        <div class="profile-user-info">
            <div class="profile-info-row">
                <div class="profile-info-name"> Th Akademik </div>
                <div class="profile-info-value">
                    <input type="text" name="th_akademik" id="th_akademik" value="<?php echo $th_akademik;?>" class="span2"  />
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name"> Kode Prodi </div>
                <div class="profile-info-value">
                    <input type="text" name="kd_prodi" id="kd_prodi" value="<?php echo $kd_prodi;?>" class="span2" readonly="readonly" />
                    <input type="hidden" name="id_prodi" id="id_prodi" value="" class="span1" readonly="readonly" />
										( <?php echo $this->model_data->nama_jurusan($prodi);?> )
                                        <!-- value : php echo $id_prodi -->
                </div>
            </div>

            <div class="profile-info-row">
                <div class="profile-info-name"> NIM </div>
                <div class="profile-info-value">
                    <input type="text" name="nim" id="nim" value="<?php echo $nim;?>" class="span3"  />
                </div>
            </div>

            <div class="profile-info-row">
                <div class="profile-info-name"> Nama Lengkap </div>
                <div class="profile-info-value">
                    <input type="text" name="nama_lengkap" id="nama_lengkap" value="<?php echo $nama;?>" class="span8" />
                </div>
            </div>

            <div class="profile-info-row">
                <div class="profile-info-name"> KTP </div>
                <div class="profile-info-value">
                    <input type="text" name="ktp" id="ktp" value="<?php echo $ktp;?>" class="span8" />
                </div>
            </div>

            <div class="profile-info-row">
                <div class="profile-info-name"> TTL </div>
                <div class="profile-info-value">
                    <input type="text" name="tempat_lahir" id="tempat_lahir" value="<?php echo $tempat_lahir;?>" class="span7" />
                    <div class="input-append">
                    <input type="text" name="tgl_lahir" id="tgl_lahir" value="<?php echo $tgl_lahir;?>" class="span6 date-picker"  data-date-format="dd-mm-yyyy"/>
                    <span class="add-on">
                        <i class="icon-calendar"></i>
                    </span>
                    </div>
                </div>
            </div>

            <div class="profile-info-row">
                <div class="profile-info-name"> Jenis Kelamin </div>
                <div class="profile-info-value">
                    <select name="sex" id="sex" class="span3">
                    <?php
					if($sex=='L'){
					?>
						<option value="">-Pilih-</option>
                        <option value="L" selected="selected">Laki-laki</option>
                        <option value="P">Perempuan</option>
                    <?php
					}elseif($sex=='P'){
					?>
	                    <option value="">-Pilih-</option>
                        <option value="L" >Laki-laki</option>
                        <option value="P" selected="selected">Perempuan</option>
                    <?php
					}else{
					?>
                    	<option value="" selected="selected">-Pilih-</option>
                        <option value="L" >Laki-laki</option>
                        <option value="P" >Perempuan</option>
					<?php }?>
                    	</select>
                    </span>
                </div>
            </div>

            <div class="profile-info-row">
                <div class="profile-info-name"> Alamat </div>
                <div class="profile-info-value">
                    <input type="text" name="alamat" id="alamat" value="<?php echo $alamat;?>" class="span11" />
                </div>
            </div>

            <div class="profile-info-row">
                <div class="profile-info-name"> Kota </div>
                <div class="profile-info-value">
                    <input type="text" name="kota" id="kota" value="<?php echo $kota;?>" class="span5" />
                </div>
            </div>

            <div class="profile-info-row">
                <div class="profile-info-name"> Telepon </div>
                <div class="profile-info-value">
                    <input type="text" class="span3" name="telp" id="telp" value="<?php echo $telp;?>" />
                </div>
            </div>

            <div class="profile-info-row">
                <div class="profile-info-name"> Email </div>
                <div class="profile-info-value">
                    <input type="text" class="span7" name="email" id="email" value="<?php echo $email;?>" />
                </div>
            </div>

            <div class="profile-info-row">
                <div class="profile-info-name"> Nama Ayah </div>
                <div class="profile-info-value">
                    <input type="text" name="nama_ayah" id="nama_ayah" value="<?php echo $nama_ayah;?>" class="span6"  />
                </div>
            </div>

            <div class="profile-info-row">
                <div class="profile-info-name"> Nama Ibu </div>
                <div class="profile-info-value">
                    <input type="text" name="nama_ibu" id="nama_ibu" value="<?php echo $nama_ibu;?>" class="span6"  />
                </div>
            </div>

            <div class="profile-info-row">
                <div class="profile-info-name"> Alamat </div>
                <div class="profile-info-value">
                    <input type="text" name="alamat_ortu" id="alamat_ortu" value="<?php echo $alamat_ortu;?>" class="span8"  />
                </div>
            </div>

            <div class="profile-info-row">
                <div class="profile-info-name"> Hp </div>
                <div class="profile-info-value">
                    <input type="text" name="hp_ortu" id="hp_ortu" value="<?php echo $hp_ortu;?>" class="span4"  />
                </div>
            </div>

						<div class="profile-info-row">
                <div class="profile-info-name"> Tanggal Masuk </div>
                <div class="profile-info-value">
										<div class="input-append">
                    <input type="text"  name="tgl_masuk" id="tgl_masuk" value="<?php echo $tgl_masuk;?>"  class="span6 date-picker"  data-date-format="dd-mm-yyyy"/>
										<span class="add-on">
                        <i class="icon-calendar"></i>
                    </span>
                    </div>
                </div>
            </div>

						<div class="profile-info-row">
                <div class="profile-info-name"> Kelas </div>
                <div class="profile-info-value">
                    <input type="text" class="span1" name="kelas" id="kelas" value="<?php echo $kelas;?>" />
                </div>
            </div>

	    <div class="profile-info-row">
                <div class="profile-info-name"> Status </div>
                <div class="profile-info-value">
                    <select name="status" id="status" class="span3">

                    <?php
                    $stt = array("","Aktif","PPL","Cuti","DO","Lulus");
                    foreach($stt as $srow){
                    	$akt="";
                    	if($status ==  $srow){
                    		$akt="selected='selected'";
                    	}
                    	  echo "<option $akt value='".$srow."' >".$srow."</option>";
                  	}

                    ?>
                     <!--
                    	<option value="" selected="selected">-Pilih-</option>

                        <option value="Aktif" >Aktif</option>
                        <option value="Cuti" >Cuti</option>
                        <option value="DO" >DO</option>
                        <option value="Keluar" >Keluar</option>
                        <option value="Lulus" >Lulus</option>
                      -->
                        </select>
                    </span>
                </div>
            </div>


		</div><!--profil info-->
		 <div class="alert alert-error" align="center">
             <button type="button" name="simpan" id="simpan" class="btn btn-mini btn-primary">
             <i class="icon-save"></i> Simpan
             </button>
              <!-- <a href="<?php echo base_url();?>index.php/maba/tambah" class="btn btn-mini btn-info">
             <i class="icon-check"></i> Tambah
             </a> -->
              <a href="<?php echo base_url();?>index.php/maba/view_data/<?=@$kd_prodi;?>" class="btn btn-mini btn-success">
             <i class="icon-double-angle-right"></i> Kembali
             </a>
        </div>
        </form>

    </div><!--/span-->
</div><!--/row-fluid-->
