<script type="text/javascript">
  $(document).ready(function(){
    $("#upload").click(function(){
      if(!$("#th_akademik_kode").val()){
        $.gritter.add({
          title: 'Peringatan..!!',
          text: 'Tahun Akademik tidak boleh kosong',
          class_name: 'gritter-error'
        });

        $("#th_akademik_kode").focus();
        return false();
      }

      if(!$("#prodi").val()){
        $.gritter.add({
          title: 'Peringatan..!!',
          text: 'Prodi tidak boleh kosong',
          class_name: 'gritter-error'
        });

        $("#prodi").focus();
        return false();
      }

      if(!$("#kelas").val()){
        $.gritter.add({
          title: 'Peringatan..!!',
          text: 'Kelas tidak boleh kosong',
          class_name: 'gritter-error'
        });

        $("#kelas").focus();
        return false();
      }

      if(!$("#jenis_tagihan_id").val()){
        $.gritter.add({
          title: 'Peringatan..!!',
          text: 'Jenis Tagihan tidak boleh kosong',
          class_name: 'gritter-error'
        });

        $("#jenis_tagihan_id").focus();
        return false();
      }

        var string = {};
        string.th_akademik_kode = $("#th_akademik_kode").val();
        string.prodi = $("#prodi").val();
        string.kelas = $("#kelas").val();
        string.jenis_tagihan_id = $("#jenis_tagihan_id").val();

        // console.log(string);

        $.ajax({
    			type	: 'POST',
    			url		: "<?php echo site_url('generatetagihan/upload_tagihan'); ?>",
    			data	: string,
    			cache	: false,
    			success	: function(data){
    				// $("#info_mhs").html(data);
            // console.log(data);
            $.gritter.add({
              title: 'Perhatian ..!!',
              text: data,
              class_name: 'gritter-success'
            });
            // $("#list_mhs").html(data);
    			}
    		});
    });
  });
</script>
<div class="alert alert-error">
	<button type="button" class="close" data-dismiss="alert">
		<i class="icon-remove"></i>
	</button>
	<strong>
		<i class="icon-remove"></i>
		Perhatian ..!!
	</strong>
  <br/>
  Berikut adalah Data Mahasiswa Aktif, Silahkan Cross Check Kembali Data Mahasiswa Sebelum di Upload ke Tagihan.
  <br/>
  Untuk mengubah status mahasiswa silahkan ke menu Mutasi Mahasiswa.
</div>
<table  class="table fpTable lcnp table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="center">No</th>
            <th class="center">NIM</th>
            <th class="center">Nama</th>
            <th class="center">Kelamin</th>
            <th class="center">SMT</th>
            <th class="center">Tagihan</th>
            <th class="center">Status</th>
        </tr>
    </thead>
    <tbody>
    	<?php
		$i=1;
		foreach($data as $dt){
      $tagihan = $this->model_data->tagihan($th_akademik_kode,$jenis_tagihan_id);
      $smt =  $this->model_global->semester($dt->nim,$th_akademik_kode);
      $status = $this->model_data->cekStatusTagihan($th_akademik_kode,$jenis_tagihan_id,$dt->nim);
      ?>
        <tr>
        	<td class="center span1"><?php echo $i++?></td>
            <td class="center"><?php echo $dt->nim;?></td>
            <td ><?php echo $dt->nama_mhs;?></td>
            <td class="center"><?php echo $dt->sex;?></td>
            <td class="center"><?php echo $smt;?></td>
            <td style="text-align:right;"><?php echo number_format($tagihan);?></td>
            <td class="center">
              <?php echo $status;?>
            </td>
        </tr>
		<?php } ?>
    </tbody>
</table>
<div class="form-actions center">
  <button type="button" name="upload" id="upload" class="btn btn-small btn-danger">
    Upload Tagihan
    <i class="icon-upload icon-on-right bigger-110"></i>
  </button>
</div>
