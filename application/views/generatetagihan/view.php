<div class="row-fluid">
<div class="table-header">
    <?php echo $judul;?>
    <div class="widget-toolbar no-border pull-right">
    <a href="<?php echo site_url('/generatetagihan/create');?>" class="btn btn-small btn-success"   name="tambah" id="tambah" >
        <i class="icon-check"></i>
        Tambah Data
    </a>
    </div>
</div>

<table  class="table fpTable lcnp table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="center">No</th>
            <th class="center">Th Akademik</th>
            <th class="center">Tanggal</th>
            <th class="center">NIM</th>
            <th class="center">Nama</th>
            <th class="center">SMT</th>
            <th class="center">Tagihan</th>
            <th class="center">Jumlah</th>
            <th class="center">Jumlah Bayar</th>
            <th class="center">Aksi</th>
        </tr>
    </thead>
    <tbody>
    	<?php
		$i=1;
		foreach($data->result() as $dt){
      $infomhs = $this->model_data->getInfoMhs($dt->nim);
      $nama = $infomhs['nama_mhs'];
      $infojenistagihan = $this->model_data->getJenisTagihan($dt->jenis_tagihan_id);
      $namatagihan = $infojenistagihan['nama'];
      ?>
        <tr>
        	<td class="center span1"><?php echo $i++?></td>
            <td class="center"><?php echo $dt->th_akademik_kode;?></td>
            <td class="center"><?php echo $this->model_global->tgl_str(substr($dt->insert_date,0,10));?></td>
            <td class="center"><?php echo $dt->nim;?></td>
            <td><?php echo $nama;?></td>
            <td class="center"><?php echo $dt->smt;?></td>
            <td ><?php echo $namatagihan;?></td>
            <td style="text-align:right;"><?php echo number_format($dt->jumlah);?></td>
            <td></td>
            <td class="td-actions"><center>
            	<div class="hidden-phone visible-desktop action-buttons">
                    <a class="green" href="#modal-table" onclick="javascript:editData('<?php echo $dt->id;?>')" data-toggle="modal">
                        <i class="icon-pencil bigger-130"></i>
                    </a>

                    <a class="red" href="<?php echo site_url();?>/ruangan/hapus/<?php echo $dt->id;?>" onClick="return confirm('Anda yakin ingin menghapus data ini?')">
                        <i class="icon-trash bigger-130"></i>
                    </a>
                </div>

                <div class="hidden-desktop visible-phone">
                    <div class="inline position-relative">
                        <button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-caret-down icon-only bigger-120"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close">
                            <li>
                                <a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
                                    <span class="green">
                                        <i class="icon-edit bigger-120"></i>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
                                    <span class="red">
                                        <i class="icon-trash bigger-120"></i>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                </center>
            </td>
        </tr>
		<?php } ?>
    </tbody>
</table>
</div>
