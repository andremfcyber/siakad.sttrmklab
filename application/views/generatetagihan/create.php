<script type="text/javascript">
  $(document).ready(function(){
    $("#th_akademik_kode").change(function(){
      var string = {};
      string.th_akademik_kode = $("#th_akademik_kode").val();
      // console.log(string);
      $.ajax({
  			type	: 'POST',
  			url		: "<?php echo site_url('generatetagihan/list_tagihan'); ?>",
  			data	: string,
  			cache	: false,
  			success	: function(data){
  				// $("#info_mhs").html(data);
          // console.log(data);
          $("#jenis_tagihan_id").html(data);
          $("#list_mhs").hide();
  			}
  		});
    });

    $("#th_akademik_kode").change(function(){
        $("#list_mhs").hide();
    });

    $("#prodi").change(function(){
        $("#list_mhs").hide();
    });

    $("#kelas").change(function(){
        $("#list_mhs").hide();
    });

    $("#jenis_tagihan_id").change(function(){
        $("#list_mhs").hide();
    });

    $("#lihat").click(function(){
      var th_akademik_kode = $("#th_akademik_kode").val();
      var prodi = $("#prodi").val();
      var kelas = $("#kelas").val();
      var jenis_tagihan_id = $("#jenis_tagihan_id").val();

      if(!$("#th_akademik_kode").val()){
  			$.gritter.add({
  				title: 'Peringatan..!!',
  				text: 'Tahun Akademik tidak boleh kosong',
  				class_name: 'gritter-error'
  			});

  			$("#th_akademik_kode").focus();
  			return false();
  		}

      if(!$("#prodi").val()){
  			$.gritter.add({
  				title: 'Peringatan..!!',
  				text: 'Prodi tidak boleh kosong',
  				class_name: 'gritter-error'
  			});

  			$("#prodi").focus();
  			return false();
  		}

      if(!$("#kelas").val()){
  			$.gritter.add({
  				title: 'Peringatan..!!',
  				text: 'Kelas tidak boleh kosong',
  				class_name: 'gritter-error'
  			});

  			$("#kelas").focus();
  			return false();
  		}

      if(!$("#jenis_tagihan_id").val()){
  			$.gritter.add({
  				title: 'Peringatan..!!',
  				text: 'Jenis Tagihan tidak boleh kosong',
  				class_name: 'gritter-error'
  			});

  			$("#jenis_tagihan_id").focus();
  			return false();
  		}

      $("#list_mhs").show();
      var string = {};
      string.th_akademik_kode = $("#th_akademik_kode").val();
      string.prodi = $("#prodi").val();
      string.kelas = $("#kelas").val();
      string.jenis_tagihan_id = $("#jenis_tagihan_id").val();
      // console.log(string);
      $.ajax({
  			type	: 'POST',
  			url		: "<?php echo site_url('generatetagihan/list_mhs'); ?>",
  			data	: string,
  			cache	: false,
  			success	: function(data){
  				// $("#info_mhs").html(data);
          // console.log(data);
          $("#list_mhs").html(data);
  			}
  		});

    });


  });
</script>
<div class="widget-box">
	<div class="widget-header">
		<h4><?php echo $judul;?></h4>

		<span class="widget-toolbar">
			<a href="#" data-action="collapse">
				<i class="icon-chevron-up"></i>
			</a>

			<a href="#" data-action="close">
				<i class="icon-remove"></i>
			</a>
		</span>
	</div>

	<div class="widget-body">
		<div class="widget-main no-padding">
      <?php
      $info = $this->session->flashdata('info');
      if(!empty($info)){
        echo $info;
      }
       ?>
       <form class="form-horizontal">
         <fieldset>
           <div class="control-group">
             <label class="control-label" for="form-field-1">Th Akademik Aktif</label>

             <div class="controls">
               <input type="text" name="th_akademik" id="th_akademik" class="span1" readonly="true" value="<?php echo $th_akademik_aktif;?>">
             </div>
           </div>

						<div class="control-group">
							<label class="control-label" for="form-field-1">Tahun Angkatan</label>

							<div class="controls">
								<select name="th_akademik_kode" id="th_akademik_kode">
                  <option value="">-Pilih-</option>
                  <?php
                  $th_akademik = $this->db->order_by('kode','DESC')->get('th_akademik')->result();
                  foreach($th_akademik as $row){
                    ?>
                    <option value="<?php echo $row->kode;?>"><?php echo $row->kode.' - '.$row->th_akademik;?></option>
                    <?php
                  }
                   ?>
                </select>
							</div>
						</div>

            <div class="control-group">
							<label class="control-label" for="form-field-1">Program Studi</label>

							<div class="controls">
								<select name="prodi" id="prodi">
                  <option value="">-Pilih-</option>
                  <?php
                  $prodi = $this->db->get('prodi')->result();
                  foreach($prodi as $row){
                    ?>
                    <option value="<?php echo $row->kd_prodi;?>"><?php echo $row->prodi;?></option>
                    <?php
                  }
                   ?>
                </select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="form-field-2">Kelas</label>

							<div class="controls">
                <select name="kelas" id="kelas" class="span1">
                  <option value="">-Pilih-</option>
                  <?php
                  $kelas = $this->db->group_by('kelas')->order_by('kelas','ASC')->get('mahasiswa')->result();
                  foreach($kelas as $row){
                    ?>
                    <option value="<?php echo $row->kelas;?>"><?php echo $row->kelas;?></option>
                    <?php
                  }
                   ?>
                </select>
							</div>
						</div>

            <div class="control-group">
							<label class="control-label" for="form-field-2">Jenis Tagihan</label>

							<div class="controls">
                <select name="jenis_tagihan_id" id="jenis_tagihan_id" class="span2">

                </select>
							</div>
						</div>
            </fieldset>

            <div class="form-actions center">
							<button type="button" name="lihat" id="lihat" class="btn btn-small btn-success">
								Lihat
								<i class="icon-search icon-on-right bigger-110"></i>
							</button>
						</div>


      </form>

		</div>
	</div>
</div>

<div id="list_mhs"></div>
