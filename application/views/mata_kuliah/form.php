<div class="widget-box ">
    <div class="widget-header">
        <h4 class="lighter smaller">
            <i class="icon-book blue"></i>
            <?php echo $judul;?>
        </h4>
    </div>

    <div class="widget-body">
    	<div class="widget-main">
        	<form action="<?php echo site_url();?>/mata_kuliah/view_data" method="post">
            <div class="control-group">
                <?php if($_SESSION['adm_kd_prodi']==null || $_SESSION['adm_kd_prodi']==0 || $_SESSION['adm_kd_prodi']==0){?>
                    <label class="control-label" for="form-field-1">Pilih Jurusan</label>
                    <div class="controls">
                        <select name="cari_jurusan" id="cari_jurusan">
                            <?php
                            $data = $this->model_data->data_jurusan();
                            foreach($data->result() as $dt){
                            ?>
                            <option value="<?php echo $dt->kd_prodi;?>"><?php echo $dt->prodi;?></option>
                            <?php
                            }
                            ?>
                        </select>
                <?php 
                }else{
                    $data = $this->model_data->data_jurusan($_SESSION['adm_kd_prodi']);
                ?>
                    <label class="control-label" for="form-field-1">Jurusan</label>
                    <div class="controls">
                        <input type="text" value="<?php echo $data->row()->prodi;?>" readonly="readonly">
                        <input type="hidden" name="cari_jurusan" id="cari_jurusan" value="<?php echo $data->row()->kd_prodi;?>">
                <?php }?>
                    </div>
                    <button type="submit" name="lanjut" id="lanjut" class="btn btn-small btn-success" >
                    Lanjut
                    <i class="icon-arrow-right icon-on-right bigger-110"></i>
                    </button>
			</div>                    
            </form>
        </div>
    </div>
</div>   
