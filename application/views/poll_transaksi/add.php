<script type="text/javascript">
$(document).ready(function(){

  // var i =$(".pilihan").length;
  // $("#add").click(function(){
  //   i++;
  //   $("#id_pilihan").append('<input type="text" name="pilihan[]" id="pilihan'+i+'" placeholder="pilihan" class="span6 pilihan" /><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn-small btn_remove"><i class="icon-trash icon-on-right bigger-110"></i></button>');
  // });
  //
  //
  // $(document).on('click','.btn_remove',function(){
  //   var button_id = $(this).attr("id");
  //   $("#pilihan"+button_id+"").remove();
  //   $("#"+button_id+"").remove();
  //   // console.log(button_id);
  // });
  getDetailPertanyaan();

  $("#poll_jenis_id").change(function(){
    getDetailPertanyaan();
  });

  function getDetailPertanyaan()
  {
    var string = {
      th_akademik : $("#th_akademik").val(),
      poll_jenis_id : $("#poll_jenis_id").val()
    };

    // console.log(string);
    $.ajax({
			type	: 'POST',
			url		: "<?php echo site_url('poll_transaksi/detail_pertanyaan'); ?>",
			data	: string,
			cache	: false,
			success	: function(data){
				// console.log(data);
        $("#detail_pertanyaan").html(data);
			}
		});
  }
});

function cekform()
{
  if(!$("#poll_jenis_id").val()){
    $.gritter.add({
      title: 'Peringatan..!!',
      text: 'Jenis tidak boleh kosong',
      class_name: 'gritter-error'
    });
    $("#poll_jenis_id").focus();
    return false;
  }

  if(!$("#th_akademik").val()){
    $.gritter.add({
      title: 'Peringatan..!!',
      text: 'Th Akademik tidak boleh kosong',
      class_name: 'gritter-error'
    });
    $("#th_akademik").focus();
    return false;
  }

  // if(!$("#pilihan").val()){
  //   $.gritter.add({
  //     title: 'Peringatan..!!',
  //     text: 'Pilihan tidak boleh kosong',
  //     class_name: 'gritter-error'
  //   });
  //   $("#pilihan").focus();
  //   return false;
  // }
}

</script>

<?php
$info = $this->session->flashdata('info');
if(!empty($info)){
  ?>
  <div class="alert alert-info"><?php echo $info;?></div>
  <?php
}
 ?>

<div class="widget-box ">
    <div class="widget-header">
        <h4 class="lighter smaller">
            <i class="icon-book blue"></i>
            <?php echo $judul;?>
        </h4>
    </div>

    <div class="widget-body">
    	<div class="widget-main">
            <div class="row-fluid">
      <form class="form-horizontal" name="my-form" id="my-form" action="<?php echo site_url('poll_transaksi/simpan');?>" method="POST" onsubmit="return cekform();">
            	<input type="hidden" name="id" id="id" value="<?php echo $id;?>" />

              <div class="control-group">
                  <label class="control-label" for="form-field-1">Th Akademik</label>
                  <div class="controls">
                    <select name="th_akademik" id="th_akademik">
                        <option value="">-Pilih-</option>
                          <?php
                          $data = $this->db->get('th_akademik');
                          foreach($data->result() as $dt){
                            if($th_akademik==$dt->th_akademik){
                              $select = "selected='selected'";
                            }else{
                              if($dt->aktif=='Ya')
                              {
                                $select = "selected='selected'";
                              }else{
                                $select = '';
                              }
                            }
                          ?>
                          <option value="<?php echo $dt->th_akademik;?>" <?php echo $select;?> ><?php echo $dt->th_akademik;?></option>
                          <?php
                          }
                          ?>
                      </select>
                  </div>
              </div>

                <div class="control-group">
                    <label class="control-label" for="form-field-1">Jenis</label>
                    <div class="controls">
                    	<select name="poll_jenis_id" id="poll_jenis_id">
                        	<option value="">-Pilih-</option>
                            <?php
              							$data = $this->db->get('poll_jenis');
              							foreach($data->result() as $dt){
                              if($poll_jenis_id==$dt->id)
                              {
                                $select = "selected='selected'";
                              }else{
                                $select = '';
                              }
              							?>
                            <option value="<?php echo $dt->id;?>" <?php echo $select;?> ><?php echo $dt->nama;?></option>
                            <?php
              							}
              							?>
                        </select>
                    </div>
                </div>

                <div id="detail_pertanyaan"></div>
        </div>
    </div>

    <div class="modal-footer">
        <div class="pagination no-margin">
        <center>
        <button type="submit" name="simpan" id="simpan" class="btn btn-small btn-success">
            <i class="icon-save"></i>
            Simpan
        </button>
        <!-- <a href="<?php echo base_url();?>index.php/poll_pertanyaan/tambah" name="add" id="add" class="btn btn-small btn-info">
            <i class="icon-check"></i>
            Tambah
        </a> -->
        <a href="<?php echo base_url();?>index.php/poll_transaksi" class="btn btn-small btn-danger">
            <i class="icon-remove"></i>
            Close
        </a>
        </center>
		</div>
    </form>
    </div>
</div>
