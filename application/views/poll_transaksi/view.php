<script type="text/javascript">
$(document).ready(function(){


});


</script>
<?php
$info = $this->session->flashdata('info');
if(!empty($info)){
  ?>
  <div class="alert alert-info"><?php echo $info;?></div>
  <?php
}
?>

<div class="row-fluid">
<div class="table-header">
    <?php echo $judul;?>
    <div class="widget-toolbar no-border pull-right">
    <a href="<?php echo site_url('poll_transaksi/tambah');?>" class="btn btn-small btn-success"  role="button" name="tambah" id="tambah" >
        <i class="icon-check"></i>
        Tambah Data
    </a>
    </div>
</div>

<table class="table table-bordered table-striped table-hover" id="mytable">
   <thead>
     <tr>
         <th class="center">No</th>
         <th class="center">Jenis</th>
         <th class="center">Tahun Akademik</th>
         <th class="center">Pertanyaan</th>
         <th class="center">Aksi</th>
     </tr>
   </thead>
</table>

</div>


<script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.js') ?>"></script>
<script type="text/javascript">
$(document).ready(function() {
      $('#mytable').dataTable({
          "ajax": "<?php echo site_url('poll_transaksi/get_json'); ?>",
          "pageLength": 20,
          "order": [[ 0, "asc" ]],
          "aoColumnDefs": [{"bSortable": false,
                           "aTargets": [ -1 , 0]}],
          "dom": 'T<"clear">lfrtip',
      });
});
</script>
