<table class="table table-bordered table-striped table-hover">
  <thead>
    <tr>
      <th class="span1 center">
        <label>
          <input name="checkall" id="checkall" type="checkbox" onClick="toggle(this)"  >
          <span class="lbl"> Check All</span>
        </label>
      </th>
      <th class="span1 center">No</th>
      <th>Pertanyaan</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $no=1;
    foreach($get->result() as $row)
    {
      $cek = $this->model_polling->getPertanyaan($th_akademik,$poll_jenis_id,$row->id);
      if($cek==1)
      {
        $cek = 'checked';
      }else{
        $cek = '';
      }
      ?>
      <tr>
        <td class="center">
          <label>
            <input type="checkbox" name="cek[]" value="<?php echo $row->id;?>" <?php echo $cek;?>>
            <span class="lbl"></span>
          </label>
        </td>
        <td class="center"><?php echo $no++;?></td>
        <td><?php echo $row->pertanyaan;?></td>
      </tr>
      <?php
    }
     ?>
  </tbody>
</table>

<script type="text/javascript">
function toggle(source) {
  // console.log(source);
  checkboxes = document.getElementsByName('cek[]');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
</script>
