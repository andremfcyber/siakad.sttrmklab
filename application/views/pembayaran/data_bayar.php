<table  class="table fpTable lcnp table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="center" >No</th>
            <th class="center" >Th Akademik</th>
            <th class="center" >Nomor</th>
            <th class="center" >Tanggal</th>
            <th class="center" >NIM</th>
            <th class="center" >Nama</th>
            <th class="center" >SMT</th>
            <th class="center" >PRODI</th>
            <th class="center" >Kelas</th>
            <th class="center" >Jenis Tagihan</th>
            <th class="center" >Jumlah</th>
            <th class="center" >Keterangan</th>
            <th class="center span2" >Aksi</th>
        </tr>
    </thead>
    <tbody>
      <?php
		$i=1;
    $tota_sisa = 0;
		foreach($data as $dt){
      $infoMhs = $this->model_data->getInfoMhs($dt->nim);
      $nama_mhs = $infoMhs['nama_mhs'];
      $prodi_mhs = $infoMhs['nama_prodi'];
      $kelas_mhs = $infoMhs['kelas'];
      $infotagihan = $this->model_data->getJenisTagihan($dt->jenis_tagihan_id);

      $namatagihan =  $infotagihan['nama'].' semester '.$infotagihan['smt'];
      ?>
        <tr>
        	<td class="center span1"><?php echo $i++?></td>
            <td class="center"><?php echo $dt->th_akademik;?></td>
            <td class="center"><?php echo $dt->nomor;?></td>
            <td class="center"><?php echo $this->model_global->tgl_str($dt->tanggal);?></td>
            <td class="center"><?php echo $dt->nim;?></td>
            <td><?php echo $nama_mhs;?></td>
            <td class="center"><?php echo $dt->smt;?></td>
            <td><?php echo $prodi_mhs;?></td>
            <td class="center"><?php echo $kelas_mhs;?></td>
            <td><?php echo $namatagihan;?></td>
            <td style="text-align:right"><?php echo number_format($dt->jumlah);?></td>
            <td><?php echo $dt->keterangan;?></td>

            <td class="center">
              <a href="javascript:void(0);" onclick="editData('<?php echo $dt->id;?>')" class="btn btn-mini btn-success"  role="button" data-toggle="modal">
                <i class="icon-edit"></i>
              </a>
              <a href="javascript:void(0);" onclick="deleteData('<?php echo $dt->id;?>')"  class="btn btn-mini btn-danger"  role="button" data-toggle="modal">
                <i class="icon-trash"></i>
              </a>
              <a href="<?php echo site_url('pembayaran/cetak/'.$dt->nomor);?>" target="_blank" class="btn btn-mini btn-primary"  role="button" data-toggle="modal">
                <i class="icon-print"></i>
              </a>
            </td>
        </tr>
		<?php
      $tota_sisa +=$dt->jumlah;
      } ?>
      <tr>
        <td colspan="10" class="center">
          <strong>Total Bayar</strong>
        </td>
        <td colspan="3" style="text-align:left" class="red">
          <strong><?php echo number_format($tota_sisa);?></strong>
        </td>
      </tr>
    </tbody>
</table>
