<script type="text/javascript">
  $(document).ready(function(){
      $('.date-picker').datepicker().next().on(ace.click_event, function(){
        $(this).prev().focus();
      });

      $(".chzn-select").chosen();

      $("#nim").change(function(){
        cari_mhs();

      });

      function cari_mhs()
      {
        var string = {};
        string.nim = $("#nim").val();
        $.ajax({
    			type	: 'POST',
    			url		: "<?php echo site_url('pembayaran/cari_mhs'); ?>",
    			data	: string,
    			cache	: false,
          dataType : 'json',
    			success	: function(data){
            // console.log(data);
    				$("#th_angkatan").val(data.th_akademik);
            $("#nama_mhs").val(data.nama_mhs);
            $("#kd_prodi").val(data.kd_prodi);
            $("#nama_prodi").val(data.nama_prodi);
            $("#kelas").val(data.kelas);
            $("#smt").val(data.smt);
            $("#max_smt").val(data.max_smt);
            data_bayar();
            cari_tagihan();
            console.log(data);
            if(parseInt(data.smt) > parseInt(data.max_smt))
            {
                // console.log('tua amat loe');
                cari_total_sks();
            }else{
              $("#sks").val('');
            }
            if(parseInt(data.isi_krs)==1){
              $('#isi_krs1').attr('checked',true);
            }else{
              $('#isi_krs0').attr('checked',true);
            }
    			}
    		});
      }

      function cari_total_sks()
      {
        var string = {};
        string.nim = $("#nim").val();
        string.smt = $("#smt").val();
        string.th_akademik_kode = $("#th_akademik_kode").val();

        $.ajax({
    			type	: 'POST',
    			url		: "<?php echo site_url('pembayaran/cari_total_sks'); ?>",
    			data	: string,
    			cache	: false,
          dataType : 'json',
    			success	: function(data){
            // console.log(data);
    				$("#sks").val(data.sks);
    			}
    		});

      }



      function cari_tagihan()
      {
        var string = {};
        // andry
//        string.th_akademik = $("#th_angkatan").val();
        string.th_akademik = $("#th_akademik").val();

        string.kd_prodi = $("#kd_prodi").val();
        string.smt = $("#smt").val();
        string.max_smt = $("#max_smt").val();

        $.ajax({
    			type	: 'POST',
    			url		: "<?php echo site_url('pembayaran/cari_tagihan'); ?>",
    			data	: string,
    			cache	: false,
    			success	: function(data){
//            console.log(data);
    				$("#jenis_tagihan_id").html(data);
    			}
    		});
      }

      function data_bayar()
      {
        var string = {};
        string.nim = $("#nim").val();
        // console.log(string);
        $.ajax({
    			type	: 'POST',
    			url		: "<?php echo site_url('pembayaran/data_bayar'); ?>",
    			data	: string,
    			cache	: false,
    			success	: function(data){
    				// $("#info_mhs").html(data);
            // console.log(data);
            $("#data_bayar").html(data);
            // $("#list_mhs").hide();
    			}
    		});
      }

      $("#simpan").click(function(){
          var jumlah = $("#jumlah").val();
          var total_bayar = $("#total_bayar").val();

          if(!$("#th_akademik").val()){
      			$.gritter.add({
      				title: 'Peringatan..!!',
      				text: 'Tahun Akademik tidak boleh kosong',
      				class_name: 'gritter-error'
      			});

      			$("#th_akademik").focus();
      			return false();
      		}

          if(!$("#nim").val()){
      			$.gritter.add({
      				title: 'Peringatan..!!',
      				text: 'NIM tidak boleh kosong',
      				class_name: 'gritter-error'
      			});

      			$("#nim").focus();
      			return false();
      		}

          if(!$("#nama_mhs").val()){
      			$.gritter.add({
      				title: 'Peringatan..!!',
      				text: 'NIM tidak boleh kosong',
      				class_name: 'gritter-error'
      			});

      			$("#nama_mhs").focus();
      			return false();
      		}

          if(!$("#jenis_tagihan_id").val()){
      			$.gritter.add({
      				title: 'Peringatan..!!',
      				text: 'Jenis Tagihan tidak boleh kosong',
      				class_name: 'gritter-error'
      			});

      			$("#jenis_tagihan_id").focus();
      			return false();
      		}

          // if(!$("#jumlah").val()){
      		// 	$.gritter.add({
      		// 		title: 'Peringatan..!!',
      		// 		text: 'Jumlah Tagihan tidak boleh kosong',
      		// 		class_name: 'gritter-error'
      		// 	});
          //
      		// 	$("#jumlah").focus();
      		// 	return false();
      		// }

          if(!$("#total_bayar").val()){
      			$.gritter.add({
      				title: 'Peringatan..!!',
      				text: 'Jumlah Tagihan tidak boleh kosong',
      				class_name: 'gritter-error'
      			});

      			$("#total_bayar").focus();
      			return false();
      		}

          if(parseInt(total_bayar) == 0){
      			$.gritter.add({
      				title: 'Peringatan..!!',
      				text: 'Total Bayar tidak boleh Nol ',
      				class_name: 'gritter-error'
      			});

      			$("#total_bayar").focus();
      			return false();
      		}

          if(parseInt(total_bayar) > parseInt(jumlah)){
      			$.gritter.add({
      				title: 'Peringatan..!!',
      				text: 'Total Bayar tidak boleh lebih besar dari Jumlah',
      				class_name: 'gritter-error'
      			});

      			$("#total_bayar").focus();
      			return false();
      		}

//          // andry
//          if(parseInt(total_bayar) != parseInt(jumlah)){
//      			$.gritter.add({
//      				title: 'Peringatan..!!',
//      				text: 'Total Bayar harus sama dari Jumlah',
//      				class_name: 'gritter-error'
//      			});
//
//      			$("#total_bayar").focus();
//      			return false();
//      		}

          var string = $("#form-entry").serialize();
          // console.log(string);

          $.ajax({
      			type	: 'POST',
      			url		: "<?php echo site_url('pembayaran/simpan'); ?>",
      			data	: string,
      			cache	: false,
      			success	: function(data){
              $.gritter.add({
        				title: 'Info..!!',
        				text: data,
        				class_name: 'gritter-success'
        			});
              data_bayar();
              kosong();
              $("#simpan").hide();
      			}
      		});
      });

      function kosong()
      {
        $("#jenis_tagihan_id").val('');
        $("#jumlah").val('');
        $("#total_bayar").val('');
        $("#keterangan").val('');
      }

      $("#jenis_tagihan_id").change(function(){
          var string = {};
          string.id = $("#jenis_tagihan_id").val();
          string.nim = $("#nim").val();
          $.ajax({
              type	: 'POST',
              url		: "<?php echo site_url('pembayaran/jumlah_tagihan'); ?>",
              data	: string,
              cache	: false,
              dataType : 'json',
              success	: function(data){
                // console.log(data);
                // var smt = data.smt;
                // var max_smt = $("#max_smt").val();
                var sks = $("#sks").val();
                // if(parseInt(sks)>0){
                // if(parseInt(smt) > parseInt(max_smt)){
                if(data.x_sks=='ya'){
                  var harga = data.jumlah;
                  var biaya_sks = parseInt(sks) * parseInt(harga);
                  $("#jumlah").val(biaya_sks);
                }else{
                  $("#jumlah").val(data.jumlah);
                }
              }
            });
      });

      $("#cetak").click(function(){
          var nomor = $("#nomor").val();
          // window.location.assign("<?php echo site_url('pembayaran/cetak');?>"+'/'+nomor);
          window.open("<?php echo site_url('pembayaran/cetak');?>"+'/'+nomor);
      });
  });

  function editData(id)
  {
    // console.log(id);
    var string = {};
    string.id = id;
    $.ajax({
        type	: 'POST',
        url		: "<?php echo site_url('pembayaran/cari_data'); ?>",
        data	: string,
        cache	: false,
        dataType : 'json',
        success	: function(data){
          // console.log(data);
          $("#nomor").val(data.nomor);
          $("#th_akademik").val(data.th_akademik);
          $("#th_akademik_kode").val(data.th_akademik_kode);
          $("#tanggal").val(data.tanggal);
          $("#jenis_tagihan_id").val(data.jenis_tagihan_id);
          $("#total_bayar").val(data.jumlah);
          $("#keterangan").val(data.keterangan);

          $("#simpan").show();
          // $("#nim").hide(); //attr('disabled',true);
          // $("#lbl_nim").sho
        }
      });
  }

  function deleteData(id)
  {
    // console.log(id);

    var x = confirm('Yakin akan menghapus data ?');
    if(x==true){
      var string = {};
      string.id = id;
      $.ajax({
          type	: 'POST',
          url		: "<?php echo site_url('pembayaran/hapus'); ?>",
          data	: string,
          cache	: false,
          success	: function(data){
            // console.log(data);
            $.gritter.add({
      				title: 'Info..!!',
      				text: data,
      				class_name: 'gritter-success'
      			});
            data_bayar();
          }
        });
      }
  }

  function data_bayar()
  {
    var string = {};
    string.nim = $("#nim").val();
    // console.log(string);
    $.ajax({
      type	: 'POST',
      url		: "<?php echo site_url('pembayaran/data_bayar'); ?>",
      data	: string,
      cache	: false,
      success	: function(data){
        $("#data_bayar").html(data);
      }
    });
  }
</script>
<div class="widget-box">
	<div class="widget-header">
		<h4><?php echo $judul;?></h4>

		<span class="widget-toolbar">
			<a href="#" data-action="collapse">
				<i class="icon-chevron-up"></i>
			</a>

			<a href="#" data-action="close">
				<i class="icon-remove"></i>
			</a>
		</span>
	</div>

	<div class="widget-body">
		<div class="widget-main no-padding">
      <?php
      $info = $this->session->flashdata('info');
      if(!empty($info)){
        echo $info;
      }
       ?>
       <form name="form-entry" id="form-entry" class="form-horizontal">
         <fieldset>
      <!-- <div class="span6"> -->

           <div class="control-group">
             <label class="control-label" for="form-field-1">No. Pembayaran</label>

             <div class="controls">
               <input type="text" name="nomor" id="nomor" class="span2" value="<?php echo $nomor;?>">
             </div>
           </div>

           <div class="control-group">
             <label class="control-label" for="form-field-1">Tanggal</label>
             <div class="controls">
               <div class="row-fluid input-append">
                 <input type="text" name="tanggal" id="tanggal" class="span4 date-picker"  data-date-format="dd-mm-yyyy" value="<?php  echo $tanggal;?>"/>
                 <span class="add-on">
										<i class="icon-calendar"></i>
									</span>
               </div>
             </div>
           </div>

           <div class="control-group">
             <label class="control-label" for="form-field-1">Th Akademik</label>

             <div class="controls">
               <input type="text" name="th_akademik_kode" id="th_akademik_kode" class="span1" readonly="true" value="<?php echo $th_akademik_kode;?>">
               <input type="text" name="th_akademik" id="th_akademik" class="span2" readonly="true" value="<?php echo $th_akademik;?>">
             </div>
           </div>

           <div class="control-group">
             <label class="control-label" for="form-field-1">Semester</label>

             <div class="controls">
               <input type="text" name="semester" id="semester" class="span2" readonly="true" value="<?php echo $semester;?>">
             </div>
           </div>

						<div class="control-group">
							<label class="control-label" for="form-field-1">NIM</label>
							<div class="controls">
								<select name="nim" id="nim" class="chzn-select" data-placeholder="Cari NIM ....">
                  <option value="">-Pilih-</option>
                  <?php
//                  $list_nim = $this->db->select('nim')->where('status','Aktif')->get('mahasiswa')->result();
                  $list_nim = $this->db->select('nim')->where("status in ('Aktif','Cuti')")->get('mahasiswa')->result();
                  foreach($list_nim as $row){
                    ?>
                    <option value="<?php echo $row->nim;?>"><?php echo $row->nim;?></option>
                    <?php
                  }
                   ?>
                </select>
							</div>
						</div>

            <div class="control-group">
							<label class="control-label" for="form-field-1">Nama</label>

							<div class="controls">
                <input type="text" name="nama_mhs" id="nama_mhs" class="span4" readonly>
							</div>
						</div>

            <div class="control-group">
							<label class="control-label" for="form-field-1">Program Studi</label>

							<div class="controls">
                <input type="text" name="kd_prodi" id="kd_prodi" class="span1" readonly>
                <input type="text" name="nama_prodi" id="nama_prodi" class="span3" readonly>
							</div>
						</div>

            <div class="control-group">
							<label class="control-label" for="form-field-1">Semester</label>
							<div class="controls">
                <input type="text" name="smt" id="smt" class="span1" readonly>
                Max Semester <input type="text" name="max_smt" id="max_smt" class="span1" readonly="true" >
							</div>
						</div>

      <!-- </div> -->
          <!-- batas -->

      <!-- <div class="span5"> -->


            <div class="control-group">
							<label class="control-label" for="form-field-1">Th Angkatan</label>
							<div class="controls">
                <input type="text" name="th_angkatan" id="th_angkatan" class="span2" readonly>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="form-field-2">Kelas</label>

							<div class="controls">
                <input type="text" name="kelas" id="kelas" class="span1" readonly>
							</div>
						</div>

            <div class="control-group">
							<label class="control-label" for="form-field-2">Jumlah SKS</label>

							<div class="controls">
                <input type="text" name="sks" id="sks" class="span1" readonly>
							</div>
						</div>

            <div class="control-group">
							<label class="control-label" for="form-field-1">Jenis Tagihan</label>
							<div class="controls">
								<select name="jenis_tagihan_id" id="jenis_tagihan_id" class="span4">
                  <option value="">-Pilih-</option>

                </select>
							</div>
						</div>

            <div class="control-group">
							<label class="control-label" for="form-field-2">Jumlah</label>

							<div class="controls">
                <input type="text" name="jumlah" id="jumlah" class="span2 text-right" readonly>
							</div>
						</div>

            <div class="control-group">
							<label class="control-label" for="form-field-2">Total Bayar</label>

							<div class="controls">
                <input type="number" name="total_bayar" id="total_bayar" class="span2 text-right" >
							</div>
						</div>

            <div class="control-group">
							<label class="control-label" for="form-field-2">Keterangan</label>

							<div class="controls">
                <input type="text" name="keterangan" id="keterangan" class="span6" >
							</div>
						</div>

            <div class="control-group">
              <label class="control-label" for="form-field-2">Mahasiswa Boleh Mengisi KRS?</label>
              
              <div class="controls">
                <input type="radio" name="isi_krs" id="isi_krs1" value="1">Ya<br>
                <input type="radio" name="isi_krs" id="isi_krs0" value="0">Tidak
              </div>
            </div>

      <!-- </div> -->

            </fieldset>

            <div class="form-actions center">
							<button type="button" name="simpan" id="simpan" class="btn btn-small btn-primary">
								Simpan
								<i class="icon-save icon-on-right bigger-110"></i>
							</button>
              <button type="button" name="cetak" id="cetak" class="btn btn-small btn-info">
								Cetak
								<i class="icon-print icon-on-right bigger-110"></i>
							</button>
              <a href="<?php echo site_url('pembayaran');?>"  class="btn btn-small btn-success">
								Tambah
								<i class="icon-plus icon-on-right bigger-110"></i>
							</a>
						</div>


      </form>

		</div>
	</div>
</div>

<div id="data_bayar"></div>
