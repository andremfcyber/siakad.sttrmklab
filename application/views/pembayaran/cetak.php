<style>
  .page {
    font-family: "Times New Roman", Times, serif;
    font-size: 14px;
    background-color:#FFFFFF;
    left:5px;
    right:5px;
    height:5.51in ; /*Ukuran Panjang Kertas */
    width: 8.50in; /*Ukuran Lebar Kertas */
    margin:1px solid #FFFFFF;
  }
  table {
    border-collapse: collapse;
    border-style: none;
  }
  table, th, td {
     border: 0px solid black;
  }
  table td {
    padding: 3px;
  }
  img {
    margin: 5px;
  }
  h2 {
    font-size: 16px;
    margin: 0;
  }
  h3 {
    font-size: 14px;
    margin: 0;
  }
  p {
    font-size: 12px;
    margin: 0;
  }
  .text-center {
    text-align: center;
  }
</style>
<?php
$nomor =  $this->uri->segment(3);
$this->db->where('nomor',$nomor);
$data = $this->db->get('bayar_mhs');
if($data->num_rows()==0){
  ?>
    <script type="text/javascript">
      alert('Maaf, Nomor Pembayaran Tidak terdaftar');
      window.location.assign("<?php echo site_url('pembayaran');?>");
    </script>
  <?php
  // redirect('pembayaran');
}
$infoBayar = $this->model_data->getInfoBayar($nomor);
$nomor = $infoBayar['nomor'];
$tanggal = $infoBayar['tanggal'];
$jumlah = $infoBayar['jumlah'];
$terbilang = $this->model_global->bilang($jumlah);
$ttd = $this->model_data->getInfoUser($infoBayar['user_id'])['nama_lengkap'];
$pembayaran = $infoBayar['jenis_tagihan'].' semester '.$infoBayar['smt'].' Tahun Akademik '.$infoBayar['th_akademik'];
$th_akademik_kode = $infoBayar['th_akademik_kode'];
$sks = $infoBayar['sks'];

$jenis_tagihan_id = $infoBayar['jenis_tagihan_id'];
$infoTagihan = $this->model_data->getJenisTagihan($jenis_tagihan_id);
$jumlah_tagihan = $infoTagihan['jumlah'];

$nim = $infoBayar['nim'];
$infoMhs = $this->model_data->getInfoMhs($nim);
$nama_mhs = $infoMhs['nama_mhs'];
$nama_prodi = $infoMhs['nama_prodi'].' ( '.$infoMhs['th_akademik'].' )';
$semester = $this->model_global->semester($nim,$th_akademik_kode);
$kelas = $infoMhs['kelas'];
?>
<div class="page">
<table width="100%" border="1">
  <tr>
    <td width="5%" >
      <img src="<?php echo base_url();?>assets/img/logo1.png" alt="" width="90" />
    </td>
    <td >
      <h2><?php echo app_setting()['data_setting'][0]->nama_pendek;?></h2>
      <h2><?php echo app_setting()['data_setting'][0]->nama_instansi;?></h2>
      <p><?php echo app_setting()['data_setting'][0]->alamat1;?><br/>
        <?php echo app_setting()['data_setting'][0]->alamat2;?><br/>
      <?php echo 'Website : http://'.app_setting()['data_setting'][0]->website.'  Email : '.app_setting()['data_setting'][0]->email;?>
      </p>
    </td>
    <td width="30%" class="text-center">
      <h3>Nomor</h3>
      <h2><?php echo $nomor;?></h2>
      <h3>Tanggal</h3>
      <h3><?php echo $this->model_global->tgl_indo($tanggal);?></h3>
    </td>
  </tr>
  <tr>
    <td colspan="3" class="text-center" style="padding:10px;">
      <h2>TANDA BUKTI PEMBAYARAN</h2>
    </td>
  </tr>
</table>
<table width="100%" border="1">
  <tr>
    <td width="200">Terima Uang</td>
    <td width="5">:</td>
    <td>Rp. <?php echo number_format($jumlah);?></td>
  </tr>
  <tr>
    <td width="200">Terbilang</td>
    <td width="5">:</td>
    <td><?php echo $terbilang;?> Rupiah</td>
  </tr>
  <tr>
    <td width="200">Untuk Pembayaran</td>
    <td width="5">:</td>
    <td><?php echo $pembayaran;?>&nbsp;
      <?php
        if($sks>0){
          echo "Jumlah SKS ".$sks.' @ Rp. '.number_format($jumlah_tagihan);
        }
       ?>
    </td>
  </tr>
  <tr>
    <td colspan="3" class="text-center"><h3>PENYETOR</h3></td>
  </tr>
  <tr>
    <td width="200">NIM</td>
    <td width="5">:</td>
    <td><?php echo $nim;?></td>
  </tr>
  <tr>
    <td width="200">Nama</td>
    <td width="5">:</td>
    <td><?php echo $nama_mhs;?></td>
  </tr>
  <tr>
    <td width="200">Program Studi</td>
    <td width="5">:</td>
    <td><?php echo $nama_prodi;?></td>
  </tr>
  <tr>
    <td width="200">Semester - Kelas</td>
    <td width="5">:</td>
    <td><?php echo $semester.' - '. $kelas;?></td>
  </tr>
</table>
<table width="100%" border="1">
  <tr>
    <td width="50%" class="text-center">
      Penerima,<br/>
      Keuangan<br/><br/><br/>
      <b><?php echo $ttd;?></b>
    </td>
    <td width="50%" class="text-center">
      <?php echo app_setting()['data_setting'][0]->kota;?>, <?php echo $this->model_global->tgl_indo(date('Y-m-d'));?><br/>
      Penyetor<br/><br/><br/>
      <strong><?php echo $nama_mhs;?></strong>
    </td>
  </tr>
</table>
Ketentuan :<br/>
Setoran diakui sah apabila telah dibubuhi cap dan tanda tangan bagian keuangan.
</div>
