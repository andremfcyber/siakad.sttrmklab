<table  class="table fpTable lcnp table-striped table-bordered table-hover dt-responsive">
    <thead>
        <tr>
            <th class="center">No</th>
            <th class="center">NIM</th>
            <th class="center">Nama Mahasiswa</th>
            <th class="center">Kelamin</th>
            <th class="center">SMT</th>
            <th class="center">Prodi</th>
            <th class="center">Kelas</th>
            <th class="center">Jenis Tagihan</th>
            <th class="center">Semester</th>
            <th class="center">Jumlah</th>
            <th class="center">Bayar</th>
            <th class="center">Sisa</th>
        </tr>
    </thead>
    <tbody>
    	<?php
        // $this->debug($data);die;
		$i=1;
    $total = 0;
		foreach($data->result() as $dt){
      $infoMhs = $this->model_data->getInfoMhs($dt->nim);
      $nama_mhs = $infoMhs['nama_mhs'];
      $sex = $infoMhs['sex'];
      $kelas = $infoMhs['kelas'];
      $nama_prodi = $infoMhs['nama_prodi'];
      $th_angkatan = $infoMhs['th_angkatan'];
      $semester = $this->model_global->semester($dt->nim,$th_angkatan);

      $infoTagihan = $this->model_data->getJenisTagihan($dt->id);
      $nama_tagihan  = $infoTagihan['nama'];

      $sisa_bayar = $dt->jumlah - $dt->jumlah_bayar;
      if($sisa_bayar>0 AND $dt->smt <= $semester){
		?>
        <tr>
        	<td class="center span1"><?php echo $i++?></td>
            <td class="center"><?php echo $dt->nim;?></td>
            <td ><?php echo $nama_mhs;?></td>
            <td class="center"><?php echo $sex=='L'?'Laki-laki':'Perempuan';?></td>
            <td class="center"><?php echo $semester;?></td>
            <td ><?php echo $nama_prodi;?></td>
            <td class="center"><?php echo $kelas;?></td>
            <td ><?php echo $nama_tagihan;?></td>
            <td class="center"><?php echo $dt->smt;?></td>
            <td style="text-align:right;"><?php echo number_format($dt->jumlah);?></td>
            <td style="text-align:right;"><?php echo number_format($dt->jumlah_bayar);?></td>
            <td style="text-align:right;"><?php echo number_format($sisa_bayar);?></td>
        </tr>
		<?php
        $total +=$sisa_bayar;
      }
    }?>
      <!-- <tr>
        <td colspan="11" class="center">
          TOTAL PIUTANG
        </td>
        <td style="text-align:right;">
            <?php echo number_format($total);?>
        </td>
      </tr> -->
    </tbody>
</table>

<div class="grid-container">
    <div class="item1">TOTAL PIUTANG : </div>
    <div class="item2"><?php echo number_format($total);?></div>
</div>