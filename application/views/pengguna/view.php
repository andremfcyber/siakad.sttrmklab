<script type="text/javascript">
$(document).ready(function(){
  $("#simpan").click(function(){
    var username	= $("#username").val();
		var nama_lengkap	= $("#nama_lengkap").val();
    var level	= $("#level").val();
    var password	= $("#password").val();
    var ulangi_password	= $("#ulangi_password").val();


		var string = $("#my-form").serialize();


		if(username.length==0){
			alert('Maaf, Username Tidak boleh kosong');
			$("#username").focus();
			return false();
		}

		if(nama_lengkap.length==0){
			alert('Maaf, Nama Lengkap Tidak boleh kosong');
			$("#nama_lengkap").focus();
			return false();
		}

    if(level.length==0){
			alert('Maaf, Level Tidak boleh kosong');
			$("#level").focus();
			return false();
		}

    if(password != ulangi_password){
			alert('Maaf, Maaf passwod tidak sama');
			$("#ulangi_password").focus();
			return false();
		}

    // if(password.length==0){
		// 	alert('Maaf, Password Tidak boleh kosong');
		// 	$("#password").focus();
		// 	return false();
		// }



		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url('pengguna/simpan'); ?>",
			data	: string,
			cache	: false,
			success	: function(data){
				alert(data);
				location.reload();
			}
		});

	});

  $("#tambah").click(function(){
        $('#username').val('');
		$('#kd_prodi').val('');
		$('#username').attr("readonly",false);
		$('#nama_lengkap').val('');
    $('#level').val('');
	});

});

function toogle_prodi(){
    if($('#level').val()=='admin'){
        $('#kd_prodi').removeAttr('disabled');
        // console.log('admin');
        
    }else{
        $('#kd_prodi').attr('disabled',true);
        // console.log('NOTadmin');
    }
}

function editData(ID){
	var cari	= ID;
	$.ajax({
		type	: "POST",
		url		: "<?php echo site_url('pengguna/cari'); ?>",
		data	: "cari="+cari,
		dataType: "json",
		success	: function(data){
			//alert(data.ref);
			$('#username').val(data.username);
			$('#username').attr("readonly","true");
            $('#nama_lengkap').val(data.nama_lengkap);
			$('#kd_prodi').val(data.kd_prodi);
      $('#level').val(data.level);
      if(data.blokir=='Ya'){
        $("#blokir_ya").prop("checked", true);
      }else{
        $("#blokir_tidak").prop("checked", true);
      }
		}
	});
    setTimeout(function(){ toogle_prodi(); },50);

}
</script>

<div class="row-fluid">
<div class="table-header">
    <?php echo $judul;?>
    <div class="widget-toolbar no-border pull-right">
    <a href="#modal-table" class="btn btn-small btn-success"  role="button" data-toggle="modal" name="tambah" id="tambah" >
        <i class="icon-check"></i>
        Tambah Data
    </a>
    </div>
</div>

<table  class="table fpTable lcnp table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="center">No</th>
            <th class="center">Username</th>
            <th class="center">Nama Lengkap</th>
            <th class="center">Level</th>
            <th class="center">Blokir</th>
            <th class="center">Aksi</th>
        </tr>
    </thead>
    <tbody>
    	<?php
		$i=1;
		foreach($data->result() as $dt){ ?>
        <tr>
        	<td class="center span1"><?php echo $i++?></td>
            <td><?php echo $dt->username;?></td>
            <td><?php echo $dt->nama_lengkap;?></td>
            <td class="center">
                <?php 
                    echo strtoupper($dt->level);
                    if(!empty($dt->kd_prodi)){
                        echo "  - ".$dt->prodi;
                    }
                ?></td>
            <td class="center"><?php echo $dt->blokir;?></td>
            <td class="td-actions"><center>
            	<div class="hidden-phone visible-desktop action-buttons">
                    <a class="green" href="#modal-table" onclick="javascript:editData('<?php echo $dt->id_username;?>')" data-toggle="modal">
                        <i class="icon-pencil bigger-130"></i>
                    </a>

                    <a class="red" href="<?php echo site_url();?>/pengguna/hapus/<?php echo $dt->id_username;?>" onClick="return confirm('Anda yakin ingin menghapus data ini?')">
                        <i class="icon-trash bigger-130"></i>
                    </a>
                </div>

                <div class="hidden-desktop visible-phone">
                    <div class="inline position-relative">
                        <button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-caret-down icon-only bigger-120"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close">
                            <li>
                                <a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
                                    <span class="green">
                                        <i class="icon-edit bigger-120"></i>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
                                    <span class="red">
                                        <i class="icon-trash bigger-120"></i>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                </center>
            </td>
        </tr>
		<?php } ?>
    </tbody>
</table>
</div>
<div id="modal-table" class="modal hide fade" tabindex="-1">
    <div class="modal-header no-padding">
        <div class="table-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            Ruang Kuliah
        </div>
    </div>

    <div class="modal-body no-padding">
        <div class="row-fluid">
            <form class="form-horizontal" name="my-form" id="my-form">
                <div class="control-group">
                    <label class="control-label" for="form-field-1">Username</label>
                    <div class="controls">
                        <input type="text" name="username" id="username"  class="span8"  />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="form-field-1">Nama Lengkap</label>
                    <div class="controls">
                      <input type="text" name="nama_lengkap" id="nama_lengkap"  class="span8"/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="form-field-1">Password</label>
                    <div class="controls">
                      <input type="password" name="password" id="password"  class="span8"/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="form-field-1">Ulangi Password</label>
                    <div class="controls">
                      <input type="password" name="ulangi_password" id="ulangi_password"  class="span8"/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="form-field-1">Level</label>
                    <div class="controls">
                      <select name="level" id="level" class="span4" onchange="toogle_prodi()">
                        <option value="">-Pilih-</option>
                        <?php
                          $list_level = $this->model_data->level_pengguna();
                          foreach($list_level as $row)
                          {
                            echo "<option value='$row'>$row</option>";
                          }
                         ?>
                      </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="form-field-1">Prodi</label>
                    <div class="controls">
                      <select name="kd_prodi" id="kd_prodi" class="span4" disabled>
                        <option value="">-Pilih-</option>
                        <?php
                          $prodi = $this->model_data->data_jurusan()->result();
                          foreach($prodi as $p)
                          {
                            echo "<option value='$p->kd_prodi'>$p->prodi</option>";
                          }
                         ?>
                      </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="form-field-1">Blokir</label>
                    <div class="controls">
                      <label>
													<input name="blokir" id="blokir_ya" type="radio" value="Ya"  />
													<span class="lbl"> Ya</span>
												</label>
                        <label>
  													<input name="blokir" id="blokir_tidak" type="radio" value="Tidak" checked="checked" />
  													<span class="lbl"> Tidak</span>
  												</label>
                    </div>
                </div>

			      </form>
        </div>
    </div>

    <div class="modal-footer">
        <div class="pagination pull-right no-margin">
        <button type="button" class="btn btn-small btn-danger pull-left" data-dismiss="modal">
            <i class="icon-remove"></i>
            Close
        </button>
        <button type="button" name="simpan" id="simpan" class="btn btn-small btn-success pull-left">
            <i class="icon-save"></i>
            Simpan
        </button>
		</div>
    </div>
</div>
