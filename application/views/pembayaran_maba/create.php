<script type="text/javascript">
  $(document).ready(function(){
      $('.date-picker').datepicker().next().on(ace.click_event, function(){
        $(this).prev().focus();
      });

      $(".chzn-select").chosen();

      $("#no_pendaftaran").change(function(){
        cari_mhs();

      });

      cari_tagihan();

      function cari_mhs()
      {
        var string = {};
        string.no_pendaftaran = $("#no_pendaftaran").val();
        $.ajax({
    			type	: 'POST',
    			url		: "<?php echo site_url('pembayaran_maba/cari_mhs'); ?>",
    			data	: string,
    			cache	: false,
          dataType : 'json',
    			success	: function(data){
            // console.log(data);
    				// $("#th_angkatan").val(data.th_akademik);
            $("#nama_mhs").val(data.nama);
            $("#kd_prodi").val(data.kd_prodi);
            $("#nama_prodi").val(data.prodi);
            // $("#kelas").val(data.kelas);
            
            data_bayar();
            
            if(parseInt(data.status_bayar)==1){
              $('#status_bayar1').attr('checked',true);
            }else{
              $('#status_bayar0').attr('checked',true);
            }
    			}
    		});
      }



      function cari_tagihan()
      {

        $.ajax({
    			type	: 'POST',
    			url		: "<?php echo site_url('pembayaran_maba/cari_tagihan'); ?>",
    			data	: '',
    			cache	: false,
    			success	: function(data){
//            console.log(data);
    				$("#master_tagihan_maba").html(data);
    			}
    		});
      }

      function data_bayar()
      {
        var string = {};
        string.no_pendaftaran = $("#no_pendaftaran").val();
        // console.log(string);
        $.ajax({
    			type	: 'POST',
    			url		: "<?php echo site_url('pembayaran_maba/data_bayar'); ?>",
    			data	: string,
    			cache	: false,
    			success	: function(data){
    				// $("#info_mhs").html(data);
            // console.log(data);
            $("#data_bayar").html(data);
            // $("#list_mhs").hide();
    			}
    		});
      }

      $("#simpan").click(function(){
          var jumlah = $("#jumlah").val();
          var total_bayar = $("#total_bayar").val();

          if(!$("#th_akademik").val()){
      			$.gritter.add({
      				title: 'Peringatan..!!',
      				text: 'Tahun Akademik tidak boleh kosong',
      				class_name: 'gritter-error'
      			});

      			$("#th_akademik").focus();
      			return false();
      		}

          if(!$("#no_pendaftaran").val()){
      			$.gritter.add({
      				title: 'Peringatan..!!',
      				text: 'No. Pendaftaran tidak boleh kosong',
      				class_name: 'gritter-error'
      			});

      			$("#no_pendaftaran").focus();
      			return false();
      		}

        //   if(!$("#nama_mhs").val()){
      		// 	$.gritter.add({
      		// 		title: 'Peringatan..!!',
      		// 		text: 'NIM tidak boleh kosong',
      		// 		class_name: 'gritter-error'
      		// 	});

      		// 	$("#nama_mhs").focus();
      		// 	return false();
      		// }

          if(!$("#master_tagihan_maba").val()){
      			$.gritter.add({
      				title: 'Peringatan..!!',
      				text: 'Jenis Tagihan tidak boleh kosong',
      				class_name: 'gritter-error'
      			});

      			$("#master_tagihan_maba").focus();
      			return false();
      		}

          // if(!$("#jumlah").val()){
      		// 	$.gritter.add({
      		// 		title: 'Peringatan..!!',
      		// 		text: 'Jumlah Tagihan tidak boleh kosong',
      		// 		class_name: 'gritter-error'
      		// 	});
          //
      		// 	$("#jumlah").focus();
      		// 	return false();
      		// }

          if(!$("#total_bayar").val()){
      			$.gritter.add({
      				title: 'Peringatan..!!',
      				text: 'Jumlah Tagihan tidak boleh kosong',
      				class_name: 'gritter-error'
      			});

      			$("#total_bayar").focus();
      			return false();
      		}

          if(parseInt(total_bayar) == 0){
      			$.gritter.add({
      				title: 'Peringatan..!!',
      				text: 'Total Bayar tidak boleh Nol ',
      				class_name: 'gritter-error'
      			});

      			$("#total_bayar").focus();
      			return false();
      		}

          if(parseInt(total_bayar) > parseInt(jumlah)){
      			$.gritter.add({
      				title: 'Peringatan..!!',
      				text: 'Total Bayar tidak boleh lebih besar dari Jumlah',
      				class_name: 'gritter-error'
      			});

      			$("#total_bayar").focus();
      			return false();
      		}

//          // andry
//          if(parseInt(total_bayar) != parseInt(jumlah)){
//      			$.gritter.add({
//      				title: 'Peringatan..!!',
//      				text: 'Total Bayar harus sama dari Jumlah',
//      				class_name: 'gritter-error'
//      			});
//
//      			$("#total_bayar").focus();
//      			return false();
//      		}

          var string = $("#form-entry").serialize();
          // console.log(string);

          $.ajax({
      			type	: 'POST',
      			url		: "<?php echo site_url('pembayaran_maba/simpan'); ?>",
      			data	: string,
      			cache	: false,
      			success	: function(data){
              $.gritter.add({
        				title: 'Info..!!',
        				text: data,
        				class_name: 'gritter-success'
        			});
              data_bayar();
              kosong();
              $("#simpan").hide();
      			}
      		});
      });

      function kosong()
      {
        $("#master_tagihan_maba").val('');
        $("#jumlah").val('');
        $("#total_bayar").val('');
        $("#keterangan").val('');
      }

      $("#master_tagihan_maba").change(function(){
          var string = {};
          string.id = $("#master_tagihan_maba").val();
          string.no_pendaftaran = $("#no_pendaftaran").val();
          $.ajax({
              type	: 'POST',
              url		: "<?php echo site_url('pembayaran_maba/jumlah_tagihan'); ?>",
              data	: string,
              cache	: false,
              dataType : 'json',
              success	: function(data){
                // console.log(data);
                $("#jumlah").val(data.jumlah);
              }
            });
      });

      $("#cetak").click(function(){
          var nomor = $("#nomor").val();
          // window.location.assign("<?php echo site_url('pembayaran_maba/cetak');?>"+'/'+nomor);
          window.open("<?php echo site_url('pembayaran_maba/cetak');?>"+'/'+nomor);
      });
  });

  function editData(id)
  {
    console.log(id);
    var string = {};
    string.id = id;
    $.ajax({
        type	: 'POST',
        url		: "<?php echo site_url('pembayaran_maba/cari_data'); ?>",
        data	: string,
        cache	: false,
        dataType : 'json',
        success	: function(data){
          // console.log(data);
          $("#nomor").val(data.nomor);
          $("#th_akademik").val(data.th_akademik);
          $("#th_akademik_kode").val(data.th_akademik_kode);
          $("#tanggal").val(data.tanggal);
          $("#master_tagihan_maba").val(data.master_tagihan_maba);
          $("#total_bayar").val(data.jumlah);
          $("#keterangan").val(data.keterangan);

          $("#simpan").show();
          // $("#nim").hide(); //attr('disabled',true);
          // $("#lbl_nim").sho
        }
      });
  }

  function deleteData(id)
  {
    // console.log(id);

    var x = confirm('Yakin akan menghapus data ?');
    if(x==true){
      var string = {};
      string.id = id;
      $.ajax({
          type	: 'POST',
          url		: "<?php echo site_url('pembayaran_maba/hapus'); ?>",
          data	: string,
          cache	: false,
          success	: function(data){
            // console.log(data);
            $.gritter.add({
      				title: 'Info..!!',
      				text: data,
      				class_name: 'gritter-success'
      			});
            data_bayar();
          }
        });
      }
  }

  function data_bayar()
  {
    var string = {};
    string.no_pendaftaran = $("#no_pendaftaran").val();
    // console.log(string);
    $.ajax({
      type	: 'POST',
      url		: "<?php echo site_url('pembayaran_maba/data_bayar'); ?>",
      data	: string,
      cache	: false,
      success	: function(data){
        $("#data_bayar").html(data);
      }
    });
  }
</script>
<div class="widget-box">
	<div class="widget-header">
		<h4><?php echo $judul;?></h4>

		<span class="widget-toolbar">
			<a href="#" data-action="collapse">
				<i class="icon-chevron-up"></i>
			</a>

			<a href="#" data-action="close">
				<i class="icon-remove"></i>
			</a>
		</span>
	</div>

	<div class="widget-body">
		<div class="widget-main no-padding">
      <?php
      $info = $this->session->flashdata('info');
      if(!empty($info)){
        echo $info;
      }
       ?>
       <form name="form-entry" id="form-entry" class="form-horizontal">
         <fieldset>
      <div class="span6">

           <div class="control-group">
             <label class="control-label" for="form-field-1">No. Pembayaran</label>

             <div class="controls">
               <input type="text" name="nomor" id="nomor" class="span2" value="<?php echo $nomor;?>">
             </div>
           </div>

           <div class="control-group">
             <label class="control-label" for="form-field-1">Tanggal</label>
             <div class="controls">
               <div class="row-fluid input-append">
                 <input type="text" name="tanggal" id="tanggal" class="span4 date-picker"  data-date-format="dd-mm-yyyy" value="<?php  echo $tanggal;?>"/>
                 <span class="add-on">
										<i class="icon-calendar"></i>
									</span>
               </div>
             </div>
           </div>

           <div class="control-group">
             <label class="control-label" for="form-field-1">Th Akademik</label>

             <div class="controls">
               <input type="text" name="th_akademik_kode" id="th_akademik_kode" class="span1" readonly="true" value="<?php echo $th_akademik_kode;?>">
               <input type="text" name="th_akademik" id="th_akademik" class="span2" readonly="true" value="<?php echo $th_akademik;?>">
             </div>
           </div>

           <!-- <div class="control-group">
             <label class="control-label" for="form-field-1">Semester</label>

             <div class="controls">
               <input type="text" name="semester" id="semester" class="span2" readonly="true" value="<?php echo $semester;?>">
             </div>
           </div> -->

						<div class="control-group">
							<label class="control-label" for="form-field-1">No. Pendaftaran</label>
							<div class="controls">
								<select name="no_pendaftaran" id="no_pendaftaran" class="chzn-select" data-placeholder="Cari No. Pendaftaran ....">
                  <option value="">-Pilih-</option>
                  <?php
//                  $list_nim = $this->db->select('nim')->where('status','Aktif')->get('mahasiswa')->result();
                  // $year = date('Y');
                  // $this->db->like('nim', $year, 'after');
                  $this->db->order_by('no_pendaftaran', 'ASC');
                  $list_no_pendaftaran = $this->db->get_where('pmb.users', array('done' => 1, 'delete' => 0, 'mhs' => 0))->result();
                  foreach($list_no_pendaftaran as $row){
                    ?>
                    <option value="<?php echo $row->no_pendaftaran;?>"><?php echo $row->no_pendaftaran;?></option>
                    <?php
                  }
                   ?>
                </select>
							</div>
						</div>

            <div class="control-group">
							<label class="control-label" for="form-field-1">Nama</label>

							<div class="controls">
                <input type="text" name="nama_mhs" id="nama_mhs" class="span4" readonly>
							</div>
						</div>

            <div class="control-group">
							<label class="control-label" for="form-field-1">Program Studi</label>

							<div class="controls">
                <input type="text" name="kd_prodi" id="kd_prodi" class="span1" readonly>
                <input type="text" name="nama_prodi" id="nama_prodi" class="span3" readonly>
							</div>
						</div>

  


            <div class="control-group">
							<label class="control-label" for="form-field-1">Th Angkatan</label>
							<div class="controls">
                <input type="text" name="th_angkatan" id="th_angkatan" class="span2" value="<?php echo $th_akademik;?>" readonly>
							</div>
						</div>

						<!-- <div class="control-group">
							<label class="control-label" for="form-field-2">Kelas</label>

							<div class="controls">
                <input type="text" name="kelas" id="kelas" class="span1" readonly>
							</div>
						</div> -->

            <div class="control-group">
							<label class="control-label" for="form-field-1">Jenis Tagihan</label>
							<div class="controls">
								<select name="master_tagihan_maba" id="master_tagihan_maba" class="span4">
                  <option value="">-Pilih</option>

                </select>
							</div>
						</div>

            <div class="control-group">
							<label class="control-label" for="form-field-2">Jumlah</label>

							<div class="controls">
                <input type="text" name="jumlah" id="jumlah" class="span2 text-right" readonly>
							</div>
						</div>

            <div class="control-group">
							<label class="control-label" for="form-field-2">Total Bayar</label>

							<div class="controls">
                <input type="number" name="total_bayar" id="total_bayar" class="span2 text-right" >
							</div>
						</div>

            <div class="control-group">
							<label class="control-label" for="form-field-2">Keterangan</label>

							<div class="controls">
                <input type="text" name="keterangan" id="keterangan" class="span6" >
							</div>
						</div>

            <div class="control-group">
              <label class="control-label" for="form-field-2">Seluruh Pembayaran LUNAS?</label>
              
              <div class="controls">
                <input type="radio" name="status_bayar" id="status_bayar1" value="1">Ya<br>
                <input type="radio" name="status_bayar" id="status_bayar0" value="0">Tidak
              </div>
            </div>

            </fieldset>

            <div class="form-actions center">
							<button type="button" name="simpan" id="simpan" class="btn btn-small btn-primary">
								Simpan
								<i class="icon-save icon-on-right bigger-110"></i>
							</button>
              <button type="button" name="cetak" id="cetak" class="btn btn-small btn-info">
                Cetak
                <i class="icon-print icon-on-right bigger-110"></i>
              </button>
              <a href="<?php echo site_url('pembayaran_maba');?>"  class="btn btn-small btn-success">
                Tambah
                <i class="icon-plus icon-on-right bigger-110"></i>
              </a>
            </div>


      </form>

    </div>
  </div>
</div>

<div id="data_bayar"></div>
