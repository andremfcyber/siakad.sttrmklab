<div class="row-fluid">
    <div class="span12">
        <!--PAGE CONTENT BEGINS-->

        <div class="alert alert-block alert-success">

            <i class="icon-ok green"></i>

            Selamat datang di
            <strong class="green">
                Aplikasi <?php echo app_setting()['data_setting'][0]->nama_aplikasi;?>
                <small>(v1.1.0)</small>
            </strong>
            ,
            <?php echo app_setting()['data_setting'][0]->nama_pendek;?> <?php echo app_setting()['data_setting'][0]->nama_instansi;?>
        </div>
</div>
</div>
<div class="row-fluid">
	<div class="span12">
            
    </div>
    <div class="grid-container-isi">
        <div class="pengguna">
            <div class="isi-icon">
                <img src="<?php echo base_url();?>assets/css/img/New/ic_pengguna.png" class="isi-img">
            </div>

            <div class="isi-data">
                <br>
                <span class="isi-data-number"><?php echo $this->model_data->jml_data('admins');?> Data</span>
                <br>
                <br>
                <div class="isi-data-ket">Pengguna</div>
            </div>
        </div>
        <div class="matkul">
            <div class="isi-icon">
                <img src="<?php echo base_url();?>assets/css/img/New/ic_matkul.png">
            </div>

            <div class="isi-data">
                <br>
                <span class="isi-data-number"><?php echo $this->model_data->jml_data('mata_kuliah');?> Data</span>
                <br>
                <br>
                <div class="isi-data-ket">Mata Kuliah</div>
            </div>
        </div>
        <div class="dosen">
            <div class="isi-icon">
                <img src="<?php echo base_url();?>assets/css/img/New/ic_dosen.png">
            </div>

            <div class="isi-data">
                <br>
                <span class="isi-data-number"><?php echo $this->model_data->jml_data('dosen');?> Data</span>
                <br>
                <br>
                <div class="isi-data-ket">Dosen</div>
            </div>
        </div>
        <div class="mahasiswa">
            <div class="isi-icon">
                <img src="<?php echo base_url();?>assets/css/img/New/ic_mahasiswa.png">
            </div>

            <div class="isi-data">
                <br>
                <span class="isi-data-number"><?php echo $this->model_data->jml_data('mahasiswa');?> Data</span>
                <br>
                <br>
                <div class="isi-data-ket">Mahasiswa</div>
            </div>
        </div>
        <div class="jadwal">
            <div class="isi-icon">
                <img src="<?php echo base_url();?>assets/css/img/New/ic_jadwal.png">
            </div>

            <div class="isi-data">
                <br>
                <span class="isi-data-number"><?php echo $this->model_data->jml_data('jadwal');?> Data</span>
                <br>
                <br>
                <div class="isi-data-ket">Jadwal</div>
            </div>
        </div>
        <div class="krs">
            <div class="isi-icon">
                <img src="<?php echo base_url();?>assets/css/img/New/ic_krs.png">
            </div>

            <div class="isi-data">
                <br>
                <span class="isi-data-number"><?php echo $this->model_data->jml_data('krs');?> Data</span>
                <br>
                <br>
                <div class="isi-data-ket">Kartu Rencana Studi</div>
            </div>
        </div>
        <div class="mutasi">
            <div class="isi-icon">
                <img src="<?php echo base_url();?>assets/css/img/New/ic_mutasi.png">
            </div>

            <div class="isi-data">
                <br>
                <span class="isi-data-number"><?php echo $this->model_data->jml_data('mutasi_mhs');?> Data</span>
                <br>
                <br>
                <div class="isi-data-ket">Mutasi Mahasiswa</div>
            </div>
        </div>
        <div class="prodi">
            <div class="isi-icon">
                <img src="<?php echo base_url();?>assets/css/img/New/ic_prodi.png">
            </div>

            <div class="isi-data">
                <br>
                <span class="isi-data-number"><?php echo $this->model_data->jml_data('prodi');?> Data</span>
                <br>
                <br>
                <div class="isi-data-ket">Program Studi</div>
            </div>
        </div>
    </div>

</div>
<br />
<div class="widget-box ">
  <div class="widget-header">
    <h4>Data Jumlah Mahasiswa</h4>
  </div>
  <div class="widget-body">
    <div class="widget-main">
    <?php
      $list_prodi = $this->model_data->data_jurusan()->result();
      foreach($list_prodi as $prodi){
        $jml_mhs = $this->model_data->jml_mhs($prodi->kd_prodi);
        echo "<p>".$prodi->prodi . " = " . $jml_mhs . "</p>"; 
      }
    ?>
    </div>
  </div>
</div>
<br />
<?php echo $this->load->view('setting');?>