<script type="text/javascript">
$(document).ready(function(){
	$("#judul").keyup(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});



	$("#simpan").click(function(){


		var string = $("#my-form").serialize();

    if(!$("#jenis").val()){
			alert('Maaf, Jenis Info Tidak boleh kosong');
			$("#jenis").focus();
			return false();
		}

		if(!$("#judul").val()){
			alert('Maaf, Judul Tidak boleh kosong');
			$("#judul").focus();
			return false();
		}

		if(!$("#info").val()){
			alert('Maaf, Info Tidak boleh kosong');
			$("#info").focus();
			return false();
		}



		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url(); ?>/info_kampus/simpan",
			data	: string,
			cache	: false,
			success	: function(data){
				alert(data);
				location.reload();
			}
		});

	});

	$("#tambah").click(function(){
		$('#id').val('');
		$('#jenis').val('');
		$('#judul').val('');
		$('#info').val('');
		$('#pengirim').val('');
	});
});

function editData(ID){
	var cari	= ID;
	$.ajax({
		type	: "POST",
		url		: "<?php echo site_url(); ?>/info_kampus/cari",
		data	: "cari="+cari,
		dataType: "json",
		success	: function(data){
			//alert(data.ref);
			$('#id').val(ID);
			$('#jenis').val(data.jenis);
			$('#judul').val(data.judul);
			$('#info').val(data.info);
			$('#pengirim').val(data.pengirim);
		}
	});

}

</script>
<div class="row-fluid">
<div class="table-header">
    <?php echo $judul;?>
    <div class="widget-toolbar no-border pull-right">
    <a href="#modal-table" class="btn btn-small btn-success"  role="button" data-toggle="modal" name="tambah" id="tambah" >
        <i class="icon-check"></i>
        Tambah Data
    </a>
    </div>
</div>

<table  class="table fpTable lcnp table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="center">No</th>
						<th class="center">Tanggal</th>
						<th class="center">Jenis</th>
						<th class="center">Judul</th>
            <th class="center">Pengirim</th>
            <th class="center">Aksi</th>
        </tr>
    </thead>
    <tbody>
    	<?php
		$i=1;
		foreach($data->result() as $dt){ ?>
        <tr>
        	<td class="center span1"><?php echo $i++?></td>
            <td class="center span2"><?php echo $dt->tgl;?></td>
            <td ><?php echo $dt->jenis;?></td>
            <td ><?php echo $dt->judul;?></td>
            <td class="center"><?php echo $dt->pengirim;?></td>

            <td class="td-actions"><center>
            	<div class="hidden-phone visible-desktop action-buttons">
                    <a class="green" href="#modal-table" onclick="javascript:editData('<?php echo $dt->id;?>')" data-toggle="modal">
                        <i class="icon-pencil bigger-130"></i>
                    </a>

                    <a class="red" href="<?php echo site_url();?>/info_kampus/hapus/<?php echo $dt->id;?>" onClick="return confirm('Anda yakin ingin menghapus data ini?')">
                        <i class="icon-trash bigger-130"></i>
                    </a>
                </div>

                <div class="hidden-desktop visible-phone">
                    <div class="inline position-relative">
                        <button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-caret-down icon-only bigger-120"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close">
                            <li>
                                <a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
                                    <span class="green">
                                        <i class="icon-edit bigger-120"></i>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
                                    <span class="red">
                                        <i class="icon-trash bigger-120"></i>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                </center>
            </td>
        </tr>
		<?php } ?>
    </tbody>
</table>
</div>

<div id="modal-table" class="modal hide fade" tabindex="-1">
    <div class="modal-header no-padding">
        <div class="table-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            Info Kampus
        </div>
    </div>

    <div class="modal-body no-padding">
        <div class="row-fluid">
            <form class="form-horizontal" name="my-form" id="my-form">
							<input type="hidden" name="id" id="id">

                <div class="control-group">
									<label class="control-label" for="form-field-1">Jenis Info</label>

									<div class="controls">
											<select name="jenis" id="jenis">
												<option value="">-Pilih-</option>
												<?php
												$list_jenis = $this->model_data->get_jenis_info();
												foreach($list_jenis as $row){
													?>
													<option value="<?= $row;?>"><?= $row;?></option>
													<?php
												}
												 ?>
											</select>
									</div>
							  </div>

                <div class="control-group">
                    <label class="control-label" for="form-field-2">Judul</label>

                    <div class="controls">
                        <input type="text" name="judul" id="judul"  class="span8"  />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="form-field-3">Info</label>

                    <div class="controls">
                        <textarea name="info" id="info" rows="10" cols="100" ></textarea>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="form-field-4">Pengirim</label>

                    <div class="controls">
                        <input type="text" name="pengirim" id="pengirim"   />
                    </div>
                </div>
			</form>
        </div>
    </div>

    <div class="modal-footer">
        <div class="pagination pull-right no-margin">
        <button type="button" class="btn btn-small btn-danger pull-left" data-dismiss="modal">
            <i class="icon-remove"></i>
            Close
        </button>
        <button type="button" name="simpan" id="simpan" class="btn btn-small btn-success pull-left">
            <i class="icon-save"></i>
            Simpan
        </button>
		</div>
    </div>
</div>
