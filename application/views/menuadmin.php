<?php
if($class=='home'){
	$home = 'class="active"';
	$master ='';
	$transaksi = '';
    $polling = '';
	$laporan = '';
	$grafik = '';
	$import = '';
	$keuangan = '';
}elseif($class=='master'){
	$home = '';
	$master ='class="active"';
	$transaksi = '';
    $polling = '';
	$laporan = '';
	$grafik = '';
	$import = '';
	$keuangan = '';
}elseif($class=='keuangan'){
	$home = '';
	$master ='';
	$transaksi = '';
    $polling = '';
	$laporan = '';
	$grafik = '';
	$import = '';
	$keuangan = 'class="active"';
}elseif($class=='transaksi'){
	$home = '';
	$master ='';
	$transaksi = 'class="active"';
    $polling = '';
	$laporan = '';
	$grafik = '';
	$import = '';
	$keuangan = '';
}elseif ($class == 'polling') {
    $home = '';
    $master = '';
    $transaksi = '';
    $polling = 'class="active"';
    $laporan = '';
    $grafik = '';
    $import = '';
    $keuangan = '';
    $lapkeuangan = '';
}elseif($class=='laporan'){
	$home = '';
	$master ='';
	$transaksi = '';
    $polling = '';
	$laporan = 'class="active"';
	$grafik = '';
	$import = '';
	$keuangan = '';
}elseif($class=='import'){
	$home = '';
	$master ='';
	$transaksi = '';
    $polling = '';
	$laporan = '';
	$grafik = '';
	$import = 'class="active"';
	$keuangan = '';
}else{
	$home = '';
	$master ='';
	$transaksi = '';
    $polling = '';
	$laporan = '';
	$grafik = 'class="active"';
	$import = '';
	$keuangan = '';
}
?>
<div class="main-container container-fluid">
<a class="menu-toggler" id="menu-toggler" href="#">
    <span class="menu-text"></span>
</a>
<div class="sidebar" id="sidebar">
    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <i class="icon-calendar"></i>
			<?php
			date_default_timezone_set('Asia/Jakarta');
			echo $this->model_global->hari_ini(date('w')).", ".$this->model_global->tgl_indo(date('Y-m-d'));
			?>
        </div>
        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>
            <span class="btn btn-info"></span>
            <span class="btn btn-warning"></span>
            <span class="btn btn-danger"></span>
        </div>
    </div><!--#sidebar-shortcuts-->

    <div align="center" class="padding-t-20">
    <img src="<?php echo base_url();?>assets/images/logo1.png" width="80">
    <h6><?php echo app_setting()['data_setting'][0]->nama_pendek.'<br/>'. app_setting()['data_setting'][0]->nama_instansi;?></h6>
    </div>

    <ul class="nav nav-list">
        <li <?php echo $home;?> >
            <a href="<?php echo base_url();?>index.php/home">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> Home </span>
            </a>
        </li>

        <li <?php echo $master;?> >
            <a href="#" class="dropdown-toggle">
                <i class="icon-desktop"></i>
                <span class="menu-text"> Master </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
							<li>
									<a href="<?php echo site_url('thakademik');?>">
											<i class="icon-double-angle-right"></i>
											Tahun Akademik
									</a>
							</li>
								<li>
										<a href="<?php echo site_url('ruangan');?>">
                        <i class="icon-double-angle-right"></i>
                        Ruangan
                    </a>
                </li>
                <!-- <li>
                    <a href="<?php echo site_url('pengguna');?>">
                        <i class="icon-double-angle-right"></i>
                        Pengguna
                    </a>
                </li>
                <li>
                    <a href="<?php echo site_url('jurusan');?>">
                        <i class="icon-double-angle-right"></i>
                        Program Studi
                    </a>
                </li> -->
                <li>
                    <a href="<?php echo site_url('mata_kuliah');?>">
                        <i class="icon-double-angle-right"></i>
                        Mata Kuliah
                    </a>
                </li>
				<li>
                    <a href="<?php echo site_url('dosen');?>">
                        <i class="icon-double-angle-right"></i>
                        Dosen
                    </a>
                </li>
				<li>
                    <a href="<?php echo site_url('mahasiswa');?>">
                        <i class="icon-double-angle-right"></i>
                        Mahasiswa
                    </a>
                </li>

                <li>
                    <a href="<?php echo site_url('maba'); ?>">
                        <i class="icon-double-angle-right"></i>
                        Mahasiswa Baru
                    </a>
                </li>

            </ul>
        </li>


        <li <?php echo $transaksi;?>>
			<a href="#" class="dropdown-toggle">
                <i class="icon-edit"></i>
                <span class="menu-text"> Transaksi </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
    				<li>
    					<a href="<?php echo site_url('info_kampus');?>">
    								<i class="icon-double-angle-right"></i>
    								Info Kampus
    						</a>
    				</li>

    				<li>
    					<a href="<?php echo site_url('materi_kuliah');?>">
    								<i class="icon-double-angle-right"></i>
    								Materi Kuliah
    						</a>
    				</li>

                    <li>
                        <a href="<?php echo site_url('materi_seminar'); ?>">
                            <i class="icon-double-angle-right"></i>
                            Materi Seminar
                        </a>
                    </li>

					<li>
						<a href="<?php echo site_url('jadwal');?>">
                            <i class="icon-double-angle-right"></i>
                            Jadwal Kuliah
                        </a>
                    </li>
                    <li>
										<a href="<?php echo site_url('krs');?>">
                        <i class="icon-double-angle-right"></i>
                        Kartu Rencana Studi (KRS)
                    </a>
                </li>
                <li>
										<a href="<?php echo site_url('nilai');?>">
                        <i class="icon-double-angle-right"></i>
                        Nilai
                    </a>
                </li>
                 <li>
									 	<a href="<?php echo site_url('mutasi_mhs');?>">
                        <i class="icon-double-angle-right"></i>
                        Mutasi Mahasiswa
                    </a>
                </li>
                 <li>
									 	<a href="<?php echo site_url('wisuda');?>">
                        <i class="icon-double-angle-right"></i>
                        Wisuda
                    </a>
                </li>
            </ul>
        </li>

        <li <?php echo $polling; ?>>
            <a href="#" class="dropdown-toggle">
                <i class="icon-check"></i>
                <span class="menu-text">
                    Kuesioner
                </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <li>
                    <a href="<?php echo site_url('poll_jenis'); ?>">
                        <i class="icon-double-angle-right"></i>
                        Jenis
                    </a>
                </li>
                <li>
                    <a href="<?php echo site_url('poll_pertanyaan'); ?>">
                        <i class="icon-double-angle-right"></i>
                        Pertanyaan
                    </a>
                </li>
                <li>
                    <a href="<?php echo site_url('poll_transaksi'); ?>">
                        <i class="icon-double-angle-right"></i>
                        Polling
                    </a>
                </li>
            </ul>
        </li>

        <li <?php echo $laporan;?>>
            <a href="#" class="dropdown-toggle">
                <i class="icon-print"></i>
                <span class="menu-text"> Laporan </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
            	 <li>
                    <a href="<?php echo base_url();?>index.php/lap_mahasiswa">
                        <i class="icon-double-angle-right"></i>
                        Mahasiswa
                    </a>
                </li>
                 <li>
                    <a href="<?php echo base_url();?>index.php/lap_dosen">
                        <i class="icon-double-angle-right"></i>
                        Dosen
                    </a>
                </li>
                 <li>
                    <a href="<?php echo base_url();?>index.php/lap_mata_kuliah">
                        <i class="icon-double-angle-right"></i>
                        Mata Kuliah
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>index.php/lap_krs">
                        <i class="icon-double-angle-right"></i>
                        KRS
                    </a>
                </li>
                 <li>
                    <a href="<?php echo base_url();?>index.php/lap_absen">
                        <i class="icon-double-angle-right"></i>
                        Absensi
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>index.php/lap_nilai">
                        <i class="icon-double-angle-right"></i>
                        Nilai
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>index.php/lap_khs">
                        <i class="icon-double-angle-right"></i>
                        KHS
                    </a>
                </li>
                <li>
                        <a href="<?php echo base_url();?>index.php/lap_khs_rekap">
                            <i class="icon-double-angle-right"></i>
                            Rekap KHS
                        </a>
                    </li>
                 <li>
                    <a href="<?php echo base_url();?>index.php/lap_mutasi_mhs">
                        <i class="icon-double-angle-right"></i>
                        Mutasi Mahasiswa
                    </a>
                </li>
                 <li>
                    <a href="<?php echo base_url();?>index.php/lap_wisuda">
                        <i class="icon-double-angle-right"></i>
                        Wisuda
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>index.php/lap_polling">
                        <i class="icon-double-angle-right"></i>
                        Kuisioner
                    </a>
                </li>
            </ul>
        </li>
				<li <?php echo $import;?>>
            <a href="#" class="dropdown-toggle">
                <i class="icon-download"></i>
                <span class="menu-text">
                    Import Data
                </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
							<li>
									<a href="<?php echo base_url();?>index.php/import/matakuliah">
											<i class="icon-double-angle-right"></i>
											Mata Kuliah
									</a>
							</li>
							<li>
									<a href="<?php echo base_url();?>index.php/import/dosen">
											<i class="icon-double-angle-right"></i>
											Dosen
									</a>
							</li>
								 <li>
                    <a href="<?php echo base_url();?>index.php/import/mahasiswa">
                        <i class="icon-double-angle-right"></i>
                        Mahasiswa
                    </a>
                </li>
            </ul>
        </li>

        <li <?php echo $grafik;?>>
            <a href="#" class="dropdown-toggle">
                <i class="icon-bar-chart"></i>
                <span class="menu-text">
                    Grafik
                </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
               	 <li>
                    <a href="<?php echo base_url();?>index.php/grafik/mhs">
                        <i class="icon-double-angle-right"></i>
                        Mahasiswa
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>index.php/grafik/dosen">
                        <i class="icon-double-angle-right"></i>
                        Dosen
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url();?>index.php/grafik/mhs_aktif">
                        <i class="icon-double-angle-right"></i>
                        Mahasiswa Aktif
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>index.php/grafik/krs">
                        <i class="icon-double-angle-right"></i>
                        KRS
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url();?>index.php/grafik/wisuda">
                        <i class="icon-double-angle-right"></i>
                        Wisuda
                    </a>
                </li>
            </ul>
        </li>
         <li>
            <a href="<?php echo base_url();?>index.php/login/logout">
                <i class="icon-off"></i>
                <span class="menu-text"> Keluar </span>
            </a>
        </li>
    </ul><!--/.nav-list-->

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left"></i>
    </div>
</div>
