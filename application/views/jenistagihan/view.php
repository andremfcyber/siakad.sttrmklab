<script type="text/javascript">
$(document).ready(function(){

  $(".chzn-select").chosen();

  $("#simpan").click(function(){
    var th_akademik_kode	= $("#th_akademik_kode").val();
    // var kode	= $("#kode").val();
    var smt	= $("#smt").val();
		var nama	= $("#nama").val();
    var jumlah	= $("#jumlah").val();
    var kd_prodi	= $("#kd_prodi").val();

		var string = $("#my-form").serialize();


    if(th_akademik_kode.length==0){
			alert('Maaf, Tahun Akademik boleh kosong');
			$("#th_akademik_kode").focus();
			return false();
		}
    if(kd_prodi.length==0){
			alert('Maaf, Program Studi tidak boleh kosong');
			$("#kd_prodi").focus();
			return false();
		}

    if(smt.length==0){
			alert('Maaf, Semester tidak boleh kosong');
			$("#smt").focus();
			return false();
		}

		// if(kode.length==0){
		// 	alert('Maaf, Kode Tidak boleh kosong');
		// 	$("#kode").focus();
		// 	return false();
		// }

		if(nama.length==0){
			alert('Maaf, Jenis Tagihan Tidak boleh kosong');
			$("#nama").focus();
			return false();
		}

    if(jumlah.length==0){
			alert('Maaf, Jumlah Tagihan Tidak boleh kosong');
			$("#jumlah").focus();
			return false();
		}

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url('jenis_tagihan/simpan'); ?>",
			data	: string,
			cache	: false,
			success	: function(data){
				alert(data);
				location.reload();
			}
		});

	});

  $("#tambah").click(function(){
		// $('#kode').val('');
		// $('#kode').attr("readonly",false);
		$('#nama').val('');
		$('#th_akademik_kode').val('');
    $('#smt').val('');
    // $('#th_akademik_kode').attr("readonly",false);
    $('#kd_prodi').val('');
		$('#jumlah').val('');

    $('#biaya_pembangunan').val('');
    $('#biaya_seragam').val('');
    $('#biaya_kesehatan').val('');
    $('#biaya_nytb').val('');
    $('#biaya_pendaftaran').val('');
    $('#biaya_topi').val('');

    $('#biaya_weekend').val('');
    $('#biaya_registrasi').val('');

    $('#biaya_pembimbingan').val('');
    $('#biaya_wisuda').val('');

    $('#spp').val('');
    $('#biaya_uang_makan').val('');
    $('#biaya_uang_saku').val('');

    $('#biaya_lab_com').val('');
    $('#biaya_lab_music').val('');
    $('#kartu_spp').val('');
    $('#biaya_ijazah').val('');
	});

});

function editData(ID){
	var cari	= ID;
  $("#id").val(ID);

	$.ajax({
		type	: "POST",
		url		: "<?php echo site_url('jenis_tagihan/cari'); ?>",
		data	: "cari="+cari,
		dataType: "json",
		success	: function(data){
			//alert(data.ref);
      $('#th_akademik_kode').val(data.th_akademik_kode);
      // $('#th_akademik_kode').attr("readonly","true");
      $('#kd_prodi').val(data.kd_prodi);
      $('#smt').val(data.smt);
      // $('#kode').val(data.kode);
			// $('#kode').attr("readonly","true");
			$('#nama').val(data.nama);
      $('#jumlah').val(data.jumlah);
      $('#x_sks').val(data.x_sks);


      $('#biaya_pembangunan').val(data.biaya_pembangunan);
      $('#biaya_seragam').val(data.biaya_seragam);
      $('#biaya_kesehatan').val(data.biaya_kesehatan);
      $('#biaya_nytb').val(data.biaya_nytb);
      $('#biaya_pendaftaran').val(data.biaya_pendaftaran);
      $('#biaya_topi').val(data.biaya_topi);

      $('#biaya_weekend').val(data.biaya_weekend);
      $('#biaya_registrasi').val(data.biaya_registrasi);

      $('#biaya_pembimbingan').val(data.biaya_pembimbingan);
      $('#biaya_wisuda').val(data.biaya_wisuda);

      $('#spp').val(data.spp);
      $('#biaya_uang_makan').val(data.biaya_uang_makan);
      $('#biaya_uang_saku').val(data.biaya_uang_saku);

      $('#biaya_lab_com').val(data.biaya_lab_com);
      $('#biaya_lab_music').val(data.biaya_lab_music);
      $('#kartu_spp').val(data.kartu_spp);
      $('#biaya_ijazah').val(data.biaya_ijazah);

		}
	});

}
</script>
<div class="row-fluid">
<div class="table-header <?php echo ($_SESSION['role'] == 'keuangan')?'keuangan':'';?>">
    <?php echo $judul;?>
    <div class="widget-toolbar no-border pull-right">
    <a href="#modal-table" class="btn btn-small btn-success"  role="button" data-toggle="modal" name="tambah" id="tambah" >
        <i class="icon-check"></i>
        Tambah Data
    </a>
    </div>
</div>

<table class="table table-bordered table-striped table-hover w-100" id="mytable">
   <thead>
     <tr>
         <th class="center">No</th>
         <th class="center">Th Akademik</th>
         <th class="center">PRODI</th>
         <th class="center">SMT</th>
         <th class="center">Jenis</th>
         <th class="center">Jumlah</th>
         <th class="center">X SKS</th>
         <th class="center">Aksi</th>
     </tr>
   </thead>
</table>

</div>

<div id="modal-table" class="modal hide fade" tabindex="-1">
    <div class="modal-header no-padding">
        <div class="table-header <?php echo ($_SESSION['role'] == 'keuangan')?'keuangan':'';?>">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            Jenis Tagihan
        </div>
    </div>

    <div class="modal-body no-padding">
        <div class="row-fluid">
            <form class="form-horizontal" name="my-form" id="my-form">
                <input type="hidden" name="id" id="id">

              <div class="control-group">
                  <label class="control-label" for="form-field-1">Th Akademik</label>
                  <div class="controls">
                      <select name="th_akademik_kode" id="th_akademik_kode" class="span4">
                        <option value="">-Pilih-</option>
                        <?php
                        foreach($list_th_akademik->result() as $row){
                          ?>
                          <option value="<?php echo $row->th_akademik;?>"><?php echo $row->th_akademik;?></option>
                          <?php
                        }
                        ?>
                      </select>
                  </div>
              </div>

              <div class="control-group">
                  <label class="control-label" for="form-field-1">Program Studi</label>
                  <div class="controls">
                      <select name="kd_prodi" id="kd_prodi" class="span6">
                        <option value="">-Pilih-</option>
                        <?php
                        $list_prodi = $this->db->get('prodi')->result();
                        foreach($list_prodi as $row){
                          ?>
                          <option value="<?php echo $row->kd_prodi;?>"><?php echo $row->prodi;?></option>
                          <?php
                        }
                        ?>
                      </select>
                  </div>
              </div>

              <div class="control-group">
                  <label class="control-label" for="form-field-1">Semester</label>
                  <div class="controls">
                      <select name="smt" id="smt" class="span2">
                        <option value="">-Pilih-</option>
                        <?php
                          for($i=1;$i<=14;$i++){
                            echo "<option value='$i'>$i</option>";
                          }
                        ?>
                      </select>
                  </div>
              </div>


                <div class="control-group">
                    <label class="control-label" for="form-field-1">Jenis</label>
                    <div class="controls">
                      <!--input type="text" name="nama" id="nama"  class="span8"/ -->
								<select name="nama" id="nama" data-placeholder="Cari Jenis ....">
			                  <option value="">-Pilih-</option>
			                  <?php
			                  $list_jenis_tagihan = $this->db->get('master_tagihan')->result();
			                  foreach($list_jenis_tagihan as $rows){
			                    ?>
			                    <option value="<?php echo $rows->nama_tagihan;?>"><?php echo $rows->nama_tagihan;?></option>
			                    <?php
			                  }
			                   ?>
		                </select>

                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="form-field-1">Jumlah</label>
                    <div class="controls">
                      <input type="number" name="jumlah" id="jumlah"  class="span6"/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="form-field-1">X SKS</label>
                    <div class="controls">
                      <select name="x_sks" id="x_sks" class="span3">
                        <option value="tidak">-</option>
                        <option value="ya">YA</option>
                        <option value="tidak">TIDAK</option>
                      </select>
                    </div>
                </div>

                <!--<div class="alert alert-info">-->
                <!--  <strong>Uang Masuk Pendidikan</strong>-->
                <!--</div>-->

                <!--<div class="control-group">-->
                <!--    <label class="control-label" for="form-field-1">Pembangunan</label>-->
                <!--    <div class="controls">-->
                <!--      <input type="number" name="biaya_pembangunan" id="biaya_pembangunan"  class="span6"/>-->
                <!--    </div>-->
                <!--</div>-->

                <!--<div class="control-group">-->
                <!--    <label class="control-label" for="form-field-1">Seragam Per T.A</label>-->
                <!--    <div class="controls">-->
                <!--      <input type="number" name="biaya_seragam" id="biaya_seragam"  class="span6"/>-->
                <!--    </div>-->
                <!--</div>-->

                <!--<div class="control-group">-->
                <!--    <label class="control-label" for="form-field-1">Kesehatan</label>-->
                <!--    <div class="controls">-->
                <!--      <input type="number" name="biaya_kesehatan" id="biaya_kesehatan"  class="span6"/>-->
                <!--    </div>-->
                <!--</div>-->

                <!--<div class="control-group">-->
                <!--    <label class="control-label" for="form-field-1">NYTB</label>-->
                <!--    <div class="controls">-->
                <!--      <input type="number" name="biaya_nytb" id="biaya_nytb"  class="span6"/>-->
                <!--    </div>-->
                <!--</div>-->

                <!--<div class="control-group">-->
                <!--    <label class="control-label" for="form-field-1">Pendaftaran</label>-->
                <!--    <div class="controls">-->
                <!--      <input type="number" name="biaya_pendaftaran" id="biaya_pendaftaran"  class="span6"/>-->
                <!--    </div>-->
                <!--</div>-->

                <!--<div class="control-group">-->
                <!--    <label class="control-label" for="form-field-1">Topi</label>-->
                <!--    <div class="controls">-->
                <!--      <input type="number" name="biaya_topi" id="biaya_topi"  class="span6"/>-->
                <!--    </div>-->
                <!--</div>-->


                <!--<div class="alert alert-info">-->
                <!--  <strong>Registrasi</strong>-->
                <!--</div>-->

                <!--<div class="control-group">-->
                <!--    <label class="control-label" for="form-field-1">Week End/Semester</label>-->
                <!--    <div class="controls">-->
                <!--      <input type="number" name="biaya_weekend" id="biaya_weekend"  class="span6"/>-->
                <!--    </div>-->
                <!--</div>-->

                <!--<div class="control-group">-->
                <!--    <label class="control-label" for="form-field-1">Registrasi/Semester</label>-->
                <!--    <div class="controls">-->
                <!--      <input type="number" name="biaya_registrasi" id="biaya_registrasi"  class="span6"/>-->
                <!--    </div>-->
                <!--</div>-->


                <!--<div class="alert alert-info">-->
                <!--  <strong>Bimbingan Dan Wisuda</strong>-->
                <!--</div>-->

                <!--<div class="control-group">-->
                <!--    <label class="control-label" for="form-field-1">Biaya Pembimbingan</label>-->
                <!--    <div class="controls">-->
                <!--      <input type="number" name="biaya_pembimbingan" id="biaya_pembimbingan"  class="span6"/>-->
                <!--    </div>-->
                <!--</div>-->

                <!--<div class="control-group">-->
                <!--    <label class="control-label" for="form-field-1">Biaya Wisuda</label>-->
                <!--    <div class="controls">-->
                <!--      <input type="number" name="biaya_wisuda" id="biaya_wisuda"  class="span6"/>-->
                <!--    </div>-->
                <!--</div>-->


                <!--<div class="alert alert-info">-->
                <!--  <strong>Uang Makan & SPP, Uang Saku</strong>-->
                <!--</div>-->

                <!--<div class="control-group">-->
                <!--    <label class="control-label" for="form-field-1">SPP/Bulan</label>-->
                <!--    <div class="controls">-->
                <!--      <input type="number" name="spp" id="spp"  class="span6"/>-->
                <!--    </div>-->
                <!--</div>-->

                <!--<div class="control-group">-->
                <!--    <label class="control-label" for="form-field-1">Uang Makan/Bulan</label>-->
                <!--    <div class="controls">-->
                <!--      <input type="number" name="biaya_uang_makan" id="biaya_uang_makan"  class="span6"/>-->
                <!--    </div>-->
                <!--</div>-->

                <!--<div class="control-group">-->
                <!--    <label class="control-label" for="form-field-1">Uang Saku/Bulan</label>-->
                <!--    <div class="controls">-->
                <!--      <input type="number" name="biaya_uang_saku" id="biaya_uang_saku"  class="span6"/>-->
                <!--    </div>-->
                <!--</div>-->


                <!--<div class="alert alert-info">-->
                <!--  <strong>Lab Com, Lab Music</strong>-->
                <!--</div>-->

                <!--<div class="control-group">-->
                <!--    <label class="control-label" for="form-field-1">Lab Com/Bulan</label>-->
                <!--    <div class="controls">-->
                <!--      <input type="number" name="biaya_lab_com" id="biaya_lab_com"  class="span6"/>-->
                <!--    </div>-->
                <!--</div>-->

                <!--<div class="control-group">-->
                <!--    <label class="control-label" for="form-field-1">Lab Music/Bulan</label>-->
                <!--    <div class="controls">-->
                <!--      <input type="number" name="biaya_lab_music" id="biaya_lab_music"  class="span6"/>-->
                <!--    </div>-->
                <!--</div>-->

                <!--<div class="alert alert-info">-->
                <!--  <strong>Kartu SPP</strong>-->
                <!--</div>-->

                <!--<div class="control-group">-->
                <!--    <label class="control-label" for="form-field-1">Kartu SPP</label>-->
                <!--    <div class="controls">-->
                <!--      <input type="number" name="kartu_spp" id="kartu_spp"  class="span6"/>-->
                <!--    </div>-->
                <!--</div>-->

                <!--<div class="alert alert-info">-->
                <!--  <strong>Ijazah</strong>-->
                <!--</div>-->

                <!--<div class="control-group">-->
                <!--    <label class="control-label" for="form-field-1">Ijazah</label>-->
                <!--    <div class="controls">-->
                <!--      <input type="number" name="biaya_ijazah" id="biaya_ijazah"  class="span6"/>-->
                <!--    </div>-->
                <!--</div>-->

			      </form>
        </div>
    </div>

    <div class="modal-footer">
        <div class="pagination pull-right no-margin">
        <button type="button" class="btn btn-small btn-danger pull-left" data-dismiss="modal">
            <i class="icon-remove"></i>
            Close
        </button>
        <button type="button" name="simpan" id="simpan" class="btn btn-small btn-success pull-left">
            <i class="icon-save"></i>
            Simpan
        </button>
		</div>
    </div>
</div>

<script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.js') ?>"></script>
<script type="text/javascript">
$(document).ready(function() {
      $('#mytable').dataTable({
          "ajax": "<?php echo site_url('jenis_tagihan/get_json'); ?>",
          "pageLength": 20,
          "order": [[ 0, "asc" ]],
          "aoColumnDefs": [{"bSortable": false,
                           "aTargets": [ -1 , 0]}],
          "dom": 'T<"clear">lfrtip',
      });
});
</script>
