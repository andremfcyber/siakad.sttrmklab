<script type="text/javascript">
$(document).ready(function(){
  $("#simpan").click(function(){
    var th_akademik_kode	= $("#th_akademik_kode").val();
    // var kode	= $("#kode").val();
    var smt	= $("#smt").val();
		var nama	= $("#nama").val();
    var jumlah	= $("#jumlah").val();
    var kd_prodi	= $("#kd_prodi").val();

		var string = $("#my-form").serialize();


    if(th_akademik_kode.length==0){
			alert('Maaf, Tahun Akademik boleh kosong');
			$("#th_akademik_kode").focus();
			return false();
		}
    if(kd_prodi.length==0){
			alert('Maaf, Program Studi tidak boleh kosong');
			$("#kd_prodi").focus();
			return false();
		}

    if(smt.length==0){
			alert('Maaf, Semester tidak boleh kosong');
			$("#smt").focus();
			return false();
		}

		// if(kode.length==0){
		// 	alert('Maaf, Kode Tidak boleh kosong');
		// 	$("#kode").focus();
		// 	return false();
		// }

		if(nama.length==0){
			alert('Maaf, Nama Tagihan Tidak boleh kosong');
			$("#nama").focus();
			return false();
		}

    if(jumlah.length==0){
			alert('Maaf, Jumlah Tagihan Tidak boleh kosong');
			$("#jumlah").focus();
			return false();
		}

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url('jenis_tagihan/simpan'); ?>",
			data	: string,
			cache	: false,
			success	: function(data){
				alert(data);
				location.reload();
			}
		});

	});

  $("#tambah").click(function(){
		// $('#kode').val('');
		// $('#kode').attr("readonly",false);
		$('#nama').val('');
		$('#th_akademik_kode').val('');
    $('#smt').val('');
    // $('#th_akademik_kode').attr("readonly",false);
    $('#kd_prodi').val('');
		$('#jumlah').val('');
	});

});

function editData(ID){
	var cari	= ID;
  $("#id").val(ID);

	$.ajax({
		type	: "POST",
		url		: "<?php echo site_url('jenis_tagihan/cari'); ?>",
		data	: "cari="+cari,
		dataType: "json",
		success	: function(data){
			//alert(data.ref);
      $('#th_akademik_kode').val(data.th_akademik_kode);
      // $('#th_akademik_kode').attr("readonly","true");
      $('#kd_prodi').val(data.kd_prodi);
      $('#smt').val(data.smt);
      // $('#kode').val(data.kode);
			// $('#kode').attr("readonly","true");
			$('#nama').val(data.nama);
      $('#jumlah').val(data.jumlah);
      $('#x_sks').val(data.x_sks);

		}
	});

}
</script>
<div class="row-fluid">
<div class="table-header">
    <?php echo $judul;?>
    <div class="widget-toolbar no-border pull-right">
    <a href="#modal-table" class="btn btn-small btn-success"  role="button" data-toggle="modal" name="tambah" id="tambah" >
        <i class="icon-check"></i>
        Tambah Data
    </a>
    </div>
</div>

<table  class="table fTable lcnp table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="center">No</th>
            <th class="center">Th Akademik</th>
            <th class="center">PRODI</th>
            <th class="center">SMT</th>
            <th class="center">Jenis</th>
            <th class="center">Jumlah</th>
            <th class="center">X SKS</th>
            <th class="center">Aksi</th>
        </tr>
    </thead>
    <tbody>
    	<?php
      // print_r($data->results());die;
		$i=1;
		foreach($data as $dt){
      $prodi = $this->model_data->nama_jurusan($dt->kd_prodi);
      ?>
        <tr>
        	<td class="center span1"><?php echo $i++?></td>
            <td class="center"><?php echo $dt->th_akademik;?></td>
            <td><?php echo $prodi;?></td>
            <td class="center"><?php echo $dt->smt;?></td>
            <td ><?php echo $dt->nama;?></td>
            <td style="text-align:right;"><?php echo number_format($dt->jumlah);?></td>
            <td class="center"><?php echo $dt->x_sks;?></td>
            <td class="td-actions"><center>
            	<div class="hidden-phone visible-desktop action-buttons">
                    <a class="green" href="#modal-table" onclick="javascript:editData('<?php echo $dt->id;?>')" data-toggle="modal">
                        <i class="icon-pencil bigger-130"></i>
                    </a>

                    <a class="red" href="<?php echo site_url();?>/jenis_tagihan/hapus/<?php echo $dt->id;?>" onClick="return confirm('Anda yakin ingin menghapus data ini?')">
                        <i class="icon-trash bigger-130"></i>
                    </a>
                </div>

                <div class="hidden-desktop visible-phone">
                    <div class="inline position-relative">
                        <button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-caret-down icon-only bigger-120"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close">
                            <li>
                                <a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
                                    <span class="green">
                                        <i class="icon-edit bigger-120"></i>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
                                    <span class="red">
                                        <i class="icon-trash bigger-120"></i>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                </center>
            </td>
        </tr>
		<?php } ?>
    </tbody>
</table>
</div>
<div class="pagination center">
<?php echo $links;?>
</div>

<div id="modal-table" class="modal hide fade" tabindex="-1">
    <div class="modal-header no-padding">
        <div class="table-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            Jenis Tagihan
        </div>
    </div>

    <div class="modal-body no-padding">
        <div class="row-fluid">
            <form class="form-horizontal" name="my-form" id="my-form">
                <input type="hidden" name="id" id="id">

              <div class="control-group">
                  <label class="control-label" for="form-field-1">Th Akademik</label>
                  <div class="controls">
                      <select name="th_akademik_kode" id="th_akademik_kode" class="span4">
                        <option value="">-Pilih-</option>
                        <?php
                        foreach($list_th_akademik->result() as $row){
                          ?>
                          <option value="<?php echo $row->th_akademik;?>"><?php echo $row->th_akademik;?></option>
                          <?php
                        }
                        ?>
                      </select>
                  </div>
              </div>

              <div class="control-group">
                  <label class="control-label" for="form-field-1">Program Studi</label>
                  <div class="controls">
                      <select name="kd_prodi" id="kd_prodi" class="span6">
                        <option value="">-Pilih-</option>
                        <?php
                        $list_prodi = $this->db->get('prodi')->result();
                        foreach($list_prodi as $row){
                          ?>
                          <option value="<?php echo $row->kd_prodi;?>"><?php echo $row->prodi;?></option>
                          <?php
                        }
                        ?>
                      </select>
                  </div>
              </div>

              <div class="control-group">
                  <label class="control-label" for="form-field-1">Semester</label>
                  <div class="controls">
                      <select name="smt" id="smt" class="span2">
                        <option value="">-Pilih-</option>
                        <?php
                          for($i=1;$i<=14;$i++){
                            echo "<option value='$i'>$i</option>";
                          }
                        ?>
                      </select>
                  </div>
              </div>


                <div class="control-group">
                    <label class="control-label" for="form-field-1">Jenis</label>
                    <div class="controls">
                      <input type="text" name="nama" id="nama"  class="span8"/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="form-field-1">Jumlah</label>
                    <div class="controls">
                      <input type="number" name="jumlah" id="jumlah"  class="span6"/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="form-field-1">X SKS</label>
                    <div class="controls">
                      <select name="x_sks" id="x_sks" class="span3">
                        <option value="tidak">-</option>
                        <option value="ya">YA</option>
                        <option value="tidak">TIDAK</option>
                      </select>
                    </div>
                </div>

			      </form>
        </div>
    </div>

    <div class="modal-footer">
        <div class="pagination pull-right no-margin">
        <button type="button" class="btn btn-small btn-danger pull-left" data-dismiss="modal">
            <i class="icon-remove"></i>
            Close
        </button>
        <button type="button" name="simpan" id="simpan" class="btn btn-small btn-success pull-left">
            <i class="icon-save"></i>
            Simpan
        </button>
		</div>
    </div>
</div>
