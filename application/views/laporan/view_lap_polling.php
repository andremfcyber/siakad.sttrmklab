<!-- <pre>
<?php //print_r($data->result());?>
</pre> -->

<table  class="table fpTable lcnp table-striped table-bordered table-hover dt-responsive">
    <thead>
        <tr>
            <th class="center">No</th>
            <th class="center span2">NIM</th>
            <th class="center">Nama Mahasiswa</th>
            <th class="center">L/P</th>
            <th class="center">PRODI</th>
            <th class="center">Tanggal</th>
            <th class="center">Penilaian</th>
        </tr>
    </thead>
    <tbody>
    	<?php
		$i=1;
		foreach($data->result() as $dt){
      $infoMhs = $this->model_data->getInfoMhs($dt->nim);
      $nama_mhs = $infoMhs['nama_mhs'];
      $sex = $infoMhs['sex'];
      $prodi = $infoMhs['kd_prodi'];
  		$tgl = $dt->insert_date; //'';// $this->model_global->tgl_indo($dt->tgl_daftar);
  		?>
          <tr>
          	<td class="center span1"><?php echo $i++?></td>
              <td class="center span2"><?php echo $dt->nim;?></td>
              <td ><?php echo $nama_mhs;?></td>
              <td class="center"><?php echo $sex;?></td>
              <td><?php echo $prodi;?></td>
              <td class="center"><?php echo $tgl;?></td>
              <td class="center"><?php echo $dt->jawaban;?></td>
          </tr>
		<?php } ?>
    </tbody>
</table>
