<script type="text/javascript">
$(document).ready(function(){

	$("#view").click(function(){
		cari_data();
	});

	function cari_data(){
		var string = $("#my-form").serialize();

		if(!$("#th_ak").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Th Akademik tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#th_ak").focus();
			return false();
		}

		// if(!$("#kd_prodi").val()){
		// 	$.gritter.add({
		// 		title: 'Peringatan..!!',
		// 		text: 'PRODI tidak boleh kosong',
		// 		class_name: 'gritter-error'
		// 	});
		// 	$("#kd_prodi").focus();
		// 	return false();
		// }

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url(); ?>/lap_polling/cari_data",
			data	: string,
			cache	: false,
			success	: function(data){
				$("#view_detail").html(data);
			    setTimeout(function(){ reinitDatatable(); },50);

            }
        });
    }

    function reinitDatatable(){

        $(".fpTable").dataTable().fnDestroy();
        $(".fpTable").dataTable({
                                bSort: true,
                                bAutoWidth: true,
                                // scrollY:        300,
                                // scrollX:        true,
                                // scrollCollapse: true,
                                // paging:         false,
                                // fixedColumns:   true,
                                "iDisplayLength": 20, "aLengthMenu": [20,40,80,120], // can be removed for basic 10 items per page
                                "sPaginationType": "full_numbers",
                                "aoColumnDefs": [{"bSortable": false,
                                                 "aTargets": [ -1 , 0]}]});

    }

	$("#poll_jenis_id").change(function(){
		cari_pertanyaan();
		cari_object();
	});

	function cari_pertanyaan()
	{
		var string = {
			th_ak : $("#th_ak").val(),
			poll_jenis_id : $("#poll_jenis_id").val()
		};

		if(!$("#th_ak").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Th Akademik tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#th_ak").focus();
			return false();
		}

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url('lap_polling/cari_pertanyaan'); ?>",
			data	: string,
			cache	: false,
			success	: function(data){
				// console.log(data);
				$("#poll_pertanyaan_id").html(data);
			}
		});
	}

	function cari_object()
	{
		var string = {
			th_ak : $("#th_ak").val(),
			poll_jenis_id : $("#poll_jenis_id").val()
		};

		if(!$("#th_ak").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Th Akademik tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#th_ak").focus();
			return false();
		}

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url('lap_polling/cari_object'); ?>",
			data	: string,
			cache	: false,
			success	: function(data){
				// console.log(data);
				$("#object").html(data);
			}
		});
	}

	$("#cetak_excel").click(function(){
		var string = $("#my-form").serialize();

		if(!$("#th_ak").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Th Akademik tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#th_ak").focus();
			return false();
		}

		// if(!$("#kd_prodi").val()){
		// 	$.gritter.add({
		// 		title: 'Peringatan..!!',
		// 		text: 'PRODI tidak boleh kosong',
		// 		class_name: 'gritter-error'
		// 	});
		// 	$("#kd_prodi").focus();
		// 	return false();
		// }

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url(); ?>/lap_polling/cetak_pdf",
			data	: string,
			cache	: false,
			success	: function(data){
				if(data=='Sukses'){
					window.location.assign("<?php echo site_url();?>/lap_polling/print_excel");
				}else{
					$.gritter.add({
						title: 'Peringatan..!!',
						text: data,
						class_name: 'gritter-error'
					});
				}
			}
		});
	});


});
</script>

<div class="widget-box ">
    <div class="widget-header">
        <h4 class="lighter smaller">
            <i class="icon-book blue"></i>
            <?php echo $judul;?>
        </h4>
    </div>

    <div class="widget-body">
    	<div class="widget-main no-padding">

            <form class="form-horizontal" name="my-form" id="my-form">
							<fieldset>
                    <div class="control-group">
                        <label class="control-label" for="form-field-1">Tahun Akademik</label>
                        <div class="controls">
                            <select name="th_ak" id="th_ak" class="span2">
                            	<option value="" selected="selected">-Pilih-</option>
                                <?php
																	$this->db->select('th_akademik');
																	$this->db->from('th_akademik');
																	$this->db->group_by('th_akademik');
																	$this->db->order_by('th_akademik','DESC');
																	$data = $this->db->get();
																	foreach($data->result() as $dt){
																	?>
                                	<option value="<?php echo $dt->th_akademik;?>"><?php echo $dt->th_akademik;?></option>
                                <?php } ?>
                                </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="form-field-1">Program Studi</label>
                        <div class="controls">
                            <select name="kd_prodi" id="kd_prodi" class="span4">
                            	<option value="" selected="selected">-Pilih-</option>
                                <?php
								$data = $this->model_data->data_jurusan();
								foreach($data->result() as $dt){
								?>
                                <option value="<?php echo $dt->kd_prodi;?>"><?php echo $dt->prodi;?></option>
								<?php } ?>
                             </select>
                        </div>
                    </div>

										<div class="control-group">
                        <label class="control-label" for="form-field-1">Jenis</label>
                        <div class="controls">
                            <select name="poll_jenis_id" id="poll_jenis_id" class="span2">
                            	<option value="" selected="selected">-Pilih-</option>
                                <?php
								$data = $this->db->get('poll_jenis');
								foreach($data->result() as $dt){
								?>
                                <option value="<?php echo $dt->id;?>"><?php echo $dt->nama;?></option>
								<?php } ?>
                             </select>
                        </div>
                    </div>

										<div class="control-group">
                        <label class="control-label" for="form-field-1">Pertanyaan</label>
                        <div class="controls">
                            <select name="poll_pertanyaan_id" id="poll_pertanyaan_id" class="span7">
                            	<option value="" selected="selected">-Pilih-</option>
                             </select>
                        </div>
                    </div>

										<div class="control-group">
                        <label class="control-label" for="form-field-1">Dosen / Karyawan</label>
                        <div class="controls">
                            <select name="object" id="object" class="span5">
                            	<option value="" selected="selected">-Pilih-</option>
                             </select>
                        </div>
                    </div>

									</fieldset>

            	<div class="form-actions center">
                     <button type="button" name="view" id="view" class="btn btn-mini btn-info">
                     <i class="icon-th"></i> Lihat Data
                     </button>
                     <button type="button" name="cetak_excel" id="cetak_excel" class="btn btn-mini btn-success">
                     <i class="icon-print"></i> Cetak EXCEL
                     </button>
           			</div>
           </form>
        </div> <!-- wg body -->
    </div> <!--wg-main-->
</div>
<div id="view_detail"></div>
