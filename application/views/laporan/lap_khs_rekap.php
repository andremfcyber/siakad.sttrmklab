<script type="text/javascript">
$(document).ready(function(){

	$("#view").click(function(){
		cari_data();
	});

	function cari_data(){
		var string = $("#my-form").serialize();

		if(!$("#th").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Tahun Angkatan tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#th").focus();
			return false();
		}

		if(!$("#kd_prodi").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'PRODI tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#kd_prodi").focus();
			return false();
		}

		// if(!$("#kelas").val()){
		// 	$.gritter.add({
		// 		title: 'Peringatan..!!',
		// 		text: 'Kelas tidak boleh kosong',
		// 		class_name: 'gritter-error'
		// 	});
		// 	$("#kelas").focus();
		// 	return false();
		// }



		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url('lap_khs_rekap/cari_data'); ?>",
			data	: string,
			cache	: false,
			success	: function(data){
				$("#view_detail").html(data);
			    setTimeout(function(){ reinitDatatable(); },50);

            }
        });
    }

    function reinitDatatable(){

        $(".fpTable").dataTable().fnDestroy();
        $(".fpTable").dataTable({
                                bSort: true,
                                bAutoWidth: true,
                                // scrollY:        300,
                                // scrollX:        true,
                                // scrollCollapse: true,
                                // paging:         false,
                                // fixedColumns:   true,
                                "iDisplayLength": 20, "aLengthMenu": [20,40,80,120], // can be removed for basic 10 items per page
                                "sPaginationType": "full_numbers",
                                "aoColumnDefs": [{"bSortable": false,
                                                 "aTargets": [ -1 , 0]}]});

    }

    $("#cetak").click(function(){

		var string = $("#my-form").serialize();

		//alert(string);

		if(!$("#th").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Tahun Angkatan tidak boleh kosong',
				class_name: 'gritter-error'
			});

			$("#th").focus();
			return false();
		}
		if(!$("#kd_prodi").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'PRODI tidak boleh kosong',
				class_name: 'gritter-error'
			});

			$("#kd_prodi").focus();
			return false();
		}

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url(); ?>/lap_khs_rekap/cetak_khs",
			data	: string,
			cache	: false,
			success	: function(data){
				if(data=='Sukses'){
					window.location.assign("<?php echo site_url();?>/lap_khs_rekap/print_khs");
				}else{
					$.gritter.add({
						title: 'Peringatan..!!',
						text: data,
						class_name: 'gritter-error'
					});
				}
			}
		});
	});

});
</script>

<div class="widget-box ">
    <div class="widget-header">
        <h4 class="lighter smaller">
            <i class="icon-book blue"></i>
            <?php echo $judul;?>
        </h4>
    </div>

    <div class="widget-body">
    	<div class="widget-main no-padding">

            <form class="form-horizontal" name="my-form" id="my-form" >
							<fieldset>
                    <div class="control-group">
                        <label class="control-label" for="form-field-1">Tahun Angkatan</label>
                        <div class="controls">
                            <select name="th" id="th" class="span2">
                            	<option value="" selected="selected">-Pilih-</option>
                                <?php
								$data = $this->db->select('th_akademik')->group_by('th_akademik')->order_by('th_akademik','DESC')->get('mahasiswa'); // $this->model_data->th_akademik_jadwal();
								foreach($data->result() as $dt){
									$th_akademik_raw = $dt->th_akademik;
									$th_akademik = substr($th_akademik_raw,0,4);
								?>


                                	<option value="<?php echo $th_akademik;?>"><?php echo $th_akademik;?></option>
                                <?php } ?>
                                </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="form-field-1">Program Studi</label>
                        <div class="controls">
                            <?php if($_SESSION['adm_kd_prodi']==null || $_SESSION['adm_kd_prodi']==0){?>
	                            <select name="kd_prodi" id="kd_prodi" class="span4">
	                            	<option value="" selected="selected">-Pilih-</option>
	                                <?php
									$data = $this->model_data->data_jurusan();
									foreach($data->result() as $dt){
									?>
	                                <option value="<?php echo $dt->kd_prodi;?>"><?php echo $dt->prodi;?></option>
									<?php } ?>
	                            </select>
	                        <?php 
                            }else{
                                $data = $this->model_data->data_jurusan($_SESSION['adm_kd_prodi']);
                            ?>
                                <input type="text" value="<?php echo $data->row()->prodi;?>" readonly="readonly">
                                <input type="hidden" name="kd_prodi" id="kd_prodi" value="<?php echo $data->row()->kd_prodi;?>">
                            <?php }?>
                        </div>
                    </div>

										<!-- <div class="control-group">
                        <label class="control-label" for="form-field-1">Kelas</label>
                        <div class="controls">
                            <select name="kelas" id="kelas" class="span2">
                            	<option value="" selected="selected">-Pilih-</option>
                                <?php
								$data = $this->db->select('kelas')->group_by('kelas')->order_by('kelas','ASC')->get('mahasiswa'); // $this->model_data->th_akademik_jadwal();
								foreach($data->result() as $dt){
								?>
                                	<option value="<?php echo $dt->kelas;?>"><?php echo $dt->kelas;?></option>
                                <?php } ?>
                                </select>
                        </div>
                    </div> -->

							</fieldset>

            <div class="form-actions center">
                     <button type="button" name="view" id="view" class="btn btn-mini btn-info">
                     <i class="icon-th"></i> Lihat Data
                     </button>
                     <button type="button" name="cetak" id="cetak" class="btn btn-mini btn-info">
                     <i class="icon-download"></i> Download Rekapan KHS
                    </button>
           </div>

           </form>
        </div> <!-- wg body -->
    </div> <!--wg-main-->
</div>
<div id="view_detail"></div>
