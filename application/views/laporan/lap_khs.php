<script type="text/javascript">
$(document).ready(function(){

	$("#view").click(function(){
		cari_data();
	});

	function cari_data(){
		var string = $("#my-form").serialize();

		if(!$("#nim").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'NIM tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#nim").focus();
			return false();
		}

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url(); ?>/lap_khs/cari_data",
			data	: string,
			cache	: false,
			success	: function(data){
				$("#view_detail").html(data);
				//setTimeout(function(){ reinitDatatable(); },50);

            }
        });
    }

    function reinitDatatable(){

        $(".fpTable").dataTable().fnDestroy();
        $(".fpTable").dataTable({
                                bSort: true,
                                bAutoWidth: true,
                                // scrollY:        300,
                                // scrollX:        true,
                                // scrollCollapse: true,
                                // paging:         false,
                                // fixedColumns:   true,
                                "iDisplayLength": 20, "aLengthMenu": [20,40,80,120], // can be removed for basic 10 items per page
                                "sPaginationType": "full_numbers",
                                "aoColumnDefs": [{"bSortable": false,
                                                 "aTargets": [ -1 , 0]}]
                                             });

    }

	$("#cetak").click(function(){

		var string = $("#my-form").serialize();

		//alert(string);

		if(!$("#nim").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Semester tidak boleh kosong',
				class_name: 'gritter-error'
			});

			$("#nim").focus();
			return false();
		}

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url(); ?>/lap_khs/cetak_khs",
			data	: string,
			cache	: false,
			success	: function(data){
				if(data=='Sukses'){
					window.location.assign("<?php echo site_url();?>/lap_khs/print_khs");
				}else{
					$.gritter.add({
						title: 'Peringatan..!!',
						text: data,
						class_name: 'gritter-error'
					});
				}
			}
		});
	});

});
</script>

<div class="widget-box ">
    <div class="widget-header">
        <h4 class="lighter smaller">
            <i class="icon-desktop blue"></i>
            <?php echo $judul;?>
        </h4>
    </div>

    <div class="widget-body">
    	<div class="widget-main no-padding">
<!-- <?php print_r(@$_SESSION);?> -->
            <form class="form-horizontal" name="my-form" id="my-form">
				<fieldset>
                    <div class="control-group">
                        <label class="control-label" for="form-field-1">Nomor Induk Mahasiswa</label>
                        <div class="controls">
                            <input type="text" name="nim" id="nim" class="span2">
                        </div>
                    </div>
				</fieldset>

            	<div class="form-actions center">
                    <button type="button" name="view" id="view" class="btn btn-mini btn-info">
                     <i class="icon-th"></i> Lihat Data
                    </button>
                    <button type="button" name="cetak" id="cetak" class="btn btn-mini btn-info">
                     <i class="icon-download"></i> Download KHS
                    </button>
				</div>

           </form>
           
           <?php
		  	echo  $this->session->flashdata('result_info');
		   ?>
        </div> <!-- wg body -->
    </div> <!--wg-main-->
</div>
<div id="view_detail" class="w-100"></div>
