<script type="text/javascript">
$(document).ready(function(){

	$("#view").click(function(){
		cari_data();
	});

	function cari_data(){
		var string = {};
		string.th_ak = $("#th_ak").val();
		string.kd_prodi = $("#kd_prodi").val();
		string.kelas = $("#kelas").val();
		string.status = $("#status").val();

		if(!$("#th_ak").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Th Akademik tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#th_ak").focus();
			return false();
		}


		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url('lap_mahasiswa/cari_data'); ?>",
			data	: string,
			cache	: false,
			success	: function(data){
				$("#view_detail").html(data);
			setTimeout(function(){ reinitDatatable(); },50);

            }
        });
    }

    function reinitDatatable(){

        $(".fpTable").dataTable().fnDestroy();
        $(".fpTable").dataTable({
                                bSort: true,
                                bAutoWidth: true,
                                // scrollY:        300,
                                // scrollX:        true,
                                // scrollCollapse: true,
                                // paging:         false,
                                // fixedColumns:   true,
                                "iDisplayLength": 20, "aLengthMenu": [20,40,80,120], // can be removed for basic 10 items per page
                                "sPaginationType": "full_numbers",
                                "aoColumnDefs": [{"bSortable": false,
                                                 "aTargets": [ -1 , 0]}]});

    }


	$("#view_mhs_aktif").click(function(){
		var string = {};
		string.th_ak = $("#th_ak").val();
		string.kd_prodi = $("#kd_prodi").val();


		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url('lap_mahasiswa/cari_data_mhs_aktif'); ?>",
			data	: string,
			cache	: false,
			success	: function(data){
				$("#view_detail").html(data);
			}
		});
	});


});
</script>

<div class="widget-box ">
    <div class="widget-header">
        <h4 class="lighter smaller">
            <i class="icon-book blue"></i>
            <?php echo $judul;?>
        </h4>
    </div>

    <div class="widget-body">
    	<div class="widget-main no-padding">

            <form class="form-horizontal" name="my-form" id="my-form" action="<?php echo base_url();?>index.php/lap_mahasiswa/cetak" method="post">
							<fieldset>
                    <div class="control-group">
                        <label class="control-label" for="form-field-1">Tahun Akademik</label>
                        <div class="controls">
                            <select name="th_ak" id="th_ak" class="span2">
                            	<option value="" selected="selected">-Pilih-</option>
                                <?php
								$data = $this->db->select('th_akademik')->group_by('th_akademik')->order_by('th_akademik','DESC')->get('mahasiswa');// $this->model_data->th_akademik_jadwal();
								foreach($data->result() as $dt){
								?>
                                	<option value="<?php echo $dt->th_akademik;?>"><?php echo $dt->th_akademik;?></option>
                                <?php } ?>
                                </select> *) Tahun Masuk Mahasiswa
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="form-field-1">Program Studi</label>
                        <div class="controls">
                            <?php if($_SESSION['adm_kd_prodi']==null || $_SESSION['adm_kd_prodi']==0){?>
                                <select name="kd_prodi" id="kd_prodi" class="span4">
                                	<option value="" selected="selected">-Pilih-</option>
                                    <?php
    								$data = $this->model_data->data_jurusan();
    								foreach($data->result() as $dt){
    								?>
                                    <option value="<?php echo $dt->kd_prodi;?>"><?php echo $dt->prodi;?></option>
    								<?php } ?>
                                 </select>
                            <?php 
                            }else{
                                $data = $this->model_data->data_jurusan($_SESSION['adm_kd_prodi']);
                            ?>
                                <input type="text" value="<?php echo $data->row()->prodi;?>" readonly="readonly">
                                <input type="hidden" name="kd_prodi" id="kd_prodi" value="<?php echo $data->row()->kd_prodi;?>">
                            <?php }?>
                        </div>
                    </div>

										<div class="control-group">
                        <label class="control-label" for="form-field-1">Kelas</label>
                        <div class="controls">
                            <select name="kelas" id="kelas" class="span1">
                            	<option value="" selected="selected">-Pilih-</option>
                                <?php
								// $data = $this->db->select('kelas')->group_by('kelas')->order_by('kelas','DESC')->get('mahasiswa');
                                //$this->model_data->th_akademik_jadwal();$dt->kelas terus $data->results()
                                $data = $this->model_data->get_kelas();

								foreach($data as $dt){
								?>
                                	<option value="<?php echo $dt;?>"><?php echo $dt;?></option>
                                <?php } ?>
                                </select> *) Tahun Masuk Mahasiswa
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="form-field-1">Status</label>
                        <div class="controls">
                            <select name="status" id="status" class="span2">
                            	<option value="" selected="selected">-Pilih-</option>
                                <?php
								$data = $this->model_data->status_mhs();
								foreach($data as $dt){
								?>
                                <option value="<?php echo $dt;?>"><?php echo $dt;?></option>
								<?php } ?>
                             </select>
                        </div>
                    </div>
							</fieldset>

            <div class="form-actions center">
                     <button type="button" name="view" id="view" class="btn btn-mini btn-info">
                     <i class="icon-th"></i> Lihat Data
                     </button>
                     <button type="submit" name="cetak" id="cetak" class="btn btn-mini btn-primary">
                     <i class="icon-print"></i> Cetak PDF
                     </button>

                     <button type="button" name="view_mhs_aktif" id="view_mhs_aktif" class="btn btn-mini btn-info">
                     <i class="icon-th"></i> View Mahasiswa Aktif
                     </button>
           </div>

           </form>

           <?php
		  	echo  $this->session->flashdata('result_info');
		   ?>
        </div> <!-- wg body -->
    </div> <!--wg-main-->
</div>
<div id="view_detail"></div>
