<?php
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=$file_name.xls");
	header("Pragma: no-cache");
	header("Expires:0");
?>

<?php
	$temp_nim=[];
	$all_nim=[];
	$lim = count($data);//216
	
	for($i=0; $i<$lim; $i++){
		// $temp_nim[$i] = $data[$i]['nim'];
		$temp_smt[$i] = $data[$i]['smt'];
	}
	$max_smt = max($temp_smt);
	// $all_nim = array_unique($temp_nim);
	$i=0;
	$temp = $data[0]['nim'];
	//echo $temp;
	//print_r($data);

?>
<table border = "1" width="100%">
    <thead>
        <tr>
            <th>No.</th>
            <th>NIM</th>
            <th>Nama</th>
            <?php for($i=1; $i<=$max_smt; $i++){?>
	            <th>Jumlah SKS Smt.<?=$i;?></th>
	            <th>Total SKS <?=$i;?> Semester</th>
	            <th>IP Smt.<?=$i;?></th>
            <?php } $j=1;?>
            <th>IPK</th>
        </tr>
    </thead>
    <tbody>
    	<?php 
		$j=1;
		$jml_sks = 0;
		$tot_nil = 0;
		for($l=1;$l<=$max_smt;$l++){
			$tot_sks[$l] = 0;
		}
		for($k=0;$k<$lim;$k++){ 
			// $jml_sks = $this->model_data->cari_jml_sks_krs_($th_ak,$dt->nim);
			
			if($data[$k]['nim']==$temp){
				if(!empty($data[$k]['nilai_akhir']) && is_numeric($data[$k]['nilai_akhir'])){
					$tot_sks[$data[$k]['smt']] = $tot_sks[$data[$k]['smt']]+$data[$k]['sks'];
					$tot_nil = $tot_nil+($data[$k]['nilai_akhir']*$data[$k]['sks']);
				}
			}else{ ?>
				<tr>
	        	<td><?php echo $j++?></td>
	            <td><?php echo $data[$k-1]['nim'];?></td>
	            <td><?php echo $data[$k-1]['nama_mhs'];?></td>
	            <?php for($i=1; $i<=$max_smt; $i++){?>
	            	<td><?=$tot_sks[$i];?></td>
	            	<td><?php
	            		$jml_sks = $jml_sks + $tot_sks[$i];
	            		echo $jml_sks;
	            	?></td>
	            	<td><?php
	            	// if($tot_sks[$i]!=0){
	            	// 	$ip = number_format($tot_nil[$i]/$tot_sks[$i],2);
	            	// }else{
	            	// 	$ip = 0;
	            	// }
	            	$ip = $this->model_data->cari_ipk($i,$temp);

	            	echo $ip;?></td>
				<?php } ?>
				<td>
					<?php 
					//echo $tot_nil." ".$jml_sks." ";
						if($jml_sks>0){
							echo number_format($tot_nil/$jml_sks,2);
						}else{
							echo number_format($tot_nil/1,2);
						}
					?>
				</td>
	        	</tr>
			<?php
			$temp = $data[$k]['nim'];
			$k=$k-1;
			$jml_sks = 0;
			$tot_nil = 0;
			for($l=1;$l<=$max_smt;$l++){
				$tot_sks[$l] = 0;
			}
			}
		} ?>
		<tr>
        	<td><?php echo $j++?></td>
            <td><?php echo $data[$k-1]['nim'];?></td>
            <td><?php echo $data[$k-1]['nama_mhs'];?></td>
            <?php for($i=1; $i<=$max_smt; $i++){?>
            	<td><?=$tot_sks[$i];?></td>
            	<td><?php
            		$jml_sks = $jml_sks + $tot_sks[$i];
            		echo $jml_sks;
            	?></td>
            	<td><?php
            	// if($tot_sks[$i]!=0){
            	// 	$ip = number_format($tot_nil[$i]/$tot_sks[$i],2);
            	// }else{
            	// 	$ip = 0;
            	// }
            	$ip = $this->model_data->cari_ipk($i,$temp);
            	echo $ip;?></td>
			<?php } ?>
			<td>
				<?php 
				// echo $tot_nil." ".$jml_sks." ";
					if($jml_sks>0){
						echo number_format($tot_nil/$jml_sks,2);
					}else{
						echo number_format($tot_nil/1,2);
					}
				?>
			</td>
	    </tr>
    </tbody>
</table>