<?php 
    // untuk loop data sebanyak x semester
    $arr = $data->result_array();
    $i=0;
    foreach ($arr as $a) {
        $b[$i] = $a['smt'];
        $i=$i+1;
    }
    $sks_ak = 0;
    // $ip_ak = 0;
    $nilai_ak = 0;
    $pembagi = 0;
    $max = max($b);

    // $cek = @$_SESSION['logged_in'];
    // $level = @$_SESSION['level'];
    // echo($cek)."<br>";
    // echo($level)."<br>";
?>
<?php for($m=1;$m<=$max;$m++){?>
    <div class="info-smt">
        <h4> SEMESTER <?= $m;?></h4>
    </div>
    <table  class="table fpTable lcnp table-striped table-bordered table-hover dt-responsive">
        <thead>
            <tr>
                <th class="center" width="10">No</th>
                <th class="center">Mata Kuliah</th>
                <th class="center" width="10">SKS</th>
                <th class="center">Dosen</th>
                <th class="center">Nilai Huruf</th>
                <th class="center">Nilai Angka</th>
                <th class="center">Nilai Bobot</th>
            </tr>
        </thead>
        <tbody>
        	<?php 

            // Biki array baru isinya data sesuai smt 
            $c=0;
            $ar = $data->result();
            $newAr = new stdClass();
            foreach($ar as $val){
                if($val->smt == $m){
                    $newAr->$c = $val;
                    $c=$c+1;
                }
            }
            // echo "<pre>";
            // print_r($newAr);
            // print_r($data->result());
            // echo "</pre>";  
            // exit;  
            // $c=0;
            // if(key($newAr)==null){
            //     echo "data kosong";
            // }else{
            //     echo "data ada";
            //     echo "<pre>";
            //     print_r($newAr);
            //     echo "</pre>";
            // }

            // exit;


    		$i=1;
    		$t_sks=0;
    		$t_nilai = 0;
    		$nilai = 0;
    		$ip = 0;
            if(key($newAr)!=null){
                    // echo "<pre>";
                    // print_r($newAr[0]->smt);
                    // echo "</pre>";

                    // exit;
        		foreach($newAr as $dt){ 
        			$nama_mk = $this->model_data->cari_nama_mk($dt->kd_mk);
        			$nama_dosen = $this->model_data->cari_nama_dosen($dt->kd_dosen);
        			$angka = $this->model_data->cari_nilai_angka($dt->nilai_akhir);
        			$huruf = $this->model_data->cari_nilai_huruf($dt->nilai_akhir);
                                $sks = $dt->sks;
        //			$nilai = $angka*$sks;
                                $nilai = (float)$dt->nilai_akhir*$sks;
        			$ip = $this->model_data->cari_ipk($dt->smt,$dt->nim);
        		?>
                <tr>
                	<td class="center"><?php echo $i++?></td>
                    <td ><?php echo $dt->kd_mk.' - '.$nama_mk.' ';?></td>
                    <td class="center"><?php echo $dt->sks;?></td>
                    <td ><?php echo $dt->kd_dosen.' - '.$nama_dosen;?></td>
                    <!--<td class="center"><?php echo $dt->nilai_akhir;?></td>-->
                    <!--<td class="center"><?php echo $angka;?></td>-->
                    <!--<td class="center"><?php echo $nilai;?></td>-->
                    <td class="center"><?php echo $huruf;?></td>
                    <td class="center"><?php echo $dt->nilai_akhir;?></td>
                    <td class="center"><?php echo $nilai;?></td>
                </tr>
        		<?php
                    if($nilai>0){
                        $t_sks = $t_sks+$dt->sks;
                        $t_nilai = $t_nilai + $nilai;
                    } 
        		
        		} 
        		//$ip =$t_nilai/$t_sks;
            }else{?>
                <td class="center text-red" colspan="7"> Data Semester <?= $m;?> Kosong </td>
            <?php 
            } ?>
        </tbody>
        <tfoot>
            <tr>
            	<td colspan="2" class="center">T o t a l</td>
                <td class="center"><?php echo $t_sks;?></td>
                <td colspan="3" class="center"></td>
                <td class="center"><?php echo $t_nilai;?></td>
            </tr>
            <tr>
            	<td colspan="4" class="center">Indeks Prestasi (IP)</td>
                <td colspan="3" class="center"><strong><?php echo number_format($ip,2);?></strong></td>
    		</tr>            
        </tfoot>
    </table>
<hr>
<?php 
    // inisialisasi sks ip dan nilai total secaara keseluruhan
    $sks_ak = $sks_ak + $t_sks;
    //$ip_ak = $ip_ak + $ip;
    $nilai_ak = $nilai_ak + $t_nilai;
}

?>
<div class="grid-container2">
    <div class="item1">Total SKS : <?php echo $sks_ak;?></div>
    <div class="item2">Indeks Prestasi Kumulatif (IPK) : 
        <?php 
        if($sks_ak>0){
            echo number_format(($nilai_ak/$sks_ak),2);
        }else{
            echo number_format(($nilai_ak/1),2);
        }
        ?>
        
    </div>
    <div class="item3">Total Nilai : <?php echo $nilai_ak;?></div>
    
</div>