<script type="text/javascript">
$(document).ready(function(){

	$("#th_ak").change(function(){
		var th_ak =$("#th_ak").val();

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url(); ?>/lap_krs/cari_smt",
			data	: "th_ak="+th_ak,
			cache	: false,
			dataType: "json",
			success	: function(data){
				$("#smt").val(data.semester);
			}
		});

	});

	$("#view").click(function(){
		cari_data();
	});

	function cari_data(){
		var string = $("#my-form").serialize();

		if(!$("#th_ak").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Th Akademik tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#th_ak").focus();
			return false();
		}

		if(!$("#smt").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Semester tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#smt").focus();
			return false();
		}

		if(!$("#th").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Tahun Angkatan Masuk tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#th").focus();
			return false();
		}

		if(!$("#kd_prodi").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'PRODI tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#kd_prodi").focus();
			return false();
		}

		if(!$("#kelas").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Kelas tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#kelas").focus();
			return false();
		}



		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url('lap_krs/cari_data'); ?>",
			data	: string,
			cache	: false,
			success	: function(data){
				$("#view_detail").html(data);
			    setTimeout(function(){ reinitDatatable(); },50);

            }
        });
    }

    function reinitDatatable(){

        $(".fpTable").dataTable().fnDestroy();
        $(".fpTable").dataTable({
                                bSort: true,
                                bAutoWidth: true,
                                // scrollY:        300,
                                // scrollX:        true,
                                // scrollCollapse: true,
                                // paging:         false,
                                // fixedColumns:   true,
                                "iDisplayLength": 20, "aLengthMenu": [20,40,80,120], // can be removed for basic 10 items per page
                                "sPaginationType": "full_numbers",
                                "aoColumnDefs": [{"bSortable": false,
                                                 "aTargets": [ -1 , 0]}]});

    }

});
</script>

<div class="widget-box ">
    <div class="widget-header">
        <h4 class="lighter smaller">
            <i class="icon-book blue"></i>
            <?php echo $judul;?>
        </h4>
    </div>

    <div class="widget-body">
    	<div class="widget-main no-padding">

            <form class="form-horizontal" name="my-form" id="my-form" >
							<fieldset>
                    <div class="control-group">
                        <label class="control-label" for="form-field-1">Tahun Akademik</label>
                        <div class="controls">
                            <select name="th_ak" id="th_ak" class="span2">
                            	<option value="" selected="selected">-Pilih-</option>
                                <?php
								$data = $this->model_data->th_akademik_jadwal();
								foreach($data->result() as $dt){
								?>
                                	<option value="<?php echo $dt->th_akademik;?>"><?php echo $dt->th_akademik;?></option>
                                <?php } ?>
                                </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="form-field-1">Semester</label>
                        <div class="controls">
                            <input type="text" name="smt" id="smt" class="span2" readonly="readonly">
                        </div>
                    </div>
                    <hr />
                    <div class="control-group">
                        <label class="control-label" for="form-field-1">Tahun Angkatan</label>
                        <div class="controls">
                            <select name="th" id="th" class="span2">
                            	<option value="" selected="selected">-Pilih-</option>
                                <?php
								$data = $this->db->select('th_akademik')->group_by('th_akademik')->order_by('th_akademik','DESC')->get('mahasiswa'); // $this->model_data->th_akademik_jadwal();
								foreach($data->result() as $dt){
								?>
                                	<option value="<?php echo $dt->th_akademik;?>"><?php echo $dt->th_akademik;?></option>
                                <?php } ?>
                                </select> (* Th Akademik Masuk Mahasiswa
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="form-field-1">Program Studi</label>
                        <div class="controls">
                            <?php if($_SESSION['adm_kd_prodi']==null || $_SESSION['adm_kd_prodi']==0){?>
	                            <select name="kd_prodi" id="kd_prodi" class="span4">
	                            	<option value="" selected="selected">-Pilih-</option>
	                                <?php
									$data = $this->model_data->data_jurusan();
									foreach($data->result() as $dt){
									?>
	                                <option value="<?php echo $dt->kd_prodi;?>"><?php echo $dt->prodi;?></option>
									<?php } ?>
	                            </select>
	                        <?php 
                            }else{
                                $data = $this->model_data->data_jurusan($_SESSION['adm_kd_prodi']);
                            ?>
                                <input type="text" value="<?php echo $data->row()->prodi;?>" readonly="readonly">
                                <input type="hidden" name="kd_prodi" id="kd_prodi" value="<?php echo $data->row()->kd_prodi;?>">
                            <?php }?>
                        </div>
                    </div>

										<div class="control-group">
                        <label class="control-label" for="form-field-1">Kelas</label>
                        <div class="controls">
                            <select name="kelas" id="kelas" class="span2">
                            	<option value="" selected="selected">-Pilih-</option>
                                <?php
								// $data = $this->db->select('kelas')->group_by('kelas')->order_by('kelas','ASC')->get('mahasiswa'); 
								// $this->model_data->th_akademik_jadwal(); $dt->kelas terus $data->results()
                                $data = $this->model_data->get_kelas();
								foreach($data as $dt){
								?>
                                	<option value="<?php echo $dt;?>"><?php echo $dt;?></option>
                                <?php } ?>
                                </select>
                        </div>
                    </div>

							</fieldset>

            <div class="form-actions center">
                     <button type="button" name="view" id="view" class="btn btn-mini btn-primary">
                     <i class="icon-th"></i> Lihat Data
                     </button>
           </div>

           </form>
        </div> <!-- wg body -->
    </div> <!--wg-main-->
</div>
<div id="view_detail"></div>
