<?php 
	$temp_nim=[];
	$all_nim=[];
	$lim = count($data);//216
	$temp_smt=[];
	for($i=0; $i<$lim; $i++){
		// $temp_nim[$i] = $data[$i]['nim'];
		$temp_smt[$i] = $data[$i]['smt'];
	}
	if(!empty($data)){
		$max_smt = max($temp_smt);	
		$i=0;
		$temp = $data[0]['nim'];
	}else{
		$max_smt = 0;	
		$i=0;
		$temp = 0;
	}
	// $all_nim = array_unique($temp_nim);
	
	//echo $temp;
	//print_r($data);

?>
<table  class="table fpTable lcnp table-striped table-bordered table-hover dt-responsive">
    <thead>
        <tr>
            <th class="center">No.</th>
            <th class="center span2">NIM</th>
            <th class="center span2">Nama</th>
            <?php for($i=1; $i<=$max_smt; $i++){?>
	            <th class="center">Jumlah SKS Smt.<?=$i;?></th>
	            <th class="center">Total SKS <?=$i;?> Semester</th>
	            <th class="center">IP Smt.<?=$i;?></th>
            <?php } $j=1;?>
            <th class="center">IPK</th>
        </tr>
    </thead>
    <tbody>
    	<?php 
		$j=1;
		$jml_sks = 0;
		$tot_nil= 0;
		for($l=1;$l<=$max_smt;$l++){
			$tot_sks[$l] = 0;
		}
		for($k=0;$k<$lim;$k++){ 
			// $jml_sks = $this->model_data->cari_jml_sks_krs_($th_ak,$dt->nim);
			
			if($data[$k]['nim']==$temp){
				if(!empty($data[$k]['nilai_akhir']) && is_numeric($data[$k]['nilai_akhir'])&&$data[$k]['nilai_akhir']>0){
					$tot_sks[$data[$k]['smt']] = $tot_sks[$data[$k]['smt']]+$data[$k]['sks'];
					$tot_nil = $tot_nil+($data[$k]['nilai_akhir']*$data[$k]['sks']);
				}
			}else{ ?>
				<tr>
	        	<td class="center span1"><?php echo $j++?></td>
	            <td class="center span2"><?php echo $data[$k-1]['nim'];?></td>
	            <td class="center span2"><?php echo $data[$k-1]['nama_mhs'];?></td>
	            <?php for($i=1; $i<=$max_smt; $i++){?>
	            	<td class="center"><?=$tot_sks[$i];?></td>
	            	<td class="center"><?php
	            		$jml_sks = $jml_sks + $tot_sks[$i];
	            		echo $jml_sks;
	            	?></td>
	            	<td class="center"><?php
	            	// if($tot_sks[$i]!=0){
	            	// 	$ip = number_format($tot_nil[$i]/$tot_sks[$i],2);
	            	// }else{
	            	// 	$ip = 0;
	            	// }
	            	$ip = $this->model_data->cari_ipk($i,$temp);

	            	echo $ip;?></td>
				<?php } ?>
				<td class="center">
					<?php 
					//echo $tot_nil." ".$jml_sks." ";
						if($jml_sks>0){
							echo number_format($tot_nil/$jml_sks,2);
						}else{
							echo number_format($tot_nil/1,2);
						}
					?>
				</td>
	        	</tr>
			<?php
			$temp = $data[$k]['nim'];
			$k=$k-1;
			$jml_sks = 0;
			$tot_nil = 0;
			for($l=1;$l<=$max_smt;$l++){
				$tot_sks[$l] = 0;
			}
			}
		} 
		?>
		<tr>
	    	<td class="center span1"><?php echo $j++?></td>
	        <td class="center span2"><?php echo @$data[$k-1]['nim'];?>
	        <td class="center span2"><?php echo @$data[$k-1]['nama_mhs'];?></td>
	        </td>
	        <?php for($i=1; $i<=$max_smt; $i++){?>
	        	<td class="center"><?=$tot_sks[$i];?></td>
	        	<td class="center"><?php
	        		$jml_sks = $jml_sks + $tot_sks[$i];
	        		echo $jml_sks;
	        	?></td>
	        	<td class="center"><?php
	        	// if($tot_sks[$i]!=0){
	        	// 	$ip = number_format($tot_nil[$i]/$tot_sks[$i],2);
	        	// }else{
	        	// 	$ip = 0;
	        	// }
	        	$ip = $this->model_data->cari_ipk($i,$temp);
	        	echo $ip;?></td>
			<?php } ?>
			<td class="center">
				<?php 
				// echo $tot_nil." ".$jml_sks." ";
					if($jml_sks>0){
						echo number_format($tot_nil/$jml_sks,2);
					}else{
						echo number_format($tot_nil/1,2);
					}
				?>
			</td>
	    </tr>
	    <?php}?>
    </tbody>
</table>