<script type="text/javascript">
$(document).ready(function(){
	/*
	function view(){
		$("#v_profil").show();
		$("#e_profil").hide();
		$("#v_orang_tua").show();
		$("#e_orang_tua").hide();

	}

	view();

	$("#edit_profil").click(function(){
		$("#v_profil").hide();
		$("#e_profil").show();
		$("#nama_lengkap").focus();
	});
	*/

});
function resetPassword()
{
	var x = confirm('Yakin akan melakukan Reset Password');
	if(x==true){
			// alert('password success reset');
			var string = {};
			string.nim = $("#nim").val();

			$.ajax({
				type	: 'POST',
				url		: "<?php echo site_url('mahasiswa/reset_password'); ?>",
				data	: string,
				cache	: false,
				success	: function(data){
					$.gritter.add({
						title: 'Peringatan..!!',
						text: data,
						class_name: 'gritter-error'
					});
				}
			});
	}
}
</script>
<div class="row-fluid">
        <div class="tabbable">
            <ul class="nav nav-tabs padding-18" id="myTab">
                <li class="active">
                    <a data-toggle="tab" href="#profil">
                        <i class="green icon-user bigger-110"></i>
                        Profile
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#orangtua">
                    	<i class="red icon-home bigger-110"></i>
                        Orang Tua
                    </a>
                </li>
				<li>
                    <a data-toggle="tab" href="#nilai">
                    	<i class="green icon-book bigger-110"></i>
                        Nilai
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#transkrip_nilai">
                    	<i class="red icon-table bigger-110"></i>
                        Transkrip Nilai
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#grafik">
                    	<i class="green icon-bar-chart bigger-110"></i>
                        Grafik IP
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#histori">
                    	<i class="red icon-film bigger-110"></i>
                        History
                    </a>
                </li>
								<li>
                    <a data-toggle="tab" href="#reset">
                    	<i class="red icon-key bigger-110"></i>
                        Reset Password
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div id="profil" class="tab-pane in active">
                    <!--awal -->
                        <?php echo $this->load->view('mahasiswa/profil_mhs');?>
                    <!--akhir-->
                </div>
                <div id="orangtua" class="tab-pane">
                    <?php echo $this->load->view('mahasiswa/orang_tua');?>
                </div>
                <div id="nilai" class="tab-pane">
                    <?php echo $this->load->view('mahasiswa/form_nilai');?>
                </div>
                <div id="transkrip_nilai" class="tab-pane">
                    <?php echo $this->load->view('mahasiswa/form_transkrip');?>
                </div>
                <div id="grafik" class="tab-pane">
                    <?php echo $this->load->view('mahasiswa/grafik_ip');?>
                </div>
                <div id="histori" class="tab-pane">
                    <?php echo $this->load->view('mahasiswa/form_bayar');?>
                </div>
								<div id="reset" class="tab-pane">
                    <a href="javascript:void(0);" class="btn btn-small btn-primary" onclick="resetPassword();">
											<i class="red icon-key"></i> Reset password
										</a>
                </div>
            </div>
        </div>
</div>
