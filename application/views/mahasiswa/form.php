<script type="text/javascript">
$(document).ready(function(){
	$(".chzn-select").chosen();

	$("#cari_nim").change(function(){
		var nim = $("#cari_nim").val();
		//alert(nim);
		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url(); ?>/mahasiswa/cari_mhs",
			data	: "nim="+nim,
			cache	: false,
			success	: function(data){
				$("#info_mhs").html(data);
			}
		});
	});
});
</script>


<div class="widget-box">
  <div class="widget-header">
    <h4><i class="icon-user"></i> <?php echo $judul;?></h4>
  </div>
  <div class="widget-body">
    <div class="widget-main">
      <div class="row-fluid">
        <div class="span6">
          <div class="widget-box">
            <div class="widget-header">
              <h4 class="smaller">Filter Jurusan</h4>
            </div>

            <div class="widget-body">
             <div class="widget-main">

                <form action="<?php echo site_url();?>/mahasiswa/view_data" method="post">
                  <?php if($_SESSION['adm_kd_prodi']==null || $_SESSION['adm_kd_prodi']==0){?>
                    <label class="control-label" for="form-field-1">Filter Jurusan</label>
                    <div class="controls">
                      <select name="cari_jurusan" id="cari_jurusan">
                      <?php
                        $data = $this->model_data->data_jurusan();
                        foreach($data->result() as $dt){
                      ?>
                         <option value="<?php echo $dt->kd_prodi;?>"><?php echo $dt->prodi;?></option>
                      <?php } ?>
                      </select>
                  <?php 
                    }else{
                        $data = $this->model_data->data_jurusan($_SESSION['adm_kd_prodi']);
                    ?>
                        <label class="control-label" for="form-field-1">Jurusan</label>
                        <div class="controls">
                        <input type="text" value="<?php echo $data->row()->prodi;?>" readonly="readonly">
                        <input type="hidden" name="cari_jurusan" id="cari_jurusan" value="<?php echo $data->row()->kd_prodi;?>" placeholder="<?php echo $data->row()->prodi;?>">
                    <?php }?>
                      <button type="submit" name="lanjut" id="lanjut" class="btn btn-small btn-success" > 
                     	  Lanjut
                        <i class="icon-arrow-right icon-on-right bigger-110"></i>
                      </button>
                    </div>
                </form>

              </div>
            </div>
          </div>
        </div><!--/span-->


        <div class="span6">
          <div class="widget-box">
            <div class="widget-header">
              <h4 class="smaller">Pencarian Mahasiswa</h4>
            </div>
            <div class="widget-body">
              <div class="widget-main">
                <label for="form-field-select-1">Masukan NIM</label>
                <select class="chzn-select span4" data-placeholder="Cari NIM ...." name="cari_nim" id="cari_nim" >
                  <option value="">Cari NIM ....</option>
                  <?php
                    $data = $this->model_data->data_all_mhs();
                    foreach($data->result() as $dt){
                  ?>
                  <option value="<?php echo $dt->nim;?>"><?php echo $dt->nim;?></option>
                  <?php } ?>
                </select>

              </div>
            </div>
          </div>
        </div><!--/span-->

      </div>

    </div>
  </div>
</div><!--/span-->

<div id="info_mhs"></div>
