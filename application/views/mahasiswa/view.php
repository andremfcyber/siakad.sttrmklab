<script type="text/javascript">
  function update_status(nim){
    var status = $('#status_'+nim).val();

    var string = "nim="+nim+"&status="+status;

    $.ajax({
      type    : 'POST',
      url     : '<?= site_url();?>/mahasiswa/update_status',
      data    : string,
      cache   : false,
      success :function(data){
        $.gritter.add({
            title: 'Peringatan..!!',
            text: data,
            class_name: 'gritter-info' 
        });
      }

    });
  }

  function update_kelas(nim){
    var kelas = $('#kelas_'+nim).val();

    var string = "nim="+nim+"&kelas="+kelas;

    $.ajax({
      type    : 'POST',
      url     : '<?= site_url();?>/mahasiswa/update_kelas',
      data    : string,
      cache   : false,
      success :function(data){
        $.gritter.add({
            title: 'Peringatan..!!',
            text: data,
            class_name: 'gritter-info' 
        });
      }

    });
  }
</script>

<div class="row-fluid">
  <div class="table-header">
    <?php echo $judul;?>
	 <?php echo @$_SESSION['sesi_kd_prodi'];?> ( <?php echo $this->model_data->nama_jurusan(@$_SESSION['sesi_kd_prodi']);?> )
    <div class="widget-toolbar no-border pull-right">
      <a href="<?php echo base_url();?>index.php/mahasiswa/tambah" class="btn btn-small btn-success" >
        <i class="icon-check"></i>
        Tambah Data
      </a>
      <a href="<?php echo site_url();?>/mahasiswa/view_data" class="btn btn-small btn-info"  >
        <i class="icon-refresh"></i>
        Refresh
      </a>
      <a href="<?php echo site_url('mahasiswa');?>" class="btn btn-small btn-warning"  >
        <i class="icon-table"></i>
        Kembali
      </a>
    </div>
  </div>

  <table class="table fpTable table-bordered table-striped table-hover w-100" id="mytable">
      <thead>
        <tr>
          <th class="center">No</th>
          <th class="center span2">Th Akademik</th>
          <th class="center span2">NIM</th>
          <th class="center">Nama Mahasiswa</th>
          <th class="center">L/P</th>
          <th class="center">HP</th>
          <th class="center">Status</th>
          <th class="center">Kelas</th>
          <th class="center">Aksi</th>
        </tr>
      </thead>
      <tbody>
      <?php
      //$data = $this->model_data->data_mk();
      $i=1;
      foreach($data as $dt){ ?>
          <tr>
            <td class="center span1"><?php echo $i++?></td>
            <td><?php echo $dt->th_akademik;?></td>
            <td><?php echo $dt->nim;?></td>
            <td><?php echo $dt->nama_mhs;?></td>
            <td class="center span1"><?php echo $dt->sex;?></td>
            <td><?php echo $dt->hp;?></td>
            <td class="center span1">
              <select name="status_<?=$dt->nim;?>" id="status_<?=$dt->nim;?>" onchange="update_status('<?=$dt->nim;?>')" style="width:75px;">
                <option value="">-</option>
                <?php
                  $data_stat = $this->model_data->status_mhs();
                  foreach($data_stat as $dt_stat){
                    if($dt_stat == $dt->status){
                      $select = "selected=true";
                    }else{
                      $select = "";
                    }
                ?>
                <option value="<?= $dt_stat;?>" <?= $select;?> ><?=$dt_stat;?></option>

              <?php }?>
              </select>
            </td>
            <td class="center span1">
              <select name="kelas_<?=$dt->nim;?>" id="kelas_<?=$dt->nim;?>" onchange="update_kelas('<?=$dt->nim;?>')" style="width:50px;">
                <option value="">-</option>
                <?php
                  $data_kelas = $this->model_data->get_kelas();
                  foreach($data_kelas as $dt_kelas){
                    if($dt_kelas == $dt->kelas){
                      $select = "selected=true";
                    }else{
                      $select = "";
                    }
                ?>
                <option value="<?= $dt_kelas;?>" <?= $select;?> ><?=$dt_kelas;?></option>
              <?php }?>
              </select>
            </td>
            <td class="center">
              <a href="<?= site_url();?>/mahasiswa/edit/<?= $dt->nim;?>" class="btn btn-mini btn-primary">Edit</a>
              <a href="<?= site_url();?>/mahasiswa/hapus/<?= $dt->nim;?>" class="btn btn-mini btn-danger" onclick="return confirm('yakin akan menghapus ?')">Hapus</a>
            </td>
          </tr>
      <?php } ?>
      </tbody>
  </table>
</div>

<!-- <script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.js') ?>"></script>
<script type="text/javascript">
$(document).ready(function() {
      $('#mytable').dataTable({
          "ajax": "<?php echo site_url('mahasiswa/get_json'); ?>",
          "pageLength": 20,
          "order": [[ 0, "asc" ]],
          "aoColumnDefs": [{"bSortable": false,
                           "aTargets": [ -1 , 0]}],
          "dom": 'T<"clear">lfrtip',
      });
});
</script> -->
<!-- <pre>
  <?php
    print_r($data);
  ?>
</pre> -->