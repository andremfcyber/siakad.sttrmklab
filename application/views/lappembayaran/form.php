<script type="text/javascript">
$(document).ready(function(){

  $('.date-picker').datepicker().next().on(ace.click_event, function(){
		$(this).prev().focus();
	});

	$("#th_ak").change(function(){
		cari_smt();

	});

	function cari_smt()
	{
		var string = {};
		string.th_ak = $("#th_ak").val();
		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url('lap_absen/cari_smt'); ?>",
			data	: string,
			cache	: false,
			dataType : 'json',
			success	: function(data){
				// console.log(data);
				$("#smt").val(data.semester);
        list_smt();
			}
		});

	}

  function list_smt()
  {
    var string = {};
		string.thak = $("#th_ak").val();
		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url('lappembayaran/list_smt'); ?>",
			data	: string,
			cache	: false,
			success	: function(data){
				// console.log(data);
				$("#semester").html(data);
			}
		});
  }

	$("#view").click(function(){
		cari_data();
	});

	function cari_data(){
		var string = $("#my-form").serialize();
    // console.log(string);
		if(!$("#th_ak").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Th Akademik tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#th_ak").focus();
			return false();
		}

		if(!$("#smt").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Semester tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#smt").focus();
			return false();
		}


		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url('lappembayaran/cari_data'); ?>",
			data	: string,
			cache	: false,
			success	: function(data){
				$("#view_detail").html(data);
			  setTimeout(function(){ reinitDatatable(); },50);

            }
        });
    }

    function reinitDatatable(){

        $(".fpTable").dataTable().fnDestroy();
        $(".fpTable").dataTable({
                                bSort: true,
                                bAutoWidth: true,
                                // scrollY:        300,
                                // scrollX:        true,
                                // scrollCollapse: true,
                                // paging:         false,
                                // fixedColumns:   true,
                                "iDisplayLength": 20, "aLengthMenu": [20,40,80,120], // can be removed for basic 10 items per page
                                "sPaginationType": "full_numbers",
                                "aoColumnDefs": [{"bSortable": false,
                                                 "aTargets": [ -1 , 0]}]});

    }


});

function cekForm()
{
  if(!$("#th_ak").val()){
    $.gritter.add({
      title: 'Peringatan..!!',
      text: 'Th Akademik tidak boleh kosong',
      class_name: 'gritter-error'
    });
    $("#th_ak").focus();
    return false;
  }

  if(!$("#smt").val()){
    $.gritter.add({
      title: 'Peringatan..!!',
      text: 'Semester tidak boleh kosong',
      class_name: 'gritter-error'
    });
    $("#smt").focus();
    return false;
  }
}
</script>

<div class="widget-box ">
    <div class="widget-header">
        <h4 class="lighter smaller">
            <i class="icon-book blue"></i>
            <?php echo $judul;?>
        </h4>
    </div>

    <div class="widget-body">
    	<div class="widget-main no-padding">

            <form class="form-horizontal" name="my-form" id="my-form" action="<?php echo site_url('lappembayaran/cetak');?>" method="POST" onsubmit="return cekForm();">
							<fieldset>
                    <div class="control-group">
                        <label class="control-label" for="form-field-1">Tahun Akademik</label>
                        <div class="controls">
                            <select name="th_ak" id="th_ak" class="span2">
                            	<option value="" selected="selected">-Pilih-</option>
                                <?php
								$data = $this->model_data->th_akademik_jadwal();
								foreach($data->result() as $dt){
								?>
                                	<option value="<?php echo $dt->th_akademik;?>"><?php echo $dt->th_akademik;?></option>
                                <?php } ?>
                                </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="form-field-1">Semester</label>
                        <div class="controls">
														<input type="text" name="smt" id="smt" class="span2" readonly="true">
                        </div>
                    </div>

                    <div class="control-group">
                      <label class="control-label" for="form-field-1">Dari Tanggal</label>
                      <div class="controls">
                        <div class="row-fluid input-append">
                          <input type="text" name="tanggal" id="tanggal" class="span2 date-picker"  data-date-format="dd-mm-yyyy" />
                          <span class="add-on">
         										<i class="icon-calendar"></i>
         									</span>
                        </div>
                      </div>
                    </div>

                    <div class="control-group">
                      <label class="control-label" for="form-field-1">Sampai Tanggal</label>
                      <div class="controls">
                        <div class="row-fluid input-append">
                          <input type="text" name="tanggal2" id="tanggal2" class="span2 date-picker"  data-date-format="dd-mm-yyyy" />
                          <span class="add-on">
         										<i class="icon-calendar"></i>
         									</span>
                        </div>
                      </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="form-field-1">Program Studi</label>
                        <div class="controls">
                            <select name="kd_prodi" id="kd_prodi" class="span4">
                            	<option value="" selected="selected">-Pilih-</option>
                                <?php
								$data = $this->model_data->data_jurusan();
								foreach($data->result() as $dt){
								?>
                                <option value="<?php echo $dt->kd_prodi;?>"><?php echo $dt->prodi;?></option>
								<?php } ?>
                             </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="form-field-1">Semester</label>
                        <div class="controls">
                            <select name="semester" id="semester" class="span1">
                            </select>
                        </div>
                    </div>

										<!-- <div class="control-group">
                        <label class="control-label" for="form-field-1">Kelas</label>
                        <div class="controls">
                            <select name="kelas" id="kelas" class="span1">
                            	<option value="" selected="selected">-Pilih-</option>
                                <?php
								$data = $this->db->select('kelas')->group_by('kelas')->order_by('kelas')->get('mahasiswa'); // $this->model_data->th_akademik_jadwal();
								foreach($data->result() as $dt){
								?>
                                	<option value="<?php echo $dt->kelas;?>"><?php echo $dt->kelas;?></option>
                                <?php } ?>
                                </select>
                        </div>
                    </div> -->


								</fieldset>

            	<div class="form-actions center">
                     <button type="button" name="view" id="view" class="btn btn-mini btn-primary">
                     <i class="icon-th"></i> Lihat Data
                     </button>
                     <!-- <button type="button" name="cetak_pdf" id="cetak_pdf" class="btn btn-mini btn-primary">
                     <i class="icon-print"></i> Cetak PDF
                     </button> -->
                     <button type="submit" name="cetak_excel" id="cetak_excel" class="btn btn-mini btn-success">
                     <i class="icon-print"></i> Cetak EXCEL
                     </button>
           		</div>
           </form>
        </div> <!-- wg body -->
    </div> <!--wg-main-->
</div>
<div id="view_detail"></div>
