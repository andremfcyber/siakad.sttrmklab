<script type="text/javascript">
$(document).ready(function(){
	$("#judul").keyup(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});


	$("#tambah").click(function(){
		$('#id').val('');
		$('#judul').val('');
		$('#kd_mk').val('');
		$('#file').val('');
	});
});

function editData(ID){
	var cari	= ID;
	$.ajax({
		type	: "POST",
		url		: "<?php echo site_url(); ?>/materi_kuliah/cari",
		data	: "cari="+cari,
		dataType: "json",
		success	: function(data){
			//alert(data.ref);
			$('#id').val(ID);
			$('#judul').val(data.judul);
			$('#smt').val(data.smt);
			$('#kd_mk').val(data.kd_mk);
			$('#file').val(data.file);
		}
	});

}

function cekForm()
{
	if(!$("#kd_mk").val()){
		alert('Maaf, Kode MK Tidak boleh kosong');
		$("#kd_mk").focus();
		return false;
  }
  
  if(!$("#smt").val()){
    alert('Maaf, Semester Tidak boleh kosong');
    $("#smt").focus();
    return false;
  }

	if(!$("#judul").val()){
		alert('Maaf, Judul Tidak boleh kosong');
		$("#judul").focus();
		return false;
	}

	if($("#id").val() == ''){
		if(!$("#file").val()){
			alert('Maaf, File Tidak boleh kosong');
			$("#file").focus();
			return false;
		}
	}
}
</script>
<div class="row-fluid">
	<?php
	$info = $this->session->flashdata('result_info');
	if(!empty($info)){
		echo $info;
	} ?>
<div class="table-header">
    <?php echo $judul;?>
    <div class="widget-toolbar no-border pull-right">
    <a href="#modal-table" class="btn btn-small btn-success"  role="button" data-toggle="modal" name="tambah" id="tambah" >
        <i class="icon-check"></i>
        Tambah Data
    </a>
    </div>
</div>

<table  class="table fpTable lcnp table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="center">No</th>
						<th class="center">Mata Kuliah</th>
						<th class="center">Semester</th>
						<th class="center">Judul</th>
            <th class="center">File</th>
            <th class="center">Aksi</th>
        </tr>
    </thead>
    <tbody>
    	<?php
		$i=1;
		foreach($data->result() as $dt){
			$mk = $this->model_data->cari_nama_mk($dt->kd_mk);
			?>

        <tr>
        	<td class="center span1"><?php echo $i++?></td>
            <td class="center"><?php echo $mk;?></td>
            <td class="center"><?php echo $dt->smt;?></td>
            <td ><?php echo $dt->judul;?></td>
            <td class="center">
							<a href="<?php echo base_url();?>assets/materi_kuliah/<?= $dt->file;?>" target="_blank">
							<?php echo $dt->file;?></a>
						</td>

            <td class="td-actions"><center>
            	<div class="hidden-phone visible-desktop action-buttons">
                    <a class="green" href="#modal-table" onclick="javascript:editData('<?php echo $dt->id;?>')" data-toggle="modal">
                        <i class="icon-pencil bigger-130"></i>
                    </a>

                    <a class="red" href="<?php echo site_url();?>/materi_kuliah/hapus/<?php echo $dt->id;?>" onClick="return confirm('Anda yakin ingin menghapus data ini?')">
                        <i class="icon-trash bigger-130"></i>
                    </a>
                </div>

                <div class="hidden-desktop visible-phone">
                    <div class="inline position-relative">
                        <button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-caret-down icon-only bigger-120"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close">
                            <li>
                                <a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
                                    <span class="green">
                                        <i class="icon-edit bigger-120"></i>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
                                    <span class="red">
                                        <i class="icon-trash bigger-120"></i>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                </center>
            </td>
        </tr>
		<?php } ?>
    </tbody>
</table>
</div>

<div id="modal-table" class="modal hide fade" tabindex="-1">
    <div class="modal-header no-padding">
        <div class="table-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            Materi Kuliah
        </div>
    </div>

    <div class="modal-body no-padding">
        <div class="row-fluid">
            <form class="form-horizontal" method="POST" action="<?= site_url();?>/materi_kuliah/simpan" name="my-form" id="my-form"
							 enctype="multipart/form-data" onsubmit="return cekForm();">
							<input type="hidden" name="id" id="id">

							<div class="control-group">
									<label class="control-label" for="form-field-1">Mata Kuliah</label>

									<div class="controls">
											<select name="kd_mk" id="kd_mk">
												<option value="">-Pilih-</option>
												<?php
												$list_mk = $this->db->get('mata_kuliah');
												foreach($list_mk->result() as $row){
													?>
													<option value="<?= $row->kd_mk;?>"><?= $row->nama_mk;?></option>
													<?php
												}
												 ?>
											</select>
									</div>
							</div>

              <div class="control-group">
									<label class="control-label" for="form-field-2">Semester</label>

									<div class="controls">
											<select name="smt" id="smt">
												<option value="">-Pilih-</option>
												<?php
												$list_smt = $this->model_data->smt();
												foreach($list_smt as $row){
													?>
													<option value="<?= $row;?>"><?= $row;?></option>
													<?php
												}
												 ?>
											</select>
									</div>
							</div>

                <div class="control-group">
                    <label class="control-label" for="form-field-3">Judul</label>

                    <div class="controls">
                        <input type="text" name="judul" id="judul"  class="span8"  />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="form-field-4">File</label>

                    <div class="controls">
                        <input type="file" name="file" id="file"  /><br>
                        <small>format : .docx .xlsx .doc .xls .pdf .txt</small>
                    </div>
                </div>

        </div>
    </div>

    <div class="modal-footer">
        <div class="pagination pull-right no-margin">
        <button type="button" class="btn btn-small btn-danger pull-left" data-dismiss="modal">
            <i class="icon-remove"></i>
            Close
        </button>
        <button type="submit" name="simpan" id="simpan" class="btn btn-small btn-success pull-left">
            <i class="icon-save"></i>
            Simpan
        </button>
		</div>
		</form>
    </div>
</div>
