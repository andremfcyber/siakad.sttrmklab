<div class="widget-box">
	<div class="widget-header">
		<h4><?php echo $judul;?></h4>
	</div>

	<div class="widget-body">
		<div class="widget-main no-padding">

    <?php
    $info = $this->session->flashdata('info');
    if(!empty($info)){
      echo $info;
    }
     ?>

    <form action="<?php echo site_url();?>/import/upload_matakuliah/" method="post" enctype="multipart/form-data">
				<fieldset>
        <input type="file" name="file"/>
        <input type="submit" value="Upload file"/>
			</fieldset>


		<div class="form-actions center">
			<a href="<?php echo base_url();?>assets/import/matakuliah.xls" class="btn btn-small btn-primary"><i class="icon icon-download"></i> Download Format</a>
		</div>

		</form>

    		</div>
    	</div>
    </div>
