<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dosen extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */

	//  function __construct()
 // 	{
	// 		// $this->load->model('model_data');
	// 		$kd_prodi ='A';
	// 		$jurusan = $this->model_data->singkat_jurusan($kd_prodi);
	// 		$level = $this->model_data->getLevel();
	// 		$level_akses = array('super admin','admin');
	// 		 if(in_array($level,$level_akses)){
	// 					$sama = true;
	// 			break;
	// 		}
	// 		if($sama==false){
	// 			redirect('home');
	// 		}
	//
 // 	}

 // public function __construct()
 //  {
 //    parent::__construct();
 //    $this->load->model('Mahasiswa');
 //  }


	public function index()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Dosen";
			$d['class'] = "master";

			//$this->session->unset_userdata('sesi_kd_prodi');
			$_SESSION['sesi_kd_prodi'] = '';
			$_SESSION['sesi_singkat_prodi'] = '';
			$_SESSION['sesi_id_prodi'] = '';

			$d['content'] = 'dosen/form';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function view_data()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Dosen";
			$d['class'] = "master";

			$var = @$_SESSION['sesi_kd_prodi'];
			if(empty($var)){
				$kd_prodi = $this->input->post('cari_jurusan');
			}else{
				$kd_prodi = $var;
			}

			$jurusan = $this->model_data->singkat_jurusan($kd_prodi);
         $id_prodi 	= $this->model_data->id_prodi($kd_prodi);

			if(!empty($jurusan)){
				$_SESSION['sesi_kd_prodi'] = $kd_prodi;
				$_SESSION['sesi_singkat_prodi'] = $jurusan;
				$_SESSION['sesi_id_prodi'] 		= $id_prodi;
				

			}
			$jur = @$_SESSION['sesi_kd_prodi'];


			$d['data'] = $this->model_data->data_dosen($jur);
			$d['content'] = 'dosen/view';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function create_kddosen()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			date_default_timezone_set('Asia/Jakarta');

			$th = substr(date('Y'),2,2);
			$kd_prodi = @$_SESSION['sesi_kd_prodi'];//$this->input->post('prodi'];
			$singkat_prodi = @$_SESSION['sesi_singkat_prodi'];
         $id_prodi = @$_SESSION['sesi_id_prodi'];

			$q = $this->db->query("SELECT MAX(right(kd_dosen,4)) as kode FROM dosen WHERE kd_prodi='$kd_prodi'");
			$row = $q->num_rows();
			if($row>0){
				foreach($q->result() as $dt){
					$no_akhir = (int) $dt->kode+1;
					$d['kode'] = $singkat_prodi.$th.'DS'.sprintf("%04s", $no_akhir);
				}
				echo json_encode($d);
			}else{
				$d['kode'] = $singkat_prodi.$th.'DS'.'0001';
				echo json_encode($d);
			}
		}else{
			redirect('login','refresh');
		}

	}

	public function cari()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$kd_dosen = $this->input->post('kd_dosen');
			$kd_prodi = $this->input->post('kd_prodi');

			// echo json_encode($kd_prodi);


			$id= array('kd_dosen' => $kd_dosen, 'kd_prodi' => $kd_prodi);

			$q = $this->db->get_where("dosen",$id);
			$row = $q->num_rows();
			if($row>0){
				foreach($q->result() as $dt){
					$tgl = $this->model_global->tgl_str($dt->tanggal_lahir);
					$d['jurusan'] = $dt->kd_prodi;
					$d['nidn'] = $dt->nidn;
					$d['kode'] = $dt->kd_dosen;
					$d['nama_dosen'] = $dt->nama_dosen;
					$d['jk'] = $dt->sex;
					$d['tempat_lahir'] = $dt->tempat_lahir;
					$d['tanggal_lahir'] = $tgl;
					$d['alamat'] = $dt->alamat;
					$d['hp'] = $dt->hp;
					$d['pendidikan'] = $dt->pendidikan;
					$d['prodi'] = $dt->prodi;
					$d['status'] = $dt->status;
					$d['id'] = $dt->id;
				}
				echo json_encode($d);
			}else{
				$d['jurusan'] = '';
				$d['nidn'] = '';
				$d['kode'] = '';
				$d['nama_dosen'] = '';
				$d['jk'] = '';
				$d['tempat_lahir'] = '';
				$d['tanggal_lahir'] = '';
				$d['alamat'] = '';
				$d['hp'] = '';
				$d['pendidikan']='';
				$d['prodi'] = '';
				$d['status'] = '';
				$d['id'] = '';
				echo json_encode($d);
			}
		}else{
			redirect('login','refresh');
		}
	}

	public function simpan()
	{

		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			$tgl = $this->model_global->tgl_sql($this->input->post('tanggal_lahir'));

			$kd_dosen = $this->input->post('kode');

			$dt['kd_dosen'] = $kd_dosen;
			$dt['kd_prodi'] = $this->input->post('jurusan');
			$dt['nidn'] = $this->input->post('nidn');
			$dt['nama_dosen'] = $this->input->post('nama_dosen');
			$dt['sex'] = $this->input->post('jk');
			$dt['tempat_lahir'] = $this->input->post('tempat_lahir');
			$dt['tanggal_lahir'] = $tgl;
			$dt['alamat'] = $this->input->post('alamat');
			$dt['hp'] = $this->input->post('hp');
			$dt['pendidikan'] = $this->input->post('pendidikan');
			$dt['prodi'] = $this->input->post('prodi');
			$dt['status'] = $this->input->post('status');
			$dt['password'] = md5($kd_dosen);

      $id_prodi = $this->input->post('id_prodi');

      // if($this->input->post('id')!=null && $this->input->post('id')!=""){
				// $id['id'] = $this->input->post('id');
				$id= array('kd_dosen' => $kd_dosen, 'kd_prodi' => $dt['kd_prodi']);
				$q = $this->db->get_where("dosen",$id);
				$row = $q->num_rows();
				if($row>0){
					$dt['tgl_update'] = date('Y-m-d h:i:s');
					$this->db->update("dosen",$dt,$id);
					echo "Data Sukses diUpdate";
				}else{
					$dt['tgl_masuk'] = date('Y-m-d');
					$dt['tgl_insert'] = date('Y-m-d h:i:s');
					$this->db->insert("dosen",$dt);
					echo "Data Sukses diSimpan";
				}
			// }else{

				// $record['nama_dosen'] 	= $dt['nama_dosen'];
				// $record['tempat_lahir'] 	= $dt['tempat_lahir'];
				// $record['tanggal_lahir']   = $dt['tanggal_lahir'];
				// $record['jenis_kelamin'] 	= $dt['sex'];
				// $record['id_agama']        = 2;
				// $record['nik']					= $dt['ktp'];
				// $record['nisn'] 				= '';
				// $record['npwp'] 				= '';
				// $record['jalan'] 				= $dt['alamat'];
				// $record['dusun'] 				= '';
				// $record['rt'] 					= '';
				// $record['rw'] 					= '';
				// $record['kelurahan'] 		= "Kebon Besar";
				// $record['id_wilayah'] 		= '070000';

				// $record['handphone']       = $dt['hp'];
				// $record['email']           = $dt['email'];
				// $record['kewarganegaraan'] = "ID";

				// $record['id_kebutuhan_khusus_ayah'] 		= '0';
				// $record['nama_ibu_kandung'] 					= '-';
				// $record['id_kebutuhan_khusus_ibu'] 			= '0';
				// $record['id_kebutuhan_khusus_mahasiswa'] 	= '0';
				// $record['penerima_kps'] 						= '0';
				// $record['nomor_kps'] 							= '0';
				// $record['kode_pos'] 								= '';
				// $record['id_jenis_tinggal'] 					= '1';

				
			// }
		}else{
			redirect('login','refresh');
		}

	}

	public function hapus()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['id']	= $this->uri->segment(3);

			$q = $this->db->get_where("dosen",$id);
			$row = $q->num_rows();
			if($row>0){
				$this->db->delete("dosen",$id);
			}
			redirect('dosen/view_data','refresh');
		}else{
			redirect('login','refresh');
		}

	}

	public function reset_password()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$kd_dosen = $this->input->post('kd_dosen'); //$this->uri->segment(3);

			$this->db->where('kd_dosen',$kd_dosen);
			$data = $this->db->get('dosen');
			if($data->num_rows()>0){
				$this->db->where('kd_dosen',$kd_dosen);
				$dt['password'] = md5($kd_dosen);
				$this->db->update('dosen',$dt);
				echo "Reset Password ".$kd_dosen." berhasil";
			}
			// redirect('home','refresh');
		}else{
			redirect('login','refresh');
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
