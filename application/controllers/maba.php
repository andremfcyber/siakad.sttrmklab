<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Maba extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */

	public function index()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Mahasiswa Baru";
			$d['class'] = "master";

			$_SESSION['sesi_kd_prodi'] = '';
			$_SESSION['sesi_singkat_prodi'] = '';
			// $this->session->set_userdata($sess_data);

			$d['content'] = 'maba/form';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function tambah()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			//$d['judul']="Mahasiswa";
			//$d['class'] = "master";
			//$d['content'] = 'mahasiswa/form_mhs';
			//$th_now = date('Y');
			//$th_next = date('Y')+1;
			$th_akademik = $this->model_global->getThAkademikAktif()['th_akademik'];// $th_now.'/'.$th_next;
			$kd_prodi = @$_SESSION['sesi_kd_prodi'];
			$id_prodi = @$_SESSION['sesi_id_prodi'];

			$singkat = @$_SESSION['sesi_singkat_prodi'];

			$nim = $this->create_nim();
			$d = array('judul' => 'Mahasiswa Baru',
						'class' => 'master',
						'th_akademik' => $th_akademik,
						'kd_prodi' => $kd_prodi,
            'prodi' => $kd_prodi,
						'nim' => $nim,
						'status_mhs' => '',
						'nama' => '',
						'ktp' => '',
						'tempat_lahir' => '',
						'tgl_lahir' => '',
						'sex'=>'',
						'alamat'=>'',
						'kota'=>'',
						'telp'=>'',
						'email'=>'',
						'nama_ayah'=>'',
						'nama_ibu'=>'',
						'alamat_ortu'=>'',
						'hp_ortu'=>'',
						'foto' => '',
						'kategori' => 0,
						'data_chart' => 0,
						'content' => 'maba/form_mhs',
						'tgl_masuk' => '',
						'kelas' => '',
						'id_prodi' => $id_prodi
						);
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function view_data()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			$d['judul']= "Mahasiswa Baru";
			$d['class'] = "master";


			$var = @$_SESSION['sesi_kd_prodi'];
			if(empty($var)){
				$kd_prodi = $this->input->post('cari_jurusan');
			}else{
				$kd_prodi = $var;
			}

			$jurusan 	= $this->model_data->singkat_jurusan($kd_prodi);
			$id_prodi 	= $this->model_data->id_prodi($kd_prodi);


			if(!empty($jurusan)){
				$_SESSION['sesi_kd_prodi'] = $kd_prodi;
				$_SESSION['sesi_singkat_prodi'] = $jurusan;
				$_SESSION['sesi_id_prodi'] = $id_prodi;
				// $this->session->set_userdata($sess_data);

			}
			$jur = @$_SESSION['sesi_kd_prodi'];
			$this->db->select('a.*, b.*, b.prodi as kd_prodi');
			$this->db->where('b.prodi',$jur);
			$this->db->where('a.done',1);
			$this->db->where('a.delete',0);
			$this->db->where('a.status_bayar',1);
			$this->db->where('a.mhs',0);
		    $this->db->from('pmb.data_pribadi as b');
		    $this->db->join('pmb.users as a','a.no_pendaftaran=b.no_pendaftaran');
			$this->db->order_by("b.no_pendaftaran", "desc");
		    // $this->db->join('satuan as c','c.id=a.satuan_id','left');
			// $this->db->form('jenis_tagihan')
		    $results = $this->db->get()->result();


			// $config = array();
			// $config['base_url'] 		= site_url('mahasiswa/view_data/');
			// $config['total_rows'] 	= $this->model_data->record_count('mahasiswa');;//200;
			// $config['per_page'] 		= 20;
			// $config['uri_segment'] 	= 3;
			//
			// $this->pagination->initialize($config);
			// $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			// $get_data = $this->model_data->fetch_data($config["per_page"], $page,'mahasiswa');

			// print_r($get_data);die;

			// $d["links"] = $this->pagination->create_links();

			// $d['data'] = $get_data;//$data = $this->model_data->data_mhs($jur);
			$d['data'] = $results;
			$d['content'] = 'maba/view';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function get_json() {
      // $this->load->model('test_model');
      // $results = $this->test_model->load_grid();
			$kd_prodi = @$_SESSION['sesi_kd_prodi'];
			$this->db->select('a.*,b.prodi as nama_prodi');
			$this->db->where('a.kd_prodi',$kd_prodi);

      $this->db->from('mahasiswa as a');
      $this->db->join('prodi as b','b.kd_prodi=a.kd_prodi','left');
		$this->db->order_by("nim", "desc");
      // $this->db->join('satuan as c','c.id=a.satuan_id','left');
			// $this->db->form('jenis_tagihan')
      $results = $this->db->get()->result_array();

      $data = array();
			$no=1;
      foreach ($results  as $r) {
          array_push($data, array(
			  $no++,
			  $r['th_akademik'],
			  $r['nama_prodi'],
              $r['nim'],
              $r['nama_mhs'],
              $r['sex'],
			  $r['hp'],
			  $r['status'],
			  $r['kelas'],
			  // '<a href="#modal-table" class="btn btn-mini btn-primary" onclick="javascript:editData('.$r['nim'].')" data-toggle="modal">Edit</a>'.
              anchor('maba/edit/'.$r['nim'], 'Edit',array('class'=>'btn btn-mini btn-primary')).'  '.
			  anchor('maba/hapus/' . $r['nim'], 'Delete',array("onclick"=>"return confirm('yakin akan menghapus ?')",'class'=>'btn btn-mini btn-danger'))
          ));
      }

      echo json_encode(array('data' => $data));
  }

	public function cari_mhs()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			$no_pendaftaran = $this->input->post('no_pendaftaran');
			$this->db->select('*');
			$this->db->from('pmb.users');
			$this->db->where('pmb.users.no_pendaftaran', $no_pendaftaran);
			$this->db->join('pmb.data_pribadi', 'pmb.data_pribadi.no_pendaftaran = pmb.users.no_pendaftaran');
			$dt['data'] = $this->db->get();

			echo $this->load->view('maba/view_mhs',$dt);

		}else{
			redirect('login','refresh');
		}
	}

	public function cari_nilai()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			$id['th_akademik'] 	= $this->input->post('thak');
			$id['nim'] 			= $this->input->post('nim');

			$dt['data'] = $this->db->get_where("krs",$id);

			echo $this->load->view('maba/view_nilai',$dt);
		}else{
			redirect('login','refresh');
		}
	}

	public function cari_transkrip()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			//$id['th_akademik'] 	= $this->input->post('thak');
			$id['nim'] 			= $this->input->post('nim');

			$dt['data'] = $this->db->get_where("krs",$id);

			echo $this->load->view('maba/view_transkrip',$dt);
		}else{
			redirect('login','refresh');
		}
	}

	public function cari_bayar()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			//$id['th_akademik'] 	= $this->input->post('thak');
			$id['nim'] 			= $this->input->post('nim');

			$dt['data'] = $this->db->get_where("bayar_mhs",$id);

			echo $this->load->view('maba/view_bayar',$dt);
		}else{
			redirect('login','refresh');
		}
	}

	public function create_nim()
	{
// andry
/*
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			date_default_timezone_set('Asia/Jakarta');

			$th = date('Y');
			$kd_prodi = @$_SESSION['sesi_kd_prodi');//$this->input->post('prodi'];
			$singkat_prodi = @$_SESSION['sesi_singkat_prodi'];

			$q = $this->db->query("SELECT MAX(right(nim,4)) as kode FROM mahasiswa WHERE kd_prodi='$kd_prodi'");
			$row = $q->num_rows();
			if($row>0){
				foreach($q->result() as $dt){
					$no_akhir = (int) $dt->kode+1;
					$nim = 'B.'.$th.'.'.'1.1.'.sprintf("%03s", $no_akhir);// $singkat_prodi.$th.sprintf("%04s", $no_akhir);
				}
				//echo json_encode($d);
				//B.2015.1.1.001
			}else{
				$nim = 'B.'.$th.'.'.'1.1.'.'001';// $singkat_prodi.$th.'0001';
				//echo json_encode($d);
			}
			return $nim;
		}else{
			redirect('login','refresh');
		}
*/
	}


	public function edit()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			$no_pendaftaran = $this->uri->segment(3);
			$_SESSION['no_pendaftaran'] = $no_pendaftaran;
			$this->db->select('*');
			$this->db->from('pmb.users');
			$this->db->where('pmb.users.no_pendaftaran', $no_pendaftaran);
			$this->db->join('pmb.data_pribadi', 'pmb.data_pribadi.no_pendaftaran = pmb.users.no_pendaftaran');
			$this->db->join('pmb.data_keluarga', 'pmb.data_keluarga.no_pendaftaran = pmb.users.no_pendaftaran');
			$data_mhs = $this->db->get();
			foreach($data_mhs->result()as $dt){
				$th_akademik = $this->model_global->getThAkademikAktif()['th_akademik'];
				$no_pendaftaran = $dt->no_pendaftaran;
				$nama = $dt->nama;
				$tempat_lahir = $dt->tempat_lahir;
				$tgl_lhr = $this->model_global->tgl_str($dt->tgl_lahir);
				$sex = $dt->jk;
				$alamat = $dt->alamat;
				$kota = '';
				$hp = $dt->tlp;
				$email = $dt->email;
				$nama_ayah  = $dt->nama_ayah;
				$nama_ibu = $dt->nama_ibu;
				$alamat_ortu = $dt->alamat_ortu;
				$hp_ortu = $dt->tlp_ortu;
				$foto = '';
				$tgl_masuk = '';
				$kelas = '';
            $ktp = '';
            $status = '';

			}


			$kd_prodi = $dt->prodi;// @$_SESSION['sesi_kd_prodi'];
			$singkat =  $this->model_data->singkat_jurusan($kd_prodi);// @$_SESSION['sesi_singkat_prodi'];
			$d = array('judul' => 'Mahasiswa Baru',
						'class' => 'master',
						'th_akademik' => $th_akademik,
						'kd_prodi' => $kd_prodi, //.' / '.$singkat
						'prodi' =>$kd_prodi,
						'nim' => '',
						'no_pendaftaran' => $no_pendaftaran,
						'status_mhs' => $status,
						'nama' => $nama,
						'tempat_lahir' => $tempat_lahir,
						'tgl_lahir' => $tgl_lhr,
						'sex'=>$sex,
						'alamat'=>$alamat,
						'kota'=>$kota,
						'telp'=>$hp,
						'email'=>$email,
						'nama_ayah'=>$nama_ayah,
						'nama_ibu'=>$nama_ibu,
						'alamat_ortu'=>$alamat_ortu,
						'hp_ortu'=>$hp_ortu,
						'foto' => $foto,
						'tgl_masuk' => $tgl_masuk,
						'kelas' => $kelas,
						'ktp' => $ktp,
						'status' => $status,
						// 'kategori' => $this->model_data->create_category_krs_nim($nim),
						// 'data_chart' => $this->model_data->create_data_krs_nim($nim),
						'content' => 'maba/form_mhs'
						);
			//print_r($d);

			$this->load->view('home',$d);

		}else{
			redirect('login','refresh');
		}
	}

	public function simpan()
	{

		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			date_default_timezone_set('Asia/Jakarta');
			$tgl_lhr = $this->model_global->tgl_sql($this->input->post('tgl_lahir'));

				 $id['nim'] = $this->input->post('nim');

         $dt['th_akademik'] = $this->input->post('th_akademik');
         $dt['kd_prodi'] = $this->input->post('kd_prodi'); //@$_SESSION['sesi_kd_prodi'];
         $id_prodi = $this->input->post('id_prodi');

         $dt['email'] = $this->input->post('email');


			$thnSkrng = substr($dt['th_akademik'], 0, 4); //'2018';//date('Y');
			$LastNum = $this->model_data->getNimLastNum($thnSkrng,$dt['kd_prodi']);

//				print_r($LastNum);
         // andry

         // cek email sudah ada yg gunakan apa belum.

         $exist = $this->model_global->Qsql_data_exist("email='".$dt['email']."'",'email','mahasiswa');

//         if($exist){
//         	echo "Maaf e-mail ".$dt['email']." Sudah ada yang menggunakan";
//         	exit();
//         }

         // pembuatan NIM
         if ($id['nim'] == ''){
         	if ($LastNum['tahun_ajar'] == ''){
	         	$nimAkhir = 1;
	         }else{
	         	$nimAkhir = $LastNum['no_urut'] + 1;
	         }
	         	$this->model_data->UpdateNimLastNum($thnSkrng,$dt['kd_prodi'],$nimAkhir);

	         $jmlLen = 3;
	         $nimAkhirr = "";
//	         echo strlen($nimAkhir)."<".$jmlLen;
	         if (strlen($nimAkhir)<=$jmlLen){
	         	for($i=0;$i<($jmlLen-strlen($nimAkhir));$i++){
	         		$nimAkhirr .= "0";
	         	}
	         	$nimAkhirr .= $nimAkhir;
	         }
	//                     echo $thnSkrng.$dt['kd_prodi'].$nimAkhirr;
//				print_r($LastNum);
	//			exit();

				$dt['nim'] = $thnSkrng.$dt['kd_prodi'].$nimAkhirr;
			}else{
					$dt['nim'] = $this->input->post('nim');
			}

//			echo $dt['nim'];
//
//			exit();

         $dt['nama_mhs'] = $this->input->post('nama_lengkap');
			$pss = $this->model_global->generateRandomString(10);
			$dt['password'] = md5($this->input->post('nim'));//md5($pss);//md5($this->input->post('nim'));

			/* email */

//			$EmailMessage = "Dear ".$dt['nama_mhs'].",<br/><br/>
//
//						Selamat anda telah menjadi Mahasiswa ".$this->config->item('nama_instansi').". <br/>
//						Berikut kami sampaikan user login & password Anda.<br/><br/>
//
//						Username: ".$dt['nim']." <br/>
//						Password: ".$pss." <br/>
//						URL Site: <a href='".base_url()."'>".$this->config->item('nama_pendek')	."</a> <br/>
//			";
//
//			$this->model_data->KirimEmail($dt['email'],"Pendaftaran Mahasiswa Baru",$EmailMessage);

			/* end email */

			$dt['sex'] 				= $this->input->post('sex');
			$dt['tempat_lahir'] 	= $this->input->post('tempat_lahir');
			$dt['tanggal_lahir'] = $tgl_lhr;
			$dt['alamat'] 			= $this->input->post('alamat');
			$dt['kota'] 			= $this->input->post('kota');
			$dt['hp'] 				= $this->input->post('telp');
			$dt['nama_ayah'] = $this->input->post('nama_ayah');
			$dt['nama_ibu'] = $this->input->post('nama_ibu');
			$dt['alamat_ortu']	= $this->input->post('alamat_ortu');
			$dt['hp_ortu'] 				= $this->input->post('hp_ortu');
			$dt['tgl_masuk'] 		= $this->model_global->tgl_sql($this->input->post('tgl_masuk'));
			$dt['kelas'] 			= $this->input->post('kelas');

			$dt['ktp'] 				= $this->input->post('ktp');


			//add by riwan
			$dt['status'] = $this->input->post('status');


			$q = $this->db->get_where("mahasiswa",$id);
			$row = $q->num_rows();


			$record['nama_mahasiswa'] 	= $dt['nama_mhs'];
			$record['tempat_lahir'] 	= $dt['tempat_lahir'];
			$record['tanggal_lahir']   = $dt['tanggal_lahir'];
			$record['jenis_kelamin'] 	= $dt['sex'];
			$record['id_agama']        = 2;
			$record['nik']					= $dt['ktp'];
			$record['nisn'] 				= '';
			$record['npwp'] 				= '';
			$record['jalan'] 				= $dt['alamat'];
			$record['dusun'] 				= '';
			$record['rt'] 					= '';
			$record['rw'] 					= '';
			$record['kelurahan'] 		= "Kebon Besar";
			$record['id_wilayah'] 		= '070000';

			$record['handphone']       = $dt['hp'];
			$record['email']           = $dt['email'];
			$record['kewarganegaraan'] = "ID";

			$record['id_kebutuhan_khusus_ayah'] 		= '0';
			$record['nama_ibu_kandung'] 					= '-';
			$record['id_kebutuhan_khusus_ibu'] 			= '0';
			$record['id_kebutuhan_khusus_mahasiswa'] 	= '0';
			$record['penerima_kps'] 						= '0';
			$record['nomor_kps'] 							= '0';
			$record['kode_pos'] 								= '';
			$record['id_jenis_tinggal'] 					= '1';

			if($row>0){

				// $dt['tgl_update'] = date('Y-m-d h:i:s');
				// $this->db->update("mahasiswa",$dt,$id);
				echo "NIM sudah ada.";
			}else{
				$dt['tgl_insert'] = date('Y-m-d h:i:s');
				$this->db->insert("mahasiswa",$dt);
				$dt_users = array(
					'mhs' => 1,
				);
				$this->db->where('no_pendaftaran', $_SESSION['no_pendaftaran']);
				$this->db->update('pmb.users', $dt_users);
				echo "Data Sukses diSimpan";
			}
		}else{
			redirect('login','refresh');
		}

	}

	public function simpan_ortu()
	{

		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			date_default_timezone_set('Asia/Jakarta');

			$nim = $this->input->post('nim');
			$data = $this->model_data->cari_data_mhs($nim);
			if($data->num_rows()>0){
				$id['nim'] = $this->input->post('nim');

				$dt['nama_ayah'] = $this->input->post('nama_ayah');
				$dt['nama_ibu'] = $this->input->post('nama_ibu');
				$dt['alamat_ortu'] = $this->input->post('alamat_ortu');
				$dt['hp_ortu'] = $this->input->post('hp_ortu');

				$record['nama_ayah'] 					= $dt['nama_ayah'];
            $record['nama_ibu_kandung'] 			= $dt['nama_ibu'];

				$q = $this->db->get_where("mahasiswa",$id);
				$row = $q->num_rows();
				if($row>0){
					$dt['tgl_update'] = date('Y-m-d h:i:s');
					$this->db->update("mahasiswa",$dt,$id);
					echo "Data Sukses diUpdate";
				}else{
					$dt['tgl_insert'] = date('Y-m-d h:i:s');
					$this->db->insert("mahasiswa",$dt);
					echo "Data Sukses diSimpan";
				}
			}else{
				echo "Maaf, NIM belum disimpan";
			}
		}else{
			redirect('login','refresh');
		}

	}

	public function hapus()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['nim']	= $this->uri->segment(3);

			$q = $this->db->get_where("mahasiswa",$id);
			$row = $q->num_rows();
			if($row>0){
				$this->db->delete("mahasiswa",$id);
			}
			redirect('maba/view_data','refresh');
		}else{
			redirect('login','refresh');
		}

	}

	public function reset_password()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$nim = $this->input->post('nim'); //$this->uri->segment(3);

			$this->db->where('nim',$nim);
			$data = $this->db->get('mahasiswa');
			if($data->num_rows()>0){
				$this->db->where('nim',$nim);
				$dt['password'] = md5($nim);
				$this->db->update('mahasiswa',$dt);
				echo "Reset Password ".$nim." berhasil";
			}
			// redirect('home','refresh');
		}else{
			redirect('login','refresh');
		}
	}

	public function update_status(){
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			date_default_timezone_set('Asia/Jakarta');
			$nim['nim'] = $this->input->post('nim');

			$data['status'] = $this->input->post('status');

			$q = $this->db->get_where("mahasiswa",$nim);
			if($q->num_rows()>0){
				
				$data['tgl_update'] = date('Y-m-d');
				$this->db->update('mahasiswa',$data,$nim);
				echo "Status Sukses diUpdate";
			}else{
				echo "tidak ada aksi";
			}

		}else{
			redirect('login','refresh');
		}
	}

	public function update_kelas(){
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			date_default_timezone_set('Asia/Jakarta');
			$nim['nim'] = $this->input->post('nim');

			$data['kelas'] = $this->input->post('kelas');

			$q = $this->db->get_where("mahasiswa",$nim);
			if($q->num_rows()>0){
				
				$data['tgl_update'] = date('Y-m-d');
				$this->db->update('mahasiswa',$data,$nim);
				echo "Kelas Sukses diUpdate";
			}else{
				echo "tidak ada aksi";
			}

		}else{
			redirect('login','refresh');
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
