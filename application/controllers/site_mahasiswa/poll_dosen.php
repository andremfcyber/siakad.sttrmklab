<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Poll_dosen extends CI_Controller {

    /**
     * Index Page for this controller.
     * Programmer : Deddy Rusdiansyah.S.Kom
     * http://deddyrusdiansyah.blogspot.com
     * http://softwarebanten.com
     * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
     * Developer : Fitria Wahyuni.S.Pd
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('model_polling');
    }

    public function index() {
        $cek = @$_SESSION['logged_in'];
        $level = @$_SESSION['level'];
        if (!empty($cek) && $level == 'mahasiswa') {

            $th_akademik = $this->model_global->getThAkademikAktif()['th_akademik']; //2017/2018
            $nim = @$_SESSION['username'];
            $jenis = 'DOSEN';

            $this->db->where('a.th_akademik', $th_akademik);
            $this->db->where('a.nim', $nim);
            $this->db->where('b.nama', $jenis);
            $this->db->from('poll_jawaban as a');
            $this->db->join('poll_jenis as b', 'a.poll_jenis_id=b.id');
            $get = $this->db->get()->num_rows();

            $kd_prodi = @$_SESSION['kd_prodi'];
            $nama_prodi = $this->model_data->getInfoProdi($kd_prodi)['prodi'];

            $this->db->select('kd_dosen');
            $this->db->where('kd_dosen !=', 'NULL');
            $this->db->where('th_akademik', $th_akademik);
            $this->db->where('nim', $nim);
            $this->db->from('poll_jawaban');
            $get_dosen_pilih = $this->db->get();
            // print_r($list_dosen_pilih);die;
            $arr = array();
            foreach ($get_dosen_pilih->result() as $value) {
                $arr[] = $value->kd_dosen;
            }
            $json_arr = $arr;

            $this->db->select('a.kd_dosen,a.nama_dosen');
            $this->db->where('a.kd_prodi', $kd_prodi);
            $this->db->where('a.status', 'Aktif');
            if (!empty($json_arr)) {
                $this->db->where_not_in('a.kd_dosen', $json_arr);
            }
            $this->db->from('dosen as a');
            $this->db->order_by('a.nama_dosen');
            $get_dosen = $this->db->get();
            // print_r($this->db->last_query());die;
            
            $d['judul'] = "Kuisioner Dosen";
            $d['sub_judul'] = "Kuisioner Dosen Program Studi " . $nama_prodi . " Tahun Akademik " . $th_akademik;
            $d['class'] = "polling";
            $d['list_dosen'] = $get_dosen;
            // $d['list_dosen_pilih'] = $list_dosen_pilih;
            // if($get > 0 )
            // {
            // 	$d['content']= 'site_mahasiswa/polling/warning';
            // }else{
            $d['content'] = 'site_mahasiswa/polling/poll_dosen';
            // }
            $this->load->view('site_mahasiswa/home', $d);
        } else {
            redirect('login', 'refresh');
        }
    }

    public function simpan() {
        $cek = @$_SESSION['logged_in'];
        $level = @$_SESSION['level'];
        if (!empty($cek) && $level == 'mahasiswa') {
            date_default_timezone_set('Asia/Jakarta');

            $th_akademik = $this->model_global->getThAkademikAktif()['th_akademik'];
            $nim = @$_SESSION['username'];
            $kd_prodi = @$_SESSION['kd_prodi'];
            // print_r($_POST);die;

            $keyth['th_akademik'] = $th_akademik;
            $keyth['nim'] = $nim;
            $keyth['poll_jenis_id'] = $this->model_polling->getIDJenis('DOSEN')['id'];
            $keyth['kd_dosen'] = $this->input->post('kd_dosen');


            $dth['th_akademik'] = $th_akademik;
            $dth['nim'] = $nim;
            $dth['poll_jenis_id'] = $this->model_polling->getIDJenis('DOSEN')['id'];
            $dth['kd_dosen'] = $this->input->post('kd_dosen');
            $dth['kd_prodi'] = $kd_prodi;
            $dth['user_id'] = @$_SESSION['username'];
            $dth['insert_date'] = date('Y-m-d H:i:s');

            $get = $this->db->get_where('poll_jawaban', $keyth);
            if ($get->num_rows() > 0) {
                $this->session->set_flashdata('info', 'Maaf, Anda sudah pernah mengisi Polling');
            } else {
                
                $this->db->insert('poll_jawaban', $dth);
                $dtd['poll_jawaban_id'] = $this->db->insert_id();
                // if(empty($dtd['poll_jawaban_id'])){
                //   echo("kosong");exit;
                // }else{
                //   echo($dtd['poll_jawaban_id']);exit;
                // }
                // echo"nice";exit;
                
                //$dtd['poll_jawaban_id'] = $this->model_polling->getIDJawaban($keyth);
                

                $get_pertanyaan = $this->model_polling->getDataPertanyaan($th_akademik, 'DOSEN');
                $row = $get_pertanyaan->row();
                $exp_pertanyaan = $row->poll_pertanyaan_id;
                $pertanyaan = explode(',', $exp_pertanyaan);
                foreach ($pertanyaan as $value) {
                    $dtd['poll_pertanyaan_id'] = $this->input->post('tanya_' . $value);
                    $dtd['jawaban'] = $this->input->post('jawab_' . $value);
                    // echo $tanya.' - '.$jawab.'<br/>';
                    $this->db->insert('poll_jawaban_detail', $dtd);
                }
                $this->session->set_flashdata('info', 'Silahkan Isi Kuesioner untuk DOSEN lainnya..!!');
                redirect('site_mahasiswa/poll_dosen', 'refresh');
            }
        } else {
            redirect('login', 'refresh');
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
