<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Info_kampus extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */

	public function index()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Info Kampus";
			$d['class'] = "transaksi";
			$d['data'] = $this->db->order_by('id','DESC')->get('info');

			$d['content'] = 'info_kampus/view';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function cari()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['id']	= $this->input->post('cari');

			$q = $this->db->get_where("info",$id);
			$row = $q->num_rows();
			if($row>0){
				foreach($q->result() as $dt){
					$d['id'] = $dt->id;
					$d['jenis'] = $dt->jenis;
					$d['judul'] = $dt->judul;
					$d['info'] = $dt->info;
					$d['pengirim'] = $dt->pengirim;
				}
			}else{
        $d['id'] = '';
				$d['jenis'] = '';        
				$d['judul'] = '';
				$d['info'] = '';
				$d['pengirim'] = '';

			}
			echo json_encode($d);
		}else{
			redirect('login','refresh');
		}
	}

	public function simpan()
	{

		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['id'] = (int) $this->input->post('id');
      $dt['jenis'] = $this->input->post('jenis');
      $dt['judul'] = $this->input->post('judul');
			$dt['info'] = $this->input->post('info');
			$dt['pengirim'] = $this->input->post('pengirim');


			$dt['username'] = @$_SESSION['username'];
			$dt['insert_date'] = date('Y-m-d H:i:s');

			/* email */

         	//$this->model_data->SubscribeEmail($dt['judul'],$dt['info']."<br/><br/>".$dt['pengirim']);

			/* end email */

			$q = $this->db->get_where("info",$id);
			$row = $q->num_rows();
			if($row>0){
				$this->db->update("info",$dt,$id);
				echo "Data Sukses diUpdate";
			}else{

				$dt['id'] = $this->model_global->MaxFromTable('id','info');

				$dt['tgl'] = date('Y-m-d');
				$this->db->insert("info",$dt);
				echo "Data Sukses diSimpan";
			}
		}else{
			redirect('login','refresh');
		}

	}

	public function hapus()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['id']	= $this->uri->segment(3);

			$q = $this->db->get_where("info",$id);
			$row = $q->num_rows();
			if($row>0){
				$this->db->delete("info",$id);
			}
			redirect('info_kampus','refresh');
		}else{
			redirect('login','refresh');
		}

	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
