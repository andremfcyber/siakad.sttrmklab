<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pembayaran extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */

	public function index()
	{
            $cek = @$_SESSION['logged_in'];
            $level = @$_SESSION['level'];
            if(!empty($cek) && $level=='admin'){
                $d['judul']="Pembayaran Mahasiswa";
                $d['class'] = "keuangan";
                $d['th_akademik_kode'] = $this->model_global->getThAkademikAktif()['kode'];
                $d['th_akademik'] = $this->model_global->getThAkademikAktif()['th_akademik'];
                $d['semester'] = $this->model_global->getThAkademikAktif()['semester'];
                $d['nomor'] = $this->generateNomor();
                $d['tanggal'] = date('d-m-Y');
                $d['content'] = 'pembayaran/create';
                $this->load->view('home',$d);
            }else{
                redirect('login','refresh');
            }
	}

	public function generateNomor()
	{
            $th = date('Y');
            $this->db->select('MAX(nomor) as akhir');
            $this->db->where('date_part(\'year\', tanggal) = \''.$th.'\'');
            $data = $this->db->get('bayar_mhs');
            if($data->num_rows()>0)
            {
                $row = $data->row();
                $akhir = substr($row->akhir,6,4)+1;
                $hasil = 'BY'.$th.sprintf("%04s",$akhir);
            }else{
                $hasil = 'BY'.$th.'00001';
            }
            return $hasil;
	}

	public function cari_total_sks()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$nim = $this->input->post('nim');
			$smt = $this->input->post('smt');
			$th_akademik = $this->input->post('th_akademik_kode');

			$this->db->select('SUM(sks) as t_sks');
			$this->db->where('nim',$nim);
			$this->db->where('smt',$smt);
			$this->db->where('th_akademik',$th_akademik);
			$dt = $this->db->get('krs');
			if($dt->num_rows()>0)
			{
				$row = $dt->row();
				$hasil = array('sks' => $row->t_sks);
			}else{
				$hasil = array('sks' => 0);
			}

      echo json_encode($hasil);
		}else{
			redirect('login','refresh');
		}
	}
	public function cari_mhs()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$nim = $this->input->post('nim');
      $dt = $this->model_data->getInfoMhs($nim);
      echo json_encode($dt);
		}else{
			redirect('login','refresh');
		}
	}

	public function cari_tagihan()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$th_akademik = $this->input->post('th_akademik');
			$kd_prodi = $this->input->post('kd_prodi');
			$smt = $this->input->post('smt');
			$max_smt = $this->input->post('max_smt');

			$this->db->where('th_akademik',$th_akademik);
			$this->db->where('kd_prodi',$kd_prodi);
			$this->db->where('smt <=',$smt);
			$data = $this->db->get('jenis_tagihan')->result();
//			echo $this->db->last_query();
			echo "<option value=''>-Pilih-</option>";
			foreach($data as $row){
				echo "<option value='$row->id'>".$row->nama." Semester ".$row->smt." | Rp. ".number_format($row->jumlah)."</option>";
			}
		}else{
			redirect('login','refresh');
		}
	}

	public function jumlah_tagihan()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$nim = $this->input->post('nim');
			$id = $this->input->post('id');
      $this->db->where('id',$id);
			$data = $this->db->get('jenis_tagihan');
			if($data->num_rows()>0)
			{
				$row = $data->row();
				$tagihan = $row->jumlah;
				$bayar = $this->model_data->getJumlahPembayaran($id,$nim);
				if($row->x_sks=='ya'){
					$sisa = $tagihan;
				}else{
					$sisa = $tagihan - $bayar;
				}
				$dt = array('jumlah' => $sisa,
									'smt' => $row->smt,
									'x_sks' => $row->x_sks
								);
			}else{
				$dt = array('jumlah' => 0,
									'smt' => 0,
									'x_sks' => ''
								);
			}
      echo json_encode($dt);
		}else{
			redirect('login','refresh');
		}
	}


  public function data_bayar()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$nim = $this->input->post('nim');
      $this->db->where('nim',$nim);
      $dt['data'] = $this->db->get('bayar_mhs')->result();
      $this->load->view('pembayaran/data_bayar',$dt);
		}else{
			redirect('login','refresh');
		}
	}

  // public function data_pembayaran()
	// {
	// 	$cek = @$_SESSION['logged_in'];
	// 	$level = @$_SESSION['level'];
	// 	if(!empty($cek) && $level=='admin'){
	// 		$tagihan_mhs_id = $this->input->post('tagihan_mhs_id');
  //     $this->db->where('tagihan_mhs_id',$tagihan_mhs_id);
  //     $dt['data'] = $this->db->get('pembayaran_mhs')->result();
  //     $this->load->view('pembayaran/data_bayar',$dt);
	// 	}else{
	// 		redirect('login','refresh');
	// 	}
	// }

  public function simpan()
	{
            $cek = @$_SESSION['logged_in'];
            $level = @$_SESSION['level'];
            if(!empty($cek) && $level=='admin'){
                date_default_timezone_set('Asia/Jakarta');

                $nomor = $this->input->post('nomor');
                $tanggal = $this->model_global->tgl_sql($this->input->post('tanggal'));
                $jenis_tagihan_id = (int)$this->input->post('jenis_tagihan_id');
                $th_akademik_kode = $this->input->post('th_akademik_kode');
                $th_akademik = $this->input->post('th_akademik');
                $nim = $this->input->post('nim');
                $smt = (int)$this->input->post('smt');

                $jumlah_awal = (int)$this->input->post('jumlah');

                $jumlah = (int)$this->input->post('total_bayar');
                $keterangan = $this->input->post('keterangan');
                $sks = (int)$this->input->post('sks');
                $isi_krs = (int)$this->input->post('isi_krs');

                $dt_isi_krs = array(
                		'isi_krs' => $isi_krs,
                );

                $ids['id']	= $jenis_tagihan_id;
                $qq = $this->db->get_where("jenis_tagihan",$ids);

                foreach($qq->result() as $dts){
                	$nama_tagihan = $dts->nama;
                }
//                echo $qs['nama'];
//                exit();

		// $nama_tagihan = $this->model_data->getInfoTagihan($tagihan_mhs_id)['nama_tagihan'];

                $dt = array('nomor' => $nomor,
                    'tanggal' => $tanggal,
                    'jenis_tagihan_id' => $jenis_tagihan_id,
                    'th_akademik_kode' => $th_akademik_kode,
                    'th_akademik' => $th_akademik,
                    'nim' => $nim,
                    'smt' => $smt,
                    'sks' => $sks,
                    'jumlah' => $jumlah,
                    'keterangan' => $keterangan
                );

                $this->db->where('nomor',$nomor);
                $data =$this->db->get('bayar_mhs');
                if($data->num_rows()>0){
                    $dt['user_id'] = @$_SESSION['username'];
                    $dt['update_date'] = date('Y-m-d H:i:s');
                    $this->db->where('nomor',$nomor);
                    $this->db->update('bayar_mhs',$dt);
                    
                    // update field isi_krs di tabel mahasiswa
                    $this->db->where('nim',$nim);
                    $this->db->update('mahasiswa',$dt_isi_krs);
                    echo "Data Berhasil di Edit";
                }else{
                    $dt['user_id'] = @$_SESSION['username'];
                    $dt['insert_date'] = date('Y-m-d H:i:s');
                    $this->db->insert('bayar_mhs',$dt);

                    $this->db->where('nim',$nim);
                    $this->db->update('mahasiswa',$dt_isi_krs);
                    echo "Data Berhasil di SIMPAN";
                }

                // andry
                // update menjadi mahasiswa aktif jika total bayar sudah 0
                // dan jenis pembayaran Uang Registrasi

                if ( (($jumlah_awal - $jumlah) == 0) && ($nama_tagihan == 'Uang Registrasi')){
                    $dtm['status'] = 'Aktif';
                    $this->db->where('nim',$nim);
                    $this->db->update('mahasiswa',$dtm);
                }

            }else{
                redirect('login','refresh');
            }
	}

	// cari_data
	public function cari_data()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id = $this->input->post('id');

			$this->db->where('id',$id);
			$data = $this->db->get('bayar_mhs');
			if($data->num_rows()>0)
			{
				$row = $data->row();
				$hasil = array(
						'nomor' => $row->nomor,
						'tanggal' => $this->model_global->tgl_str($row->tanggal),
						'th_akademik' => $row->th_akademik,
						'th_akademik_kode' => $row->th_akademik_kode,
						// 'nim' => $row->nim,
						'jenis_tagihan_id' => $row->jenis_tagihan_id,
						'jumlah' => $row->jumlah,
						'keterangan' => $row->keterangan
				);
			}else{
				$hasil = array(
					'nomor' => '',
					'tanggal' => '',
					'th_akademik' => '',
					'th_akademik_kode' => '',
					// 'nim' => $row->nim,
					'jenis_tagihan_id' => '',
					'jumlah' => '',
					'keterangan' => ''
				);
			}

			echo json_encode($hasil);

		}else{
			redirect('login','refresh');
		}
	}


	public function cetak()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

      $this->load->view('pembayaran/cetak');
		}else{
			redirect('login','refresh');
		}
	}

	public function hapus()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id = $this->input->post('id');//$this->uri->segment(3);

			$this->db->where('id',$id);
			$data = $this->db->get('bayar_mhs');
			if($data->num_rows()>0)
			{
				$this->db->where('id',$id);
				$data = $this->db->delete('bayar_mhs');
				echo "Data berhasil di Hapus";
			}
      // $this->load->view('pembayaran/cetak');
			// redirect('pembayaran');
		}else{
			redirect('login','refresh');
		}
	}




}
