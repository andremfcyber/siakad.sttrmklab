<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ValidasiWisuda extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */

	public function index()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Validasi Wisuda";
			$d['class'] = "keuangan";
      // $d['th_akademik_aktif'] = $this->model_global->getThAkademikAktif()['kode'];
      $d['data'] = $this->db->order_by('id','DESC')->get('wisuda_validasi');
			$d['content'] = 'validasiwisuda/view';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

  public function create()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Tambah Validasi Wisuda";
			$d['class'] = "keuangan";
      $d['th_akademik_aktif'] = $this->model_global->getThAkademikAktif()['kode'];
      // $d['data'] = $this->db->order_by('id','DESC')->get('dispensasi');
      $d['nim'] = '';
			$d['content'] = 'validasiwisuda/create';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

  public function edit()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Edit Validasi Wisuda";
			$d['class'] = "keuangan";
      $d['th_akademik_aktif'] = $this->model_global->getThAkademikAktif()['kode'];
      // $d['data'] = $this->db->order_by('id','DESC')->get('dispensasi');
      $d['nim'] = $this->uri->segment(3);
			$d['content'] = 'validasiwisuda/create';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

  public function cari_mhs()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$nim = $this->input->post('nim');
      $dt = $this->model_data->getInfoMhs($nim);
      echo json_encode($dt);
		}else{
			redirect('login','refresh');
		}
	}

  public function cari_data()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$nim = $this->input->post('nim');
      $th_akademik_kode = $this->input->post('th_akademik');

      $dt = $this->model_data->getInfoMhs($nim);

      $this->db->where('nim',$nim);
      $this->db->where('th_akademik_kode',$th_akademik_kode);
      $data = $this->db->get('wisuda_validasi');
      if($data->num_rows()>0)
      {
        $row = $data->row();
        $dt['keterangan'] = $row->keterangan;
      }else{
        $dt['keterangan'] = '';
      }

      echo json_encode($dt);
		}else{
			redirect('login','refresh');
		}
	}



  public function simpan()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
      date_default_timezone_set('Asia/Jakarta');

      $nim = $this->input->post('nim');
      $kd_prodi = $this->input->post('kd_prodi');
      $kelas = $this->input->post('kelas');
      $th_akademik_kode = $this->input->post('th_akademik');
      $th_angkatan_kode = $this->input->post('th_angkatan');
      $smt =  $this->model_global->semester($nim,$th_akademik_kode);
      $keterangan = $this->input->post('keterangan');

      $dt = array(
                'nim' => $nim,
                'kd_prodi' => $kd_prodi,
                'kelas' => $kelas,
                'tanggal' => date('Y-m-d'),
                'th_akademik_kode' => $th_akademik_kode,
                // 'th_angkatan_kode' => $th_angkatan_kode,
                'smt' => $smt,
                'keterangan' => $keterangan,
                'user_id' => @$_SESSION['username']
                );


      $id = array('th_akademik_kode' => $th_akademik_kode,
                  'nim' => $nim
                  );
      $data = $this->db->get_where('wisuda_validasi',$id);
      if($data->num_rows()>0){
				// $dt['update_date'] = date('Y-m-d H:i:s');
        // $this->db->update('tagihan_mhs',$dt,$id);
        echo "Dispensasi Mahasiswa sudah pernah Dibuat";
      }else{
				$dt['insert_date'] = date('Y-m-d H:i:s');
        $this->db->insert('wisuda_validasi',$dt);
        echo "Data Mahasiswa berhasil di SIMPAN";
      }

		}else{
			redirect('login','refresh');
		}
	}

  public function hapus()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['id']	= $this->uri->segment(3);

			$q = $this->db->get_where("wisuda_validasi",$id);
			$row = $q->num_rows();
			if($row>0){
				$this->db->delete("wisuda_validasi",$id);

			}
			redirect('validasiwisuda','refresh');
		}else{
			redirect('login','refresh');
		}

	}

}
