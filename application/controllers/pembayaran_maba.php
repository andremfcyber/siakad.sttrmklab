<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pembayaran_maba extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */

	public function index()
	{
            $cek = @$_SESSION['logged_in'];
            $level = @$_SESSION['level'];
            if(!empty($cek) && $level=='admin'){

            	// redirect('home');
                $d['judul']="Pembayaran Mahasiswa Baru";
                $d['class'] = "keuangan";
                $d['th_akademik_kode'] = $this->model_global->getThAkademikAktif()['kode'];
                $d['th_akademik'] = $this->model_global->getThAkademikAktif()['th_akademik'];
                $d['semester'] = $this->model_global->getThAkademikAktif()['semester'];
                $d['nomor'] = $this->generateNomor();
                $d['tanggal'] = date('d-m-Y');
                $d['content'] = 'pembayaran_maba/create';
                $this->load->view('home',$d);
            }else{
                redirect('login','refresh');
            }
	}

	public function generateNomor()
	{
            $th = date('Y');
            $this->db->select('MAX(nomor) as akhir');
            $this->db->where('date_part(\'year\', tanggal) = \''.$th.'\'');
            $data = $this->db->get('bayar_maba');
            if($data->num_rows()>0)
            {
                $row = $data->row();
                $akhir = substr($row->akhir,6,4)+1;
                $hasil = 'BY'.$th.sprintf("%04s",$akhir);
            }else{
                $hasil = 'BY'.$th.'00001';
            }
            return $hasil;
	}

	public function cari_mhs()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$no_pendaftaran = $this->input->post('no_pendaftaran');
			$this->db->select('pmb.users.no_pendaftaran, pmb.users.status_bayar, pmb.users.mhs, pmb.data_pribadi.nama, pmb.data_pribadi.prodi as kd_prodi, prodi.prodi, prodi.singkat');
			$this->db->from('pmb.users');
			$this->db->where('pmb.users.no_pendaftaran', $no_pendaftaran);
			$this->db->join('pmb.data_pribadi', 'pmb.data_pribadi.no_pendaftaran = pmb.users.no_pendaftaran');
			$this->db->join('prodi', 'prodi.kd_prodi = pmb.data_pribadi.prodi');
      $dt = $this->db->get()->row();
      echo json_encode($dt);
		}else{
			redirect('login','refresh');
		}
	}

	public function cari_tagihan()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			$data = $this->db->get('master_tagihan_maba')->result();

			echo "<option value=''>-Pilih-</option>";
			foreach($data as $row){
				echo "<option value='$row->id'>".$row->nama_tagihan." | Rp. ".number_format($row->jumlah)."</option>";
			}
		}else{
			redirect('login','refresh');
		}
	}

	public function jumlah_tagihan()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$no_pendaftaran = $this->input->post('no_pendaftaran');
			$id = $this->input->post('id');
      $this->db->where('id',$id);
			$data = $this->db->get('master_tagihan_maba');
			if($data->num_rows()>0)
			{
				$row = $data->row();
				$tagihan = $row->jumlah;
				$bayar = $this->model_data->getJumlahPembayaranMaba($id,$no_pendaftaran);
				$sisa = $tagihan - $bayar;
				
				$dt = array('jumlah' => $sisa);
			}else{
				$dt = array('jumlah' => 0);
			}
      echo json_encode($dt);
		}else{
			redirect('login','refresh');
		}
	}


  public function data_bayar()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$no_pendaftaran = $this->input->post('no_pendaftaran');
      $this->db->where('no_pendaftaran',$no_pendaftaran);
      $dt['data'] = $this->db->get('bayar_maba')->result();
      $this->load->view('pembayaran_maba/data_bayar',$dt);
		}else{
			redirect('login','refresh');
		}
	}

  // public function data_pembayaran()
	// {
	// 	$cek = @$_SESSION['logged_in'];
	// 	$level = @$_SESSION['level'];
	// 	if(!empty($cek) && $level=='admin'){
	// 		$tagihan_mhs_id = $this->input->post('tagihan_mhs_id');
  //     $this->db->where('tagihan_mhs_id',$tagihan_mhs_id);
  //     $dt['data'] = $this->db->get('pembayaran_mhs')->result();
  //     $this->load->view('pembayaran/data_bayar',$dt);
	// 	}else{
	// 		redirect('login','refresh');
	// 	}
	// }

  public function simpan()
	{
    $cek = @$_SESSION['logged_in'];
    $level = @$_SESSION['level'];
    if(!empty($cek) && $level=='admin'){
        date_default_timezone_set('Asia/Jakarta');

        $nomor = $this->input->post('nomor');
        $tanggal = $this->model_global->tgl_sql($this->input->post('tanggal'));
        $jenis_tagihan_maba_id = (int)$this->input->post('master_tagihan_maba');
        $th_akademik_kode = $this->input->post('th_akademik_kode');
        $th_akademik = $this->input->post('th_akademik');
        $no_pendaftaran = $this->input->post('no_pendaftaran');

        $jumlah_awal = (int)$this->input->post('jumlah');

        $jumlah = (int)$this->input->post('total_bayar');
        $keterangan = $this->input->post('keterangan');

        $status_bayar = (int)$this->input->post('status_bayar');

        $dt_status_bayar = array(
        		'status_bayar' => $status_bayar,
        );

        $ids['id']	= $jenis_tagihan_maba_id;
        $qq = $this->db->get_where("master_tagihan_maba",$ids);

        foreach($qq->result() as $dts){
        	$nama_tagihan = $dts->nama_tagihan;
        }

        $dt = array(
        		'nomor' => $nomor,
            'tanggal' => $tanggal,
            'jenis_tagihan_maba_id' => $jenis_tagihan_maba_id,
            'th_akademik_kode' => $th_akademik_kode,
            'th_akademik' => $th_akademik,
            'no_pendaftaran' => $no_pendaftaran,
            'jumlah' => $jumlah,
            'keterangan' => $keterangan
        );

        $this->db->where('nomor',$nomor);
        $data =$this->db->get('bayar_maba');
        if($data->num_rows()>0){
            $dt['user_id'] = @$_SESSION['username'];
            $dt['update_date'] = date('Y-m-d H:i:s');
            $this->db->where('nomor',$nomor);
            $this->db->update('bayar_maba',$dt);
            
            // update field status_bayar di tabel pmb.users
            $this->db->where('no_pendaftaran',$no_pendaftaran);
            $this->db->update('pmb.users',$dt_status_bayar);
            echo "Data Berhasil di Edit";
        }else{
            $dt['user_id'] = @$_SESSION['username'];
            $dt['insert_date'] = date('Y-m-d H:i:s');
            $this->db->insert('bayar_maba',$dt);

            $this->db->where('no_pendaftaran',$no_pendaftaran);
            $this->db->update('pmb.users',$dt_status_bayar);
            echo "Data Berhasil di SIMPAN";
        }

        // andry
        // update menjadi mahasiswa aktif jika total bayar sudah 0
        // dan jenis pembayaran Uang Registrasi

        // if ( (($jumlah_awal - $jumlah) == 0) && ($nama_tagihan == 'Uang Registrasi')){
        //     $dtm['status'] = 'Aktif';
        //     $this->db->where('nim',$nim);
        //     $this->db->update('mahasiswa',$dtm);
        // }

    }else{
        redirect('login','refresh');
    }
	}

	// cari_data
	public function cari_data()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id = $this->input->post('id');

			$this->db->where('id',$id);
			$data = $this->db->get('bayar_maba');
			if($data->num_rows()>0)
			{
				$row = $data->row();
				$hasil = array(
						'nomor' => $row->nomor,
						'tanggal' => $this->model_global->tgl_str($row->tanggal),
						'th_akademik' => $row->th_akademik,
						'th_akademik_kode' => $row->th_akademik_kode,
						'no_pendaftaran' => $row->no_pendaftaran,
						'jenis_tagihan_maba_id' => $row->jenis_tagihan_maba_id,
						'jumlah' => $row->jumlah,
						'keterangan' => $row->keterangan
				);
			}else{
				$hasil = array(
					'nomor' => '',
					'tanggal' => '',
					'th_akademik' => '',
					'th_akademik_kode' => '',
					'no_pendaftaran' => $row->no_pendaftaran,
					'jenis_tagihan_maba_id' => '',
					'jumlah' => '',
					'keterangan' => ''
				);
			}

			echo json_encode($hasil);

		}else{
			redirect('login','refresh');
		}
	}


	public function cetak()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

      $this->load->view('pembayaran_maba/cetak');
		}else{
			redirect('login','refresh');
		}
	}

	public function hapus()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id = $this->input->post('id');//$this->uri->segment(3);

			$this->db->where('id',$id);
			$data = $this->db->get('bayar_maba');
			if($data->num_rows()>0)
			{
				$this->db->where('id',$id);
				$data = $this->db->delete('bayar_maba');
				echo "Data berhasil di Hapus";
			}
      // $this->load->view('pembayaran/cetak');
			// redirect('pembayaran');
		}else{
			redirect('login','refresh');
		}
	}




}
