<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */
	public function index()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Dashboard";
			$d['class'] = "home";

			$krs['form'] ='KRS';
			$q = $this->db->get_where('setting',$krs);
			if($q->num_rows()>0){
				foreach($q->result() as $dt){
					$tgl = $this->model_global->tgl_str($dt->tgl_close);
					$d['tgl_krs'] = $tgl;
				}
			}else{
				$d['tgl_krs'] = '';
			}

			$wisuda['form'] ='Wisuda';
			$q = $this->db->get_where('setting',$wisuda);
			if($q->num_rows()>0){
				foreach($q->result() as $dt){
					$tgl = $this->model_global->tgl_str($dt->tgl_close);
					$d['tgl_wisuda'] = $tgl;
				}
			}else{
				$d['tgl_wisuda'] = '';
			}

			$wisuda['form'] ='Kuisioner';
			$q = $this->db->get_where('setting',$wisuda);
			if($q->num_rows()>0){
				foreach($q->result() as $dt){
					$tgl = $this->model_global->tgl_str($dt->tgl_close);
					$d['tgl_kuisioner'] = $tgl;
				}
			}else{
				$d['tgl_kuisioner'] = '';
			}

			$d['content']= 'isi';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function update_krs()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			$id['id'] = 1;
			$id['form'] ='KRS';

			$tgl = $this->model_global->tgl_sql($this->input->post('tgl_krs'));
			$dt['tgl_close'] = $tgl;

			$q = $this->db->get_where("setting",$id);
			$row = $q->num_rows();
			if($row>0){
				$this->db->update("setting",$dt,$id);
			}else{
				$dt['id'] = 1;
				$dt['form'] = 'KRS';
				$this->db->insert("setting",$dt);
			}
			echo "Tanggal KRS sukses di update";

		}else{
			redirect('login','refresh');
		}
	}

	public function update_wisuda()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			$id['id'] = 2;
			$id['form'] ='Wisuda';

			$tgl = $this->model_global->tgl_sql($this->input->post('tgl_wisuda'));
			$dt['tgl_close'] = $tgl;

			$q = $this->db->get_where("setting",$id);
			$row = $q->num_rows();
			if($row>0){
				$this->db->update("setting",$dt,$id);
			}else{
				$dt['id'] = 2;
				$dt['form'] = 'Wisuda';
				$this->db->insert("setting",$dt);
			}
			echo "Tanggal Wisuda sukses di update";

		}else{
			redirect('login','refresh');
		}
	}

	public function reset_status_krs()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			$this->db->update('mahasiswa', array('isi_krs' => 0));
			echo "Status Isi KRS Berhasil di Reset";

		}else{
			redirect('login','refresh');
		}
	}

	public function update_kuisioner()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			$id['id'] = 3;
			$id['form'] ='Kuisioner';

			$tgl = $this->model_global->tgl_sql($this->input->post('tgl_kuisioner'));
			$dt['tgl_close'] = $tgl;

			$q = $this->db->get_where("setting",$id);
			$row = $q->num_rows();
			if($row>0){
				$this->db->update("setting",$dt,$id);
			}else{
				$dt['id'] = 3;
				$dt['form'] = 'Kuisioner';
				$this->db->insert("setting",$dt);
			}
			echo "Tanggal Kuisioner sukses di update";

		}else{
			redirect('login','refresh');
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
