<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class GenerateTagihan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
   * Developer : Fitria Wahyuni.S.Pd
	 */

	public function index()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Generate Tagihan";
			$d['class'] = "keuangan";
      $d['data'] = $this->db->order_by('id','DESC')->get('tagihan_mhs');
			$d['content'] = 'generatetagihan/view';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

  public function create()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
      // echo "Create Tagihan";
			$d['judul']="Generate Tagihan Mahasiswa";
			$d['class'] = "keuangan";
      $d['th_akademik_aktif'] = $this->model_global->getThAkademikAktif()['kode'];
      // $d['data'] = $this->db->order_by('id','DESC')->get('tagihan_mhs');
			$d['content'] = 'generatetagihan/create';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

  public function list_tagihan()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
      $th_akademik_kode = $this->input->post('th_akademik_kode');
      // echo $th_akademik;
      $this->db->where('th_akademik_kode',$th_akademik_kode);
      $data = $this->db->get('jenis_tagihan')->result();
      echo "<option value=''>-Pilih-</option>";
      foreach($data as $row){
        echo "<option value='$row->id'>$row->nama</option>";
      }

		}else{
			redirect('login','refresh');
		}
	}

  public function list_mhs()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			// $th_akademik_aktif = $this->model_global->getThAkademikAktif()['kode'];
      $th_akademik_kode = $this->input->post('th_akademik_kode');
      $th_akademik = $this->model_data->getThAkademik($th_akademik_kode);
      // echo $th_akademik;die;

      $kd_prodi = $this->input->post('prodi');
      $kelas = $this->input->post('kelas');
      $jenis_tagihan_id = $this->input->post('jenis_tagihan_id');

      $this->db->where('th_akademik',$th_akademik);
      $this->db->where('kd_prodi',$kd_prodi);
      $this->db->where('kelas',$kelas);
      $this->db->where('status','Aktif');
      $dt['data'] = $this->db->get('mahasiswa')->result();
      // print_r($dt);die;
      $dt['th_akademik_kode'] = $th_akademik_kode;
      $dt['th_akademik'] = $th_akademik;
      $dt['jenis_tagihan_id'] = $jenis_tagihan_id;

      $this->load->view('generatetagihan/list_mhs',$dt);
      // print_r($data);
      // echo "<option value=''>-Pilih-</option>";
      // foreach($data as $row){
      //   echo "<option value='$row->id'>$row->nama</option>";
      // }

		}else{
			redirect('login','refresh');
		}
	}

  public function upload_tagihan()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$th_akademik_aktif = $this->model_global->getThAkademikAktif()['kode'];
			// echo $th_akademik_aktif;die;

      $th_akademik_kode = $this->input->post('th_akademik_kode');
      $th_akademik = $this->model_data->getThAkademik($th_akademik_kode);
      // echo $th_akademik;die;

      $kd_prodi = $this->input->post('prodi');
      $kelas = $this->input->post('kelas');
      $jenis_tagihan_id = $this->input->post('jenis_tagihan_id');

			$this->db->where('th_akademik_kode',$th_akademik_aktif);
			$this->db->where('kelas',$kelas);
      $this->db->where('jenis_tagihan_id',$jenis_tagihan_id);
      $tagihan = $this->db->get('tagihan_mhs');
      if($tagihan->num_rows()>0){
        echo "Data Tagihan sudah pernah di UPLOAD silahkan periksa kembali";
        die;
      }

      $this->db->where('th_akademik',$th_akademik);
      $this->db->where('kd_prodi',$kd_prodi);
      $this->db->where('kelas',$kelas);
      $this->db->where('status','Aktif');
      $data = $this->db->get('mahasiswa')->result();
			foreach($data as $dt){
        date_default_timezone_set('Asia/Jakarta');
        $jumlah = $this->model_data->tagihan($th_akademik_kode,$jenis_tagihan_id);
				// $nama_tagihan = $this->model_data->getJenisTagihan($jenis_tagihan_id);
        $smt =  $this->model_global->semester($dt->nim,$th_akademik_kode);

        $data = array('jenis_tagihan_id' => $jenis_tagihan_id,
											// 'nama_tagihan' => $nama_tagihan,
                      'th_akademik_kode' => $th_akademik_aktif,
											'th_angkatan_kode' => $th_akademik_kode,
                      'nim' => $dt->nim,
											'kelas' => $kelas,
											'kd_prodi' => $kd_prodi,
                      'smt' => $smt,
                      'jumlah' => $jumlah,
                      'user_id' => @$_SESSION['username'],
                      'insert_date' => date('Y-m-d H:i:s')
                      );
        $this->db->where('th_akademik_kode',$th_akademik_aktif);
        $this->db->where('jenis_tagihan_id',$jenis_tagihan_id);
        $this->db->where('nim',$dt->nim);
        $tagihan = $this->db->get('tagihan_mhs');
        if($tagihan->num_rows()==0){
          $this->db->insert('tagihan_mhs',$data);
        }
      }

      echo "Data Tagihan berhasil di UPLOAD";
      // print_r($dt);die;
      // $dt['th_akademik_kode'] = $th_akademik_kode;
      // $dt['th_akademik'] = $th_akademik;
      // $dt['jenis_tagihan_id'] = $jenis_tagihan_id;
      //
      // $this->load->view('generatetagihan/list_mhs',$dt);
      // print_r($data);
      // echo "<option value=''>-Pilih-</option>";
      // foreach($data as $row){
      //   echo "<option value='$row->id'>$row->nama</option>";
      // }

		}else{
			redirect('login','refresh');
		}
	}





}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
