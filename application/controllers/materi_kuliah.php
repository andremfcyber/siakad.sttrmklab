<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Materi_kuliah extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */

	public function index()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Materi Kuliah";
			$d['class'] = "transaksi";
			$d['data'] = $this->db->order_by('id','DESC')->get('materi_kuliah');

			$d['content'] = 'materi_kuliah/view';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function cari()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['id']	= $this->input->post('cari');

			$q = $this->db->get_where("materi_kuliah",$id);
			$row = $q->num_rows();
			if($row>0){
				foreach($q->result() as $dt){
					$d['id'] = $dt->id;
					$d['kd_mk'] = $dt->kd_mk;
					$d['judul'] = $dt->judul;
					$d['file'] = $dt->file;
					$d['smt'] = $dt->smt;
				}
			}else{
				$d['id'] = '';
				$d['kd_mk'] = '';
				$d['judul'] = '';
				$d['file'] = '';
				$d['smt'] = '';
			}
			echo json_encode($d);
		}else{
			redirect('login','refresh');
		}
	}

	public function simpan()
	{

		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];

		$kd_mk = $this->input->post('kd_mk');
		if(!empty($cek) && $level=='admin'){
			$id['id'] = (int) $this->input->post('id');

      $dt['kd_mk'] = $kd_mk;
      $dt['smt'] = $this->input->post('smt');
			$dt['judul'] = $this->input->post('judul');

			$dt['username'] = @$_SESSION['username'];
			$dt['insert_date'] = date('Y-m-d H:i:s');

			$nama_file = $kd_mk.'_'.$_FILES['file']['name'];

         if ($nama_file){
				$config['upload_path'] = './assets/materi_kuliah/';
				$config['allowed_types'] = 'docx|xlsx|doc|xls|pdf|txt';
				$config['max_size'] = '0';
				$config['overwrite'] = TRUE;
				$config['file_name'] = $nama_file;
				$this->load->library('upload', $config);


					if($this->upload->do_upload('file')){
						$tp=$this->upload->data();
						$ori = $tp['file_name'];
						$dt['file'] = $ori;
						$info = 'Upload File Success. ';
					}else{
						$info =  $this->upload->display_errors();
					}
			}


			$q = $this->db->get_where("materi_kuliah",$id);
			$row = $q->num_rows();
			if($row>0){
				$this->db->update("materi_kuliah",$dt,$id);
				//echo "Data Sukses diUpdate";
				$info .='Update success';
			}else{
				$this->db->insert("materi_kuliah",$dt);
				$info .='Insert success';
				//echo "Data Sukses diSimpan";
			}
			$this->session->set_flashdata('result_info', '<center>'.$info.'</center>');
			redirect('materi_kuliah');
		}else{
			redirect('login','refresh');
		}

	}

	public function hapus()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['id']	= $this->uri->segment(3);

			$q = $this->db->get_where("materi_kuliah",$id);
			
			// Path for unlink files

			$nama_file = $q->row()->file;
			$path = './assets/materi_kuliah/'.$nama_file;
	
			$row = $q->num_rows();
			if($row>0){
				if($this->db->delete("materi_kuliah",$id)){
					unlink($path);
				}
			}
			redirect('materi_kuliah','refresh');
		}else{
			redirect('login','refresh');
		}

	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
