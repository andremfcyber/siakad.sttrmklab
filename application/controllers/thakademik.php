<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Thakademik extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */

	public function index()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Tahun Akademik";
			$d['class'] = "master";
      $d['data'] = $this->db->order_by('kode','DESC')->get('th_akademik');
			$d['content'] = 'thakademik/view';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

  // public function create()
  // {
  //   $cek = @$_SESSION['logged_in'];
	// 	$level = @$_SESSION['level'];
	// 	if(!empty($cek) && $level=='admin'){
	// 		$d['judul']="Tambah Tahun Akademik";
	// 		$d['class'] = "master";
  //
	// 		$d['content'] = 'thakademik/form';
	// 		$this->load->view('home',$d);
	// 	}else{
	// 		redirect('login','refresh');
	// 	}
  // }

	public function cari()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['id']	= $this->input->post('cari');

//			$this->db->order_by('kode','DESC')
			$q = $this->db->order_by('kode','DESC')->get_where("th_akademik",$id);
			$row = $q->num_rows();
			if($row>0){
				foreach($q->result() as $dt){
					$d['kode'] = $dt->kode;
					$d['th_akademik'] = $dt->th_akademik;
					$d['aktif'] = $dt->aktif;
					$d['keterangan'] = $dt->keterangan;
					$d['semester'] = $dt->semester;
				}
				echo json_encode($d);
			}else{
				$d['kode'] 		= '';
				$d['th_akademik'] 		= '';
				$d['aktif'] 	= '';
				$d['keterangan'] 		= '';
				$d['semester'] 		= '';
				echo json_encode($d);
			}
		}else{
			redirect('login','refresh');
		}
	}

	public function simpan()
	{

		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['id'] = $this->input->post('id');
			// $id['th_akademik'] = $this->input->post('th_akademik');
			$aktif = $this->input->post('aktif');
			$aktif_hidden = $this->input->post('aktif_hidden');

         if($aktif=='Ya' && $aktif_hidden=='Tidak'){
//         	$this->model_data->CreateHistoryAktifMahasiswa();

				$this->db->where('aktif','Ya');
				$dt['aktif'] ='Tidak';
				$this->db->update('th_akademik',$dt);
				$this->db->last_query();
			}
//exit();


			$dt['kode'] = $this->input->post('kode');
			$dt['th_akademik'] = $this->input->post('th_akademik');
			$dt['aktif'] = $aktif;
			$dt['semester'] = $this->input->post('semester');
			$dt['keterangan'] = $this->input->post('keterangan');
      $dt['user_id'] = @$_SESSION['username'];
			$q = $this->db->get_where("th_akademik",$id);


			$row = $q->num_rows();
			if($row>0){
				$dt['update_date'] = date('Y-m-d H:i:s');
				$this->db->update("th_akademik",$dt,$id);
				echo "Data Sukses diUpdate";
			}else{
				$dt['id'] = $this->model_global->MaxFromTable('id','th_akademik');

				$dt['insert_date'] = date('Y-m-d H:i:s');
				$this->db->insert("th_akademik",$dt);
				echo "Data Sukses diSimpan";
			}
		}else{
			redirect('login','refresh');
		}

	}

	public function hapus()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['id']	= $this->uri->segment(3);

			$q = $this->db->get_where("th_akademik",$id);
			$row = $q->num_rows();
			if($row>0){
				$this->db->delete("th_akademik",$id);
			}
			redirect('thakademik','refresh');
		}else{
			redirect('login','refresh');
		}

	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
