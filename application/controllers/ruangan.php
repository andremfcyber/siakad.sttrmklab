<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ruangan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */

	public function index()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Ruangan";
			$d['class'] = "master";
      $d['data'] = $this->db->get('ruang_kuliah');
			$d['content'] = 'ruangkuliah/view';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}


	public function cari()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['id']	= $this->input->post('cari');

			$q = $this->db->get_where("ruang_kuliah",$id);
			$row = $q->num_rows();
			if($row>0){
				foreach($q->result() as $dt){
					$d['kode'] = $dt->kode;
					$d['nama'] = $dt->nama;
				}
				echo json_encode($d);
			}else{
				$d['kode'] 		= '';
				$d['nama'] 	= '';
				echo json_encode($d);
			}
		}else{
			redirect('login','refresh');
		}
	}

	public function simpan()
	{

		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['kode'] = $this->input->post('kode');

			$dt['kode'] = $this->input->post('kode');
			$dt['nama'] = $this->input->post('nama');
      		$dt['user_id'] = @$_SESSION['username'];

			$q = $this->db->get_where("ruang_kuliah",$id);
			$row = $q->num_rows();
			if($row>0){
        $dt['update_date'] = date('Y-m-d H:i:s');
				$this->db->update("ruang_kuliah",$dt,$id);
				echo "Data Sukses diUpdate";
			}else{
        		$dt['insert_date'] = date('Y-m-d H:i:s');
    			// echo "<pre>";
				// print_r($dt);
				// echo "</pre>";
				// exit;
				$this->db->insert("ruang_kuliah",$dt);
				echo "Data Sukses diSimpan";
			}
		}else{
			redirect('login','refresh');
		}

	}

	public function hapus()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['id']	= $this->uri->segment(3);

			$q = $this->db->get_where("ruang_kuliah",$id);
			$row = $q->num_rows();
			if($row>0){
				$this->db->delete("ruang_kuliah",$id);
			}
			redirect('ruangan','refresh');
		}else{
			redirect('login','refresh');
		}

	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
