<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Grafik extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */
	public function mhs()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Grafik Mahasiswa";
			$d['class'] = "grafik";

			date_default_timezone_set('Asia/Jakarta');
			// $thak = $this->input->post('thak');
			// if(empty($thak)){
			// 	$x_smt = $this->model_global->cari_semester();
			// 	if($x_smt=='ganjil'){
			// 		$smt = 1;
			// 	}else{
			// 		$smt = 2;
			// 	}
			// 	$th = date('Y').$smt;
			// 	$d['th'] = $th;
			// }else{
			// 	$th = $thak;
			// 	$d['th'] = $th;
			// }

			$thak = $this->input->post('thak');
			if(empty($thak)){
				$thak = $this->model_global->getThAkademikAktif()['th_akademik'];
			}else{
				$thak = $thak;
			}
			$d['th'] = $thak;
			$th = $thak;

			$total = $this->model_data->data_chart_mhs_total($th);
			$aktif = $this->model_data->data_chart_mhs($th,'Aktif');
			$lulus = $this->model_data->data_chart_mhs($th,'Lulus');
			$cuti = $this->model_data->data_chart_mhs($th,'Cuti');
			$do = $this->model_data->data_chart_mhs($th,'DO');
      $meninggal = $this->model_data->data_chart_mhs($th,'Meninggal');
      $laki = $this->model_data->data_chart_mhs_gender($th, 'L');
      $perempuan = $this->model_data->data_chart_mhs_gender($th, 'P');

			$d['mhs_total'] = $total;
			$d['mhs_aktif'] = $aktif; //@number_format($total/$aktif*100,1);
			$d['mhs_lulus'] = $lulus; //@number_format($total/$lulus*100,1);
			$d['mhs_cuti'] = $cuti; //@number_format($total/$cuti*100,1);
			$d['mhs_do'] = $do; //@number_format($total/$do*100,1);
      $d['mhs_meninggal'] = $meninggal; //@number_format($total/$meninggal*100,1);
      
      $d['mhs_L'] = $laki;
      $d['mhs_P'] = $perempuan;

			$d['content']= 'grafik/mhs';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function dosen()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Grafik Dosen";
			$d['class'] = "grafik";

			date_default_timezone_set('Asia/Jakarta');

			$cat = $this->db->get('prodi')->result();
			// $arr_cat = implode(',',$cat);
			$arr = array();
			foreach($cat as $value)
			{
				// $temp = intval($value->th_akademik);
				$this->db->where('status','Aktif');
				$this->db->where('kd_prodi',$value->kd_prodi);
				$jml = $this->db->get('dosen')->num_rows();

				$temp = array(
					'name' => $value->prodi,
					'y' => intval($jml)
				);
				array_push($arr,$temp);
				// $temp = $value->th_akademik;
				// array_push($arr,$temp);

			}
			$d['data'] = json_encode($arr);

			// $ti = $this->model_data->data_chart_dosen('55-201');
			// $si = $this->model_data->data_chart_dosen('57-201');
			$this->db->where('status','Aktif');
			$d['total'] = $this->db->get('dosen')->num_rows();
			// $d['si'] = $si;
			// $d['ti'] = $ti;

			$d['content']= 'grafik/dosen';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function mhs_aktif()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Grafik Mahasiswa Aktif";
			$d['class'] = "grafik";

			date_default_timezone_set('Asia/Jakarta');
			$thak = $this->input->post('thak');
			if(empty($thak)){
				$thak = $this->model_global->getThAkademikAktif()['th_akademik'];
			}else{
				$thak = $thak;
			}
			$d['th'] = $thak;

			$this->db->select('th_akademik');
			$this->db->where('th_akademik <=',$thak);
			$this->db->group_by('th_akademik');
			$this->db->order_by('th_akademik','DESC');
			$this->db->limit(3);
			$cat = $this->db->get('mahasiswa')->result();
			// $arr_cat = implode(',',$cat);
			$arr = array();
			foreach($cat as $value)
			{
				// $temp = intval($value->th_akademik);
				$temp = $value->th_akademik;
				array_push($arr,$temp);

			}
			$val_arr = json_encode($arr);
			// print_r($val_arr);die;
			$d['category'] = $val_arr;

			$prodi = $this->db->get('prodi')->result();
			$arr1 = array();
			foreach($prodi as $value)
			{
				$arr2 = $this->getJmlMhs($thak,$value->kd_prodi);
				// $temp_jml = intval($value->th_akademik);
				$temp = array(
					'name' => $value->prodi,
					'data' => $arr2
				);
				array_push($arr1,$temp);

			}
			$val_arr1 =json_encode($arr1);
			// print_r($val_arr1);die;
			$d['data'] = $val_arr1;

			$d['content']= 'grafik/mhs_aktif';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function getJmlMhs($thak,$kdprodi)
	{
		$this->db->select('th_akademik');
		$this->db->where('th_akademik <=',$thak);
		$this->db->group_by('th_akademik');
		$this->db->order_by('th_akademik','DESC');
		$this->db->limit(3);
		$cat = $this->db->get('mahasiswa')->result();
		// $arr_cat = implode(',',$cat);
		$arr = array();
		foreach($cat as $value)
		{
			$this->db->where('th_akademik',$value->th_akademik);
			$this->db->where('status','Aktif');
			$this->db->where('kd_prodi',$kdprodi);
			$jml_mhs =  intval($this->db->get('mahasiswa')->num_rows());
			array_push($arr,$jml_mhs);
		}
		return $arr;
	}

	public function krs()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Grafik KRS Mahasiswa";
			$d['class'] = "grafik";



			date_default_timezone_set('Asia/Jakarta');
			// $thak = $this->input->post('thak');
			// if(empty($thak)){
			// 	$x_smt = $this->model_global->cari_semester();
			// 	if($x_smt=='ganjil'){
			// 		$smt = 1;
			// 	}else{
			// 		$smt = 2;
			// 	}
			// 	$th = date('Y').$smt;
			// 	$d['th'] = $th;
			// }else{
			// 	$th = $thak;
			// 	$d['th'] = $th;
			// }
			//
			// $d['category'] = $this->model_data->create_category($th);
			// $d['ti'] = $this->model_data->data_chart_krs($th,'55-201');
			// $d['si'] = $this->model_data->data_chart_krs($th,'57-201');

			$thak = $this->input->post('thak');
			if(empty($thak)){
				$thak = $this->model_global->getThAkademikAktif()['kode'];
			}else{
				$thak = $thak;
			}
			$d['th'] = $thak;


			$this->db->select('kode');
			$this->db->where('kode <=',$thak);
			$this->db->order_by('kode','DESC');
			$this->db->limit(4);
			$cat = $this->db->get('th_akademik')->result();
			// $arr_cat = implode(',',$cat);
			$arr = array();
			foreach($cat as $value)
			{
				// $temp = intval($value->th_akademik);
				$temp = $value->kode;
				array_push($arr,$temp);

			}
			$d['category'] = json_encode($arr);

			$prodi = $this->db->get('prodi')->result();
			$arr2 = array();
			foreach($prodi as $value2)
			{
				$this->db->select('kode');
				$this->db->where('kode <=',$thak);
				$this->db->order_by('kode','DESC');
				$this->db->limit(4);
				$cat = $this->db->get('th_akademik')->result();
				// $arr_cat = implode(',',$cat);
				$arr = array();
				$arr3 = array();
				foreach($cat as $value)
				{
					// $temp = intval($value->th_akademik);
					// array_push($arr,$temp);
					$this->db->select('th_akademik,semester,nim');
					$this->db->where('th_akademik',$value->kode);
					// $this->db->where('status','Aktif');
					$this->db->where('kd_prodi',$value2->kd_prodi);
					$this->db->group_by('th_akademik,semester,nim');
					$jml_mhs =  intval($this->db->get('krs')->num_rows());
					// print_r($this->db->last_query());die;
					array_push($arr3,$jml_mhs);
				}

				// $temp_jml = intval($value->th_akademik);
				$temp2 = array(
					'name' => $value2->prodi,
					'data' => $arr3
				);
				array_push($arr2,$temp2);

			}
			// $this->debug($arr2);die;
			$d['data'] = json_encode($arr2);

			$d['content']= 'grafik/krs';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function wisuda()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Grafik Wisuda";
			$d['class'] = "grafik";



			date_default_timezone_set('Asia/Jakarta');
			// $thak = $this->input->post('thak');
			// if(empty($thak)){
			// 	$x_smt = $this->model_global->cari_semester();
			// 	if($x_smt=='ganjil'){
			// 		$smt = 1;
			// 	}else{
			// 		$smt = 2;
			// 	}
			// 	$th = date('Y').$smt;
			// 	$d['th'] = $th;
			// }else{
			// 	$th = $thak;
			// 	$d['th'] = $th;
			// }
			//
			// $d['category'] = $this->model_data->create_category($th);
			// $d['ti'] = $this->model_data->data_chart_wisuda($th,'55-201');
			// $d['si'] = $this->model_data->data_chart_wisuda($th,'57-201');

			$thak = $this->input->post('thak');
			if(empty($thak)){
				$thak = $this->model_global->getThAkademikAktif()['kode'];
			}else{
				$thak = $thak;
			}
			$d['th'] = $thak;


			$this->db->select('kode');
			$this->db->where('kode <=',$thak);
			$this->db->order_by('kode','DESC');
			$this->db->limit(4);
			$cat = $this->db->get('th_akademik')->result();
			// $arr_cat = implode(',',$cat);
			$arr = array();
			foreach($cat as $value)
			{
				// $temp = intval($value->th_akademik);
				$temp = $value->kode;
				array_push($arr,$temp);

			}
			$d['category'] = json_encode($arr);

			$prodi = $this->db->get('prodi')->result();
			$arr2 = array();
			foreach($prodi as $value2)
			{
				$this->db->select('kode');
				$this->db->where('kode <=',$thak);
				$this->db->order_by('kode','DESC');
				$this->db->limit(4);
				$cat = $this->db->get('th_akademik')->result();
				// $arr_cat = implode(',',$cat);
				$arr = array();
				$arr3 = array();
				foreach($cat as $value)
				{
					$this->db->where('th_akademik',$value->kode);
					$this->db->where('kd_prodi',$value2->kd_prodi);
					$jml_mhs =  intval($this->db->get('wisuda')->num_rows());
					array_push($arr3,$jml_mhs);
				}

				// $temp_jml = intval($value->th_akademik);
				$temp2 = array(
					'name' => $value2->prodi,
					'data' => $arr3
				);
				array_push($arr2,$temp2);

			}
			// $this->debug($arr2);die;
			$d['data'] = json_encode($arr2);

			$d['content']= 'grafik/wisuda';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function polling()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Grafik Kuesioner";
			$d['class'] = "grafik";



			date_default_timezone_set('Asia/Jakarta');

			$thak = $this->input->post('thak');
			if(empty($thak)){
				$thak = $this->model_global->getThAkademikAktif()['kode'];
			}else{
				$thak = $thak;
			}
			$d['th'] = $thak;

			$d['content']= 'grafik/polling';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function getPolling()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			$d['data'] = '';

			$d['judul']="Grafik Kuesioner";
			$d['class'] = "grafik";
			
			
			
			date_default_timezone_set('Asia/Jakarta');
			
			$thak = $this->input->post('thak');
			if(empty($thak)){
				$thak = $this->model_global->getThAkademikAktif()['kode'];
			}else{
				$thak = $thak;
			}
			$d['th'] = $thak;


			$this->db->select('kode');
			$this->db->where('kode <=',$thak);
			$this->db->order_by('kode','DESC');
			$this->db->limit(4);
			$cat = $this->db->get('th_akademik')->result();
			// $arr_cat = implode(',',$cat);
			$arr = array();
			foreach($cat as $value)
			{
				// $temp = intval($value->th_akademik);
				$temp = $value->kode;
				array_push($arr,$temp);
			
			}
			$d['category'] = json_encode($arr);
			
			$prodi = $this->db->get('prodi')->result();
			$arr2 = array();
			foreach($prodi as $value2)
			{
				$this->db->select('kode');
				$this->db->where('kode <=',$thak);
				$this->db->order_by('kode','DESC');
				$this->db->limit(4);
				$cat = $this->db->get('th_akademik')->result();
				// $arr_cat = implode(',',$cat);
				$arr = array();
				$arr3 = array();
				foreach($cat as $value)
				{
					$this->db->where('th_akademik',$value->kode);
					$this->db->where('kd_prodi',$value2->kd_prodi);
					$jml_mhs =  intval($this->db->get('wisuda')->num_rows());
					array_push($arr3,$jml_mhs);
				}
			
				// $temp_jml = intval($value->th_akademik);
				$temp2 = array(
					'name' => $value2->prodi,
					'data' => $arr3
				);
				array_push($arr2,$temp2);
			
			}
			// $this->debug($arr2);die;
			$d['data'] = json_encode($arr2);

			$d['content']= 'grafik/getPolling';
			$this->load->view('grafik/getPolling',$d);
		}else{
			redirect('login','refresh');
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
