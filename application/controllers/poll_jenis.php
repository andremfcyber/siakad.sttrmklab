<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Poll_jenis extends CI_Controller {

    /**
     * Index Page for this controller.
     * Programmer : Deddy Rusdiansyah.S.Kom
     * http://deddyrusdiansyah.blogspot.com
     * http://softwarebanten.com
     * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
     * Developer : Fitria Wahyuni.S.Pd
     */
    public function index() {
        $cek = @$_SESSION['logged_in'];
        $level = @$_SESSION['level'];
        if (!empty($cek) && $level == 'admin') {
            $d['judul'] = "Jenis Kuesioner";
            $d['class'] = "polling";
            $d['data'] = $this->db->order_by('id', 'DESC')->get('poll_jenis');

            $d['content'] = 'poll_jenis/view';
            $this->load->view('home', $d);
        } else {
            redirect('login', 'refresh');
        }
    }

    public function cari() {
        $cek = @$_SESSION['logged_in'];
        $level = @$_SESSION['level'];
        if (!empty($cek) && $level == 'admin') {
            $id['id'] = $this->input->post('cari');

            $q = $this->db->get_where("poll_jenis", $id);
            $row = $q->num_rows();
            if ($row > 0) {
                foreach ($q->result() as $dt) {
                    $d['id'] = $dt->id;
                    $d['nama'] = $dt->nama;
                }
            } else {
                $d['id'] = '';
                $d['nama'] = '';
            }
            echo json_encode($d);
        } else {
            redirect('login', 'refresh');
        }
    }

    public function simpan() {

        $cek = @$_SESSION['logged_in'];
        $level = @$_SESSION['level'];
        if (!empty($cek) && $level == 'admin') {
            $id['id'] = (int) $this->input->post('id');
            
            $dt['nama'] = $this->input->post('nama');
            $dt['user_id'] = @$_SESSION['username'];


            $q = $this->db->get_where("poll_jenis", $id);
            $row = $q->num_rows();
            if ($row > 0) {
                $dt['update_date'] = date('Y-m-d H:i:s');
                $this->db->update("poll_jenis", $dt, $id);
                echo "Data Sukses diUpdate";
            } else {
                $dt['insert_date'] = date('Y-m-d H:i:s');
                $this->db->insert("poll_jenis", $dt);
                echo "Data Sukses diSimpan";
            }
        } else {
            redirect('login', 'refresh');
        }
    }

    public function hapus() {
        $cek = @$_SESSION['logged_in'];
        $level = @$_SESSION['level'];
        if (!empty($cek) && $level == 'admin') {
            $id['id'] = $this->uri->segment(3);

            $q = $this->db->get_where("poll_jenis", $id);
            $row = $q->num_rows();
            if ($row > 0) {
                $this->db->delete("poll_jenis", $id);
            }
            redirect('poll_jenis', 'refresh');
        } else {
            redirect('login', 'refresh');
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
