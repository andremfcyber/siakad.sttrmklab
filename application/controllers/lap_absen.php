<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lap_absen extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */
	public function index()
	{
		$cek = @$_SESSION['logged_in'];
		if(!empty($cek)){
			$d['judul']="Absensi Mahasiswa";
			$d['class'] = "laporan";

			$d['content']= 'laporan/lap_absen';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function cari_smt()
	{
		$kode = $this->input->post('th_ak');
		$this->db->where('kode',$kode);
		$data = $this->db->get('th_akademik');
		if($data->num_rows()>0)
		{
			$row = $data->row();
			$hasil = array('semester' => $row->semester);
		}else{
			$hasil = array('semester' => '');
		}
		echo json_encode($hasil);
	}

	public function cari_mata_kuliah()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['th_akademik']	= $this->input->post('th_ak');
			$id['kd_prodi']	= $this->input->post('kd_prodi');
			$id['semester']	= $this->input->post('smt');
			$id['kelas']	= $this->input->post('kelas');

			$this->db->order_by('kd_mk');
			$q = $this->db->get_where("jadwal",$id);
			$row = $q->num_rows();
			if($row>0){
			?>
            	<option value="">-Pilih Mata Kuliah-</option>
            <?php
				foreach($q->result() as $dt){
					$nama_mk = $this->model_data->cari_nama_mk($dt->kd_mk);
					$nama_dosen = $this->model_data->cari_nama_dosen($dt->kd_dosen);
				?>
                	<option value="<?php echo $dt->id_jadwal;?>"><?php echo $dt->kd_mk;?> - <?php echo $nama_mk;?> - <?php echo $dt->kd_dosen;?> - <?php echo $nama_dosen;?> - <?php echo $dt->hari.' - '.$dt->pukul.' - '.$dt->ruang;?></option>
                <?php
				}
			}else{
			?>
            <option value="">-Belum Ada Mata Kuliah-</option>
            <?php }
		}else{
			redirect('login','refresh');
		}
	}

	public function cari_data()
	{
		$cek = @$_SESSION['logged_in'];
		if(!empty($cek)){

			// $th_ak = $this->input->post('th_ak');
			// $smt = $this->input->post('smt');
			// $kd_prodi = $this->input->post('kd_prodi');
			$jadwal = $this->input->post('mk');
			// $kelas = $this->input->post('kelas');

			$this->db->where('a.id_jadwal',$jadwal);
			$this->db->from('krs as a');
			$this->db->join('mahasiswa as b','a.nim=b.nim');
			$this->db->order_by('b.nama_mhs', 'asc');

			// $query = "SELECT *
			// 			FROM krs as a
			// 			JOIN mahasiswa as b
			// 			ON a.nim=b.nim
			// 		WHERE a.id_jadwal='$jadwal'";

			$q = $this->db->get(); // $this->db->query($query);
			$r = $q->num_rows();
			if($r>0){
				$dt['data'] = $q;
				echo $this->load->view('laporan/view_lap_absen',$dt);
			}else{
				echo $this->load->view('laporan/view_kosong');
			}
		}else{
			redirect('login','refresh');
		}
	}

	public function cetak_pdf()
	{
		$cek = @$_SESSION['logged_in'];
		if(!empty($cek)){

			$th_ak = $this->input->post('th_ak');
			$smt = $this->input->post('smt');
			$kd_prodi = $this->input->post('kd_prodi');
			$jadwal = $this->input->post('mk');
			$periode = $this->input->post('periode');

			$query = "SELECT *
						FROM krs as a
						JOIN mahasiswa as b
						ON a.nim=b.nim
					WHERE a.id_jadwal='$jadwal' order by b.nama_mhs";

			$q = $this->db->query($query);
			$r = $q->num_rows();
			if($r>0){
				$_SESSION['id_jadwal'] = $jadwal;
				$_SESSION['periode'] = $periode;
				// $this->session->set_userdata($sess_data);
				echo "Sukses";
			}else{
				echo "Maaf, Tidak Ada data";
			}

		}else{
			redirect('login','refresh');
		}
	}


	public function print_pdf()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			$jadwal = @$_SESSION['id_jadwal'];
			$periode = strtoupper(@$_SESSION['periode']);

			$q = $this->db->query("SELECT krs.*, b.nama_mhs, dosen.nidn, prodi.prodi, prodi.singkat, prodi.ketua_prodi, prodi.nik  FROM krs 
				JOIN prodi ON krs.kd_prodi = prodi.kd_prodi
				JOIN dosen ON krs.kd_dosen= dosen.kd_dosen 
				JOIN mahasiswa as b ON krs.nim=b.nim
				WHERE id_jadwal='$jadwal' order by b.nama_mhs asc ");
			$r = $q->num_rows();
			// map_y

			echo "<pre>";
			// print_r($this->db->last_query());
			print_r($q->result());
			echo "</pre>";exit;
			if($r>0){
				foreach($q->result() as $dt){
					$th_ak 	= substr($dt->th_akademik,0,4);
					$smt_r = $this->model_data->smtToRomawi($dt->smt);
					$smt 	= $dt->semester;
					$kd_dosen 	= $dt->kd_dosen;
					$nama_dosen = $dt->nm_dosen;
					$kd_mk	= $dt->kd_mk;
					$nama_mk = $dt->nama_mk;
					$sks = $dt->sks;
					$hari = $dt->hari;
					$pukul = $dt->pukul;
					$ruang = $dt->ruang;
					$singkat = $dt->singkat;
					$k_prodi = $dt->ketua_prodi;
					$nidn = $dt->nidn;
					$nik = $dt->nik;
				}
			  $kls = str_replace('Kelas ', '', $ruang);
			  if($smt == 'Ganjil'){
			  	$th = $th_ak;
			  }elseif($smt == 'Genap'){
			  	$th = $th_ak+1;
			  }else{
			  	$th = "";
			  }

			  $pdf=new reportProduct();
			  $pdf->setKriteria("cetak_laporan");
			  $pdf->setNama("CETAK LAPORAN");
			  $pdf->AliasNbPages();
			  $pdf->AddPage("P","A4");
				//foreach($data->result() as $t){
					$A4[0]=210;
					$A4[1]=297;
					$Q[0]=216;
					$Q[1]=279;
					$pdf->SetTitle('Laporan Aplikasi');
					$pdf->SetCreator('Programmer IT with fpdf');

					$h = 5;
					$w = 163;
					$pdf->SetFont('Times','',18);
					$pdf->Cell(10,250,'',0,0,'C'); //gap kiri
					$pdf->image('assets/img/logo_no.png',18,5,25,25);
					$pdf->SetX(37);
					$pdf->SetFont('Times','B',12);
					$pdf->Cell($w,$h,"DAFTAR HADIR PRODI ".$singkat." TAHUN AKADEMIK ".$th_ak."/".($th_ak+1),0,0,'C');
					$pdf->Ln();
					$pdf->SetX(37);
					$pdf->Cell($w,$h,"SEMESTER ".strtoupper($smt)." PERIODE ".$periode." ".$th,0,0,'C');
					$pdf->Ln();
					$pdf->SetX(37);
					$pdf->Cell($w,$h,$this->config->item('nama_pendek')." ".$this->config->item('nama_instansi'),0,0,'C');

					//Column widths
					$pdf->Ln(10);
					$pdf->SetX(19);
					$pdf->Cell(30,$h,'Mata Kuliah',0,0,'L');
					$pdf->Cell(2,$h,': '.$nama_mk,0,0,'L');
					$pdf->SetX(165);
					$pdf->Cell(35,$h,'SEMESTER '.$smt_r,1,1,'C');
					$pdf->SetX(19);
					$pdf->Cell(30,$h,'Dosen',0,0,'L');
					$pdf->Cell(2,$h,': '.$nama_dosen,0,0,'L');
					$pdf->SetX(165);
					$pdf->Cell(35,$h,strtoupper($kls),1,1,'C');
					
					$pdf->SetX(19);
					$pdf->Cell(30,$h,'SKS',0,0,'L');
					$pdf->Cell(2,$h,': '.$sks,0,1,'L');

					$l=8;
					$w = array($l,50,$l,$l,$l,$l,$l,$l,15,$l,$l,$l,$l,$l,$l,15,25);

					//Header

					$pdf->SetFont('Times','B',10);
					$pdf->SetFillColor(204,204,204);
    				$pdf->SetTextColor(0);
					$fill = true;
					$h=6;
					$h2 = $h+$h;
					$pdf->SetX(20);
					$pdf->Cell($l,$h2,'No',1,0,'C',$fill);
					$pdf->Cell(45,$h2,'NAMA MAHASISWA',1,0,'C',$fill);
					$pdf->Cell(128,$h,'TANGGAL',1,1,'C',$fill);
					$pdf->SetX(73);
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Ln();

					//data
					//$pdf->SetFillColor(224,235,255);
					// $h = 6;
					$pdf->SetFillColor(204,204,204);
    				$pdf->SetTextColor(0);
					$fill = false;
					$no=1;
					$jmlsks = 0;

					// echo "<pre>";
					// // print_r($this->db->last_query());
					// print_r($q->result());
					// echo "</pre>";exit;
					foreach($q->result() as $row)
					{
						// $nama_mhs = $this->model_data->cari_nama_mhs($row->nim);
						$pdf->SetX(20);
						$pdf->SetFont('Times','',7);
						$pdf->Cell($l,$h,$no,1,0,'C');
						$pdf->Cell(45,$h,strtoupper($row->nama_mhs),1,0,'L');
						$pdf->Cell($l,$h,'',1,0,'C');
						$pdf->Cell($l,$h,'',1,0,'C');
						$pdf->Cell($l,$h,'',1,0,'C');
						$pdf->Cell($l,$h,'',1,0,'C');
						$pdf->Cell($l,$h,'',1,0,'C');
						$pdf->Cell($l,$h,'',1,0,'C');
						$pdf->Cell($l,$h,'',1,0,'C');
						$pdf->Cell($l,$h,'',1,0,'C');
						$pdf->Cell($l,$h,'',1,0,'C');
						$pdf->Cell($l,$h,'',1,0,'C');
						$pdf->Cell($l,$h,'',1,0,'C');
						$pdf->Cell($l,$h,'',1,0,'C');
						$pdf->Cell($l,$h,'',1,0,'C');
						$pdf->Cell($l,$h,'',1,0,'C');
						$pdf->Cell($l,$h,'',1,0,'C');
						$pdf->Cell($l,$h,'',1,0,'C');
						$pdf->Ln();
						$fill = !$fill;
						$no++;
					}
					
					$pdf->SetFont('Times','B',10);
					$pdf->SetX(20);
					$pdf->Cell($l+45,$h,'Paraf Dosen',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Ln();

					// REKAPITULASI
					$l_tot = 181;
					$pdf->SetFont('Times','B',10);
					$pdf->SetX(20);
					$pdf->Cell($l_tot,$h,'REKAPITULASI KEHADIRAN MAHASISWA',1,1,'C');
					$pdf->SetX(20);
					$pdf->Cell($l+45,$h2,'KETERANGAN',1,0,'C',true);
					$pdf->Cell(128,$h,'TANGGAL',1,1,'C',true);
					$pdf->SetX(73);
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Ln();
					
					$pdf->SetFont('Times','',10);
					$pdf->SetX(20);
					$pdf->Cell($l+45,$h,'HADIR',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Ln();

					$pdf->SetX(20);
					$pdf->Cell($l+45,$h,'TIDAK HADIR',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Cell($l,$h,'',1,0,'C');
					$pdf->Ln();

					$h = 5;
					$pdf->SetFont('Times','B',10);
					//$pdf->Cell(50,$h,'Menyetujui',0,0,'C');
					//$pdf->SetX(100);
					//$pdf->Cell(100,$h,'Serang, '.$this->model_global->tgl_indo(date('Y-m-d')),0,1,'C');
					$pdf->SetX(20);
					$pdf->Cell(50,$h,'Ketua Program Studi,',0,0,'L');
					$pdf->SetX(150);
					$pdf->Cell(100,$h,'Dosen Pengampu,',0,1,'L');
					$pdf->Ln(20);
					$pdf->SetFont('Times','BU',10);
					$pdf->SetX(20);
					$pdf->Cell(50,$h,$k_prodi,0,0,'L');
					$pdf->SetX(150);
					$pdf->Cell(100,$h,$nama_dosen,0,1,'L');
					$pdf->SetFont('Times','B',10);
					$pdf->SetX(20);
					$pdf->Cell(50,$h,'NIDN.'.$nik,0,0,'L');
					$pdf->SetX(150);
					$pdf->Cell(100,$h,'NIDN.'.$nidn,0,1,'L');
				//}

				//}
				$pdf->Output('ABSENSI_'.$th_ak.'_'.$smt.'_'.$kd_dosen.'.pdf','D');
					// $pdf->Output();
			}else{
				$this->session->set_flashdata('result_info', '<center>Tidak Ada Data</center>');
				redirect('lap_absen');
				//echo "Maaf Tidak ada data";
			}
		}else{
			redirect('login','refresh');
		}
	}

	public function print_excel()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			$jadwal = @$_SESSION['id_jadwal'];

			$q = $this->db->query("SELECT * FROM krs  JOIN mahasiswa as b ON krs.nim=b.nim WHERE id_jadwal='$jadwal' order by b.nama_mhs ");
			$r = $q->num_rows();

			if($r>0){
				foreach($q->result() as $dt){
					$th_ak 	= $dt->th_akademik;
					$smt 	= $dt->semester;
					$kd_dosen 	= $dt->kd_dosen;
					$nama_dosen = $dt->nm_dosen;
					$kd_mk	= $dt->kd_mk;
					$nama_mk = $dt->nama_mk;
					$sks = $dt->sks;
					$hari = $dt->hari;
					$pukul = $dt->pukul;
					$ruang = $dt->ruang;
				}
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=absensi_".$th_ak.'_'.$smt.'_'.$kd_dosen.".xls");
				header("Pragma: no-cache");
				header("Expires: 0");
			?>
            <p>ABSENSI MAHASISWA</p>
            <p><?php echo $th_ak.' - '.$smt;?></p>
            <p>Mata Kuliah : <?php echo $kd_mk.'-'.$nama_mk;?></p>
            <p>Dosen : <?php echo $kd_dosen.'-'.$nama_dosen;?></p>
            <table border="1" width="100%">
            	<thead>
                	<tr>
                    	<td>No</td>
                        <td>NIM</td>
                        <td>NAMA</td>
                        <td>1</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                        <td>5</td>
                        <td>6</td>
                        <td>UTS</td>
                        <td>8</td>
                        <td>9</td>
                        <td>10</td>
                        <td>11</td>
                        <td>12</td>
                        <td>13</td>
                        <td>UAS</td>
                        <td>KET</td>
					</tr>
				</thead>
                <tbody>
                <?php
				$no=1;
				foreach($q->result() as $dt){
					// $nama_mhs = $this->model_data->cari_nama_mhs($dt->nim);
				?>
                <tr>
                	<td><?php echo $no;?></td>
					<td><?php echo $dt->nim;?></td>
                    <td><?php echo $dt->nama_mhs;?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            <?php
				$no++;
				}
			?>
            	</tbody>
               </table>
             <?php
			}
		}else{
			redirect('login','refresh');
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
