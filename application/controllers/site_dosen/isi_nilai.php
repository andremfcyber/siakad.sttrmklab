<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Isi_nilai extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */
	public function index()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='dosen'){

			$d['judul']="Nilai Mahasiswa";
			$d['class'] = "master";

			$d['content']= 'site_dosen/isi_nilai/form';
			$this->load->view('site_dosen/home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function cari_smt()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='dosen'){
			$kd_dosen = @$_SESSION['username'];
			$th_ak	= $this->input->post('th_ak');
			if(!empty($th_ak)){
				if(substr($th_ak,4,1)==1){
					$smt = 'Ganjil';
				}else{
					$smt = 'Genap';
				}

				$d['semester'] = $smt;
				//$d['smt'] = $this->model_global->semester($nim,$th_ak);
			}else{
				$d['semester'] = '';
				//$d['smt'] = '';
			}
			echo json_encode($d);

		}else{
			redirect('login','refresh');
		}
	}

	public function cari_mata_kuliah()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='dosen'){
			$id['th_akademik']	=  $this->input->post('th_ak');
			$id['kd_dosen']	= @$_SESSION['username'];
			/*
			$id['kd_prodi']	= $this->input->post('kd_prodi');
			$id['semester']	= $this->input->post('semester');
			*/
			$this->db->select('kd_prodi,kd_mk,kd_dosen,hari,pukul,ruang,kelas,id_jadwal');
			$this->db->from('krs');
			$this->db->where($id);
			$this->db->group_by('kd_prodi,kd_mk,kd_dosen, hari, pukul, ruang, kelas, id_jadwal');
			$q = $this->db->get();
			$row = $q->num_rows();
			// echo "<option value=''>".$this->db->last_query()."</option>";exit;
			if($row>0){
				echo "<option value=''>-Pilih Mata Kuliah-</option>";
				foreach($q->result() as $dt){
          $namaProd = $this->model_data->singkat_jurusan($dt->kd_prodi);
					$infoMK = $this->model_data->getInfoMK($dt->kd_mk);
					$nama_mk = $infoMK['nama_mk']; // $this->model_data->cari_nama_mk($dt->kd_mk);
					$smt_mk = $infoMK['smt'];
					$nama_dosen = $this->model_data->cari_nama_dosen($dt->kd_dosen);
				?>
                	<option value="<?php echo $dt->id_jadwal;?>"><?php echo $namaProd;?> - <?php echo $dt->kd_mk;?> - <?php echo $nama_mk.' - smt '.$smt_mk;?> - <?php echo $dt->kd_dosen;?> - <?php echo $nama_dosen;?> - <?php echo $dt->hari.' - '.$dt->pukul.' - '.$dt->ruang.' - kelas '.$dt->kelas;?></option>
                <?php
				}
			}else{
				echo "<option value=''>Belum Ada Mata Kuliah</option>";
			}
		}else{
			redirect('login','refresh');
		}
	}

	public function cari_data()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='dosen'){

			$kd_dosen = @$_SESSION['username'];
			$th_ak = $this->input->post('thak');
			$id_jadwal = $this->input->post('id_jadwal');

			// $where = "WHERE th_akademik='$th_ak' AND kd_mk='$kd_mk' AND kd_dosen='$kd_dosen'";
			$this->db->where('th_akademik',$th_ak);
			$this->db->where('id_jadwal',$id_jadwal);
			$this->db->where('kd_dosen',$kd_dosen);
			$this->db->order_by('nim', 'asc');

			$q = $this->db->get('krs'); // $this->db->query("SELECT * FROM krs $where ");
			if($q->num_rows()>0){
				$dt['data'] = $q;
				echo $this->load->view('site_dosen/isi_nilai/view',$dt);
			}else{
				echo $this->load->view('site_dosen/view_kosong');
			}

		}else{
			redirect('login','refresh');
		}
	}

	public function simpan_nilai()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='dosen'){
			date_default_timezone_set('Asia/Jakarta');
			$id['id_krs'] = $this->input->post('id');

			$nilai = str_replace('*','+',$this->input->post('nilai'));
			$data['nilai_akhir'] = $nilai;

			$q = $this->db->get_where("krs",$id);
			if($q->num_rows()>0){
				$data['user_id_dosen'] = @$_SESSION['username'];
				$data['tgl_update'] = date('Y-m-d');
				$this->db->update('krs',$data,$id);
				echo "Nilai Sukses disimpan";
			}else{
				echo "tidak ada aksi";
			}

		}else{
			redirect('login','refresh');
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
