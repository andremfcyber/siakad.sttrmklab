<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lappembayaran extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */
	public function index()
	{
		$cek = @$_SESSION['logged_in'];
		if(!empty($cek)){
			$d['judul']="Laporan Pembayaran Mahasiswa";
			$d['class'] = "lapkeuangan";

			$d['content']= 'lappembayaran/form';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

  public function list_smt()
  {
    $thak = $this->input->post('thak');

    $this->db->select('smt');
    $this->db->where('th_akademik_kode',$thak);
    $this->db->group_by('smt');
    $this->db->order_by('smt','ASC');
    $data = $this->db->get('bayar_mhs');
    echo "<option value=''>-</option>";
    foreach($data->result() as $row)
    {
      echo "<option value='$row->smt'>$row->smt</option>";
    }
  }
  public function cari_data()
	{
		$cek = @$_SESSION['logged_in'];
		if(!empty($cek)){

      $th_akademik = $this->input->post('th_ak');
      $kd_prodi = $this->input->post('kd_prodi');
      $tanggal = $this->input->post('tanggal');
			$tanggal2 = $this->input->post('tanggal2');
      $kelas = $this->input->post('kelas');
      $semester = $this->input->post('semester');

      $this->db->where('a.th_akademik_kode',$th_akademik);

      if(!empty($kd_prodi)){
        $this->db->where('b.kd_prodi',$kd_prodi);
      }

      // if(!empty($tanggal)){
      //   $tgl = $this->model_global->tgl_sql($tanggal);
      //   $this->db->where('a.tanggal',$tgl);
      // }

			if(!empty($tanggal) AND !empty($tanggal2)){
        $tgl = $this->model_global->tgl_sql($tanggal);
				$tgl2 = $this->model_global->tgl_sql($tanggal2);
        $this->db->where('a.tanggal >=',$tgl);
				$this->db->where('a.tanggal <=',$tgl2);
      }

      if(!empty($kelas)){
        $this->db->where('b.kelas',$kelas);
      }

      if(!empty($semester)){
        $this->db->where('a.smt',$semester);
      }

      $this->db->order_by('a.nomor');
      $this->db->from('bayar_mhs as a');
      $this->db->join('mahasiswa as b','a.nim=b.nim');
      $d['data'] = $this->db->get();
			$this->load->view('lappembayaran/view',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function cetak()
	{


		$th_akademik = $this->input->post('th_ak');
		$kd_prodi = $this->input->post('kd_prodi');
		$tanggal = $this->input->post('tanggal');
		$tanggal2 = $this->input->post('tanggal2');
		$kelas = $this->input->post('kelas');
		$semester = $this->input->post('semester');

		$this->db->where('a.th_akademik_kode',$th_akademik);

		if(!empty($kd_prodi)){
			$this->db->where('b.kd_prodi',$kd_prodi);
		}

		if(!empty($tanggal) AND !empty($tanggal2)){
			$tgl = $this->model_global->tgl_sql($tanggal);
			$tgl2 = $this->model_global->tgl_sql($tanggal2);
			$this->db->where('a.tanggal >=',$tgl);
			$this->db->where('a.tanggal <=',$tgl2);
		}

		if(!empty($kelas)){
			$this->db->where('b.kelas',$kelas);
		}

		if(!empty($semester)){
			$this->db->where('a.smt',$semester);
		}

		$this->db->order_by('a.nomor');
		$this->db->from('bayar_mhs as a');
		$this->db->join('mahasiswa as b','a.nim=b.nim');
		$d['data'] = $this->db->get();

		// header("Content-type: application/octet-stream");
		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=Laporan_Pembayaran_".$th_akademik.".xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->view('lappembayaran/view',$d);
	}

}
