<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jenis_tagihan extends CI_Controller {

    /**
     * Index Page for this controller.
     * Programmer : Deddy Rusdiansyah.S.Kom
     * http://deddyrusdiansyah.blogspot.com
     * http://softwarebanten.com
     * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
     * Developer : Fitria Wahyuni.S.Pd
     */
    public function __construct() {
        parent::__construct();
        //  $this->load->helper("url");
        //  $this->load->library('pagination');
    }

    public function index() {

        $cek = @$_SESSION['logged_in'];
        $level = @$_SESSION['level'];
        if (!empty($cek) && $level == 'admin') {

            // print_r($this->uri->segment(3));
            // $config = array();
            // $config['base_url'] 		= site_url('jenis_tagihan/index/');
            // $config['total_rows'] 	= $this->model_data->record_count('jenis_tagihan');;//200;
            // $config['per_page'] 		= 20;
            // $config['uri_segment'] 	= 3;
            //
			// $this->pagination->initialize($config);
            // $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            // $get_data = $this->model_data->fetch_data($config["per_page"], $page,'jenis_tagihan');
            // print_r($get_data);die;
            // $d["links"] = $this->pagination->create_links();

            $d['judul'] = "Jenis Tagihan";
            $d['class'] = "keuangan";
            // $d['data'] = $get_data;
//            $d['list_th_akademik'] = $this->db->select('th_akademik.th_akademik')->group_by('th_akademik, kode')->order_by('kode', 'DESC')->get('th_akademik');
            $d['list_th_akademik'] = $this->db->select('th_akademik.th_akademik')->group_by('th_akademik,kode')->order_by('kode', 'DESC')->get('th_akademik');
            $d['content'] = 'jenistagihan/view';
            $this->load->view('home', $d);
        } else {
            redirect('login', 'refresh');
        }
    }

    public function get_json() {
        // $this->load->model('test_model');
        // $results = $this->test_model->load_grid();
        $this->db->select('a.*,b.prodi as nama_prodi');
        $this->db->from('jenis_tagihan as a');
        $this->db->join('prodi as b', 'b.kd_prodi=a.kd_prodi', 'left');


			$this->db->order_by("a.th_akademik", "desc");
			$this->db->order_by("b.prodi", "asc");
			$this->db->order_by("a.smt", "asc");

        // $this->db->join('satuan as c','c.id=a.satuan_id','left');
        // $this->db->form('jenis_tagihan')
        $results = $this->db->get()->result_array();
        $data = array();
        $no = 1;
        foreach ($results as $r) {
            array_push($data, array(
                $no++,
                $r['th_akademik'],
                $r['nama_prodi'],
                $r['smt'],
                $r['nama'],
                number_format($r['jumlah']),
                $r['x_sks'],
               
                '<a href="#modal-table" class="btn btn-mini btn-primary" onclick="javascript:editData(' . $r['id'] . ')" data-toggle="modal">Edit</a>' .
                // anchor('#modal-table', 'Edit',array('class'=>'btn btn-mini btn-primary')).'  '.
                anchor('jenis_tagihan/hapus/' . $r['id'], 'Delete', array("onclick" => "return confirm('yakin akan menghapus ?')", 'class' => 'btn btn-mini btn-danger'))
            ));
        }

        echo json_encode(array('data' => $data));
    }

    public function cari() {
        $cek = @$_SESSION['logged_in'];
        $level = @$_SESSION['level'];
        if (!empty($cek) && $level == 'admin') {
            $id['id'] = $this->input->post('cari');

            $q = $this->db->get_where("jenis_tagihan", $id);
            $row = $q->num_rows();
            if ($row > 0) {
                foreach ($q->result() as $dt) {
                    $d['th_akademik_kode'] = $dt->th_akademik;
                    $d['kd_prodi'] = $dt->kd_prodi;
                    $d['smt'] = $dt->smt;
                    // $d['kode'] = $dt->kode;
                    $d['nama'] = $dt->nama;
                    $d['jumlah'] = $dt->jumlah;
                    $d['x_sks'] = $dt->x_sks;

                    $d['biaya_pembangunan'] = $dt->biaya_pembangunan;
                    $d['biaya_seragam'] = $dt->biaya_seragam;
                    $d['biaya_kesehatan'] = $dt->biaya_kesehatan;
                    $d['biaya_nytb'] = $dt->biaya_nytb;
                    $d['biaya_pendaftaran'] = $dt->biaya_pendaftaran;
                    $d['biaya_topi'] = $dt->biaya_topi;

                    $d['biaya_weekend'] = $dt->biaya_weekend;
                    $d['biaya_registrasi'] = $dt->biaya_registrasi;

                    $d['biaya_pembimbingan'] = $dt->biaya_pembimbingan;
                    $d['biaya_wisuda'] = $dt->biaya_wisuda;

                    $d['spp'] = $dt->spp;
                    $d['biaya_uang_makan'] = $dt->biaya_uang_makan;
                    $d['biaya_uang_saku'] = $dt->biaya_uang_saku;

                    $d['biaya_lab_com'] = $dt->biaya_kesehatan;
                    $d['biaya_lab_music'] = $dt->biaya_nytb;
                    $d['kartu_spp'] = $dt->biaya_pendaftaran;
                    $d['biaya_ijazah'] = $dt->biaya_topi;
                }
                echo json_encode($d);
            } else {
                $d['th_akademik_kode'] = '';
                $d['kd_prodi'] = '';
                $d['smt'] = '';
                // $d['kode'] 		= '';
                $d['nama'] = '';
                $d['jumlah'] = '';
                $d['x_sks'] = '';

                $d['biaya_pembangunan'] = '';
                $d['biaya_seragam'] = '';
                $d['biaya_kesehatan'] = '';
                $d['biaya_nytb'] = '';
                $d['biaya_pendaftaran'] = '';
                $d['biaya_topi'] = '';

                $d['biaya_weekend'] = '';
                $d['biaya_registrasi'] = '';

                $d['biaya_pembimbingan'] = '';
                $d['biaya_wisuda'] = '';

                $d['spp'] = '';
                $d['biaya_uang_makan'] = '';
                $d['biaya_uang_saku'] = '';

                $d['biaya_lab_com'] = '';
                $d['biaya_lab_music'] = '';
                $d['kartu_spp'] = '';
                $d['biaya_ijazah'] = '';
                echo json_encode($d);
            }
        } else {
            redirect('login', 'refresh');
        }
    }

    public function simpan() {

        $cek = @$_SESSION['logged_in'];
        $level = @$_SESSION['level'];
        if (!empty($cek) && $level == 'admin') {
            // $id['th_akademik_kode'] = $this->input->post('th_akademik_kode');
            // $id['kode'] = $this->input->post('kode');
            $id['id'] = (int) $this->input->post('id');

            $dt['th_akademik'] = $this->input->post('th_akademik_kode');
            $dt['kd_prodi'] = $this->input->post('kd_prodi');
            // $dt['kode'] = $this->input->post('kode');
            $dt['smt'] = (int) $this->input->post('smt');
            $dt['nama'] = $this->input->post('nama');
            $dt['jumlah'] = (int) $this->input->post('jumlah');
            $dt['x_sks'] = $this->input->post('x_sks');
            $dt['user_id'] = @$_SESSION['username'];


            // Uang Masuk Pendidikan
            $dt['biaya_pembangunan'] = $this->input->post('biaya_pembangunan');
            $dt['biaya_seragam'] = $this->input->post('biaya_seragam');
            $dt['biaya_kesehatan'] = $this->input->post('biaya_kesehatan');
            $dt['biaya_nytb'] = $this->input->post('biaya_nytb');
            $dt['biaya_pendaftaran'] = $this->input->post('biaya_pendaftaran');
            $dt['biaya_topi'] = $this->input->post('biaya_topi');

            // Registrasi
            $dt['biaya_weekend'] = $this->input->post('biaya_weekend');
            $dt['biaya_registrasi'] = $this->input->post('biaya_registrasi');

            // Bimbingan Dan Wisuda
            $dt['biaya_pembimbingan'] = $this->input->post('biaya_pembimbingan');
            $dt['biaya_wisuda'] = $this->input->post('biaya_wisuda');

            // Uang Makan & SPP, Uang Saku
            $dt['spp'] = $this->input->post('spp');
            $dt['biaya_uang_makan'] = $this->input->post('biaya_uang_makan');
            $dt['biaya_uang_saku'] = $this->input->post('biaya_uang_saku');

            // Lab Com, Lab Music
            $dt['biaya_lab_com'] = $this->input->post('biaya_lab_com');
            $dt['biaya_lab_music'] = $this->input->post('biaya_lab_music');

            // Kartu SPP
            $dt['kartu_spp'] = $this->input->post('kartu_spp');

            // Ijazah
            $dt['biaya_ijazah'] = $this->input->post('biaya_ijazah');

            $q = $this->db->get_where("jenis_tagihan", $id);
            $row = $q->num_rows();
            if ($row > 0) {
                $dt['update_date'] = date('Y-m-d H:i:s');
                $this->db->update("jenis_tagihan", $dt, $id);
                echo "Data Sukses diUpdate";
            } else {
                $dt['insert_date'] = date('Y-m-d H:i:s');
                $this->db->insert("jenis_tagihan", $dt);
                echo "Data Sukses diSimpan";
            }
        } else {
            redirect('login', 'refresh');
        }
    }

    public function hapus() {
        $cek = @$_SESSION['logged_in'];
        $level = @$_SESSION['level'];
        if (!empty($cek) && $level == 'admin') {
            $id['id'] = $this->uri->segment(3);

            $q = $this->db->get_where("jenis_tagihan", $id);
            $row = $q->num_rows();
            if ($row > 0) {
                $this->db->delete("jenis_tagihan", $id);
            }
            redirect('jenis_tagihan', 'refresh');
        } else {
            redirect('login', 'refresh');
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
