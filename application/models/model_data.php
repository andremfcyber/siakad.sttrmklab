<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_data extends CI_Model {

    /**
     * @author : Deddy Rusdiansyah
     * @web : http://deddyrusdiansyah.blogspot.com
     * @keterangan : Model untuk menangani semua query database aplikasi
     * */
    //  public function getDataPembayaran($jenis_tagihan_id)
    //  {
    // 	 $this->db->where('jenis_tagihan_id',$jenis_tagihan_id);
    // 	 $data = $this->db->get('pembayaran_mhs');
    // 	 if($data->num_rows()>0)
    // 	 {
    // 		 $row = $data->row();
    // 		 $hasil = array('')
    // 	 }else{
    //
	// 	 }
    // 	 return $hasil;
    //  }

    public function __construct() {
        parent::__construct();
    }

    public function record_count($table, $where = null) {
        return $this->db->count_all($table);
    }

    public function fetch_data($limit, $start, $table, $where = null) {
        if ($where) {
            $this->db->where('kd_prodi', $where);
        }
        $this->db->limit($limit, $start);
        $query = $this->db->get($table);

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getLevel() {
        $username = @$_SESSION['username'];
        $infoUser = $this->model_data->getInfoUser($username);
        $level = $infoUser['level'];
        return $level;
    }

    public function getInfoUser($username) {
        $this->db->where('username', $username);
        $data = $this->db->get('admins');
        if ($data->num_rows() > 0) {
            $row = $data->row();
            $hasil = array(
                'nama_lengkap' => $row->nama_lengkap,
                'level' => $row->level
            );
        } else {
            $hasil = array(
                'nama_lengkap' => '',
                'level' => ''
            );
        }
        return $hasil;
    }

    public function getInfoBayar($nomor) {
        $this->db->where('nomor', $nomor);
        $data = $this->db->get('bayar_mhs');
        if ($data->num_rows() > 0) {
            $row = $data->row();
            $hasil = array(
                'nomor' => $row->nomor,
                'jenis_tagihan_id' => $row->jenis_tagihan_id,
                'jenis_tagihan' => $this->model_data->getJenisTagihan($row->jenis_tagihan_id)['nama'],
                'th_akademik' => $row->th_akademik,
                'th_akademik_kode' => $row->th_akademik_kode,
                'nim' => $row->nim,
                'smt' => $row->smt,
                'sks' => $row->sks,
                'tanggal' => $row->tanggal,
                'jumlah' => $row->jumlah,
                'user_id' => $row->user_id
            );
        } else {
            $hasil = array(
                'nomor' => '',
                'jenis_tagihan_id' => '',
                'jenis_tagihan' => '',
                'th_akademik' => '',
                'th_akademik_kode' => '',
                'nim' => '',
                'smt' => '',
                'sks' => '',
                'tanggal' => '',
                'jumlah' => '',
                'user_id' => ''
            );
        }
        return $hasil;
    }

    public function getInfoBayarMaba($nomor) {
        $this->db->where('nomor', $nomor);
        $data = $this->db->get('bayar_maba');
        if ($data->num_rows() > 0) {
            $row = $data->row();
            $hasil = array(
                'nomor' => $row->nomor,
                'jenis_tagihan_maba_id' => $row->jenis_tagihan_maba_id,
                'jenis_tagihan' => $this->db->get_where('master_tagihan_maba', array('id' => $row->jenis_tagihan_maba_id))->row_array()['nama_tagihan'],
                'th_akademik' => $row->th_akademik,
                'th_akademik_kode' => $row->th_akademik_kode,
                'no_pendaftaran' => $row->no_pendaftaran,
                'tanggal' => $row->tanggal,
                'jumlah' => $row->jumlah,
                'user_id' => $row->user_id
            );
        } else {
            $hasil = array(
                'nomor' => '',
                'jenis_tagihan_maba_id' => '',
                'jenis_tagihan' => '',
                'th_akademik' => '',
                'th_akademik_kode' => '',
                'no_pendaftaran' => '',
                'tanggal' => '',
                'jumlah' => '',
                'user_id' => ''
            );
        }
        return $hasil;
    }

    public function getJumlahPembayaran($jenis_tagihan_id, $nim) {
        $this->db->select('SUM(jumlah) as total');
        $this->db->where('nim', $nim);
        $this->db->where('jenis_tagihan_id', $jenis_tagihan_id);
        $data = $this->db->get('bayar_mhs');
        if ($data->num_rows() > 0) {
            $row = $data->row();
            $hasil = $row->total;
        } else {
            $hasil = 0;
        }
        return $hasil;
    }

    public function getJumlahPembayaranMaba($jenis_tagihan_maba_id, $no_pendaftaran) {
        $this->db->select('SUM(jumlah) as total');
        $this->db->where('no_pendaftaran', $no_pendaftaran);
        $this->db->where('jenis_tagihan_maba_id', $jenis_tagihan_maba_id);
        $data = $this->db->get('bayar_maba');
        if ($data->num_rows() > 0) {
            $row = $data->row();
            $hasil = $row->total;
        } else {
            $hasil = 0;
        }
        return $hasil;
    }

    public function cekStatusTagihan($th_akademik_kode, $jenis_tagihan_id, $nim) {
        $this->db->where('th_angkatan_kode', $th_akademik_kode);
        $this->db->where('jenis_tagihan_id', $jenis_tagihan_id);
        $this->db->where('nim', $nim);
        $data = $this->db->get('tagihan_mhs');
        if ($data->num_rows() > 0) {
            $hasil = "<span class='badge badge-success'> Sudah di Upload</span>";
        } else {
            $hasil = "<span class='badge badge-important'>Belum di Upload</span>";
        }
        return $hasil;
    }

    public function getInfoJadwal($id_jadwal) {
        $this->db->where('id_jadwal', $id_jadwal);
        $data = $this->db->get('jadwal');
        if ($data->num_rows() > 0) {
            $row = $data->row();
            $hasil = array(
                'th_akademik' => $row->th_akademik,
                'semester' => $row->semester,
                'kd_prodi' => $row->kd_prodi,
                'kd_mk' => $row->kd_mk,
                'smt' => $row->smt,
                'kd_dosen' => $row->kd_dosen,
                'hari' => $row->hari,
                'pukul' => $row->pukul,
                'jam_mulai' => $row->jam_mulai,
                'jam_selesai' => $row->jam_selesai,
                'ruang' => $row->ruang
            );
        } else {
            $hasil = array(
                'th_akademik' => '',
                'semester' => '',
                'kd_prodi' => '',
                'kd_mk' => '',
                'smt' => '',
                'kd_dosen' => '',
                'hari' => '',
                'pukul' => '',
                'jam_mulai' => '',
                'jam_selesai' => '',
                'ruang' => ''
            );
        }
        return $hasil;
    }

    public function getInfoMK($kd_mk) {
        $this->db->where('kd_mk', $kd_mk);
        $data = $this->db->get('mata_kuliah');
        if ($data->num_rows() > 0) {
            $row = $data->row();
            $hasil = array(
                'kd_prodi' => $row->kd_prodi,
                'nama_mk' => $row->nama_mk,
                'sks' => $row->sks,
                'smt' => $row->smt,
                'semester' => $row->semester
            );
        } else {
            $hasil = array(
                'kd_prodi' => '',
                'nama_mk' => '',
                'sks' => '',
                'smt' => '',
                'semester' => ''
            );
        }
        return $hasil;
    }

    public function getInfoMhs($nim) {
        $this->db->where('nim', $nim);
        $data = $this->db->get('mahasiswa');
        if ($data->num_rows() > 0) {
            $row = $data->row();
            $hasil = array('nama_mhs' => $row->nama_mhs,
                'sex' => $row->sex,
                'th_akademik' => $row->th_akademik,
                'th_angkatan' => substr($row->th_akademik, 0, 4) . '1',
                'kd_prodi' => $row->kd_prodi,
                'kelas' => $row->kelas,
                'hp' => $row->hp,
                'email' => $row->email,
                'nama_prodi' => $this->nama_jurusan($row->kd_prodi),
                'smt' => $this->model_global->semester($nim, $row->th_akademik),
                'max_smt' => $this->model_data->getInfoProdi($row->kd_prodi)['max_semester'],
                'smt_aktif' => $this->model_global->semester($nim, $row->th_akademik),
                'isi_krs' => $row->isi_krs
            );
        } else {
            $hasil = array('nama_mhs' => '',
                'sex' => '',
                'th_akademik' => '',
                'th_angkatan' => '',
                'kd_prodi' => '',
                'kelas' => '',
                'hp' => '',
                'email' => '',
                'nama_prodi' => '',
                'smt' => '',
                'max_smt' => '',
                'smt_aktif' => '',
                'isi_krs' => ''
            );
        }
        return $hasil;
    }

    public function getInfoMaba($no_pendaftaran) {
        $this->db->where('no_pendaftaran', $no_pendaftaran);
        $data = $this->db->get('pmb.data_pribadi');

        if ($data->num_rows() > 0) {
            $this->db->select('pmb.users.email');
            $this->db->from('pmb.users');
            $this->db->where('no_pendaftaran', $no_pendaftaran);
            $email = $this->db->get()->row()->email;
            $row = $data->row();
            $hasil = array('nama_mhs' => $row->nama,
                'sex' => ($row->jk == 'L')?'Laki-Laki':'Perempuan',
                'th_akademik' => $this->model_global->getThAkademikAktif()['th_akademik'],
                // 'th_angkatan' => substr($row->th_akademik, 0, 4) . '1',
                'kd_prodi' => $row->prodi,
                // 'kelas' => $row->kelas,
                'hp' => $row->tlp,
                'email' => ($email!==null)?$email:'',
                'nama_prodi' => $this->nama_jurusan($row->prodi),
                // 'smt' => $this->model_global->semester($no_pendaftaran, $row->th_akademik),
                // 'max_smt' => $this->model_data->getInfoProdi($row->kd_prodi)['max_semester'],
                // 'smt_aktif' => $this->model_global->semester($no_pendaftaran, $row->th_akademik),
                // 'status_bayar' => $row->status_bayar,
                // 'mhs' => $row->mhs
            );
        } else {
            $hasil = array('nama_mhs' => '',
                'sex' => '',
                // 'th_akademik' => '',
                // 'th_angkatan' => '',
                'kd_prodi' => '',
                // 'kelas' => '',
                'hp' => '',
                'email' => '',
                'nama_prodi' => '',
                // 'smt' => '',
                // 'max_smt' => '',
                // 'smt_aktif' => '',
                // 'status_bayar' => '',
                // 'mhs' => ''
            );
        }
        return $hasil;
    }

    public function getInfoTagihan($id) {
        $this->db->where('id', $id);
        $data = $this->db->get('tagihan_mhs');
        if ($data->num_rows() > 0) {
            $row = $data->row();
            $hasil = array('jenis_tagihan_id' => $row->jenis_tagihan_id,
                'kode_tagihan' => $this->getJenisTagihan($row->jenis_tagihan_id)['kode'],
                'nama_tagihan' => $this->getJenisTagihan($row->jenis_tagihan_id)['nama'],
                'nim' => $row->nim,
                'kd_prodi' => $row->kd_prodi,
                'kelas' => $row->kelas,
                'th_akademik_kode' => $row->th_akademik_kode,
                'th_angkatan_kode' => $row->th_angkatan_kode,
                'smt' => $row->smt,
                'jumlah' => $row->jumlah
            );
        } else {
            $hasil = array('jenis_tagihan_id' => '',
                'kode_tagihan' => '',
                'nama_tagihan' => '',
                'nim' => '',
                'kd_prodi' => '',
                'kelas' => '',
                'th_akademik_kode' => '',
                'th_angkatan_kode' => '',
                'smt' => '',
                'jumlah' => 0
            );
        }
        return $hasil;
    }

    public function getJenisTagihan($id) {
        $this->db->where('id', $id);
        $data = $this->db->get('jenis_tagihan');
        if ($data->num_rows() > 0) {
            $row = $data->row();
            $hasil = array(
                'th_akademik' => $row->th_akademik,
                'nama' => $row->nama,
                'smt' => $row->smt,
                'kd_prodi' => $row->kd_prodi,
                'jumlah' => $row->jumlah
            );
        } else {
            $hasil = array(
                'th_akademik' => '',
                'nama' => '',
                'smt' => '',
                'kd_prodi' => '',
                'jumlah' => 0
            );
        }
        return $hasil;
    }

    public function getInfoProdi($kd_prodi) {
        $this->db->where('kd_prodi', $kd_prodi);
        $data = $this->db->get('prodi');
        if ($data->num_rows() > 0) {
            $row = $data->row();
            $hasil = array(
                'prodi' => $row->prodi,
                'singkat' => $row->singkat,
                'ketua_prodi' => $row->ketua_prodi,
                'nik' => $row->nik,
                'akreditasi' => $row->akreditasi,
                'max_semester' => $row->max_semester
            );
        } else {
            $hasil = array(
                'prodi' => '',
                'singkat' => '',
                'ketua_prodi' => '',
                'nik' => '',
                'akreditasi' => '',
                'max_semester' => ''
            );
        }
        return $hasil;
    }

    public function getThAkademik($kode) {
        $this->db->where('kode', $kode);
        $data = $this->db->get('th_akademik');
        if ($data->num_rows() > 0) {
            $row = $data->row();
            $hasil = $row->th_akademik;
        } else {
            $hasil = '';
        }
        return $hasil;
    }

    public function tagihan($th_akademik_kode, $id) {
        $this->db->where('th_akademik_kode', $th_akademik_kode);
        $this->db->where('id', $id);
        $data = $this->db->get('jenis_tagihan');
        if ($data->num_rows() > 0) {
            $row = $data->row();
            $hasil = $row->jumlah;
        } else {
            $hasil = 0;
        }
        return $hasil;
    }

    public function data_jurusan($kd_prodi = null) {
        if($kd_prodi!=null && $kd_prodi!=0){
            $q = $this->db->get_where('prodi', array('kd_prodi' => $kd_prodi));
            return $q;
        }else{
            $q = $this->db->order_by('kd_prodi');
            $q = $this->db->get('prodi');
            return $q;
        }
    }

    public function nama_jurusan($id) {
        $q = $this->db->query("SELECT * FROM prodi WHERE kd_prodi='$id'");
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $dt) {
                $hasil = $dt->prodi;
            }
        } else {
            $hasil = '';
        }
        return $hasil;
    }

    public function singkat_jurusan($id) {
        $q = $this->db->query("SELECT * FROM prodi WHERE kd_prodi='$id'");
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $dt) {
                $hasil = $dt->singkat;
            }
        } else {
            $hasil = '';
        }
        return $hasil;
    }

    public function id_prodi($id) {
        $q = $this->db->query("SELECT * FROM prodi WHERE kd_prodi='$id'");
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $dt) {
                $hasil = $dt->id_prodi;
            }
        } else {
            $hasil = '';
        }
        return $hasil;
    }

    public function data_mk($jur) {
        $id['kd_prodi'] = $jur;
        $q = $this->db->order_by('kd_mk');
        $q = $this->db->get_where('mata_kuliah', $id);
        return $q;
    }

    public function data_dosen($jur) {
        // $id['kd_prodi'] = $jur;
        // $q = $this->db->order_by('kd_dosen');
        // $q = $this->db->get_where('dosen', $id);
        // return $q;
        if($jur == 'all'){
            $a = '';
        }else{
            $a = 'where a.kd_prodi::integer ='.$jur;
        }
        $q = $this->db->query("SELECT a.*, b.prodi FROM dosen a left join prodi b on a.kd_prodi = b.kd_prodi $a");
        return $q;
    }

    public function th_akademik_jadwal() {
        $q = $this->db->query("SELECT th_akademik FROM jadwal GROUP BY th_akademik ORDER BY th_akademik desc");
        return $q;
    }

    public function th_akademik_krs_mhs($nim) {
        $q = $this->db->query("SELECT th_akademik FROM krs WHERE nim='$nim' GROUP BY th_akademik ORDER BY th_akademik desc");
        return $q;
    }

    public function th_akademik_krs_dosen($key) {
        $q = $this->db->query("SELECT th_akademik FROM krs WHERE kd_dosen='$key' GROUP BY th_akademik ORDER BY th_akademik desc");
        return $q;
    }

    public function data_mhs($jur) {
        $id['kd_prodi'] = $jur;
        $q = $this->db->order_by('nim');
        $q = $this->db->get_where('mahasiswa', $id);
        return $q;
    }

    public function data_all_mhs() {
        $this->db->where('status', 'Aktif');
        $this->db->order_by('nim');
        $q = $this->db->get('mahasiswa');
        return $q;
    }

    /*     * * jumlah data ** */

    public function jml_data($table) {
        $q = $this->db->get($table);
        return $q->num_rows();
    }

    public function jml_data_jadwal($th, $smt, $prodi) {
        $key['th_akademik'] = $th;
        $key['semester'] = $smt;
        $key['kd_prodi'] = $prodi;
        $q = $this->db->get_where("jadwal", $key);
        $row = $q->num_rows();
        return $row;
    }

    public function data_krs($th, $smt, $prodi) {

        $q = $this->db->query("SELECT a.th_akademik,a.semester,b.kd_prodi,a.nim
								FROM krs as a
								JOIN jadwal as b
								ON a.id_jadwal = b.id_jadwal
								WHERE a.th_akademik='$th' AND a.semester='$smt' AND b.kd_prodi='$prodi'
								GROUP BY a.th_akademik,a.nim,a.semester,b.kd_prodi");
        return $q;
    }

    public function jml_data_krs($th, $smt, $prodi) {

        $q = $this->db->query("SELECT a.th_akademik,a.semester,b.kd_prodi
                                FROM krs as a
                                JOIN jadwal as b
                                ON a.id_jadwal = b.id_jadwal
                                WHERE a.th_akademik='$th' AND a.semester='$smt' AND b.kd_prodi='$prodi'
                                GROUP BY a.th_akademik,a.nim,a.semester,b.kd_prodi");
        $row = $q->num_rows();
        return $row;
    }

    public function jml_data_nilai($th, $smt, $mk) {

        $q = $this->db->query("SELECT *
								FROM krs
								WHERE th_akademik='$th' AND semester='$smt' AND kd_mk='$mk' AND nilai_akhir IS NOT NULL");
        $row = $q->num_rows();
        return $row;
    }

    /*     * * data table ** */

    public function data($table) {
        $q = $this->db->get($table);
        return $q->result();
    }

    /*     * ** REFERENSI ** */

    public function semester() {
        $q = array('ganjil', 'genap');
        return $q;
    }

    public function smt() {
        $q = array('1', '2', '3', '4', '5', '6', '7', '8');
        return $q;
    }

    public function hari_kuliah() {
        $q = array('Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu', 'Minggu');
        return $q;
    }

    public function jam_kuliah() {
        $q = array('08.00 - 10.00', '10.00 - 12.00', '13.00 - 15.00', '15.00 - 17.00', '19.00 - 20.00', '20.00 - 22.00');
        return $q;
    }

    public function ruang_kuliah() {
        $q = array('R01', 'R02', 'R03', 'R04');
        return $q;
    }

    public function status_mhs_mutasi() {
        $q = array('Mutasi', 'Cuti', 'DO', 'Lulus');
        return $q;
    }

    public function status_mhs() {
        $q = array('Aktif', 'PPL', 'Cuti', 'DO', 'Lulus');
        return $q;
    }

    public function get_kelas(){
        $q = array('I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII');
        return $q;
    }

    public function get_jenis_info(){
      $q = array('UMUM', 'DOSEN', 'MAHASISWA');
        return $q;
    }

    public function status_dosen() {
        $q = array('Aktif', 'Keluar');
        return $q;
    }

    public function level_pengguna() {
        $q = array('super admin', 'admin', 'keuangan');
        return $q;
    }

    public function nilai() {
        //$q = array('A','A-','B+','B','B-','C+','C');
        $q = array('4.0','3.7','3.3','3.0','2.7','2.3','2.0');
//        $q = array('A', 'B', 'C', 'D', 'E');
        return $q;
    }

    public function jenjang_pendidikan() {
        $q = array('SMA / Sederajat', 'D3', 'S1', 'S2', 'S3');
        return $q;
    }

    /*     * * cari_data * */

    public function cari_id_username($u) {
        $q = $this->db->query("SELECT * FROM admins WHERE username='$u'");
        foreach ($q->result() as $dt) {
            $hasil = $dt->id_username;
        }
        return $hasil;
    }

    public function cari_foto_username($u) {
        $q = $this->db->query("SELECT * FROM admins WHERE username='$u'");
        foreach ($q->result() as $dt) {
            $hasil = $dt->foto;
        }
        return $hasil;
    }

    public function cari_foto_mahasiswa($u) {
        $q = $this->db->query("SELECT * FROM mahasiswa WHERE nim='$u'");
        foreach ($q->result() as $dt) {
            $hasil = $dt->file_foto;
        }
        return $hasil;
    }

    public function cari_foto_dosen($u) {
        $q = $this->db->query("SELECT * FROM dosen WHERE kd_dosen='$u'");
        foreach ($q->result() as $dt) {
            $hasil = $dt->file_foto;
        }
        return $hasil;
    }

    public function cari_data_mhs($nim) {
        $id['nim'] = $nim;
        $q = $this->db->get_where("mahasiswa", $id);
        return $q;
    }

    public function cari_semester_aktif($nim) {
        $q = $this->db->query("SELECT smt FROM krs WHERE nim='$nim' GROUP BY smt ORDER BY smt");
        return $q;
    }

    public function cari_smt_krs($th, $smt, $nim) {
        $q = $this->db->query("SELECT smt FROM krs WHERE th_akademik='$th' AND semester='$smt' AND nim='$nim' GROUP BY smt");
        foreach ($q->result() as $dt) {
            $hasil = $dt->smt;
        }
        return $hasil;
    }

    public function cari_sks_jadwal($id) {
        $q = $this->db->query("SELECT * FROM jadwal WHERE id_jadwal='$id'");
        foreach ($q->result() as $dt) {
            $mk = $dt->kd_mk;
            $q_mk = $this->db->query("SELECT sks FROM mata_kuliah WHERE kd_mk='$mk'");
            foreach ($q_mk->result() as $dt_mk) {
                $hasil = $dt_mk->sks;
            }
        }
        return $hasil;
    }

    public function cari_jml_sks_krs($th, $smt, $nim) {
        $q = $this->db->query("SELECT SUM(sks) as t_sks FROM krs WHERE th_akademik='$th' AND semester='$smt' AND nim='$nim' GROUP BY smt");
        if ($q->num_rows > 0) {
            foreach ($q->result() as $dt) {
                $hasil = $dt->t_sks;
            }
        } else {
            $hasil = 0;
        }
        return $hasil;
    }

    public function cari_jml_sks_krs_($th, $nim) {
        $q = $this->db->query("SELECT SUM(sks) as t_sks FROM krs WHERE th_akademik='$th' AND nim='$nim' GROUP BY smt");
        if ($q->num_rows > 0) {
            foreach ($q->result() as $dt) {
                $hasil = $dt->t_sks;
            }
        } else {
            $hasil = 0;
        }
        return $hasil;
    }

    public function cari_ipk_lalu($smt, $nim) {
        if ($smt > 1) {

            $smt = $smt - 1;
            $q = $this->db->query("SELECT * FROM krs WHERE smt='$smt' AND nim='$nim'");
            $t_sks = 0;
            $t_nilai = 0;
            $t_akhir = 0;
            if ($q->num_rows() > 0) {
                foreach ($q->result() as $dt) {
                    $t_sks = $t_sks + $dt->sks;
                    $n_angka = $this->model_data->cari_nilai_angka($dt->nilai_akhir);
                    $t_nilai = (float)$t_nilai + (float)$n_angka;
                    $akhir = (float)$n_angka * (int)$dt->sks;
                    $t_akhir = $t_akhir + $akhir;
                }
                $h = number_format($t_akhir / $t_sks, 2);
            } else {
                $h = 0;
            }
        } else {
            $h = 0;
        }
        return $h;
    }

    public function cari_ip($nim) {

        $q = $this->db->query("SELECT * FROM krs WHERE nim='$nim' AND nilai_akhir<>''");
        if ($q->num_rows() > 0) {
            $t_sks = 0;
            $t_nilai = 0;
            $t_akhir = 0;
            $h = 0;
            foreach ($q->result() as $dt) {
                $t_sks = $t_sks + $dt->sks;
                $n_angka = $this->model_data->cari_nilai_angka($dt->nilai_akhir);
                $t_nilai = $t_nilai + $n_angka;
                $akhir = $n_angka * $dt->sks;
                $t_akhir = $t_akhir + $akhir;
            }
            $h = number_format($t_akhir / $t_sks, 2);
        } else {
            $h = 0;
        }
        return $h;
    }

    public function cari_ipk($smt, $nim) {

        $q = $this->db->query("SELECT * FROM krs WHERE smt='$smt' AND nim='$nim' AND nilai_akhir<>''");
        if ($q->num_rows() > 0) {
            $t_sks = 0;
            $t_nilai = 0;
            $t_akhir = 0;
            foreach ($q->result() as $dt) {
                $t_sks = $t_sks + $dt->sks;
//                $n_angka = $this->model_data->cari_nilai_angka($dt->nilai_akhir);
                $n_angka = (float)$dt->nilai_akhir;
                $t_nilai = $t_nilai + $n_angka;
                $akhir = $n_angka * $dt->sks;
                $t_akhir = $t_akhir + $akhir;
            }
            $h = number_format($t_akhir / $t_sks, 2);
        } else {
            $h = 0;
        }
        return $h;
    }

    public function cari_nilai_angka($nilai) {
        if ($nilai == 'A') {
            $h = 4;
        } elseif ($nilai == 'B') {
            $h = 3;
        } elseif ($nilai == 'C') {
            $h = 2;
        } elseif ($nilai == 'D') {
            $h = 1;
        } elseif ($nilai == 'E') {
            $h = 0;
        } else {
            $h = '';
        }
        return $h;
    }

    public function cari_nilai_huruf($nilai) {

//    	select * from nilai_huruf where 2.8 between range_from and range_to

        if ($nilai == '4.0') {
            $h = 'A';
        } elseif ($nilai == '3.7') {
            $h = 'A-';
        } elseif ($nilai == '3.3') {
            $h = 'B+';
        } elseif ($nilai == '3.0') {
            $h = 'B';
        } elseif ($nilai == '2.7') {
            $h = 'B-';
        } elseif ($nilai == '2.3') {
            $h = 'C+';
        } elseif ($nilai == '2.0') {
            $h = 'C';
        } else {
            $h = '';
        }
        return $h;
    }

    public function cari_nama_mk($key) {
        $q = $this->db->query("SELECT * FROM mata_kuliah WHERE kd_mk='$key'");
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $dt) {
                $hasil = $dt->nama_mk;
            }
        } else {
            $hasil = '';
        }
        return $hasil;
    }

    public function cari_nama_dosen($key) {
        $q = $this->db->query("SELECT * FROM dosen WHERE kd_dosen='$key'");
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $dt) {
                $hasil = $dt->nama_dosen;
            }
        } else {
            $hasil = '';
        }
        return $hasil;
    }

    public function cari_nama_ka_prodi($key) {
        $q = $this->db->query("SELECT * FROM prodi WHERE kd_prodi='$key'");
        foreach ($q->result() as $dt) {
            $hasil = array('nama' => $dt->ketua_prodi, 'nik' => $dt->nik);
        }
        return $hasil;
    }

    public function cari_data_mhs_lengkap($key) {
        $q = $this->db->query("SELECT * FROM mahasiswa WHERE nim='$key'");
        foreach ($q->result() as $dt) {
            $hasil = array('nama' => $dt->nama_mhs, 'sex' => $dt->sex);
        }
        return $hasil;
    }

    public function cari_nama_mhs($key) {
        $q = $this->db->query("SELECT * FROM mahasiswa WHERE nim='$key'");
        foreach ($q->result() as $dt) {
            $hasil = $dt->nama_mhs;
        }
        return $hasil;
    }

    public function cari_kd_prodi_mhs($key) {
        $q = $this->db->query("SELECT * FROM mahasiswa WHERE nim='$key'");
        foreach ($q->result() as $dt) {
            $hasil = $dt->kd_prodi;
        }
        return $hasil;
    }

    public function cari_mk_jadwal($key) {
        $q = $this->db->query("SELECT * FROM jadwal WHERE id_jadwal='$key'");
        foreach ($q->result() as $dt) {
            $hasil = $dt->kd_mk;
        }
        return $hasil;
    }

    public function jml_sks_mhs($th_ak, $smt, $nim) {
        $q = $this->db->query("SELECT SUM(sks) as t_sks FROM krs WHERE th_akademik='$th_ak' AND semester='$smt' AND nim='$nim'");
        $r = $q->num_rows();
        if ($r > 0) {
            foreach ($q->result() as $dt) {
                $h = $dt->t_sks;
            }
        } else {
            $h = 0;
        }
        return $h;
    }

    public function jml_mhs($kd_prodi){
      $q = $this->db->get_where('mahasiswa', array('kd_prodi' => $kd_prodi));
      if($q->num_rows()>0){
        return $q->num_rows();
      }else{
        return 0;
      }
    }
    
    public function cari_jml_mhs_mk($thak, $kdmk, $kddosen, $kelas) {
        $this->db->where('th_akademik', $thak);
        $this->db->where('kd_mk', $kdmk);
        $this->db->where('kd_dosen', $kddosen);
        $this->db->where('kelas', $kelas);
        $q = $this->db->get('krs'); // $this->db->query("SELECT * FROM krs WHERE th_akademik='$thak' AND kd_mk='$kdmk' AND kd_dosen='$kddosen'");
        $r = $q->num_rows();
        /*
          if($r>0){
          foreach($q->result() as $dt){
          $h = $dt->t_sks;
          }
          }else{
          $h = 0;
          }
         */
        return $r;
    }

    public function create_category_mhs($th) {
        $q = $this->db->query("SELECT th_akademik FROM mahasiswa WHERE th_akademik='$th' group by th_akademik");
        $hasil = '';
        foreach ($q->result() as $dt) {
            $hasil .=$dt->th_akademik . ',';
            $x = $dt->th_akademik;
        }
        //$hasil .= $x+9;
        return $hasil;
    }

    public function create_category($th) {
        $q = $this->db->query("SELECT th_akademik FROM mahasiswa WHERE th_akademik<='$th' group by th_akademik");
        $hasil = '';
        foreach ($q->result() as $dt) {
            $hasil .=$dt->th_akademik . ',';
            $x = $dt->th_akademik;
        }
        //$hasil .= $x+9;
        return $hasil;
    }

    public function create_category_krs_nim($nim) {
        $q = $this->db->query("SELECT th_akademik FROM krs WHERE nim='$nim' group by th_akademik ORDER BY th_akademik");
        $hasil = '';
        foreach ($q->result() as $dt) {
            $hasil .=$dt->th_akademik . ',';
            $x = $dt->th_akademik;
        }
        //$hasil .= $x+9;
        return $hasil;
    }

    public function create_data_krs_nim($nim) {
        $q = $this->db->query("SELECT th_akademik,smt FROM krs WHERE nim='$nim' group by th_akademik, smt ORDER BY th_akademik");
        //$data = array();
        $data = '[';
        foreach ($q->result() as $dt) {
            $th = $dt->th_akademik;
            $smt = $dt->smt;
            //$this->db->where(array('th_akademik'=>$th,'nim'=>$nim));
            //$this->db->from('krs');
            //$q2 = $this->db->get();
            $data .= $this->model_data->cari_ipk($smt, $nim) . ','; //$q2->num_rows();
        }
        $data .=']';
        //return json_encode($data);
        return $data;
    }

    public function data_chart($key) {
        $q = $this->db->query("SELECT th_akademik FROM krs group by th_akademik LIMIT 3");
        if ($q->num_rows() > 0) {
            $data = array();
            foreach ($q->result() as $dt) {
                $th = $dt->th_akademik;
                $q2 = $this->db->query("SELECT * FROM krs WHERE th_akademik='$th' AND kd_prodi='$key' GROUP BY nim,th_akademik");
                $data[] = $q2->num_rows();
            }
        } else {
            $data = 0;
        }
        return json_encode($data);
    }

    public function data_chart_mhs_aktif($th, $key) {
        $q = $this->db->query("SELECT th_akademik FROM mahasiswa WHERE th_akademik<='$th' group by th_akademik LIMIT 3");
        if ($q->num_rows() > 0) {
            $data = array();
            foreach ($q->result() as $dt) {
                $th = $dt->th_akademik;

                $this->db->where(array('th_akademik' => $th, 'kd_prodi' => $key, 'status' => 'Aktif'));
                //$this->db->group_by('nim','th_akadmeik');
                $this->db->from('mahasiswa');
                $q2 = $this->db->get();
                //$q2 = $this->db->query("SELECT * FROM krs WHERE th_akademik='$th' AND kd_prodi='$key' GROUP BY nim,th_akademik");
                $data[] = $q2->num_rows();
            }
        } else {
            $data = 0;
        }
        return json_encode($data);
    }

    public function data_chart_krs($th, $key) {
        $q = $this->db->query("SELECT th_akademik FROM krs WHERE th_akademik<='$th' group by th_akademik LIMIT 3");
        if ($q->num_rows() > 0) {
            $data = array();
            foreach ($q->result() as $dt) {
                $th = $dt->th_akademik;

                $this->db->where(array('th_akademik' => $th, 'kd_prodi' => $key));
                //$this->db->group_by('nim','th_akadmeik');
                $this->db->from('krs');
                $q2 = $this->db->get();
                //$q2 = $this->db->query("SELECT * FROM krs WHERE th_akademik='$th' AND kd_prodi='$key' GROUP BY nim,th_akademik");
                $data[] = $q2->num_rows();
            }
        } else {
            $data = 0;
        }
        return json_encode($data);
    }

    public function data_chart_wisuda($th, $key) {
        $q = $this->db->query("SELECT th_akademik FROM wisuda WHERE th_akademik<='$th' group by th_akademik LIMIT 3");
        if ($q->num_rows() > 0) {
            $data = array();
            foreach ($q->result() as $dt) {
                $th = $dt->th_akademik;

                $q2 = $this->db->query("SELECT a.th_akademik,b.kd_prodi
                                        FROM wisuda as a
                                        JOIN mahasiswa as b
                                        ON a.nim = b.nim
                                        WHERE a.th_akademik='" . $th . "' AND b.kd_prodi='" . $key . "'");
                $data[] = $q2->num_rows();
            }
        } else {
            $data = 0;
        }
        return json_encode($data);
    }

    public function data_chart_mhs($th, $status) {
        $q = $this->db->query("SELECT th_akademik FROM mahasiswa WHERE th_akademik='$th' group by th_akademik LIMIT 1");
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $dt) {
                $th = $dt->th_akademik;

                $this->db->where(array('th_akademik' => $th, 'status' => $status));
                $this->db->from('mahasiswa');
                $q2 = $this->db->get();
                $data = $q2->num_rows();
            }
        } else {
            $data = 0;
        }
        return $data;
    }

    public function data_chart_mhs_gender($th,$gender) {
      $q = $this->db->query("SELECT th_akademik FROM mahasiswa WHERE th_akademik='$th' group by th_akademik LIMIT 1");
      if ($q->num_rows() > 0) {
          foreach ($q->result() as $dt) {
              $th = $dt->th_akademik;

              $this->db->where(array('th_akademik' => $th, 'sex' => $gender));
              $this->db->from('mahasiswa');
              $q2 = $this->db->get();
              $data = $q2->num_rows();
          }
      } else {
          $data = 0;
      }
      return $data;
  }

    public function data_chart_mhs_total($th) {
        $q = $this->db->query("SELECT th_akademik FROM mahasiswa WHERE th_akademik='$th' group by th_akademik LIMIT 1");
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $dt) {
                $th = $dt->th_akademik;

                $this->db->where(array('th_akademik' => $th));
                $this->db->from('mahasiswa');
                $q2 = $this->db->get();
                $data = $q2->num_rows();
            }
        } else {
            $data = 0;
        }
        return $data;
    }

    public function data_chart_dosen($key) {

        $this->db->where(array('kd_prodi' => $key, 'status' => 'Aktif'));
        $this->db->from('dosen');
        $q2 = $this->db->get();
        if ($q2->num_rows() > 0) {
            $data = $q2->num_rows();
        } else {
            $data = 0;
        }
        return $data;
    }


    // Andry
    public function getNimLastNum($id,$kdprodi) {
        $q = $this->db->query("SELECT tahun_ajar, no_urut FROM nim_last_number WHERE tahun_ajar='$id' and no_id='$kdprodi'");
        foreach ($q->result() as $dt) {
            $hasil = array('tahun_ajar' => $dt->tahun_ajar, 'no_urut' => $dt->no_urut);
        }
        return @$hasil;
    }

	public function UpdateNimLastNum($id,$kdprodi,$no_urut) {
		$q = $this->db->query("UPDATE nim_last_number SET tahun_ajar='$id', no_urut='$no_urut' WHERE no_id='$kdprodi'");
	}

	public function KirimEmail($EmailTo,$EmailSubject,$EmailMessage){
			/* email */

			$config = Array(
			    'protocol' => 'smtp',
			    'smtp_host' => 'ssl://mail.akademik.binerapps.com',
			    'smtp_port' => 465,
			    'smtp_user' => 'admin@akademik.binerapps.com',
			    'smtp_pass' => 'superboy123',
			    'mailtype'  => 'html',
			    'charset'   => 'iso-8859-1'
			);
			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");

			$this->load->library('email', $config);

			$this->email->from('admin@akademik.binerapps.com', 'Admin Akademik '.$this->config->item('nama_instansi').'');
			$this->email->to($EmailTo);
//			$this->email->cc('another@another-example.com');
			$this->email->bcc('andry.krisnanda@gmail.com');

			$this->email->subject($EmailSubject);
			$this->email->message($EmailMessage);
			$this->email->send();
	}

    public function SubscribeEmail($EmailSubject,$EmailMessage) {
        $q = $this->db->query("select nim, nama_mhs, email, status
											from mahasiswa
											where status in('Aktif','Cuti')
											and email <> ''
										 ");

        $hasil = '';
        foreach ($q->result() as $dt) {

        	   $MSG = "Dear ".$dt->nama_mhs.", <br/><br/>Info Kampus: <br/>".$EmailMessage;
        		$this->KirimEmail($dt->email,"[INFO KAMPUS] ".$EmailSubject,$MSG);

        }
        //$hasil .= $x+9;
//        return $hasil;
    }

	public function CreateHistoryAktifMahasiswa(){
        $q = $this->db->query("select
											t1.th_akademik,
											t1.nim,
											t1.status
										from mahasiswa t1
										where t1.status ='Aktif'
										");

        foreach ($q->result() as $row) {
				$dt['th_akademik'] 	= $row->th_akademik;
   			$dt['nim'] 				= $row->nim;
				$dt['status'] 			= $row->status;
				$dt['date_created'] 	= date('Y-m-d H:i:s');

				@$this->db->insert("mahasiswa_aktif_history",$dt);
        }

	     $q = $this->db->query("update
										 mahasiswa
										 set status = 'Cuti'
										 where status ='Aktif'
										");

   }

    public function th_akademik_aktif() {
        $q = $this->db->query("SELECT * FROM th_akademik WHERE aktif='Ya'");
        foreach ($q->result() as $dt) {
            $hasil = $dt->kode;
        }
        return $hasil;
    }

    public function smtToRomawi($smt=null){
        switch ($smt) {
            case 1:
                return "I";
                break;
            case 2:
                return "II";
                break;
            case 3:
                return "III";
                break;
            case 4:
                return "IV";
                break;
            case 5:
                return "V";
                break;
            case 6:
                return "VI";
                break;
            case 7:
                return "VII";
                break;
            case 8:
                return "VIII";
                break;
            default:
                return 0;
        }
    }

    public function getCurrentSmt($nim){
      $this->db->select_max('smt');
      $this->db->where('nim', $nim);
      $res = $this->db->get('krs');
      return $res->result()[0]->smt;
    }
}



/* End of file app_model.php */
/* Location: ./application/models/app_model.php */
