<?php 

    if (!function_exists('prodi_list')) {
        function prodi_list() {
            $CI = get_instance();
            $CI->load->model('Model_data');
            $d['prodi_list'] = $CI->Model_data->data_jurusan()->result();
            return $d;
        }
    }

?>