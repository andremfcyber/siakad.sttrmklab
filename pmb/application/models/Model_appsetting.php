<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Model_appsetting extends CI_Model {

    public function get_setting($id = null){
		$this->db->select('*');		
		$this->db->from('app_setting');

        if($id != null):
            $this->db->where('id', $id);
        endif;

		return $this->db->get();
	}

}