<?php
  class Model_Savedata extends CI_Model {
    /*
   * Programmer : Fitria Wahyuni.S.Pd
   * Keterangan : Model khusus untuk transaksi data
   */

    public function save_data_identitas($data){
      return $this->db->insert('pmb.data_identitas', $data);
    }
  
    public function save_data_keluarga($data){
      return $this->db->insert('pmb.data_keluarga', $data);      
    }

    public function save_data_pp($data){
      return $this->db->insert('pmb.data_pp', $data);     
    }

    public function save_data_rohani($data){
      return $this->db->insert('pmb.data_rohani', $data);    
    }

    public function save_data_lain($data){
      return $this->db->insert('pmb.data_lain', $data);     
    }

    public function save_data_rekomendasi($data){
      return $this->db->insert('pmb.data_rekomendasi', $data);     
    }

    public function save_data_tekad_dan_keputusan($data){
      return $this->db->insert('pmb.data_tekad_dan_keputusan', $data);     
    }

    public function save_data_upload($data){
      return $this->db->insert('pmb.data_upload', $data);     
    }

    public function update_data_identitas($data){
      $id['no_pendaftaran'] = $data['no_pendaftaran'];
      return $this->db->update('pmb.data_identitas', $data, $id);
    }
  
    public function update_data_keluarga($data){
      $id['no_pendaftaran'] = $data['no_pendaftaran'];
      return $this->db->update('pmb.data_keluarga', $data, $id);
    }

    public function update_data_pp($data){
      $id['no_pendaftaran'] = $data['no_pendaftaran'];
      return $this->db->update('pmb.data_pp', $data, $id);     
    }

    public function update_data_rohani($data){
      $id['no_pendaftaran'] = $data['no_pendaftaran'];
      return $this->db->update('pmb.data_rohani', $data, $id); 
    }

    public function update_data_lain($data){
      $id['no_pendaftaran'] = $data['no_pendaftaran'];
      return $this->db->update('pmb.data_lain', $data, $id);   
    }

    public function update_data_rekomendasi($data){
      $id['no_pendaftaran'] = $data['no_pendaftaran'];
      return $this->db->update('pmb.data_rekomendasi', $data, $id);   
    }

    public function update_data_tekad_dan_keputusan($data){
      $id['no_pendaftaran'] = $data['no_pendaftaran'];
      return $this->db->update('pmb.data_tekad_dan_keputusan', $data, $id);   
    }

    public function update_data_upload($data){
      $id['no_pendaftaran'] = $data['no_pendaftaran'];
      return $this->db->update('pmb.data_upload', $data, $id);   
    }

    public function update_done($noPendaftaran){
      $id['no_pendaftaran'] = $noPendaftaran;
      $data['done'] = 1;
      return $this->db->update('pmb.users', $data, $id);   
    }
  }
?>