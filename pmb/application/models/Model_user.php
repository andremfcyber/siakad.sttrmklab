<?php
  class Model_user extends CI_Model {
    /*
   * Programmer : Fitria Wahyuni.S.Pd
   * Keterangan : Model khusus untuk transaksi data user dan admin
   */
    
    public function register($encPassword){
      $data = array(
          'username' => $this->input->post('username'),
          'email' => $this->input->post('email'),
          'password' => $encPassword,
          'delete' => 0
      );

      $email = $this->input->post('email');
      $this->db->insert('pmb.users', $data);
      $id = $this->db->get_where('pmb.users', array('email' => $email))->row()->id_pmbU;
      $date = date("y");
      $date_ = (string)((int)$date+1);
      $noPendaftaran = $date.$date_.sprintf("%05s",$id);
      $this->db->set('no_pendaftaran', $noPendaftaran);
      $this->db->where('id_pmbU', $id);
      $this->db->update('pmb.users');
      return $this->db->get_where('pmb.users', array('id_pmbU' => $id))->row();
    }

    public function check_email_exists($email){
			$query = $this->db->get_where('pmb.users',array('email' => $email));

			if(empty($query->row_array())){
				return true;
			}else{
				return false;
			}
    }
    
    public function login($username, $password){
      $this->db->where('username', $username);
      $this->db->where('password', $password);
      $this->db->where('delete',  0);

      $result = $this->db->get('pmb.users');

      if($result->num_rows() == 1){
        return $result->row(0)->id_pmbU;
      }else{
        return false;
      }
    }

    public function loginAdmin($username, $password){
      $this->db->where('username', $username);
      $this->db->where('password', $password);

      $result = $this->db->get('pmb.admins');

      if($result->num_rows() == 1){
        return $result->row(0)->username;
      }else{
        return false;
      }
    }

    public function getNoPendaftaran($id){
      return $this->db->get_where('pmb.users', array('id_pmbU' => $id))->row(0)->no_pendaftaran;
    }

    public function delete($np){
      $data_file = array(
        'file_foto' => NULL,
        'file_ktp' =>NULL,
        'file_ijazah' =>NULL,
        'file_kk' =>NULL,
        'tgl_update' => date("Y-m-d H:i:s")

      );
      $data = array(
        'delete' => 1,
      );
      $id = array(
        'no_pendaftaran' => $np,
      );
      $this->db->update('pmb.data_upload', $data_file, $id);
      return $this->db->update('pmb.users', $data, $id);     
    }
  }
?>