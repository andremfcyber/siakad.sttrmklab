    <section class="col-sm-9">
      <form action="<?php echo site_url();?>savedata/saveDataKeluarga" id="my-form" name="my-form" method="POST">
        <table class="table table-striped table-bordered table-sm table-rounded">
          <tbody>
            <tr>
              <th colspan = "2">B. DATA KELUARGA</th>
            </tr>
            <tr class="row">
              <td class="col-4">
                <label for="nama_ayah" class="col-form-label-sm">Nama Ayah</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8 float-left" id="nama_ayah" name="nama_ayah" placeholder="Ayah">
                <input type="text" class="form-control form-control-sm col-2 float-left" id="umur_ayah" name="umur_ayah" placeholder="Umur">
                <label for="umur_ayah" class="col-form-label-sm">thn</label>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="nama_ibu" class="col-form-label-sm">Nama Ibu</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8 float-left" id="nama_ibu" name="nama_ibu" placeholder="Ibu">
                <input type="text" class="form-control form-control-sm col-2 float-left" id="umur_ibu" name="umur_ibu" placeholder="Umur">
                <label for="umur_ibu" class="col-form-label-sm">thn</label>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="alamat_ortu" class="col-form-label-sm">Alamat Lengkap Orang Tua</label></td>
              <td class="col-8">
                <textarea class="form-control form-control-sm col-12" id="alamat_ortu" name="alamat_ortu" rows="3"></textarea>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="tlp_ortu" class="col-form-label-sm">No. Telepon Orang Tua</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-6" id="tlp_ortu" name="tlp_ortu">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="pend_ayah" class="col-form-label-sm">Pendidikan Terakhir Ayah</label></td>
              <td class="col-8">
                <select name="pend_ayah" id="pend_ayah" class="custom-select custom-select-sm col-4">
                  <option value="">-Pilih-</option>
                  <option value="Tidak Sekolah">Tidak Sekolah</option>
                  <option value="Tidak Lulus SD">Tidak Lulus SD</option>
                  <option value="Lulus SD">Lulus SD</option>
                  <option value="Lulus SMP">Lulus SMP</option>
                  <option value="Lulus SMA">Lulus SMA</option>
                  <option value="Diploma 1/2/3">Diploma 1/2/3</option>
                  <option value="Sarjana(S1)">Sarjana(S1)</option>
                  <option value="Magister(S2)">Magister(S2)</option>
                  <option value="Doktor(S3)">Doktor(S3)</option>
                </select>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="pend_ibu" class="col-form-label-sm">Pendidikan Terakhir Ibu</label></td>
              <td class="col-8">
                <select name="pend_ibu" id="pend_ibu" class="custom-select custom-select-sm col-4">
                  <option value="">-Pilih-</option>
                  <option value="Tidak Sekolah">Tidak Sekolah</option>
                  <option value="Tidak Lulus SD">Tidak Lulus SD</option>
                  <option value="Lulus SD">Lulus SD</option>
                  <option value="Lulus SMP">Lulus SMP</option>
                  <option value="Lulus SMA">Lulus SMA</option>
                  <option value="Diploma 1/2/3">Diploma 1/2/3</option>
                  <option value="Sarjana(S1)">Sarjana(S1)</option>
                  <option value="Magister(S2)">Magister(S2)</option>
                  <option value="Doktor(S3)">Doktor(S3)</option>
                </select>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="pekerjaan_ayah" class="col-form-label-sm">Pekerjaan Ayah</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-6" id="pekerjaan_ayah" name="pekerjaan_ayah" placeholder="Ayah">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="pekerjaan_ibu" class="col-form-label-sm">Pekerjaan Ibu</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-6" id="pekerjaan_ibu" name="pekerjaan_ibu" placeholder="Ibu">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="agama_ayah" class="col-form-label-sm">Agama Ayah</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-4" id="agama_ayah" name="agama_ayah" placeholder="Ayah">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="agama_ibu" class="col-form-label-sm">Agama Ibu</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-4" id="agama_ibu" name="agama_ibu" placeholder="Ibu">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="nama_wali" class="col-form-label-sm">Nama Wali</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nama_wali" name="nama_wali" placeholder="Wali">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="alamat_wali" class="col-form-label-sm">Alamat Lengkap Wali</label></td>
              <td class="col-8">
                <textarea class="form-control form-control-sm col-12" id="alamat_wali" name="alamat_wali" rows="3"></textarea>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="tlp_wali" class="col-form-label-sm">No. Telepon Wali</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-6" id="tlp_wali" name="tlp_wali">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="pekerjaan_wali" class="col-form-label-sm">Pekerjaan Wali</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-6" id="pekerjaan_wali" name="pekerjaan_wali" placeholder="Wali">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="hubungan_wali" class="col-form-label-sm">Hubungan dengan Wali</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-6" id="hubungan_wali" name="hubungan_wali" placeholder="Wali">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="agama_wali" class="col-form-label-sm">Agama Wali</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-6" id="agama_wali" name="agama_wali" placeholder="Wali">
              </td>
            </tr>
            <tr class="row">
              <td colspan="2">
              <label for="sk" class="col-form-label-sm">Nama Saudara Kandung <em>(Termasuk Anda)</em> Berdasarkan Nomor Urut</label><br>
                <table class="col-sm-12">
                  <tr class="col-form-label-sm text-center">
                    <th width="5%">No.</th>
                    <th width="30%">Nama Saudara Kandung</th>
                    <th width="10%">Jenis Kelamin</th>
                    <th width="10%">Umur (Tahun)</th>
                    <th width="25%">Pend. Terakhir</th>
                    <th width="20%">Agama</th>
                  </tr>
                  <tr>
                    <td class="text-center">1</td>
                    <td><input type="text" id="nama_sk1" name="nama_sk1" class="form-control form-control-sm col-12"></td>
                    <td>
                      <select name="jk_sk1" id="jk_sk1" class="custom-select custom-select-sm col-12">
                        <option value="">-</option>
                        <option value="L">L</option>
                        <option value="P">P</option>
                      </select> 
                    </td>
                    <td><input type="text" id="umur_sk1" name="umur_sk1" class="form-control form-control-sm col-12"></td>
                    <td><input type="text" id="pend_sk1" name="pend_sk1" class="form-control form-control-sm col-12"></td>
                    <td><input type="text" id="agama_sk1" name="agama_sk1" class="form-control form-control-sm col-12"></td>
                  </tr>
                  <tr>
                    <td class="text-center">2</td>
                    <td><input type="text" id="nama_sk2" name="nama_sk2" class="form-control form-control-sm col-12"></td>
                    <td>
                      <select name="jk_sk2" id="jk_sk2" class="custom-select custom-select-sm col-12">
                        <option value="">-</option>
                        <option value="L">L</option>
                        <option value="P">P</option>
                      </select> 
                    </td>
                    <td><input type="text" id="umur_sk2" name="umur_sk2" class="form-control form-control-sm col-12"></td>
                    <td><input type="text" id="pend_sk2" name="pend_sk2" class="form-control form-control-sm col-12"></td>
                    <td><input type="text" id="agama_sk2" name="agama_sk2" class="form-control form-control-sm col-12"></td>
                  </tr>
                  <tr>
                    <td class="text-center identifier-no">3</td>
                    <td><input type="text" id="nama_sk3" name="nama_sk3" class="form-control form-control-sm col-12"></td>
                    <td>
                      <select name="jk_sk3" id="jk_sk3" class="custom-select custom-select-sm col-12">
                        <option value="">-</option>
                        <option value="L">L</option>
                        <option value="P">P</option>
                      </select> 
                    </td>
                    <td><input type="text" id="umur_sk3" name="umur_sk3" class="form-control form-control-sm col-12"></td>
                    <td><input type="text" id="pend_sk3" name="pend_sk3" class="form-control form-control-sm col-12"></td>
                    <td><input type="text" id="agama_sk3" name="agama_sk3" class="form-control form-control-sm col-12"></td>
                  </tr>
                  <tr id="tr-tambah">
                    <td colspan="6"><a class="btn" href="" id="addRowSaudara">&nbsp<i class="fas fa-plus-circle"></i></a></td>
                  </tr>
                </table>
                <br>
                <input type="text" name="jml_saudara" id="jml_saudara" value="3" hidden>
              </td>
            
            </tr>
            <tr class="row">
              <td class="col-4"><label for="thn_pcr" class="col-form-label-sm">Apakah Anda Berpacaran/Bertunangan?</label></td>
              <td class="col-8">
                <input type="text" id="thn_pcr" name="thn_pcr" class="form-control form-control-sm col-4" placeholder="Tahun" onkeyup="cek_stat('#thn_pcr',['#nama_pcr','#agama_pcr', '#hub_pcr']);">
                <small>&nbsp Kosongkan jika tidak</small>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="nama_pcr" class="col-form-label-sm">Nama Pacar/Tunangan Anda</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nama_pcr" name="nama_pcr" disabled>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="agama_pcr" class="col-form-label-sm">Agama Pacar/Tunangan Anda</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-4" id="agama_pcr" name="agama_pcr" disabled>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="hub_pcr" class="col-form-label-sm">Bagaimana Hubungan Anda Dengannya Saat Ini?</label></td>
              <td class="col-8">
              <input type="text" class="form-control form-control-sm col-12" id="hub_pcr" name="hub_pcr" disabled>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="tanggapan_pcr" class="col-form-label-sm">Bagaimana Tanggapan Pacar/Tunangan/Pasangan Anda Atas Keputusan Anda Masuk SETIA?</label></td>
              <td class="col-8">
              <input type="text" class="form-control form-control-sm col-12" id="tanggapan_pcr" name="tanggapan_pcr">  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="thn_div" class="col-form-label-sm">Apakah Anda Pernah Bercerai?</label></td>
              <td class="col-8">
                <input type="text" id="thn_div" name="thn_div" class="form-control form-control-sm col-4" placeholder="Tahun" onkeyup="cek_stat('#thn_div','#alasan_div');">
                <small>&nbsp Kosongkan jika tidak</small>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="alasan_div" class="col-form-label-sm">Alasan Anda Bercerai</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-12" id="alasan_div" name="alasan_div" disabled>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="stat_tp" class="col-form-label-sm">Apakah Anda Menjadi Penanggung Ekonomi Keluarga?</label></td>
              <td class="col-8">
                <select name="stat_tp" id="stat_tp" class="custom-select custom-select-sm col-2">
                  <option value="1">Ya</option>
                  <option value="0">Tidak</option>
                </select> 
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="nama_tp" class="col-form-label-sm">Jika Anda Masuk SETIA, Siapakah yang Akan Menanggung Ekonomi Keluarga Anda?</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nama_tp" name="nama_tp">
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <center>
                  <?php
                    // if(!empty($data_identitas)&&!empty($data_pp)&&!empty($data_rekomendasi)&&!empty($data_tekad_dan_keputusan)&&!empty($data_lain)&&!empty($data_upload)){
                    if(!empty($data_identitas)&&!empty($data_pp)&&!empty($data_rekomendasi)&&!empty($data_tekad_dan_keputusan)&&!empty($data_lain)){
                  ?>
                    <input type="text" name="done" value="1" hidden>
                    <button type="submit" class="btn btn-sm btn-success"><i class="far fa-file-alt"></i> Save dan Lihat Semua Data</a>
                  <?php
                    }else{
                  ?>
                    <a href="<?php echo site_url();?>data_identitas" class="btn btn-sm btn-info"><i class="fas fa-arrow-alt-circle-left"></i> Kembali</a>
                    <button type="submit" id="simpan" class="btn btn-sm btn-primary"><i class="fas fa-arrow-alt-circle-right"></i> Lanjut Ke Step 3</button>
                  <?php
                    }
                  ?>
                </center>
              </td>
            </tr>
          </tbody>
        </table>
        <input type="text" id="noPendaftaran" value="<?=@$this->session->userdata('no_pendaftaran');?>" hidden>
      </form>
    </section>
  </div>
</main>

<script>
document.addEventListener('DOMContentLoaded', function(){
  let noPendaftaran = document.getElementById('noPendaftaran').value;
  let params = "no_pendaftaran="+noPendaftaran;

  let xhr = new XMLHttpRequest();
  xhr.open('POST','<?=site_url();?>getdata/get_data_keluarga',true);
  xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
  xhr.onload = function(){
    if(this.status == 200){
      // console.log(this.responseText);
      if(this.responseText){
        let r = JSON.parse(this.responseText);
        const namaSK = r.nama_sk.split("|");
        let SKLength = namaSK.length;
        let i;
        // console.log(SKLength);
        if(SKLength>3){
          // let SKElement = SKLength - 3;
          for(i=4; i<=SKLength; i++){
            let tr_tombol = document.querySelector('#tr-tambah');
            let no = i;
            // console.log(no);
            tr_tombol.insertAdjacentHTML("beforebegin",`
            <tr>
              <td class="text-center identifier-no">${no}</td>
              <td><input type="text" id="nama_sk${no}" name="nama_sk${no}" class="form-control form-control-sm col-12"></td>
              <td>
                <select name="jk_sk${no}" id="jk_sk${no}" class="custom-select custom-select-sm col-12">
                  <option value="">-</option>
                  <option value="L">L</option>
                  <option value="P">P</option>
                </select> 
              </td>
              <td><input type="text" id="umur_sk${no}" name="umur_sk${no}" class="form-control form-control-sm col-12"></td>
              <td><input type="text" id="pend_sk${no}" name="pend_sk${no}" class="form-control form-control-sm col-12"></td>
              <td><input type="text" id="agama_sk${no}" name="agama_sk${no}" class="form-control form-control-sm col-12"></td>
            </tr>
            `);
            document.querySelector('#jml_saudara').value = no;
          } 
        }
      }
    }
  }
  xhr.send(params); 
  // getData();
});

window.setTimeout(getData, 250);


function getData(){
    let noPendaftaran = document.getElementById('noPendaftaran').value;
    let params = "no_pendaftaran="+noPendaftaran;

    let xhr = new XMLHttpRequest();
    xhr.open('POST','<?=site_url();?>getdata/get_data_keluarga',true);
    xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    xhr.onload = function(){
      if(this.status == 200){
        // console.log(this.responseText);
        if(this.responseText){
          let r = JSON.parse(this.responseText);

          // Check thn_pcr ada/engga, trus input disabled nya on atau off berdasarkan itu
          if(r.thn_pcr){
            const enmp = document.querySelector('#nama_pcr');
            enmp.removeAttribute('disabled');
            const eap = document.querySelector('#agama_pcr');
            eap.removeAttribute('disabled');
            const ehbp = document.querySelector('#hub_pcr');
            ehbp.removeAttribute('disabled');
          }
          if(r.thn_div){
            const eald = document.querySelector('#alasan_div');
            eald.removeAttribute('disabled');
          }

          document.querySelector('#nama_ayah').value = r.nama_ayah;
          document.querySelector('#umur_ayah').value = r.umur_ayah;
          document.querySelector('#nama_ibu').value = r.nama_ibu;
          document.querySelector('#umur_ibu').value = r.umur_ibu;
          document.querySelector('#alamat_ortu').value = r.alamat_ortu;
          document.querySelector('#tlp_ortu').value = r.tlp_ortu;
          document.querySelector('#pend_ayah').value = r.pend_ayah;
          document.querySelector('#pend_ibu').value = r.pend_ibu;
          document.querySelector('#pekerjaan_ayah').value = r.pekerjaan_ayah;
          document.querySelector('#pekerjaan_ibu').value = r.pekerjaan_ibu;
          document.querySelector('#agama_ayah').value = r.agama_ayah;
          document.querySelector('#agama_ibu').value = r.agama_ibu;
          document.querySelector('#nama_wali').value = r.nama_wali;
          document.querySelector('#alamat_wali').value = r.alamat_wali;
          document.querySelector('#tlp_wali').value = r.tlp_wali;
          document.querySelector('#pekerjaan_wali').value = r.pekerjaan_wali;
          document.querySelector('#hubungan_wali').value = r.hubungan_wali;
          document.querySelector('#agama_wali').value = r.agama_wali;

          const namaSK = r.nama_sk.split("|");
          const jkSK = r.jk_sk.split("|");
          const umurSK = r.umur_sk.split("|");
          const pendSK = r.pend_sk.split("|");
          const agamaSK = r.agama_sk.split("|");
          let SKLength = namaSK.length;
          let i;
          for(i=1; i<=SKLength; i++){
            document.querySelector(`#nama_sk${i}`).value = namaSK[i-1];
            document.querySelector(`#jk_sk${i}`).value = jkSK[i-1];
            document.querySelector(`#umur_sk${i}`).value = umurSK[i-1];
            document.querySelector(`#pend_sk${i}`).value = pendSK[i-1];
            document.querySelector(`#agama_sk${i}`).value = agamaSK[i-1];
          }
          document.querySelector('#thn_pcr').value = r.thn_pcr;
          document.querySelector('#nama_pcr').value = r.nama_pcr;
          document.querySelector('#agama_pcr').value = r.agama_pcr;
          document.querySelector('#hub_pcr').value = r.hub_pcr;
          document.querySelector('#tanggapan_pcr').value = r.tanggapan_pcr;
          document.querySelector('#thn_div').value = r.thn_div;
          document.querySelector('#alasan_div').value = r.alasan_div;
          document.querySelector('#stat_tp').value = r.stat_tp;
          document.querySelector('#nama_tp').value = r.nama_tp;
        }
      }
    }
    xhr.send(params);
  }

  function cek_stat(par,att){
    // console.log(par);
    // console.log(att);
    let val = document.querySelector(par).value;
    if(val !== ""){
      if(Array.isArray(att)){
        // console.log("nice");
        att.forEach(function(att_single){
          const e = document.querySelector(att_single);
          e.removeAttribute('disabled');
        });
      }else{
        const e = document.querySelector(att);
        e.removeAttribute('disabled');
      }
    }else{
      if(Array.isArray(att)){
        // console.log("nice");
        att.forEach(function(att_single){
          const e = document.querySelector(att_single);
          e.setAttribute('disabled', true);
          e.value = "";
        });
      }else{
        const e = document.querySelector(att);
        e.setAttribute('disabled', true);
        e.value = "";
      }
    }
  }

  document.getElementById('addRowSaudara').addEventListener('click', function(e){
    e.preventDefault();
    let saudara = document.querySelectorAll('.identifier-no');
    let saudara_last = saudara[saudara.length-1];
    let tr_tombol = document.querySelector('#tr-tambah');
    let no = Number(saudara_last.innerHTML) + 1;
    // console.log(no);
    tr_tombol.insertAdjacentHTML("beforebegin",`
    <tr>
      <td class="text-center identifier-no">${no}</td>
      <td><input type="text" id="nama_sk${no}" name="nama_sk${no}" class="form-control form-control-sm col-12"></td>
      <td>
        <select name="jk_sk${no}" id="jk_sk${no}" class="custom-select custom-select-sm col-12">
          <option value="">-</option>
          <option value="L">L</option>
          <option value="P">P</option>
        </select> 
      </td>
      <td><input type="text" id="umur_sk${no}" name="umur_sk${no}" class="form-control form-control-sm col-12"></td>
      <td><input type="text" id="pend_sk${no}" name="pend_sk${no}" class="form-control form-control-sm col-12"></td>
      <td><input type="text" id="agama_sk${no}" name="agama_sk${no}" class="form-control form-control-sm col-12"></td>
    </tr>
    `);
    document.querySelector('#jml_saudara').value = no;
  });
  
</script>