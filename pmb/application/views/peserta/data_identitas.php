<style>
body {
    background:#DDD!important;
}
    .table-striped tbody tr:nth-of-type(odd) {
    background-color: rgb(201 190 190 / 5%);
}
.shadow {
    border-radius: 12px;
    padding: 12px;
    box-shadow: 5px 0 7px 0 rgb(0 0 0 / 30%);
    background: #FFF;
}

.th {
    background: #ededed;
}
.h-pesertas {
    box-shadow: 5px 0 14px 0 rgb(0 0 0);
}
/* width */
::-webkit-scrollbar {
  width: 5px;
  height: 5px;
}

/* Track */
::-webkit-scrollbar-track {
  background: #f1f1f1; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #888; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #555; 
}

</style>


    <section class="col-sm-9">
      <form class="shadow" action="<?php echo site_url();?>savedata/saveDataPribadi" id="my-form" name="my-form" method="POST">
        <table class="table table-striped table-bordered table-sm table-rounded table-hover">
          <tbody>
            <tr>
              <th class="th" colspan = "2">A. IDENTITAS</th>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="nama" class="col-form-label-sm required-label">Nama Lengkap</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nama" name="nama" required>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="tempat_lahir tgl_lahir" class="col-form-label-sm required-label">Tempat dan Tanggal Lahir</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-4 float-left" id="tempat_lahir" name="tempat_lahir" placeholder="Kota" required>
                <input type="date" class="form-control form-control-sm col-4 float-left set" id="tgl_lahir" name="tgl_lahir" required>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="jk" class="col-form-label-sm required-label">Jenis Kelamin</label></td>
              <td class="col-8">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="jk" id="jk1" value="L" required>
                <label class="col-form-label-sm radio" for="jk1">Laki-Laki</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="jk" id="jk2" value="P">
                <label class="col-form-label-sm radio" for="jk2">Perempuan</label>
              </div> 
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="pekerjaan" class="col-form-label-sm required-label">Pekerjaan</label></td>
              <td class="col-8">
                <select name="pekerjaan" id="pekerjaan" class="custom-select custom-select-sm col-4" required>
                  <option value="">-Pilih-</option>
                  <option value="Pegawai Negeri">Pegawai Negeri</option>
                  <option value="Pegawai Swasta">Pegawai Swasta</option>
                  <option value="Pedagang">Pedagang</option>
                  <option value="Ibu Rumah Tangga">Ibu Rumah Tangga</option>
                  <option value="Hamba Tuhan">Hamba Tuhan</option>
                  <option value="Belum Bekerja">Belum Bekerja</option>
                </select>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="alamat" class="col-form-label-sm required-label">Alamat Lengkap</label></td>
              <td class="col-8">
                <textarea class="form-control form-control-sm col-12" id="alamat" name="alamat" rows="3" required></textarea>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="kewarganegaraan" class="col-form-label-sm required-label">Suku/Bangsa</label></td>
              <td class="col-8">
                <select name="kewarganegaraan" id="kewarganegaraan" class="custom-select custom-select-sm col-4" required>
                  <option value="">-Pilih-</option>
                  <option value="WNI">WNI</option>
                  <option value="WNA">WNA</option>
                </select>  
              </td>
            </tr>


            <!--- DATA ORANG TUA ///////////////////////////////////////////////////////////////////////////////////////-->
            <tr>
              <th class="th">DATA PRIBADI</th>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm required-label">Nama Lengkap (Sesuai KTP)*</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nama_ortu" name="nama_ortu" required>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm required-label">Tempat Tanggal Lahir*</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="tmptgl_ortu" name="tmptgl_ortu" required>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm required-label">NIK*</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nik_ortu" name="nik_ortu" required>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm required-label">Jenis Kelamin*</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="jeniskelamin_ortu" name="jeniskelamin_ortu" required>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm required-label">Alamat Lengkap *</label></td>
              <td class="col-8">
                
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm required-label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desa/Kelurahan </label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="dusun_ortu" name="dusun_ortu" required>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm required-label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kecamatan</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="kecamatan_ortu" name="kecamatan_ortu" required>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm required-label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kabupaten/Kota</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="kota_ortu" name="kota_ortu" required>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm required-label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Provinsi</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="profvinsi_ortu" name="profvinsi_ortu" required>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm required-label">Pekerjaan:*</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="pekerjaan_ortu" name="pekerjaan_ortu" required>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm required-label">No. Telepon dan WA*</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="notlp_ortu" name="notlp_ortu" required>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm required-label">Email*	</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="email_ortu" name="email_ortu" required>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm required-label">Sudah Berkeluarga*</label></td>
              <td class="col-8">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="sudah_berkeluarga_ortu" id="sudah_berkeluarga_ortu1" value="Menikah" required>
                <label class="col-form-label-sm radio" >Menikah</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="sudah_berkeluarga_ortu" id="sudah_berkeluarga_ortu2" value="Belum">
                <label class="col-form-label-sm radio" >Belum</label>
              </div> 
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="sudah_berkeluarga_ortu" id="sudah_berkeluarga_ortu3" value="Duda">
                <label class="col-form-label-sm radio"  >Duda</label>
              </div> 
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="sudah_berkeluarga_ortu" id="sudah_berkeluarga_ortu4" value="Janda">
                <label class="col-form-label-sm radio"  >Janda</label>
              </div> 
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm required-label">Jaminan Kesehatan yang Anda Ikuti?*</label></td>
              <td class="col-8">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="jaminan_kesehatan_ortu" id="jaminan_kesehatan_ortu1" value="BPJS" required>
                <label class="col-form-label-sm radio" >BPJS    </label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="jaminan_kesehatan_ortu" id="jaminan_kesehatan_ortu2" value="KIS">
                <label class="col-form-label-sm radio" >KIS </label>
              </div> 
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="jaminan_kesehatan_ortu" id="jaminan_kesehatan_ortu3" value="Lainnya">
                <label class="col-form-label-sm radio"  >Lainnya     </label>
              </div> 
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="jaminan_kesehatan_ortu" id="jaminan_kesehatan_ortu4" value="Belum Ada">
                <label class="col-form-label-sm radio" >Belum Ada</label>
              </div> 
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm  ">Kewarganegaraan	</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="kewarganegaraan_ortu" name="kewarganegaraan_ortu"  >
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label  class="col-form-label-sm required-label">Pendidikan Terakhir*</label></td>
              <td class="col-8">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="pendidikan_terakhir_ortu" id="pendidikan_terakhir_ortu1" value="SD" required>
                <label class="col-form-label-sm radio"  >SD         </label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="pendidikan_terakhir_ortu" id="pendidikan_terakhir_ortu2" value="SMP">
                <label class="col-form-label-sm radio"  >SMP     </label>
              </div> 
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="pendidikan_terakhir_ortu" id="pendidikan_terakhir_ortu3" value="SMA/Sederajat">
                <label class="col-form-label-sm radio"  >SMA/Sederajat     </label>
              </div> 
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="pendidikan_terakhir_ortu" id="pendidikan_terakhir_ortu4" value="S1">
                <label class="col-form-label-sm radio"  >S1 </label>
              </div> 
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="pendidikan_terakhir_ortu" id="pendidikan_terakhir_ortu4" value="S2">
                <label class="col-form-label-sm radio" >S2 </label>
              </div> 
              </td>
            </tr>
            
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm required-label">NISN*		</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nisn_ortu" name="nisn_ortu" required>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm required-label">Pernah Sekolah Alkitab?*</label></td>
              <td class="col-8">
                
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama Sekolah Alkitab</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="namasekolah_ortu" name="namasekolah_ortu"  >
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kelas/Tingkat	</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="kelastingkat_ortu" name="kelastingkat_ortu"  >
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tahun SA</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="tahunsa_ortu" name="tahunsa_ortu"  >
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm required-label">Asal Gereja *			</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="asalgereja_ortu" name="asalgereja_ortu" required>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm required-label ">Nama Pemimpin Jemaat	<br>	(Gembala, Ketua Jemaat, dll) * </label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="pemimpinjemaat_ortu" name="pemimpinjemaat_ortu" required >
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm required-label "> Apakah biasa melayani? <br> Jika ya, melayani apa*
              </label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="apakabisamelayani_ortu" name="apakabisamelayani_ortu" required >
              </td>
            </tr>

            <tr class="row">
              <td class="col-4"><label   class="col-form-label-sm required-label">Program Studi: *</label></td>
              <td class="col-8">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="programstudi_ortu" id="programstudi_ortu1" value="BPJS" required>
                <label class="col-form-label-sm radio"  >Teologi              </label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="programstudi_ortu" id="programstudi_ortu2" value="KIS">
                <label class="col-form-label-sm radio" >Pendidikan Agama Kristen     </label>
              </div> 
            
              </td>
            </tr>
 
            <!--- / END DATA ORANG TUA -->
            <!--- DATA ORANG TUA WALI -->
            
            <tr>
              <th class="th">DATA ORANG Tua/Wali</th>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm required-label">Ayah*</label></td>
              <td class="col-8">
                
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama Ayah Kandung (Nama Lengkap) </label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="namaayah_ortuwali" name="namaayah_ortuwali"  >
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIK 	</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nikayah_ortuwali" name="nikayah_ortuwali"  >
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tempat Tanggal Lahir</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="tglayah_ortuwali" name="tglayah_ortuwali"  >
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm required-label">Ibu*</label></td>
              <td class="col-8">
                
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama Ibu Kandung (Nama Lengkap) </label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="namaibu_ortuwali" name="namaibu_ortuwali"  >
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIK 	</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nikibu_ortuwali" name="nikibu_ortuwali"  >
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tempat Tanggal Lahir</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="tglibu_ortuwali" name="tglibu_ortuwali"  >
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm required-label">Wali*</label></td>
              <td class="col-8">
                
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama Wali</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="namawali_ortuwali" name="namawali_ortuwali"  >
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIK 	</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nikwali_ortuwali" name="nikwali_ortuwali"  >
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tempat Tanggal Lahir</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="tglwali_ortuwali" name="tglwali_ortuwali"  >
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label class="col-form-label-sm required-label">Mengetahui STT RMK dari *</label></td>
              <td class="col-8">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="mengetahuisttrmk" id="mengetahuisttrmk1" value="Teman" required>
                <label class="col-form-label-sm radio"  >Teman             </label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="mengetahuisttrmk" id="mengetahuisttrmk2" value="Saudara">
                <label class="col-form-label-sm radio"  >Saudara         </label>
              </div> 
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="mengetahuisttrmk" id="mengetahuisttrmk3" value="WhatsApp">
                <label class="col-form-label-sm radio">WhatsApp     </label>
              </div> 
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="mengetahuisttrmk" id="mengetahuisttrmk4" value="Instagram">
                <label class="col-form-label-sm radio">Instagram </label>
              </div> 
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="mengetahuisttrmk" id="mengetahuisttrmk5" value="Facebook">
                <label class="col-form-label-sm radio">Facebook     </label>
              </div> 
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="mengetahuisttrmk" id="mengetahuisttrmk6" value="Guru/Sekolah ">
                <label class="col-form-label-sm radio">Guru/Sekolah     </label>
              </div> 
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="mengetahuisttrmk" id="mengetahuisttrmk7" value="Brosur">
                <label class="col-form-label-sm radio">Brosur         </label>
              </div> 
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="mengetahuisttrmk" id="mengetahuisttrmk8" value="Spanduk">
                <label class="col-form-label-sm radio">Spanduk         </label>
              </div> 
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="mengetahuisttrmk" id="mengetahuisttrmk9" value="Lokasi Kampus">
                <label class="col-form-label-sm radio">Lokasi Kampus   </label>
              </div> 
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="mengetahuisttrmk" id="mengetahuisttrmk10" value="Yang lain">
                <label class="col-form-label-sm radio">Yang lain     </label>
              </div> 
              </td>
            </tr>
            <!--- / END DATA ORANG TUA WALI /////////////////////////////////////////////////////////////////////////////   -->



            <tr>
              <th class="th">KEANGGOTAAN GEREJA</th>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="gereja_anggota" class="col-form-label-sm required-label">Anggota Gereja</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="gereja_anggota" name="gereja_anggota" required>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="status_baptis tgl_baptis" class="col-form-label-sm required-label">Sudah/belum dibaptis?</label></td>
              <td class="col-sm-2">
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="status_baptis" id="status_baptis1" value="1" required>
                  <label class="col-form-label-sm radio" for="status_baptis1">Sudah</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="status_baptis" id="status_baptis2" value="0">
                  <label class="col-form-label-sm radio" for="status_baptis2">Belum</label>
                </div> 
              </td>
              <td class="col-6">
              <!--<input type="date" class="form-control form-control-sm col-6 set" id="tgl_baptis" name="tgl_baptis">-->
              </td>
            </tr>
            <!--<tr class="row">-->
            <!--  <td class="col-4"><label for="status_anggota_sidi gereja_sidi" class="col-form-label-sm required-label">Anggota Sidi?</label></td>-->
            <!--  <td class="col-sm-2">-->
            <!--    <div class="form-check form-check-inline">-->
            <!--      <input class="form-check-input" type="radio" name="status_anggota_sidi" id="status_anggota_sidi1" value="1" required>-->
            <!--      <label class="col-form-label-sm radio" for="status_anggota_sidi1">Ya</label>-->
            <!--    </div>-->
            <!--    <div class="form-check form-check-inline">-->
            <!--      <input class="form-check-input" type="radio" name="status_anggota_sidi" id="status_anggota_sidi2" value="0">-->
            <!--      <label class="col-form-label-sm radio" for="status_anggota_sidi2">Tidak</label>-->
            <!--    </div> -->
            <!--  </td>-->
            <!--  <td class="col-6">-->
            <!--    <input type="text" class="form-control form-control-sm col-8" id="gereja_sidi" name="gereja_sidi" placeholder="Jika ya, sidi di gereja?">-->
            <!--  </td>-->
            <!--</tr>-->

            <!--<tr class="row">-->
            <!--  <td class="col-4"><label for="status_anggota_sidi_penuh" class="col-form-label-sm required-label">Anggota penuh?</label></td>-->
            <!--  <td class="col-8">-->
            <!--  <div class="form-check form-check-inline">-->
            <!--    <input class="form-check-input" type="radio" name="status_anggota_sidi_penuh" id="status_anggota_sidi_penuh1" value="1" required>-->
            <!--    <label class="col-form-label-sm radio" for="status_anggota_sidi_penuh1">Ya</label>-->
            <!--  </div>-->
            <!--  <div class="form-check form-check-inline">-->
            <!--    <input class="form-check-input" type="radio" name="status_anggota_sidi_penuh" id="status_anggota_sidi_penuh2" value="0">-->
            <!--    <label class="col-form-label-sm radio" for="status_anggota_sidi_penuh2">Tidak</label>-->
            <!--  </div> -->
            <!--  </td>-->
            <!--</tr>-->
            
            <tr>
              <th class="th">PACAR/TUNANGAN</th>
              <tr>
                    <td class="col-form-label-sm"><i>Yang dimaksud dengan pacar ialah hubungan cinta yang telah diketahui oleh orang tua masing-masing,
tetapi belum diresmikan. Sedangkan tunangan ialah hubungan yang sudah diresmikan oleh keluarga.</i></td>
              </tr>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="nama_pacar" class="col-form-label-sm">Nama Pacar/Tunangan</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nama_pacar" name="nama_pacar">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="pekerjaan_pacar" class="col-form-label-sm">Pekerjaan/Sekolah</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="pekerjaan_pacar" name="pekerjaan_pacar">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="agama_pacar" class="col-form-label-sm">Agama/Gereja</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="agama_pacar" name="agama_pacar">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="sudah_menerima_tuhan_yesus" class="col-form-label-sm">Sudah menerima Tuhan Yesus</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="sudah_menerima_tuhan_yesus" name="sudah_menerima_tuhan_yesus">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4" colspan="2"><label for="hubungan_dimulai" class="col-form-label-sm">Kapan hubungan tersebut dimulai</label>
              <p class="col-form-label-sm"><i>(harap dijelaskan, hubungan cinta kasih tersebut sudah berada dalam taraf yang bagaimana?)</i></p>
              </td>
              <td class="col-8">
                <textarea class="form-control form-control-sm col-12" id="hubungan_dimulai" name="hubungan_dimulai" rows="3" ></textarea>
              </td>
            </tr>
<?php /****
            <tr>
              <th colspan="2" class="col-form-label-sm"><i>Bagian ini khusus bagi mereka yang sudah berkeluarga jika pernah menikah dan karena sesuatu hal terjadi
perceraian, harap dijelaskan alasan-alasan perceraian dan melampirkan 1 ex foto copy surat cerai resmi dari
pengadilan Negeri yang menetapkan perceraian tersebut.</i></th>
            </tr>
            <tr>
              <td class="col-form-label-sm"><i>Telah menikah dengan</i></td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="nama_pasangan" class="col-form-label-sm">Nama Lengkap</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nama_pasangan" name="nama_pasangan">
              </td>
            </tr>
             <tr class="row">
              <td class="col-4"><label for="tempat_lahir_pasangan tanggal_lahir_pasangan" class="col-form-label-sm">Tempat dan Tanggal Lahir Pasangan</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-4 float-left" id="tempat_lahir_pasangan" name="tempat_lahir_pasangan" placeholder="Kota">
                <input type="date" class="form-control form-control-sm col-4 set" id="tgl_lahir_pasangan" name="tgl_lahir_pasangan" >
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="pekerjaan_pasangan" class="col-form-label-sm">Pekerjaan</label></td>
              <td class="col-8">
                <select name="pekerjaan_pasangan" id="pekerjaan_pasangan" class="custom-select custom-select-sm col-4">
                  <option value="">-Pilih-</option>
                  <option value="Pegawai Negeri">Pegawai Negeri</option>
                  <option value="Pegawai Swasta">Pegawai Swasta</option>
                  <option value="Pedagang">Pedagang</option>
                  <option value="Ibu Rumah Tangga">Ibu Rumah Tangga</option>
                  <option value="Hamba Tuhan">Hamba Tuhan</option>
                  <option value="Belum Bekerja">Belum Bekerja</option>
                </select>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="pend_terakhir_pasangan" class="col-form-label-sm">Pendidikan Terakhir</label></td>
              <td class="col-8">
                <select name="pend_terakhir_pasangan" id="pend_terakhir_pasangan" class="custom-select custom-select-sm col-4">
                  <option value="">-Pilih-</option>
                  <option value="Lulus SLTA/SMA">Lulus SLTA/SMA</option>
                  <option value="Sarjana Teologi">Sarjana Teologi</option>
                  <option value="Sarjana Umum">Sarjana Umum</option>
                </select>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="agama_pasangan" class="col-form-label-sm">Agama/Gereja</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="agama_pasangan" name="agama_pasangan">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="sudah_menerima_tuhan_yesus" class="col-form-label-sm">Sudah menerima Tuhan Yesus</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="pasangan_sudah_menerima_tuhan_yesus" name="pasangan_sudah_menerima_tuhan_yesus">
              </td>
            </tr>
            <tr>
              <td class="col-form-label-sm"><i>Jumlah Anak</i></td>
            </tr>
            <tr>
              <th class="col-form-label-sm"><i>Anak 1</i></th>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="nama_anak1" class="col-form-label-sm">Nama Lengkap</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nama_anak1" name="nama_anak1" >
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="tempat_lahir_anak1 tgl_lahir_anak1" class="col-form-label-sm -label">Tempat dan Tanggal Lahir</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-4 float-left" id="tempat_lahir_anak1" name="tempat_lahir_anak1" placeholder="Kota" >
                <input type="date" class="form-control form-control-sm col-4 float-left set" id="tgl_lahir_anak1" name="tgl_lahir_anak1" >
                <input type="text" class="form-control form-control-sm col-4 float-left" id="keterangan_anak1" name="keterangan_anak1" placeholder="Keterangan" >
              </td>
            </tr>

            <tr>
              <th class="col-form-label-sm"><i>Anak 2</i></th>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="nama_anak2" class="col-form-label-sm -label">Nama Lengkap</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nama_anak2" name="nama_anak2" >
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="tempat_lahir_anak2 tgl_lahir_anak2" class="col-form-label-sm -label">Tempat dan Tanggal Lahir</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-4 float-left" id="tempat_lahir_anak2" name="tempat_lahir_anak2" placeholder="Kota" >
                <input type="date" class="form-control form-control-sm col-4 float-left set" id="tgl_lahir_anak2" name="tgl_lahir_anak2" >
                <input type="text" class="form-control form-control-sm col-4 float-left" id="keterangan_anak2" name="keterangan_anak2" placeholder="Keterangan" >
              </td>
            </tr>

            <tr>
              <th class="col-form-label-sm"><i>Anak 3</i></th>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="nama_anak3" class="col-form-label-sm -label">Nama Lengkap</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nama_anak3" name="nama_anak3" >
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="tempat_lahir_anak3 tgl_lahir_anak3" class="col-form-label-sm -label">Tempat dan Tanggal Lahir</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-4 float-left" id="tempat_lahir_anak3" name="tempat_lahir_anak3" placeholder="Kota" >
                <input type="date" class="form-control form-control-sm col-4 float-left set" id="tgl_lahir_anak3" name="tgl_lahir_anak3" >
                <input type="text" class="form-control form-control-sm col-4 float-left" id="keterangan_anak3" name="keterangan_anak3" placeholder="Keterangan" >
              </td>
            </tr>


            <tr>
              <th class="col-form-label-sm"><i>Anak 4</i></th>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="nama_anak4" class="col-form-label-sm">Nama Lengkap</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nama_anak4" name="nama_anak4">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="tempat_lahir_anak4 tgl_lahir_anak4" class="col-form-label-sm">Tempat dan Tanggal Lahir</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-4 float-left" id="tempat_lahir_anak4" name="tempat_lahir_anak4" placeholder="Kota">
                <input type="date" class="form-control form-control-sm col-4 float-left set" id="tgl_lahir_anak4" name="tgl_lahir_anak4">
                <input type="text" class="form-control form-control-sm col-4 float-left" id="keterangan_anak4" name="keterangan_anak4" placeholder="Keterangan">
              </td>
            </tr>

            <tr>
              <th class="col-form-label-sm"><i>Anak 5</i></th>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="nama_anak5" class="col-form-label-sm">Nama Lengkap</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nama_anak5" name="nama_anak5">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="tempat_lahir_anak5 tgl_lahir_anak5" class="col-form-label-sm">Tempat dan Tanggal Lahir</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-4 float-left" id="tempat_lahir_anak5" name="tempat_lahir_anak5" placeholder="Kota">
                <input type="date" class="form-control form-control-sm col-4 float-left set" id="tgl_lahir_anak5" name="tgl_lahir_anak5">
                <input type="text" class="form-control form-control-sm col-4 float-left" id="keterangan_anak5" name="keterangan_anak5" placeholder="Keterangan">
              </td>
            </tr>

            <tr>
              <th class="col-form-label-sm"><i>Anak 6</i></th>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="nama_anak6" class="col-form-label-sm">Nama Lengkap</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nama_anak6" name="nama_anak6">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="tempat_lahir_anak6 tgl_lahir_anak6" class="col-form-label-sm">Tempat dan Tanggal Lahir</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-4 float-left" id="tempat_lahir_anak6" name="tempat_lahir_anak6" placeholder="Kota">
                <input type="date" class="form-control form-control-sm col-4 float-left set" id="tgl_lahir_anak6" name="tgl_lahir_anak6">
                <input type="text" class="form-control form-control-sm col-4 float-left" id="keterangan_anak6" name="keterangan_anak6" placeholder="Keterangan">
              </td>
            </tr>
            
           
            
            <tr>
              <th class="col-form-label-sm"><i>Siapakah yang akan bertanggung jawab dalam menanggung biaya hidup bagi keluarga (isteri dan
anak-anak), jika saudara diterima di STTE dan apakah dia sanggup membiayai saudara sekeluarga
dan menyewa kontrakan rumah di Tanjung Enim bagi saudara?</i></th>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="nama_pembiaya" class="col-form-label-sm">Nama Lengkap</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nama_pembiaya" name="nama_pembiaya">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="umur_pembiaya" class="col-form-label-sm">Umur</label></td>
              <td class="col-8">
                <input type="number" class="form-control form-control-sm col-8" id="umur_pembiaya" name="umur_pembiaya">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="pekerjaan_pembiaya" class="col-form-label-sm">Pekerjaan</label></td>
              <td class="col-8">
                <select name="pekerjaan_pembiaya" id="pekerjaan_pembiaya" class="custom-select custom-select-sm col-4">
                  <option value="">-Pilih-</option>
                  <option value="Pegawai Negeri">Pegawai Negeri</option>
                  <option value="Pegawai Swasta">Pegawai Swasta</option>
                  <option value="Pedagang">Pedagang</option>
                  <option value="Ibu Rumah Tangga">Ibu Rumah Tangga</option>
                  <option value="Hamba Tuhan">Hamba Tuhan</option>
                  <option value="Belum Bekerja">Belum Bekerja</option>
                </select>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="alamat_pembiaya" class="col-form-label-sm ">Alamat Lengkap</label></td>
              <td class="col-8">
                <textarea class="form-control form-control-sm col-12" id="alamat_pembiaya" name="alamat_pembiaya" rows="3"></textarea>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="agama_pembiaya" class="col-form-label-sm">Agama/Gereja</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="agama_pembiaya" name="agama_pembiaya">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="status_persetujuan_masuk_pembiaya tgl_baptis" class="col-form-label-sm ">Apakah mereka setuju saudara masuk STTE</label></td>
              <td class="col-sm-2">
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="status_persetujuan_masuk_pembiaya" id="status_persetujuan_masuk_pembiaya1" value="1">
                  <label class="col-form-label-sm radio" for="status_persetujuan_masuk1">Ya</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="status_persetujuan_masuk_pembiaya" id="status_persetujuan_masuk_pembiaya2" value="0">
                  <label class="col-form-label-sm radio" for="status_persetujuan_masuk2">Tidak</label>
                </div> 
              </td>
              <td class="col-6">
                <input type="text" class="form-control form-control-sm col-12" id="alasan_pembiaya" name="alasan_pembiaya" placeholder="Alasan (harap dijelaskan secara singkat)">
              </td>
            </tr>
             ****/ ?>
            <tr>
              <td colspan="2" >
                <center>
                  <?php
                    // if(!empty($data_identitas)&&!empty($data_pp)&&!empty($data_rekomendasi)&&!empty($data_tekad_dan_keputusan)&&!empty($data_lain)&&!empty($data_upload)){
                    if(!empty($data_identitas)&&!empty($data_pp)&&!empty($data_rekomendasi)&&!empty($data_tekad_dan_keputusan)&&!empty($data_lain)){
                  ?>
                    <input type="text" name="done" value="1" hidden>
                    <button type="submit" class="btn btn-sm btn-success"><i class="far fa-file-alt"></i> Save dan Lihat Semua Data</a>
                  <?php
                    }else{
                  ?>
                    <a href="<?= site_url();?>home/logout" class="btn btn-sm btn-info"><i class="fas fa-arrow-alt-circle-left"></i> Cancel</a>
                    <button type="submit" id="simpan" class="btn btn-sm btn-primary"><i class="fas fa-arrow-alt-circle-right"></i> Lanjut Ke Step 2</button>
                  <?php
                    }
                  ?>
                </center>
              </td>
            </tr>
          </tbody>
        </table>
        <input type="text" id="noPendaftaran" value="<?=@$this->session->userdata('no_pendaftaran');?>" hidden>
      </form>
    </section>
  </div>
</main>

<script>
  document.addEventListener('DOMContentLoaded', function(){
    let currentDate = new Date();
    let y = currentDate.getFullYear();
    let m = (`0${currentDate.getMonth()+1}`).slice(-2);
    let d = (`0${currentDate.getDate()}`).slice(-2);
    const eDate = document.querySelectorAll('input[type="date"].set');

    eDate.forEach(function(e){
      e.setAttribute('min',`${y-77}-01-01`);
      e.setAttribute('max',`${y}-${m}-${d}`);
      // console.log(e);
    });
    getData();
  });

  function getData(){
    let noPendaftaran = document.getElementById('noPendaftaran').value;
    let params = "no_pendaftaran="+noPendaftaran;

    let xhr = new XMLHttpRequest();
    xhr.open('POST','<?=site_url();?>getdata/get_data_identitas',true);
    xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    xhr.onload = function(){
      if(this.status == 200){
        if(this.responseText){
          let r = JSON.parse(this.responseText);
          
          // Check Marital_stat nikah/ belom, trus input disabled nya on atau off berdasarkan itu
          // assignTo(r.marital_stat);
          console.log(r.nama_ortu);
          
          // document.querySelector('#prodi').value = r.kd_prodi;
          document.querySelector('#nama').value = r.nama;
          document.querySelector('#tempat_lahir').value = r.tempat_lahir;
          document.querySelector('#tgl_lahir').value = r.tgl_lahir;
          if(document.querySelector('#jk1').value == r.jk){
            document.querySelector('#jk1').setAttribute('checked',true);
          }else if(document.querySelector('#jk2').value == r.jk){
            document.querySelector('#jk2').setAttribute('checked',true);
          }
          document.querySelector('#pekerjaan').value = r.pekerjaan;
          document.querySelector('#alamat').value = r.alamat;
          document.querySelector('#kewarganegaraan').value = r.kewarganegaraan;
          document.querySelector('#gereja_anggota').value = r.gereja_anggota;

          if(document.querySelector('#status_baptis1').value == r.status_baptis){
            document.querySelector('#status_baptis1').setAttribute('checked',true);
          }else if(document.querySelector('#status_baptis2').value == r.status_baptis){
            document.querySelector('#status_baptis2').setAttribute('checked',true);
          }
          document.querySelector('#tgl_baptis').value = r.tgl_baptis;

          if(document.querySelector('#status_anggota_sidi1').value == r.status_anggota_sidi){
            document.querySelector('#status_anggota_sidi1').setAttribute('checked',true);
          }else if(document.querySelector('#status_anggota_sidi2').value == r.status_anggota_sidi){
            document.querySelector('#status_anggota_sidi2').setAttribute('checked',true);
          }
          document.querySelector('#gereja_sidi').value = r.gereja_sidi;

          if(document.querySelector('#status_anggota_sidi_penuh1').value == r.status_anggota_sidi_penuh){
            document.querySelector('#status_anggota_sidi_penuh1').setAttribute('checked',true);
          }else if(document.querySelector('#status_anggota_sidi_penuh2').value == r.status_anggota_sidi_penuh){
            document.querySelector('#status_anggota_sidi_penuh2').setAttribute('checked',true);
          }

            // DATA ORTU  //////////////////
           
        //   $("#nama_ortu").val(nama_ortu);
          document.querySelector('#nama_ortu').value = r.nama_ortu;
          document.querySelector('#tmptgl_ortu').value = r.tmptgl_ortu;
          document.querySelector('#nik_ortu').value = r.nik_ortu;
          document.querySelector('#jeniskelamin_ortu').value = r.jeniskelamin_ortu;
          document.querySelector('#dusun_ortu').value = r.dusun_ortu;
          document.querySelector('#kecamatan_ortu').value = r.kecamatan_ortu;
          document.querySelector('#kota_ortu').value = r.kota_ortu;
          document.querySelector('#profvinsi_ortu').value = r.profvinsi_ortu;
          document.querySelector('#pekerjaan_ortu').value = r.pekerjaan_ortu;
          document.querySelector('#notlp_ortu').value = r.notlp_ortu;
          document.querySelector('#email_ortu').value = r.email_ortu; 

          if(document.querySelector('#sudah_berkeluarga_ortu1').value == r.sudah_berkeluarga_ortu){
            document.querySelector('#sudah_berkeluarga_ortu1').setAttribute('checked',true);
          }else if(document.querySelector('#sudah_berkeluarga_ortu2').value == r.sudah_berkeluarga_ortu){
            document.querySelector('#sudah_berkeluarga_ortu2').setAttribute('checked',true);
          } else if(document.querySelector('#sudah_berkeluarga_ortu3').value == r.sudah_berkeluarga_ortu){
            document.querySelector('#sudah_berkeluarga_ortu3').setAttribute('checked',true);
          }else if(document.querySelector('#sudah_berkeluarga_ortu4').value == r.sudah_berkeluarga_ortu){
            document.querySelector('#sudah_berkeluarga_ortu4').setAttribute('checked',true);
          }

          if(document.querySelector('#jaminan_kesehatan_ortu1').value == r.jaminan_kesehatan_ortu){
            document.querySelector('#jaminan_kesehatan_ortu1').setAttribute('checked',true);
          }else if(document.querySelector('#jaminan_kesehatan_ortu2').value == r.jaminan_kesehatan_ortu){
            document.querySelector('#jaminan_kesehatan_ortu2').setAttribute('checked',true);
          } else if(document.querySelector('#jaminan_kesehatan_ortu3').value == r.jaminan_kesehatan_ortu){
            document.querySelector('#jaminan_kesehatan_ortu3').setAttribute('checked',true);
          }else if(document.querySelector('#jaminan_kesehatan_ortu4').value == r.jaminan_kesehatan_ortu){
            document.querySelector('#jaminan_kesehatan_ortu4').setAttribute('checked',true);
          }
 
          document.querySelector('#kewarganegaraan_ortu').value = r.kewarganegaraan_ortu; 

          if(document.querySelector('#jaminan_kesehatan_ortu1').value == r.pendidikan_terakhir_ortu){
            document.querySelector('#pendidikan_terakhir_ortu1').setAttribute('checked',true);
          }else if(document.querySelector('#pendidikan_terakhir_ortu2').value == r.pendidikan_terakhir_ortu){
            document.querySelector('#pendidikan_terakhir_ortu2').setAttribute('checked',true);
          } else if(document.querySelector('#pendidikan_terakhir_ortu3').value == r.pendidikan_terakhir_ortu){
            document.querySelector('#pendidikan_terakhir_ortu3').setAttribute('checked',true);
          }else if(document.querySelector('#pendidikan_terakhir_ortu4').value == r.pendidikan_terakhir_ortu){
            document.querySelector('#pendidikan_terakhir_ortu4').setAttribute('checked',true);
          }else if(document.querySelector('#pendidikan_terakhir_ortu5').value == r.pendidikan_terakhir_ortu){
            document.querySelector('#pendidikan_terakhir_ortu5').setAttribute('checked',true);
          }

          
          document.querySelector('#nisn_ortu').value = r.nisn_ortu; 
          document.querySelector('#namasekolah_ortu').value = r.namasekolah_ortu; 
          document.querySelector('#kelastingkat_ortu').value = r.kelastingkat_ortu; 
          document.querySelector('#tahunsa_ortu').value = r.tahunsa_ortu; 
          document.querySelector('#asalgereja_ortu').value = r.asalgereja_ortu; 
          document.querySelector('#pemimpinjemaat_ortu').value = r.pemimpinjemaat_ortu; 
          document.querySelector('#apakabisamelayani_ortu').value = r.apakabisamelayani_ortu;  

          if(document.querySelector('#programstudi_ortu1').value == r.programstudi_ortu){
            document.querySelector('#programstudi_ortu1').setAttribute('checked',true);
          }else if(document.querySelector('#programstudi_ortu2').value == r.programstudi_ortu){
            document.querySelector('#programstudi_ortu2').setAttribute('checked',true);
          } 

          
          document.querySelector('#namaayah_ortuwali').value = r.namaayah_ortuwali;  
          document.querySelector('#nikayah_ortuwali').value = r.nikayah_ortuwali;  
          document.querySelector('#tglayah_ortuwali').value = r.tglayah_ortuwali;  
          document.querySelector('#namaibu_ortuwali').value = r.namaibu_ortuwali;  
          document.querySelector('#nikibu_ortuwali').value = r.nikibu_ortuwali;  
          document.querySelector('#tglibu_ortuwali').value = r.tglibu_ortuwali;  
          document.querySelector('#namawali_ortuwali').value = r.namawali_ortuwali;  
          document.querySelector('#nikwali_ortuwali').value = r.nikwali_ortuwali;  
          document.querySelector('#tglwali_ortuwali').value = r.tglwali_ortuwali;   

          if(document.querySelector('#mengetahuisttrmk1').value == r.mengetahuisttrmk){
            document.querySelector('#mengetahuisttrmk1').setAttribute('checked',true);
          }else if(document.querySelector('#mengetahuisttrmk2').value == r.mengetahuisttrmk){
            document.querySelector('#mengetahuisttrmk2').setAttribute('checked',true);
          } else if(document.querySelector('#mengetahuisttrmk3').value == r.mengetahuisttrmk){
            document.querySelector('#mengetahuisttrmk3').setAttribute('checked',true);
          }else if(document.querySelector('#mengetahuisttrmk4').value == r.mengetahuisttrmk){
            document.querySelector('#mengetahuisttrmk4').setAttribute('checked',true);
          }else if(document.querySelector('#mengetahuisttrmk5').value == r.mengetahuisttrmk){
            document.querySelector('#mengetahuisttrmk5').setAttribute('checked',true);
          }else if(document.querySelector('#mengetahuisttrmk6').value == r.mengetahuisttrmk){
            document.querySelector('#mengetahuisttrmk6').setAttribute('checked',true);
          }else if(document.querySelector('#mengetahuisttrmk7').value == r.mengetahuisttrmk){
            document.querySelector('#mengetahuisttrmk7').setAttribute('checked',true);
          }else if(document.querySelector('#mengetahuisttrmk8').value == r.mengetahuisttrmk){
            document.querySelector('#mengetahuisttrmk8').setAttribute('checked',true);
          }else if(document.querySelector('#mengetahuisttrmk9').value == r.mengetahuisttrmk){
            document.querySelector('#mengetahuisttrmk9').setAttribute('checked',true);
          }else if(document.querySelector('#mengetahuisttrmk10').value == r.mengetahuisttrmk){
            document.querySelector('#mengetahuisttrmk10').setAttribute('checked',true);
          }

          // END ORTU WALI //////////////////
          
          // PACAR/TUNANGAN
          document.querySelector('#nama_pacar').value = r.nama_pacar;
          document.querySelector('#pekerjaan_pacar').value = r.pekerjaan_pacar;
          document.querySelector('#agama_pacar').value = r.agama_pacar;
          document.querySelector('#sudah_menerima_tuhan_yesus').value = r.sudah_menerima_tuhan_yesus;
          document.querySelector('#hubungan_dimulai').value = r.hubungan_dimulai;
          
          // PASANGAN
          document.querySelector('#nama_pasangan').value = r.nama_pasangan;
          document.querySelector('#tempat_lahir_pasangan').value = r.tempat_lahir_pasangan;
          document.querySelector('#tgl_lahir_pasangan').value = r.tgl_lahir_pasangan;
          document.querySelector('#pekerjaan_pasangan').value = r.pekerjaan_pasangan;
          document.querySelector('#pend_terakhir_pasangan').value = r.pend_terakhir_pasangan;
          document.querySelector('#agama_pasangan').value = r.agama_pasangan;
          document.querySelector('#pasangan_sudah_menerima_tuhan_yesus').value = r.pasangan_sudah_menerima_tuhan_yesus;


          // ANAK
          document.querySelector('#nama_anak1').value = r.nama_anak1;
          document.querySelector('#tempat_lahir_anak1').value = r.tempat_lahir_anak1;
          document.querySelector('#tgl_lahir_anak1').value = r.tgl_lahir_anak1;
          document.querySelector('#keterangan_anak1').value = r.keterangan_anak1;

          document.querySelector('#nama_anak2').value = r.nama_anak2;
          document.querySelector('#tempat_lahir_anak2').value = r.tempat_lahir_anak2;
          document.querySelector('#tgl_lahir_anak2').value = r.tgl_lahir_anak2;
          document.querySelector('#keterangan_anak2').value = r.keterangan_anak2;

          document.querySelector('#nama_anak3').value = r.nama_anak3;
          document.querySelector('#tempat_lahir_anak3').value = r.tempat_lahir_anak3;
          document.querySelector('#tgl_lahir_anak3').value = r.tgl_lahir_anak3;
          document.querySelector('#keterangan_anak3').value = r.keterangan_anak3;

          document.querySelector('#nama_anak4').value = r.nama_anak4;
          document.querySelector('#tempat_lahir_anak4').value = r.tempat_lahir_anak4;
          document.querySelector('#tgl_lahir_anak4').value = r.tgl_lahir_anak4;
          document.querySelector('#keterangan_anak4').value = r.keterangan_anak4;

          document.querySelector('#nama_anak5').value = r.nama_anak5;
          document.querySelector('#tempat_lahir_anak5').value = r.tempat_lahir_anak5;
          document.querySelector('#tgl_lahir_anak5').value = r.tgl_lahir_anak5;
          document.querySelector('#keterangan_anak5').value = r.keterangan_anak5;


          document.querySelector('#nama_anak6').value = r.nama_anak6;
          document.querySelector('#tempat_lahir_anak6').value = r.tempat_lahir_anak6;
          document.querySelector('#tgl_lahir_anak6').value = r.tgl_lahir_anak6;
          document.querySelector('#keterangan_anak6').value = r.keterangan_anak6;



          document.querySelector('#nama_pembiaya').value = r.nama_pembiaya;
          document.querySelector('#umur_pembiaya').value = r.umur_pembiaya;
          document.querySelector('#pekerjaan_pembiaya').value = r.pekerjaan_pembiaya;
          document.querySelector('#alamat_pembiaya').value = r.alamat_pembiaya;
          document.querySelector('#agama_pembiaya').value = r.agama_pembiaya;
          if(document.querySelector('#status_persetujuan_masuk_pembiaya1').value == r.jk){
            document.querySelector('#status_persetujuan_masuk_pembiaya1').setAttribute('checked',true);
          }else if(document.querySelector('#status_persetujuan_masuk_pembiaya2').value == r.jk){
            document.querySelector('#status_persetujuan_masuk_pembiaya2').setAttribute('checked',true);
          }
          document.querySelector('#alasan_pembiaya').value = r.alasan_pembiaya;
        }
      }
    }
    xhr.send(params);
  }

  // function assignTo(r){
  //   if(r == 'Menikah' || r == 'Duda/Janda'){
  //     disabledOff(['#nama_pasangan','#tempat_lahir_pasangan','#tgl_lahir_pasangan']);
  //   }else{
  //     disabledOn(['#nama_pasangan','#tempat_lahir_pasangan','#tgl_lahir_pasangan']);
  //   }
  // }

  // function disabledOn(kelas){
  //   //console.log(kelas);
  //   if(Array.isArray(kelas)){
  //     //console.log(kelas);
  //     kelas.forEach(function(kelas_single){
  //       const e = document.querySelector(kelas_single);
  //       e.setAttribute('disabled',true);
  //       e.value = "";
  //     });
  //   }else{
  //     const e = document.querySelector(kelas);
  //     e.setAttribute('disabled',true);
  //     e.value = "";
  //   }
  // }

  function disabledOff(kelas){
    //console.log(kelas);
    if(Array.isArray(kelas)){
      //console.log(kelas);
      kelas.forEach(function(kelas_single){
        const e = document.querySelector(kelas_single);
        e.removeAttribute('disabled');
      });
    }else{
      const e = document.querySelector(kelas);
      e.removeAttribute('disabled');
    }
  }
  

</script>