    <section class="col-sm-9">
      <form action="<?php echo site_url();?>savedata/saveDataRekomendasi" id="my-form" name="my-form" method="POST">
        <table class="table table-striped table-bordered table-sm table-rounded">
          <tbody>
            <tr>
              <th colspan = "2">C. REKOMENDASI</th>
            </tr>
            
            <tr class="row">
              <td class="col-12">
              <label for="sk" class="col-form-label-sm required-label"><b>Apakah saudara diutus secara resmi?</b></label><br>
              <p class="col-form-label-sm">Catatan: <br>
              
                1. Bagi calon yang diutus resmi oleh Gembala Sidang/Sponsor harap melampirkan surat rekomendasi <br>
                2. Bagi calon yang melamar secara pribadi dengan biaya sendiri, cukup melampirkan surat keterangan dari Gembala Sidang yang menjelaskan keanggotaan saudara</p>
              
                <!--1. Bagi calon yang diutus resmi oleh Gereja/Yayasan/Persekutuan Doa harap melampirkan surat-->
                <!--rekomendasi dari lembaga <br>-->
                <!--2. Bagi calon yang melamar secara pribadi dengan biaya sendiri, cukup melampirkan surat keterangan-->
                <!--dari majelis Gereja/ majelis jemaat yang menjelaskan keanggotaan saudara.</p>-->
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="utusan_majelis_jemaat" class="col-form-label-sm">Oleh Gembala Sidang?</label></td>
              <td class="col-8">
                <select name="utusan_majelis_jemaat" id="utusan_majelis_jemaat" class="custom-select custom-select-sm col-3" required>
                  <option value="">-Pilih-</option>
                  <option value="Ya">Ya</option>
                  <option value="Tidak">Tidak</option>
                </select>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="utusan_majelis_klasis" class="col-form-label-sm">Oleh Sponsor?</label></td>
              <td class="col-8">
                <select name="utusan_majelis_klasis" id="utusan_majelis_klasis" class="custom-select custom-select-sm col-3" required>
                  <option value="">-Pilih-</option>
                  <option value="Ya">Ya</option>
                  <option value="Tidak">Tidak</option>
                </select>  
              </td>
            </tr>
            <!--<tr class="row">-->
            <!--  <td class="col-4"><label for="utusan_majelis_sinode" class="col-form-label-sm">Oleh Saudara Sendiri</label></td>-->
            <!--  <td class="col-8">-->
            <!--    <select name="utusan_majelis_sinode" id="utusan_majelis_sinode" class="custom-select custom-select-sm col-3" required>-->
            <!--      <option value="">-Pilih-</option>-->
            <!--      <option value="Ya">Ya</option>-->
            <!--      <option value="Tidak">Tidak</option>-->
            <!--    </select>  -->
            <!--  </td>-->
            <!--</tr>-->
            <!--<tr class="row">-->
            <!--  <td class="col-4"><label for="utusan_yayasan_pi" class="col-form-label-sm">Oleh yayasan P.I?</label></td>-->
            <!--  <td class="col-8">-->
            <!--    <select name="utusan_yayasan_pi" id="utusan_yayasan_pi" class="custom-select custom-select-sm col-3" required>-->
            <!--      <option value="">-Pilih-</option>-->
            <!--      <option value="Ya">Ya</option>-->
            <!--      <option value="Tidak">Tidak</option>-->
            <!--    </select>  -->
            <!--  </td>-->
            <!--</tr>-->
            <!--<tr class="row">-->
            <!--  <td class="col-4"><label for="utusan_persekutuan_doa" class="col-form-label-sm">Oleh persekutuan doa?</label></td>-->
            <!--  <td class="col-8">-->
            <!--    <select name="utusan_persekutuan_doa" id="utusan_persekutuan_doa" class="custom-select custom-select-sm col-3" required>-->
            <!--      <option value="">-Pilih-</option>-->
            <!--      <option value="Ya">Ya</option>-->
            <!--      <option value="Tidak">Tidak</option>-->
            <!--    </select>  -->
            <!--  </td>-->
            <!--</tr>-->
            <tr class="row">
              <td class="col-4"><label for="utusan_pribadi" class="col-form-label-sm">Oleh saudara pribadi?</label></td>
              <td class="col-8">
                <select name="utusan_pribadi" id="utusan_pribadi" class="custom-select custom-select-sm col-3" required>
                  <option value="">-Pilih-</option>
                  <option value="Ya">Ya</option>
                  <option value="Tidak">Tidak</option>
                </select>  
              </td>
            </tr>


            <tr class="row">
              <td class="col-12">
              <label for="sk" class="col-form-label-sm"><b>Pemberi Rekomendasi</b></label><br>
              <p class="col-form-label-sm">Sebutkan dua orang hamba Tuhan (pimpinan gereja/Pimpinan persekutuan
doa/pimpinan lembaga kerohanian lainnya), yang sungguh-sungguh mengenal saudara secara pribadi,
membaca serta membenarkan dan juga bertanggung jawab atas apa yang saudara isi dan saksikan di dalam
daftar isian ini</p>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="nama_pemberi_rekomendasi1" class="col-form-label-sm">1. Nama</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nama_pemberi_rekomendasi1" name="nama_pemberi_rekomendasi1">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="umur_pemberi_rekomendasi1" class="col-form-label-sm">Umur</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="umur_pemberi_rekomendasi1" name="umur_pemberi_rekomendasi1">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="pekerjaan_pemberi_rekomendasi1" class="col-form-label-sm">Pekerjaan</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="pekerjaan_pemberi_rekomendasi1" name="pekerjaan_pemberi_rekomendasi1">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="jabatan_gerejawi_permberi_rekomendasi1" class="col-form-label-sm">Jabatan Gerejawi</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="jabatan_gerejawi_permberi_rekomendasi1" name="jabatan_gerejawi_permberi_rekomendasi1">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="alamat_pemberi_rekomendasi1" class="col-form-label-sm">Alamat</label></td>
              <td class="col-8">
                <textarea class="form-control form-control-sm col-12" id="alamat_pemberi_rekomendasi1" name="alamat_pemberi_rekomendasi1" rows="3"></textarea>
              </td>
            </tr>



            <tr class="row">
              <td class="col-4"><label for="nama_pemberi_rekomendasi2" class="col-form-label-sm">2. Nama</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nama_pemberi_rekomendasi2" name="nama_pemberi_rekomendasi2">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="umur_pemberi_rekomendasi2" class="col-form-label-sm">Umur</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="umur_pemberi_rekomendasi2" name="umur_pemberi_rekomendasi2">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="pekerjaan_pemberi_rekomendasi2" class="col-form-label-sm">Pekerjaan</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="pekerjaan_pemberi_rekomendasi2" name="pekerjaan_pemberi_rekomendasi2">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="jabatan_gerejawi_permberi_rekomendasi2" class="col-form-label-sm">Jabatan Gerejawi</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="jabatan_gerejawi_permberi_rekomendasi2" name="jabatan_gerejawi_permberi_rekomendasi2">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="alamat_pemberi_rekomendasi2" class="col-form-label-sm">Alamat</label></td>
              <td class="col-8">
                <textarea class="form-control form-control-sm col-12" id="alamat_pemberi_rekomendasi2" name="alamat_pemberi_rekomendasi2" rows="3"></textarea>
              </td>
            </tr>


            <tr class="row">
              <td class="col-12">
              <label for="sk" class="col-form-label-sm"><b>Siapakah atau badan apakah yang akan membiayai saudara selama mengikuti pendidikan di STTRMK?</b></label><br>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="nama_badan" class="col-form-label-sm">Nama Badan</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nama_badan" name="nama_badan">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="nama_pimpinan" class="col-form-label-sm">Nama Pimpinan</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nama_pimpinan" name="nama_pimpinan">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="umur" class="col-form-label-sm">Umur</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="umur" name="umur">
              </td>
            </tr>
            
            <tr class="row">
              <td class="col-4"><label for="pekerjaan" class="col-form-label-sm">Pekerjaan</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="pekerjaan" name="pekerjaan">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="alamat" class="col-form-label-sm">Alamat</label></td>
              <td class="col-8">
                <textarea class="form-control form-control-sm col-12" id="alamat" name="alamat" rows="3"></textarea>
              </td>
            </tr>


            <tr>
              <td colspan="2">
                <center>
                  <?php
                    // if(!empty($data_identitas)&&!empty($data_pp)&&!empty($data_rekomendasi)&&!empty($data_tekad_dan_keputusan)&&!empty($data_lain)&&!empty($data_upload)){
                    if(!empty($data_identitas)&&!empty($data_pp)&&!empty($data_rekomendasi)&&!empty($data_tekad_dan_keputusan)&&!empty($data_lain)){
                  ?>
                    <input type="text" name="done" value="1" hidden>
                    <button type="submit" class="btn btn-sm btn-success"><i class="far fa-file-alt"></i> Save dan Lihat Semua Data</a>
                  <?php
                    }else{
                  ?>
                    <a href="<?php echo site_url();?>data_pp" class="btn btn-sm btn-info"><i class="fas fa-arrow-alt-circle-left"></i> Kembali</a>
                    <button type="submit" id="simpan" class="btn btn-sm btn-primary"><i class="fas fa-arrow-alt-circle-right"></i> Lanjut Ke Step 4</button>
                  <?php
                    }
                  ?>
                </center>
              </td>
            </tr>
          </tbody>
        </table>
        <input type="text" id="noPendaftaran" value="<?=@$this->session->userdata('no_pendaftaran');?>" hidden>
      </form>
    </section>
  </div>
</main>

<script>
document.addEventListener('DOMContentLoaded', getData);

function getData(){
  let noPendaftaran = document.getElementById('noPendaftaran').value;
  let params = "no_pendaftaran="+noPendaftaran;

  let xhr = new XMLHttpRequest();
  xhr.open('POST','<?=site_url();?>getdata/get_data_rekomendasi',true);
  xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
  xhr.onload = function(){
    if(this.status == 200){
      // console.log(this.responseText);
      if(this.responseText){
        let r = JSON.parse(this.responseText);

        document.querySelector('#utusan_majelis_jemaat').value = r.utusan_majelis_jemaat;
        document.querySelector('#utusan_majelis_klasis').value = r.utusan_majelis_klasis;
        document.querySelector('#utusan_majelis_sinode').value = r.utusan_majelis_sinode;
        document.querySelector('#utusan_yayasan_pi').value = r.utusan_yayasan_pi;
        document.querySelector('#utusan_persekutuan_doa').value = r.utusan_persekutuan_doa;
        document.querySelector('#utusan_pribadi').value = r.utusan_pribadi;

        document.querySelector('#nama_pemberi_rekomendasi1').value = r.nama_pemberi_rekomendasi1;
        document.querySelector('#umur_pemberi_rekomendasi1').value = r.umur_pemberi_rekomendasi1;
        document.querySelector('#pekerjaan_pemberi_rekomendasi1').value = r.pekerjaan_pemberi_rekomendasi1;
        document.querySelector('#jabatan_gerejawi_permberi_rekomendasi1').value = r.jabatan_gerejawi_permberi_rekomendasi1;
        document.querySelector('#alamat_pemberi_rekomendasi1').value = r.alamat_pemberi_rekomendasi1;

        document.querySelector('#nama_pemberi_rekomendasi2').value = r.nama_pemberi_rekomendasi2;
        document.querySelector('#umur_pemberi_rekomendasi2').value = r.umur_pemberi_rekomendasi2;
        document.querySelector('#pekerjaan_pemberi_rekomendasi2').value = r.pekerjaan_pemberi_rekomendasi2;
        document.querySelector('#jabatan_gerejawi_permberi_rekomendasi2').value = r.jabatan_gerejawi_permberi_rekomendasi2;
        document.querySelector('#alamat_pemberi_rekomendasi2').value = r.alamat_pemberi_rekomendasi2;

        document.querySelector('#nama_badan').value = r.nama_badan;
        document.querySelector('#nama_pimpinan').value = r.nama_pimpinan;
        document.querySelector('#umur').value = r.umur;
        document.querySelector('#pekerjaan').value = r.pekerjaan;
        document.querySelector('#alamat').value = r.alamat;
      }
    }
  }
  xhr.send(params);
}
  

</script>