<section class="col-sm-9">
      <form action="<?php echo site_url();?>savedata/saveDataTekadDanKeputusan" id="my-form" name="my-form" method="POST">
        <table class="table table-striped table-bordered table-sm table-rounded">
          <tbody>
            <tr>
              <th colspan = "2">D. TEKAD DAN KEPUTUSAN</th>
            </tr>

            <tr class="row">
              <td class="col-12">
              <label for="sk" class="col-form-label-sm"><b>Sediakah saudara mentaati peraturan-peraturan STTRMK? : Ya / Tidak, jelaskan mengapa saudara bersedia
mentaati peraturan STTRMK!</b></label><br>
              </td>
            </tr>

            <tr class="row">
              <td class="col-4"><label for="utusan_majelis_jemaat" class="col-form-label-sm">Bersedia mentaati peraturan-peraturan STTRMK? ?</label></td>
              <td class="col-8">
                <select name="ketersediaan_mentaati_peraturan" id="ketersediaan_mentaati_peraturan" class="custom-select custom-select-sm col-3" required>
                  <option value="">-Pilih-</option>
                  <option value="Ya">Ya</option>
                  <option value="Tidak">Tidak</option>
                </select>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="penjelasan_ketersediaan_mentaati_peraturan" class="col-form-label-sm">Penjelasan</label></td>
              <td class="col-8">
                <textarea class="form-control form-control-sm col-12" id="penjelasan_ketersediaan_mentaati_peraturan" name="penjelasan_ketersediaan_mentaati_peraturan" rows="3"></textarea>
              </td>
            </tr>


            <tr class="row">
              <td class="col-12">
              <label for="sk" class="col-form-label-sm"><b>Sediakan saudara menerima pengarahan/bimbingan dan teguran dari pimpinan/staf dan mahasiswa STTRMK?
: Ya / Tidak. Jelaskan mengapa saudara bersedia</b></label><br>
              </td>
            </tr>

            <tr class="row">
              <td class="col-4"><label for="utusan_majelis_jemaat" class="col-form-label-sm">Bersedia menerima pengarahan/bimbingan dan teguran?</label></td>
              <td class="col-8">
                <select name="ketersediaan_menerima_pengarahan" id="ketersediaan_menerima_pengarahan" class="custom-select custom-select-sm col-3" required>
                  <option value="">-Pilih-</option>
                  <option value="Ya">Ya</option>
                  <option value="Tidak">Tidak</option>
                </select>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="alamat_pemberi_rekomendasi1" class="col-form-label-sm">Penjelasan</label></td>
              <td class="col-8">
                <textarea class="form-control form-control-sm col-12" id="penjelasan_ketersediaan_menerima_pengarahan" name="penjelasan_ketersediaan_menerima_pengarahan" rows="3"></textarea>
              </td>
            </tr>



            <tr class="row">
              <td class="col-12">
              <label for="sk" class="col-form-label-sm"><b>Sediakah saudara menerima dan menjalankan praktek pelayanan yang diatur dan ditetapkan oleh staf
STTRMK ? : Ya / Tidak. Jelaskan mengapa saudara bersedia</b></label><br>
              </td>
            </tr>

            <tr class="row">
              <td class="col-4"><label for="utusan_majelis_jemaat" class="col-form-label-sm">Bersedia menerima dan menjalankan praktek pelayanan?</label></td>
              <td class="col-8">
                <select name="ketersediaan_menjalankan_praktek" id="ketersediaan_menjalankan_praktek" class="custom-select custom-select-sm col-3" required>
                  <option value="">-Pilih-</option>
                  <option value="Ya">Ya</option>
                  <option value="Tidak">Tidak</option>
                </select>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="alamat_pemberi_rekomendasi1" class="col-form-label-sm">Penjelasan</label></td>
              <td class="col-8">
                <textarea class="form-control form-control-sm col-12" id="penjelasan_ketersediaan_menjalankan_praktek" name="penjelasan_ketersediaan_menjalankan_praktek" rows="3"></textarea>
              </td>
            </tr>


            <tr>
              <td colspan="2">
                <center>
                  <?php
                    // if(!empty($data_identitas)&&!empty($data_pp)&&!empty($data_rekomendasi)&&!empty($data_tekad_dan_keputusan)&&!empty($data_lain)&&!empty($data_upload)){
                    if(!empty($data_identitas)&&!empty($data_pp)&&!empty($data_rekomendasi)&&!empty($data_tekad_dan_keputusan)&&!empty($data_lain)){
                  ?>
                    <input type="text" name="done" value="1" hidden>
                    <button type="submit" class="btn btn-sm btn-success"><i class="far fa-file-alt"></i> Save dan Lihat Semua Data</a>
                  <?php
                    }else{
                  ?>
                    <a href="<?php echo site_url();?>data_rekomendasi" class="btn btn-sm btn-info"><i class="fas fa-arrow-alt-circle-left"></i> Kembali</a>
                    <button type="submit" id="simpan" class="btn btn-sm btn-primary"><i class="fas fa-arrow-alt-circle-right"></i> Lanjut Ke Step 5</button>
                  <?php
                    }
                  ?>
                </center>
              </td>
            </tr>
          </tbody>
        </table>
        <input type="text" id="noPendaftaran" value="<?=@$this->session->userdata('no_pendaftaran');?>" hidden>
      </form>
    </section>
  </div>
</main>

<script>
document.addEventListener('DOMContentLoaded', getData);

function getData(){
  let noPendaftaran = document.getElementById('noPendaftaran').value;
  let params = "no_pendaftaran="+noPendaftaran;

  let xhr = new XMLHttpRequest();
  xhr.open('POST','<?=site_url();?>getdata/get_data_tekad_dan_keputusan',true);
  xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
  xhr.onload = function(){
    if(this.status == 200){
      // console.log(this.responseText);
      if(this.responseText){
        let r = JSON.parse(this.responseText);

        document.querySelector('#ketersediaan_mentaati_peraturan').value = r.ketersediaan_mentaati_peraturan;
        document.querySelector('#penjelasan_ketersediaan_mentaati_peraturan').value = r.penjelasan_ketersediaan_mentaati_peraturan;

        document.querySelector('#ketersediaan_menerima_pengarahan').value = r.ketersediaan_menerima_pengarahan;
        document.querySelector('#penjelasan_ketersediaan_menerima_pengarahan').value = r.penjelasan_ketersediaan_menerima_pengarahan;

        document.querySelector('#ketersediaan_menjalankan_praktek').value = r.ketersediaan_menjalankan_praktek;
        document.querySelector('#penjelasan_ketersediaan_menjalankan_praktek').value = r.penjelasan_ketersediaan_menjalankan_praktek;
      }
    }
  }
  xhr.send(params);
}

  

</script>