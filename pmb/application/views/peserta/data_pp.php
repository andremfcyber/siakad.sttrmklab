    <section class="col-sm-9">
      <form action="<?php echo site_url();?>savedata/saveDataPp" id="my-form" name="my-form" method="POST">
        <table class="table table-striped table-bordered table-sm table-rounded">
          <tbody>
            <tr>
              <th colspan = "2">B. DATA PENDIDIKAN DAN RIWAYAT PEKERJAAN</th>
            </tr>
            <tr>
              <td colspan="2">
              <label for="sk" class="col-form-label-sm"><b>Pendidikan Dasar dan Menengah</b></label><br>
                <table class="col-sm-12">
                  <tr class="col-form-label-sm text-center">
                    <th width="10%">No.</th>
                    <th width="30%">Pendidikan Formal</th>
                    <th width="40%">Nama Tempat Pendidikan</th>
                    <th width="20%">Tahun Ijazah</th>
                  </tr>
                  <tr>
                    <td class="text-center">1</td>
                    <td>Taman Kanak-Kanak</td>
                    <td>
                      <input type="text" id="tk" name="tk" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="number" id="tk_thn" name="tk_thn" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-center">2</td>
                    <td>SD</td>
                    <td>
                      <input type="text" id="sd" name="sd" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="number" id="sd_thn" name="sd_thn" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-center">3</td>
                    <td>SMP</td>
                    <td>
                      <input type="text" id="smp" name="smp" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="number" id="smp_thn" name="smp_thn" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-center">4</td>
                    <td>SMA/SMK/STM</td>
                    <td>
                      <input type="text" id="sma" name="sma" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="number" id="sma_thn" name="sma_thn" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td colspan="4">
                      <label for="sk" class="col-form-label-sm"><b>Pendidikan Tinggi</b></label><br>
                    </td>
                  </tr>
                  <tr class="col-form-label-sm text-center">
                    <th width="10%">No.</th>
                    <th width="30%">Pendidikan Formal</th>
                    <th width="40%">Nama Tempat Pendidikan</th>
                    <th width="20%">Tahun Ijazah</th>
                  </tr>
                  <tr>
                    <td class="text-center">5</td>
                    <td>
                      <input type="text" id="pendidikantinggi1_jenjang" name="pendidikantinggi1_jenjang" class="form-control form-control-sm col-12" placeholder="Contoh: D1, D2, D3 ...">
                    </td>
                    <td>
                      <input type="text" id="pendidikantinggi1_nama" name="pendidikantinggi1_nama" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="number" id="pendidikantinggi1_tahun_ijazah" name="pendidikantinggi1_tahun_ijazah" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-center">6</td>
                    <td>
                      <input type="text" id="pendidikantinggi2_jenjang" name="pendidikantinggi2_jenjang" class="form-control form-control-sm col-12" placeholder="Contoh: D1, D2, D3 ...">
                    </td>
                    <td>
                      <input type="text" id="pendidikantinggi2_nama" name="pendidikantinggi2_nama" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="number" id="pendidikantinggi2_tahun_ijazah" name="pendidikantinggi2_tahun_ijazah" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-center">7</td>
                    <td>
                      <input type="text" id="pendidikantinggi3_jenjang" name="pendidikantinggi3_jenjang" class="form-control form-control-sm col-12" placeholder="Contoh: D1, D2, D3 ...">
                    </td>
                    <td>
                      <input type="text" id="pendidikantinggi3_nama" name="pendidikantinggi3_nama" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="number" id="pendidikantinggi3_tahun_ijazah" name="pendidikantinggi3_tahun_ijazah" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                </table>
                <br>
              </td>
            </tr>
            <tr>
              <td colspan="2">
              <label for="sk" class="col-form-label-sm"><b>Kursus-Kursus</b></label><br>
                <table class="col-sm-12">
                  <tr class="col-form-label-sm text-center">
                    <th width="10%">No.</th>
                    <th width="30%">Kursus</th>
                    <th width="40%">Nama Lembaga Penyelenggara</th>
                    <th width="20%">Tahun Ijazah</th>
                  </tr>
                  <tr>
                    <td class="text-center">8</td>
                    <td>
                      <input type="text" id="kursus1" name="kursus1" class="form-control form-control-sm col-12" placeholder="Contoh: Kursus Komputer ...">
                    </td>
                    <td>
                      <input type="text" id="kursus1_nama" name="kursus1_nama" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="number" id="kursus1_ijazah" name="kursus1_ijazah" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-center">9</td>
                    <td>
                      <input type="text" id="kursus2" name="kursus2" class="form-control form-control-sm col-12" placeholder="Contoh: Kursus Komputer ...">
                    </td>
                    <td>
                      <input type="text" id="kursus2_nama" name="kursus2_nama" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="number" id="kursus2_ijazah" name="kursus2_ijazah" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-center">10</td>
                    <td>
                      <input type="text" id="kursus3" name="kursus3" class="form-control form-control-sm col-12" placeholder="Contoh: Kursus Komputer ...">
                    </td>
                    <td>
                      <input type="text" id="kursus3_nama" name="kursus3_nama" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="number" id="kursus3_ijazah" name="kursus3_ijazah" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                </table>
                <br>
              </td>
            </tr>

            <tr>
              <td colspan="2">
              <label for="sk" class="col-form-label-sm"><b>Bakat/Keterampilan Yang Dimiliki</b></label><br>
                <table class="col-sm-12">
                  <tr class="col-form-label-sm text-center">
                    <th width="10%">No.</th>
                    <th width="45%">Bakat dan Karunia</th>
                    <th width="45%">Keterampilan</th>
                  </tr>
                  <tr>
                    <td class="text-center">1</td>
                    <td>
                      <input type="text" id="bakat1" name="bakat1" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="keterampilan1" name="keterampilan1" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-center">2</td>
                    <td>
                      <input type="text" id="bakat2" name="bakat2" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="keterampilan2" name="keterampilan2" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-center">3</td>
                    <td>
                      <input type="text" id="bakat3" name="bakat3" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="keterampilan3" name="keterampilan3" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                </table>
                <br>
              </td>
            </tr>
            
            <tr>
              <td colspan="2">
              <label for="sk" class="col-form-label-sm"><b>Riwayat Pekerjaan</b></label><br>
                <table class="col-sm-12">
                  <tr class="col-form-label-sm text-center">
                    <th width="10%">No.</th>
                    <th width="35%">Jenis Pekerjaan</th>
                    <th width="35%">Jabatan</th>
                    <th width="20%">Lamanya</th>
                  </tr>
                  <tr>
                    <td class="text-center">1</td>
                    
                    <td>
                      <input type="text" id="jenis_kerja1" name="jenis_kerja1" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="jabatan_kerja1" name="jabatan_kerja1" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="number" id="thn_kerja1" name="thn_kerja1" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-center">2</td>
                    
                    <td>
                      <input type="text" id="jenis_kerja2" name="jenis_kerja2" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="jabatan_kerja2" name="jabatan_kerja2" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="number" id="thn_kerja2" name="thn_kerja2" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-center">3</td>
                    
                    <td>
                      <input type="text" id="jenis_kerja3" name="jenis_kerja3" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="jabatan_kerja3" name="jabatan_kerja3" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="number" id="thn_kerja3" name="thn_kerja3" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                </table>
                <br>
              </td>
            </tr>


            <tr>
              <td colspan="2">
              <label for="sk" class="col-form-label-sm"><b>Riwayat Pelayanan</b></label><br>
                <table class="col-sm-12">
                  <tr class="col-form-label-sm text-center">
                    <th width="10%">No.</th>
                    <th width="35%">Jenis Pelayanan</th>
                    <th width="35%">Jabatan</th>
                    <th width="20%">Lamanya</th>
                  </tr>
                  <tr>
                    <td class="text-center">1</td>
                    <td>
                      <input type="text" id="jenis_pelayanan1" name="jenis_pelayanan1" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="jabatan_pelayanan1" name="jabatan_pelayanan1" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="number" id="thn_pelayanan1" name="thn_pelayanan1" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-center">2</td>
                    <td>
                      <input type="text" id="jenis_pelayanan2" name="jenis_pelayanan2" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="jabatan_pelayanan2" name="jabatan_pelayanan2" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="number" id="thn_pelayanan2" name="thn_pelayanan2" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-center">3</td>
                    <td>
                      <input type="text" id="jenis_pelayanan3" name="jenis_pelayanan3" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="jabatan_pelayanan3" name="jabatan_pelayanan3" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="number" id="thn_pelayanan3" name="thn_pelayanan3" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                </table>
                <br>
              </td>
            </tr>

            <tr>
              <td colspan="2">
              <label for="sk" class="col-form-label-sm"><b>Kesaksian pertobatan (kapan saudara bertobat. Uraikan dengan latar belakang kehidupan saudara)</b></label><br>
                <table class="col-sm-12">
                  <tr>
                    <td>
                    <textarea name="kesaksian_pertobatan" id="kesaksian_pertobatan"  class="form-control form-control-sm col-12" rows="8"></textarea>
                    </td>
                  </tr>
                </table>
                <br>
              </td>
            </tr>

            <tr>
              <td colspan="2">
              <label for="sk" class="col-form-label-sm"><b>Di mana Saudara bertobat?</b></label><br>
                <table class="col-sm-12">
                  <tr>
                    <td>
                    <textarea name="tempat_pertobatan" id="tempat_pertobatan"  class="form-control form-control-sm col-12" rows="8"></textarea>
                    </td>
                  </tr>
                </table>
                <br>
              </td>
            </tr>

            <tr>
              <td colspan="2">
              <label for="sk" class="col-form-label-sm"><b>Bagaimana Saudara mengalami pertobatan itu dan siapa yang melayani saudara?</b></label><br>
                <table class="col-sm-12">
                  <tr>
                    <td>
                    <textarea name="deskripsi_pertobatan" id="deskripsi_pertobatan"  class="form-control form-control-sm col-12" rows="8"></textarea>
                    </td>
                  </tr>
                </table>
                <br>
              </td>
            </tr>

            <tr>
              <td colspan="2">
              <label for="sk" class="col-form-label-sm"><b>Uraikan secara singkat perkembangan rohani sejak saudara bertobat</b></label><br>
                <table class="col-sm-12">
                  <tr>
                    <td>
                    <textarea name="perkembangan_rohani" id="perkembangan_rohani"  class="form-control form-control-sm col-12" rows="8"></textarea>
                    </td>
                  </tr>
                </table>
                <br>
              </td>
            </tr>

            <tr>
              <td colspan="2">
              <label for="sk" class="col-form-label-sm"><b>Apakah setelah bertobat saudara membaca Alkitab dan berdoa setiap hari?</b></label><br>
                <table class="col-sm-12">
                  <tr>
                    <td>
                    <textarea name="berdoa" id="berdoa"  class="form-control form-control-sm col-12" rows="8"></textarea>
                    </td>
                  </tr>
                </table>
                <br>
              </td>
            </tr>

            <tr>
              <td colspan="2">
              <label for="sk" class="col-form-label-sm"><b>Kesaksian menerima panggilan melayani Tuhan</b></label><br>
                <table class="col-sm-12">
                  <tr>
                    <td>
                    <textarea name="kesaksian_menerima_panggilan_melayani_tuhan" id="kesaksian_menerima_panggilan_melayani_tuhan"  class="form-control form-control-sm col-12" rows="8"></textarea>
                    </td>
                  </tr>
                </table>
                <br>
              </td>
            </tr>

            <tr>
              <td colspan="2">
              <label for="sk" class="col-form-label-sm"><b>Apa dasar Panggilan Saudara</b></label><br>
                <table class="col-sm-12">
                  <tr>
                    <td>
                    <textarea name="dasar_panggilan" id="dasar_panggilan"  class="form-control form-control-sm col-12" rows="8"></textarea>
                    </td>
                  </tr>
                </table>
                <br>
              </td>
            </tr>

            <tr>
              <td colspan="2">
              <label for="sk" class="col-form-label-sm"><b>Siapa yang membimbing Saudara</b></label><br>
                <table class="col-sm-12">
                  <tr>
                    <td>
                    <textarea name="pembimbing" id="pembimbing"  class="form-control form-control-sm col-12" rows="8"></textarea>
                    </td>
                  </tr>
                </table>
                <br>
              </td>
            </tr>

            <tr>
              <td colspan="2">
              <label for="sk" class="col-form-label-sm"><b>Apakah saudara mengambil bagian dalam pelayanan, di mana ?</b></label><br>
                <table class="col-sm-12">
                  <tr>
                    <td>
                    <textarea name="tempat_pelayanan" id="tempat_pelayanan"  class="form-control form-control-sm col-12" rows="8"></textarea>
                    </td>
                  </tr>
                </table>
                <br>
              </td>
            </tr>

            <tr>
              <td colspan="2">
              <label for="sk" class="col-form-label-sm"><b>Pernahkah saudara menceritakan Injil pada orang lain?</b></label><br>
                <table class="col-sm-12">
                  <tr>
                    <td>
                    <textarea name="penceritaan_injil" id="penceritaan_injil"  class="form-control form-control-sm col-12" rows="8"></textarea>
                    </td>
                  </tr>
                </table>
                <br>
              </td>
            </tr>

            

            

            <tr>
              <td colspan="2">
                <center>
                  <?php
                    // if(!empty($data_identitas)&&!empty($data_pp)&&!empty($data_rekomendasi)&&!empty($data_tekad_dan_keputusan)&&!empty($data_lain)&&!empty($data_upload)){
                    if(!empty($data_identitas)&&!empty($data_pp)&&!empty($data_rekomendasi)&&!empty($data_tekad_dan_keputusan)&&!empty($data_lain)){
                  ?>
                    <input type="text" name="done" value="1" hidden>
                    <button type="submit" class="btn btn-sm btn-success"><i class="far fa-file-alt"></i> Save dan Lihat Semua Data</a>
                  <?php
                    }else{
                  ?>
                    <a href="<?php echo site_url();?>data_identitas" class="btn btn-sm btn-info"><i class="fas fa-arrow-alt-circle-left"></i> Kembali</a>
                    <button type="submit" name="simpan" id="simpan" class="btn btn-sm btn-primary"><i class="fas fa-arrow-alt-circle-right"></i> Lanjut Ke Step 3</button>
                  <?php
                    }
                  ?>
                </center>
              </td>
            </tr>
          </tbody>
        </table>
        <input type="text" id="noPendaftaran" value="<?=@$this->session->userdata('no_pendaftaran');?>" hidden>
      </form>
    </section>
  </div>
</main>

<script>
  document.addEventListener('DOMContentLoaded', getData());

  function getData(){
    let noPendaftaran = document.getElementById('noPendaftaran').value;
    let params = "no_pendaftaran="+noPendaftaran;

    let xhr = new XMLHttpRequest();
    xhr.open('POST','<?=site_url();?>getdata/get_data_pp',true);
    xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    xhr.onload = function(){
      if(this.status == 200){
        if(this.responseText){
          let r = JSON.parse(this.responseText);
          
          document.querySelector('#tk').value = r.tk;
          document.querySelector('#tk_thn').value = r.tk_thn;
          document.querySelector('#sd').value = r.sd;
          document.querySelector('#sd_thn').value = r.sd_thn;
          document.querySelector('#smp').value = r.smp;
          document.querySelector('#smp_thn').value = r.smp_thn;
          document.querySelector('#sma').value = r.sma;
          document.querySelector('#sma_thn').value = r.sma_thn;

          document.querySelector('#pendidikantinggi1_jenjang').value = r.pendidikantinggi1_jenjang;
          document.querySelector('#pendidikantinggi1_nama').value = r.pendidikantinggi1_nama;
          document.querySelector('#pendidikantinggi1_tahun_ijazah').value = r.pendidikantinggi1_tahun_ijazah;

          document.querySelector('#pendidikantinggi2_jenjang').value = r.pendidikantinggi2_jenjang;
          document.querySelector('#pendidikantinggi2_nama').value = r.pendidikantinggi2_nama;
          document.querySelector('#pendidikantinggi2_tahun_ijazah').value = r.pendidikantinggi2_tahun_ijazah;

          document.querySelector('#pendidikantinggi3_jenjang').value = r.pendidikantinggi3_jenjang;
          document.querySelector('#pendidikantinggi3_nama').value = r.pendidikantinggi3_nama;
          document.querySelector('#pendidikantinggi3_tahun_ijazah').value = r.pendidikantinggi3_tahun_ijazah;

          document.querySelector('#kursus1').value = r.kursus1;
          document.querySelector('#kursus1_nama').value = r.kursus1_nama;
          document.querySelector('#kursus1_ijazah').value = r.kursus1_ijazah;

          document.querySelector('#kursus2').value = r.kursus2;
          document.querySelector('#kursus2_nama').value = r.kursus2_nama;
          document.querySelector('#kursus2_ijazah').value = r.kursus2_ijazah;

          document.querySelector('#kursus3').value = r.kursus3;
          document.querySelector('#kursus3_nama').value = r.kursus3_nama;
          document.querySelector('#kursus3_ijazah').value = r.kursus3_ijazah;

          document.querySelector('#bakat1').value = r.bakat1;
          document.querySelector('#keterampilan1').value = r.keterampilan1;

          document.querySelector('#bakat2').value = r.bakat2;
          document.querySelector('#keterampilan2').value = r.keterampilan2;

          document.querySelector('#bakat3').value = r.bakat3;
          document.querySelector('#keterampilan3').value = r.keterampilan3;

          document.querySelector('#jenis_kerja1').value = r.jenis_kerja1;
          document.querySelector('#jabatan_kerja1 ').value = r.jabatan_kerja1 ;
          document.querySelector('#thn_kerja1').value = r.thn_kerja1;

          document.querySelector('#jenis_kerja2').value = r.jenis_kerja2;
          document.querySelector('#jabatan_kerja2 ').value = r.jabatan_kerja2 ;
          document.querySelector('#thn_kerja2').value = r.thn_kerja2;

          document.querySelector('#jenis_kerja3').value = r.jenis_kerja3;
          document.querySelector('#jabatan_kerja3 ').value = r.jabatan_kerja3 ;
          document.querySelector('#thn_kerja3').value = r.thn_kerja3;

          document.querySelector('#jenis_pelayanan1').value = r.jenis_pelayanan1;
          document.querySelector('#jabatan_pelayanan1 ').value = r.jabatan_pelayanan1 ;
          document.querySelector('#thn_pelayanan1').value = r.thn_pelayanan1;

          document.querySelector('#jenis_pelayanan2').value = r.jenis_pelayanan2;
          document.querySelector('#jabatan_pelayanan2 ').value = r.jabatan_pelayanan2 ;
          document.querySelector('#thn_pelayanan2').value = r.thn_pelayanan2;

          document.querySelector('#jenis_pelayanan3').value = r.jenis_pelayanan3;
          document.querySelector('#jabatan_pelayanan3 ').value = r.jabatan_pelayanan3 ;
          document.querySelector('#thn_pelayanan3').value = r.thn_pelayanan3;


          document.querySelector('#kesaksian_pertobatan').value = r.kesaksian_pertobatan;
          document.querySelector('#tempat_pertobatan').value = r.tempat_pertobatan;
          document.querySelector('#deskripsi_pertobatan').value = r.deskripsi_pertobatan;
          document.querySelector('#perkembangan_rohani').value = r.perkembangan_rohani;
          document.querySelector('#berdoa').value = r.berdoa;
          document.querySelector('#kesaksian_menerima_panggilan_melayani_tuhan').value = r.kesaksian_menerima_panggilan_melayani_tuhan;
          document.querySelector('#dasar_panggilan').value = r.dasar_panggilan;
          document.querySelector('#pembimbing').value = r.pembimbing;
          document.querySelector('#tempat_pelayanan').value = r.tempat_pelayanan;
          document.querySelector('#penceritaan_injil').value = r.penceritaan_injil;
          
        }
      }
    }
    xhr.send(params);
  }
</script>