    <section class="col-sm-9">
      <form action="<?php echo site_url();?>savedata/saveDataPribadi" id="my-form" name="my-form" method="POST">
        <table class="table table-striped table-bordered table-sm table-rounded">
          <tbody>
            <tr>
              <th colspan = "2">A. DATA PRIBADI</th>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="nama" class="col-form-label-sm required-label">Nama</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nama" name="nama" required>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="jk" class="col-form-label-sm required-label">Jenis Kelamin</label></td>
              <td class="col-8">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="jk" id="jk1" value="L" required>
                <label class="col-form-label-sm radio" for="jk1">Laki-Laki</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="jk" id="jk2" value="P">
                <label class="col-form-label-sm radio" for="jk2">Perempuan</label>
              </div> 
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="prodi" class="col-form-label-sm required-label">Prodi</label></td>
              <td class="col-8">
                <select name="prodi" id="prodi" class="custom-select custom-select-sm col-6" required>
                  <option value="">-Pilih-</option>
                  <?php
                    $data = $this->model_data->data_jurusan();
                    foreach($data->result() as $dt){
                  ?>
                  <option value="<?= $dt->kd_prodi;?>"><?= $dt->prodi;?></option>
                  <?php
                    }
                  ?>
                </select>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="tempat_lahir tgl_lahir" class="col-form-label-sm required-label">Tempat dan Tanggal Lahir</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-4 float-left" id="tempat_lahir" name="tempat_lahir" placeholder="Kota" required>
                <input type="date" class="form-control form-control-sm col-4 float-left set" id="tgl_lahir" name="tgl_lahir" required>
                <small class="txt-format">Format : mm/dd/yyyy</small>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="kewarganegaraan" class="col-form-label-sm required-label">Kewarganegaraan</label></td>
              <td class="col-8">
                <select name="kewarganegaraan" id="kewarganegaraan" class="custom-select custom-select-sm col-4" required>
                  <option value="">-Pilih-</option>
                  <option value="WNI">WNI</option>
                  <option value="WNA">WNA</option>
                </select>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="alamat" class="col-form-label-sm required-label">Alamat Lengkap</label></td>
              <td class="col-8">
                <textarea class="form-control form-control-sm col-12" id="alamat" name="alamat" rows="3" required></textarea>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="tlp" class="col-form-label-sm required-label">No. Telepon</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-6" id="tlp" name="tlp" required>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="alamat_saudara" class="col-form-label-sm required-label">Alamat Lengkap Saudara/Kenalan Anda di Jabotabekban</label></td>
              <td class="col-8">
                <textarea class="form-control form-control-sm col-12" id="alamat_saudara" name="alamat_saudara" rows="3" required></textarea>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="tlp_saudara" class="col-form-label-sm required-label">No. Telepon Saudara/Kenalan Anda</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-6" id="tlp_saudara" name="tlp_saudara" required>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="pend_terakhir" class="col-form-label-sm required-label">Pendidikan Formal Terakhir</label></td>
              <td class="col-8">
                <select name="pend_terakhir" id="pend_terakhir" class="custom-select custom-select-sm col-4" required>
                  <option value="">-Pilih-</option>
                  <option value="Lulus SLTA/SMA">Lulus SLTA/SMA</option>
                  <option value="Sarjana Teologi">Sarjana Teologi</option>
                  <option value="Sarjana Umum">Sarjana Umum</option>
                </select>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="asal_sekolah thn_lulus" class="col-form-label-sm required-label">Nama Sekolah Terakhir dan Tahun Lulus</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-9 float-left" id="asal_sekolah" name="asal_sekolah" placeholder="Nama Sekolah" required>
                <input type="text" class="form-control form-control-sm col-3" id="thn_lulus" name="thn_lulus" placeholder="Tahun Lulus" required>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="alamat_sekolah" class="col-form-label-sm required-label">Alamat Sekolah Terakhir</label></td>
              <td class="col-8">
                <textarea class="form-control form-control-sm col-12" id="alamat_sekolah" name="alamat_sekolah" rows="3" required></textarea>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="tinggal_dirumah" class="col-form-label-sm required-label">Terakhir Anda Tinggal di Rumah</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="tinggal_dirumah" name="tinggal_dirumah" required>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="marital_stat" class="col-form-label-sm required-label">Status Marital</label></td>
              <td class="col-8">
                <select name="marital_stat" id="marital_stat" class="custom-select custom-select-sm col-4" onchange="assignTo(this.value);" required>
                  <option value="">-Pilih-</option>
                  <option value="Belum Menikah">Belum Menikah</option>
                  <option value="Menikah">Menikah</option>
                  <option value="Duda/Janda">Duda/Janda</option>
                </select>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="nama_pasangan" class="col-form-label-sm">Nama Pasangan</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nama_pasangan" name="nama_pasangan" disabled>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="tempat_lahir_pasangan tanggal_lahir_pasangan" class="col-form-label-sm">Tempat dan Tanggal Lahir Pasangan</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-4 float-left" id="tempat_lahir_pasangan" name="tempat_lahir_pasangan" placeholder="Kota" disabled>
                <input type="date" class="form-control form-control-sm col-4 set" id="tgl_lahir_pasangan" name="tgl_lahir_pasangan" disabled>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="jumlah anak" class="col-form-label-sm">Jumlah Anak</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-3 float-left" id="jml_anak_L" name="jml_anak_L" placeholder="Anak Laki-laki">
                <input type="text" class="form-control form-control-sm col-3" id="jml_anak_P" name="jml_anak_P" placeholder="Anak Perempuan">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="gereja_anggota" class="col-form-label-sm">Anda Menjadi Anggota Gereja</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="gereja_anggota" name="gereja_anggota">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="alamat_gereja_anggota" class="col-form-label-sm">Alamat Gereja</label></td>
              <td class="col-8">
                <textarea class="form-control form-control-sm col-12" id="alamat_gereja_anggota" name="alamat_gereja_anggota" rows="3"></textarea>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="ibadah_melayani_gereja" class="col-form-label-sm">Apakah Anda Beribadah dan Melayani di Gereja yang Sama?</label></td>
              <td class="col-8">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="ibadah_melayani_gereja" id="ibadah_melayani_gereja1" value="1">
                <label class="col-form-label-sm radio" for="jk1">Ya</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="ibadah_melayani_gereja" id="ibadah_melayani_gereja2" value="0">
                <label class="col-form-label-sm radio" for="jk2">Tidak</label>
              </div> 
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="gereja_ibadah" class="col-form-label-sm">Nama Gereja Tempat Anda Beribadah</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="gereja_ibadah" name="gereja_ibadah">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="alamat_gereja_ibadah" class="col-form-label-sm">Alamat Gereja</label></td>
              <td class="col-8">
                <textarea class="form-control form-control-sm col-12" id="alamat_gereja_ibadah" name="alamat_gereja_ibadah" rows="3"></textarea>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="gereja_pelayanan" class="col-form-label-sm">Apakah Pelayanan Utama Anda di Gereja Tersebut</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-5" id="gereja_pelayanan" name="gereja_pelayanan">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="pekerjaan" class="col-form-label-sm">Pekerjaan</label></td>
              <td class="col-8">
                <select name="pekerjaan" id="pekerjaan" class="custom-select custom-select-sm col-4">
                  <option value="">-Pilih-</option>
                  <option value="Pegawai Negeri">Pegawai Negeri</option>
                  <option value="Pegawai Swasta">Pegawai Swasta</option>
                  <option value="Pedagang">Pedagang</option>
                  <option value="Ibu Rumah Tangga">Ibu Rumah Tangga</option>
                  <option value="Hamba Tuhan">Hamba Tuhan</option>
                  <option value="Belum Bekerja">Belum Bekerja</option>
                </select>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="nama_perusahaan" class="col-form-label-sm">Nama Perusahaan</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nama_perusahaan" name="nama_perusahaan">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="alamat_perusahaan" class="col-form-label-sm">Alamat Perusahaan</label></td>
              <td class="col-8">
                <textarea class="form-control form-control-sm col-12" id="alamat_perusahaan" name="alamat_perusahaan" rows="3"></textarea>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="bidang_perusahaan" class="col-form-label-sm">Perusahaan Bergerak di Bidang</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-5" id="bidang_perusahaan" name="bidang_perusahaan">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="minat" class="col-form-label-sm">Minat Utama (Bidang)</label></td>
              <td class="form-check col-8">
                <div class="row padding-l-20">
                  <div class="col-sm-4">
                    <input class="form-check-input" type="radio" name="minat" id="minat1" value="Teologi" onclick="disabledOn('#minat11');">
                    <label class="col-form-label-sm radio" for="minat1">Teologi</label><br>
                    <input class="form-check-input" type="radio" name="minat" id="minat2" value="Psikologi" onclick="disabledOn('#minat11');">
                    <label class="col-form-label-sm radio" for="minat2">Psikologi</label><br>
                    <input class="form-check-input" type="radio" name="minat" id="minat3" value="Filsafat" onclick="disabledOn('#minat11');">
                    <label class="col-form-label-sm radio" for="minat3">Filsafat</label><br>
                    <input class="form-check-input" type="radio" name="minat" id="minat4" value="Ekonomi" onclick="disabledOn('#minat11');">
                    <label class="col-form-label-sm radio" for="minat4">Ekonomi</label><br>
                    <input class="form-check-input" type="radio" name="minat" id="minat5" value="Ilmu Pasti" onclick="disabledOn('#minat11');">
                    <label class="col-form-label-sm radio" for="minat5">Ilmu Pasti</label>
                  </div>
                  <div clminats="col-sm-8">
                    <input class="form-check-input" type="radio" name="minat" id="minat6" value="Politik & Hukum" onclick="disabledOn('#minat11');">
                    <label class="col-form-label-sm radio" for="minat6">Politik & Hukum</label><br>
                    <input class="form-check-input" type="radio" name="minat" id="minat7" value="Sosial" onclick="disabledOn('#minat11');">
                    <label class="col-form-label-sm radio" for="minat7">Sosial</label><br>
                    <input class="form-check-input" type="radio" name="minat" id="minat8" value="Musik" onclick="disabledOn('#minat11');">
                    <label class="col-form-label-sm radio" for="minat8">Musik</label><br>
                    <input class="form-check-input" type="radio" name="minat" id="minat9" value="Komputer" onclick="disabledOn('#minat11');">
                    <label class="col-form-label-sm radio" for="minat9">Komputer</label><br>
                    <input class="form-check-input" type="radio" name="minat" id="minat10" value="" onclick="disabledOff('#minat11');">
                    <label class="col-form-label-sm radio" for="minat10">Lainnya</label>
                    <input type="text" name="minat11" id="minat11" class="col-8" disabled>
                  </div> 
                </div>
              </td>
            </tr>
            <tr>
              <td colspan="2" class="col-form-label-sm">Untuk Pertanyaan Berikut ini Silahkan isi dengan huruf <em>A</em> untuk hal yang dikuasai dengan <em>SANGAT BAIK</em>, <em>B</em> jika dikuasai dengan <em>BAIK</em>, <em>C</em> jika <em>KURANG</em> dan <em>D</em> jika <em>TIDAK BISA SAMA SEKALI</em></th>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="inggris" class="col-form-label-sm">Kemampuan Penguasaan Bahasa Inggris</label></td>
              <td class="col-8">
                <select name="inggris" id="inggris" class="custom-select custom-select-sm col-4">
                  <option value="">-Pilih-</option>
                  <option value="A">A (Sangat Baik)</option>
                  <option value="B">B (Baik)</option>
                  <option value="C">C (Kurang Menguasai)</option>
                  <option value="C">D (Tidak Bisa Sama Sekali)</option>
                </select>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="bahasa_asing" class="col-form-label-sm">Kemampuan Bahasa Asing</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="bahasa_asing" name="bahasa_asing">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="bakat" class="col-form-label-sm">Bakat dan Talenta Anda</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-10" id="bakat" name="bakat">
                <small>Contoh : Gitar, Piano, Mengajar, Drama, Layout Setting, dll</small>
              </td>
            </tr>
            <tr>
              <td colspan="2" >
                <center>
                  <?php
                    // if(!empty($data_pribadi)&&!empty($data_keluarga)&&!empty($data_pp)&&!empty($data_rohani)&&!empty($data_lain)&&!empty($data_upload)){
                    if(!empty($data_pribadi)&&!empty($data_keluarga)&&!empty($data_pp)&&!empty($data_rohani)&&!empty($data_lain)){
                  ?>
                    <input type="text" name="done" value="1" hidden>
                    <button type="submit" class="btn btn-sm btn-success"><i class="far fa-file-alt"></i> Save dan Lihat Semua Data</a>
                  <?php
                    }else{
                  ?>
                    <a href="<?= site_url();?>home/logout" class="btn btn-sm btn-info"><i class="fas fa-arrow-alt-circle-left"></i> Cancel</a>
                    <button type="submit" id="simpan" class="btn btn-sm btn-primary"><i class="fas fa-arrow-alt-circle-right"></i> Lanjut Ke Step 2</button>
                  <?php
                    }
                  ?>
                </center>
              </td>
            </tr>
          </tbody>
        </table>
        <input type="text" id="noPendaftaran" value="<?=@$this->session->userdata('no_pendaftaran');?>" hidden>
      </form>
    </section>
  </div>
</main>

<script>
  document.addEventListener('DOMContentLoaded', function(){
    let currentDate = new Date();
    let y = currentDate.getFullYear();
    let m = (`0${currentDate.getMonth()+1}`).slice(-2);
    let d = (`0${currentDate.getDate()}`).slice(-2);
    const eDate = document.querySelectorAll('input[type="date"].set');

    eDate.forEach(function(e){
      e.setAttribute('min',`${y-77}-01-01`);
      e.setAttribute('max',`${y}-${m}-${d}`);
      // console.log(e);
    });
    getData();
  });

  function getData(){
    let noPendaftaran = document.getElementById('noPendaftaran').value;
    let params = "no_pendaftaran="+noPendaftaran;

    let xhr = new XMLHttpRequest();
    xhr.open('POST','<?=site_url();?>getdata/get_data_pribadi',true);
    xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    xhr.onload = function(){
      if(this.status == 200){
        if(this.responseText){
          let r = JSON.parse(this.responseText);
          
          // Check Marital_stat nikah/ belom, trus input disabled nya on atau off berdasarkan itu
          assignTo(r.marital_stat);
          
          document.querySelector('#nama').value = r.nama;
          if(document.querySelector('#jk1').value == r.jk){
            document.querySelector('#jk1').setAttribute('checked',true);
          }else if(document.querySelector('#jk2').value == r.jk){
            document.querySelector('#jk2').setAttribute('checked',true);
          }
          document.querySelector('#prodi').value = r.kd_prodi;
          document.querySelector('#tempat_lahir').value = r.tempat_lahir;
          document.querySelector('#tgl_lahir').value = r.tgl_lahir;
          document.querySelector('#kewarganegaraan').value = r.kewarganegaraan;
          document.querySelector('#alamat').value = r.alamat;
          document.querySelector('#tlp').value = r.tlp;
          document.querySelector('#alamat_saudara').value = r.alamat_saudara;
          document.querySelector('#tlp_saudara').value = r.tlp_saudara;
          document.querySelector('#pend_terakhir').value = r.pend_terakhir;
          document.querySelector('#asal_sekolah').value = r.asal_sekolah;
          document.querySelector('#thn_lulus').value = r.thn_lulus;
          document.querySelector('#alamat_sekolah').value = r.alamat_sekolah;
          document.querySelector('#tinggal_dirumah').value = r.tinggal_dirumah;
          document.querySelector('#marital_stat').value = r.marital_stat;
          document.querySelector('#jml_anak_L').value = r.jml_anak_L;
          document.querySelector('#jml_anak_P').value = r.jml_anak_P;
          document.querySelector('#gereja_anggota').value = r.gereja_anggota;
          document.querySelector('#alamat_gereja_anggota').value = r.alamat_gereja_anggota;
          
          if(document.querySelector('#ibadah_melayani_gereja1').value == r.ibadah_melayani_gereja){
            document.querySelector('#ibadah_melayani_gereja1').setAttribute('checked',true);
          }else if(document.querySelector('#ibadah_melayani_gereja2').value == r.ibadah_melayani_gereja){
            document.querySelector('#ibadah_melayani_gereja2').setAttribute('checked',true);
          }
          document.querySelector('#gereja_ibadah').value = r.gereja_ibadah;
          document.querySelector('#alamat_gereja_ibadah').value = r.alamat_gereja_ibadah;
          document.querySelector('#gereja_pelayanan').value = r.gereja_pelayanan;
          document.querySelector('#pekerjaan').value = r.pekerjaan;
          document.querySelector('#nama_perusahaan').value = r.nama_perusahaan;
          document.querySelector('#alamat_perusahaan').value = r.alamat_perusahaan;
          document.querySelector('#bidang_perusahaan').value = r.bidang_perusahaan;
          if(document.querySelector('#minat1').value == r.minat){
            document.querySelector('#minat1').setAttribute('checked',true);
          }else if(document.querySelector('#minat2').value == r.minat){
            document.querySelector('#minat2').setAttribute('checked',true);
          }else if(document.querySelector('#minat3').value == r.minat){
            document.querySelector('#minat3').setAttribute('checked',true);
          }else if(document.querySelector('#minat4').value == r.minat){
            document.querySelector('#minat4').setAttribute('checked',true);
          }else if(document.querySelector('#minat5').value == r.minat){
            document.querySelector('#minat5').setAttribute('checked',true);
          }else if(document.querySelector('#minat6').value == r.minat){
            document.querySelector('#minat6').setAttribute('checked',true);
          }else if(document.querySelector('#minat7').value == r.minat){
            document.querySelector('#minat7').setAttribute('checked',true);
          }else if(document.querySelector('#minat8').value == r.minat){
            document.querySelector('#minat8').setAttribute('checked',true);
          }else if(document.querySelector('#minat9').value == r.minat){
            document.querySelector('#minat9').setAttribute('checked',true);
          }else if(r.minat){
            document.querySelector('#minat10').setAttribute('checked',true);
            document.querySelector('#minat11').value = r.minat;
            document.querySelector('#minat11').removeAttribute('disabled');
          }
          document.querySelector('#inggris').value = r.inggris;
          document.querySelector('#bahasa_asing').value = r.bahasa_asing;
          document.querySelector('#bakat').value = r.bakat;
          document.querySelector('#nama_pasangan').value = r.nama_pasangan;
          document.querySelector('#tempat_lahir_pasangan').value = r.tempat_lahir_pasangan;
          document.querySelector('#tgl_lahir_pasangan').value = r.tgl_lahir_pasangan;
        }
      }
    }
    xhr.send(params);
  }

  function assignTo(r){
    if(r == 'Menikah' || r == 'Duda/Janda'){
      disabledOff(['#nama_pasangan','#tempat_lahir_pasangan','#tgl_lahir_pasangan']);
    }else{
      disabledOn(['#nama_pasangan','#tempat_lahir_pasangan','#tgl_lahir_pasangan']);
    }
  }

  function disabledOn(kelas){
    //console.log(kelas);
    if(Array.isArray(kelas)){
      //console.log(kelas);
      kelas.forEach(function(kelas_single){
        const e = document.querySelector(kelas_single);
        e.setAttribute('disabled',true);
        e.value = "";
      });
    }else{
      const e = document.querySelector(kelas);
      e.setAttribute('disabled',true);
      e.value = "";
    }
  }

  function disabledOff(kelas){
    //console.log(kelas);
    if(Array.isArray(kelas)){
      //console.log(kelas);
      kelas.forEach(function(kelas_single){
        const e = document.querySelector(kelas_single);
        e.removeAttribute('disabled');
      });
    }else{
      const e = document.querySelector(kelas);
      e.removeAttribute('disabled');
    }
  }
  

</script>