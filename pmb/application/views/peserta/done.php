<style>
body {
    background:#DDD!important;
}
    .table-striped tbody tr:nth-of-type(odd) {
    background-color: rgb(201 190 190 / 5%);
}
.shadow {
        box-shadow: 5px 0 7px 0 rgb(0 0 0 / 30%);
}


.table tbody > tr > th {
    background: #ededed;
}
.h-pesertas {
    box-shadow: 5px 0 14px 0 rgb(0 0 0);
}
/* width */
::-webkit-scrollbar {
  width: 5px;
  height: 5px;
}

/* Track */
::-webkit-scrollbar-track {
  background: #f1f1f1; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #888; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #555; 
}
.navbar-expand-lg {
    border-top: 7px solid #4caf50; 
}
</style>



<section class="col-sm-9">
  <nav class="navbar shadow navbar-expand-lg navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="menu">
      <div class="navbar-nav">
        <a class="nav-item nav-link active" href="" id="data_identitas">Data Identitas</a>
        <a class="nav-item nav-link" href="" id="data_pp">Data Pendidikan dan Riwayat Pekerjaan</a>
        <a class="nav-item nav-link" href="" id="data_rekomendasi">Data Rekomendasi</a>
        <a class="nav-item nav-link" href="" id="data_tekad_dan_keputusan">Data Tekad dan Keputusan</a>
        <a class="nav-item nav-link" href="" id="data_lain">Data Lain-Lain</a>
        
        <!-- <a class="nav-item nav-link" href="" id="data_upload">Data Upload File</a>
        <a class="nav-item nav-link" href="<?=site_url();?>home/print_pdf" id="print_pdf" target="_blank">Print PDF</a> -->
        <!-- <a class="nav-item nav-link" href="<?=site_url();?>home/logout" id="logout">Logout</a> -->
      </div>
    </div>
  </nav>

  <!-- data_identitas -->
  <section class="bg-light shadow row padding-data flag">
    <table class="table table-striped">
      <pre>
      <?php //print_r($data_identitas);?>
      </pre>
      <thead>
        <tr>
          <th colspan="3" scope="col" class="text-center">No. Pendaftaran : <?=$data_identitas['no_pendaftaran'];?></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th colspan="3" scope="col">IDENTITAS</th>
        </tr>
        <?php $i=1; ?>
        <tr>
          <td scope="row" width="5%"><?php echo $i++; ?></td>
          <td width="35%">Nama Lengkap</td>
          <td width="60%">: <?=$data_identitas['nama'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Tempat, Tanggal Lahir</td>
          <td>: <?=$data_identitas['tempat_lahir'];?>, <?= date('d M Y', strtotime($data_identitas['tgl_lahir']));?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Jenis Kelamin</td>
          <td>: <?php
          if ($data_identitas['jk']=='P'){
            echo "Perempuan";  
          }else if  ($data_identitas['jk']=='L'){
            echo "Laki-Laki";
          } else {
            echo "";
          }
          ?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Pekerjaan</td>
          <td width="60%">: <?=$data_identitas['pekerjaan'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Alamat</td>
          <td width="60%">: <?=$data_identitas['alamat'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Kewarganegaraan</td>
          <td width="60%">: <?=$data_identitas['kewarganegaraan'];?></td>
        </tr>


            <!--- DATA ORANG TUA ///////////////////////////////////////////////////////////////////////////////////////-->
            <tr>
              <th colspan="3" scope="col">DATA ORANG TUA</th>
            </tr>
            <tr> 
              <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"> <label class="col-form-label-sm required-label">Nama Lengkap (Sesuai KTP)*</label></td>
              <td >
              <?=$data_identitas['nama_ortu'];?>
              </td>
            </tr>
            <tr >
              
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm required-label">Tempat Tanggal Lahir*</label></td>
              <td width="60%">
              <?=$data_identitas['tmptgl_ortu'];?>  
              </td>
            </tr>
            <tr >
              
              <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm required-label">NIK*</label></td>
              <td width="60%">
              <?=$data_identitas['nik_ortu'];?>
              </td>
            </tr>
            <tr >
              
              <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm required-label">Jenis Kelamin*</label></td>
              <td width="60%">
              <?=$data_identitas['jeniskelamin_ortu'];?>
              </td>
            </tr>
            <tr >
              
              <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm required-label">Alamat Lengkap *</label></td>
              <td width="60%">
                
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm required-label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desa/Kelurahan </label></td>
              <td width="60%">
                <?=$data_identitas['dusun_ortu'];?>  
              </td>
            </tr>
            <tr >
            
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm required-label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kecamatan</label></td>
              <td width="60%">
              <?=$data_identitas['kecamatan_ortu'];?>  
              </td>
            </tr>
            <tr >
          
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm required-label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kabupaten/Kota</label></td>
              <td width="60%">
              <?=$data_identitas['kota_ortu'];?>  
              </td>
            </tr>
            <tr >
          
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm required-label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Provinsi</label></td>
              <td width="60%">
              <?=$data_identitas['profvinsi_ortu'];?>  
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm required-label">Pekerjaan:*</label></td>
              <td width="60%">
              <?=$data_identitas['pekerjaan_ortu'];?>   
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm required-label">No. Telepon dan WA*</label></td>
              <td width="60%">
              <?=$data_identitas['notlp_ortu'];?>  
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm required-label">Email*  </label></td>
              <td width="60%">
              <?=$data_identitas['email_ortu'];?>  
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm required-label">Sudah Berkeluarga*</label></td>
              <td width="60%">

              <?=$data_identitas['sudah_berkeluarga_ortu'];?>
             
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm required-label">Jaminan Kesehatan yang Anda Ikuti?*</label></td>
              <td width="60%">

              <?=$data_identitas['jaminan_kesehatan_ortu'];?>
             
              </td>
            </tr>
            <tr >
             <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm  ">Kewarganegaraan  </label></td>
              <td width="60%">
              <?=$data_identitas['kewarganegaraan_ortu'];?> 
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label  class="col-form-label-sm required-label">Pendidikan Terakhir*</label></td>
              <td width="60%">
              <?=$data_identitas['pendidikan_terakhir_ortu'];?> 
              </td>
            </tr>
            
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm required-label">NISN*   </label></td>
              <td width="60%">
              <?=$data_identitas['nisn_ortu'];?>  
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm required-label">Pernah Sekolah Alkitab?*</label></td>
              <td width="60%">
                
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama Sekolah Alkitab</label></td>
              <td width="60%">
              <?=$data_identitas['namasekolah_ortu'];?>  
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kelas/Tingkat  </label></td>
              <td width="60%">
              <?=$data_identitas['kelastingkat_ortu'];?>  
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tahun SA</label></td>
              <td width="60%">
              <?=$data_identitas['tahunsa_ortu'];?> 
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm required-label">Asal Gereja *     </label></td>
              <td width="60%">
              <?=$data_identitas['asalgereja_ortu'];?>  
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm required-label ">Nama Pemimpin Jemaat <br>  (Gembala, Ketua Jemaat, dll) * </label></td>
              <td width="60%">
              <?=$data_identitas['pemimpinjemaat_ortu'];?> 
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm required-label "> Apakah biasa melayani? <br> Jika ya, melayani apa*
              </label></td>
              <td width="60%">
              <?=$data_identitas['apakabisamelayani_ortu'];?>   
              </td>
            </tr>

            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label   class="col-form-label-sm required-label">Program Studi: *</label></td>
              <td width="60%">
              <?=$data_identitas['programstudi_ortu'];?>   
             
            
              </td>
            </tr>
 
            <!--- / END DATA ORANG TUA -->
            <!--- DATA ORANG TUA WALI -->
            
            <tr>
              <th colspan="3" scope="col">DATA ORANG Tua/Wali</th>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm required-label">Ayah*</label></td>
              <td width="60%">
                
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama Ayah Kandung (Nama Lengkap) </label></td>
              <td width="60%">
              <?=$data_identitas['namaayah_ortuwali'];?>   
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIK  </label></td>
              <td width="60%">
              <?=$data_identitas['nikayah_ortuwali'];?>   
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tempat Tanggal Lahir</label></td>
              <td width="60%">
              <?=$data_identitas['tglayah_ortuwali'];?>   
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm required-label">Ibu*</label></td>
              <td width="60%">
                
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama Ibu Kandung (Nama Lengkap) </label></td>
              <td width="60%">
              <?=$data_identitas['namaibu_ortuwali'];?>  
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIK  </label></td>
              <td width="60%">
              <?=$data_identitas['nikibu_ortuwali'];?>   
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tempat Tanggal Lahir</label></td>
              <td width="60%">
              <?=$data_identitas['tglibu_ortuwali'];?>   
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm required-label">Wali*</label></td>
              <td width="60%">
                
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama Wali</label></td>
              <td width="60%">
              <?=$data_identitas['namawali_ortuwali'];?>   
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIK  </label></td>
              <td width="60%">
              <?=$data_identitas['nikwali_ortuwali'];?>    
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm  ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tempat Tanggal Lahir</label></td>
              <td width="60%">
              <?=$data_identitas['tglwali_ortuwali'];?>   
              </td>
            </tr>
            <tr >
            <td scope="row"><?php echo $i++; ?></td>
              <td width="35%"><label class="col-form-label-sm required-label">Mengetahui STT RMK dari *</label></td>
              <td width="60%">
              <?=$data_identitas['mengetahuisttrmk'];?>   
             
              </td>
            </tr>
            <!--- / END DATA ORANG TUA WALI /////////////////////////////////////////////////////////////////////////////   -->




        
        <tr>
          <th colspan="3" scope="col">KEANGGOTAAN GEREJA</th>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Gereja Anggota</td>
          <td width="60%">: <?=$data_identitas['gereja_anggota'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Status Baptis</td>
          <td>: <?php
          if ($data_identitas['status_baptis'] == 1){
            echo "Sudah";  
          }else if ($data_identitas['status_baptis'] == 0){
            echo "Belum";
          } else {
            echo "";
          }
          ?></td>
        </tr>
        <?php /****
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Tgl Baptis</td>
          <td width="60%">: <?= date('d M Y', strtotime($data_identitas['tgl_baptis']));?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Anggota Sidi</td>
          <td>: <?php
          if ($data_identitas['gereja_sidi'] == 1){
            echo "Ya";  
          }else if ($data_identitas['gereja_sidi'] == 0) {
            echo "Tidak";
          } else {
            echo "";
          }
          ?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Anggota Penuh</td>
          <td>: <?php
          if ($data_identitas['status_anggota_sidi_penuh'] == 1){
            echo "Ya";  
          }else if ($data_identitas['status_anggota_sidi_penuh'] == 0) {
            echo "Tidak";
          } else {
            echo "";
          }
          ?></td>
        </tr>
        ****/ ?>
        
        <tr>
          <th colspan="3" scope="col">PACAR/TUNANGAN</th>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Nama Pacar</td>
          <td width="60%">: <?=$data_identitas['nama_pacar'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Pekerjaan Pacar</td>
          <td width="60%">: <?=$data_identitas['pekerjaan_pacar'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Agama Pacar</td>
          <td width="60%">: <?=$data_identitas['agama_pacar'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Sudah Menerima Tuhan Yesus</td>
          <td width="60%">: <?=$data_identitas['sudah_menerima_tuhan_yesus'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Hubungan Dimulai</td>
          <td width="60%">: <?=$data_identitas['hubungan_dimulai'];?></td>
        </tr>

        <?php /***
        <tr>
          <th colspan="3" scope="col">Bagian khusus bagi mereka yang sudah berkeluarga </th>
        </tr>
        <tr>
          <td colspan="3" scope="col">Telah menikah dengan</td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Nama Lengkap</td>
          <td width="60%">: <?=$data_identitas['nama_pasangan'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Tempat, Tanggal Lahir</td>
          <td>: <?=$data_identitas['tempat_lahir_pasangan'];?>
            <?php
            if($data_identitas['tgl_lahir_pasangan'] == NULL || $data_identitas['tgl_lahir_pasangan'] == ""){
              echo "";
            }else{
              echo ", ".date('d M Y', strtotime($data_identitas['tgl_lahir_pasangan']));
            }
            ?>
          </td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Pekerjaan</td>
          <td width="60%">: <?=$data_identitas['pekerjaan_pasangan'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Pendidikan Terkahir</td>
          <td width="60%">: <?=$data_identitas['pend_terakhir_pasangan'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Agama/Gereja</td>
          <td width="60%">: <?=$data_identitas['agama_pasangan'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Sudah menerima Tuhan Yesus</td>
          <td width="60%">: <?=$data_identitas['pasangan_sudah_menerima_tuhan_yesus'];?></td>
        </tr>



        <tr>
          <td colspan="3" scope="col">Jumlah Anak</td>
        </tr>
        <tr>
          <td colspan="3" scope="col">Anak 1</td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Nama Lengkap</td>
          <td width="60%">: <?=$data_identitas['nama_anak1'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Tempat, Tanggal Lahir</td>
          <td>: <?=$data_identitas['tempat_lahir_anak1'];?>
            <?php
            if($data_identitas['tgl_lahir_anak1'] == NULL || $data_identitas['tgl_lahir_anak1'] == ""){
              echo "";
            }else{
              echo ", ".date('d M Y', strtotime($data_identitas['tgl_lahir_anak1']));
            }
            ?>
          </td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Keterangan</td>
          <td width="60%">: <?=$data_identitas['keterangan_anak1'];?></td>
        </tr>

        <tr>
          <td colspan="3" scope="col">Anak 2</td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Nama Lengkap</td>
          <td width="60%">: <?=$data_identitas['nama_anak2'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Tempat, Tanggal Lahir</td>
          <td>: <?=$data_identitas['tempat_lahir_anak2'];?>
            <?php
            if($data_identitas['tgl_lahir_anak2'] == NULL || $data_identitas['tgl_lahir_anak2'] == ""){
              echo "";
            }else{
              echo ", ".date('d M Y', strtotime($data_identitas['tgl_lahir_anak2']));
            }
            ?>
          </td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Keterangan</td>
          <td width="60%">: <?=$data_identitas['keterangan_anak2'];?></td>
        </tr>

        <tr>
          <td colspan="3" scope="col">Anak 3</td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Nama Lengkap</td>
          <td width="60%">: <?=$data_identitas['nama_anak3'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Tempat, Tanggal Lahir</td>
          <td>: <?=$data_identitas['tempat_lahir_anak3'];?>
            <?php
            if($data_identitas['tgl_lahir_anak3'] == NULL || $data_identitas['tgl_lahir_anak3'] == ""){
              echo "";
            }else{
              echo ", ".date('d M Y', strtotime($data_identitas['tgl_lahir_anak3']));
            }
            ?>
          </td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Keterangan</td>
          <td width="60%">: <?=$data_identitas['keterangan_anak3'];?></td>
        </tr>

        <tr>
          <td colspan="3" scope="col">Anak 4</td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Nama Lengkap</td>
          <td width="60%">: <?=$data_identitas['nama_anak4'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Tempat, Tanggal Lahir</td>
          <td>: <?=$data_identitas['tempat_lahir_anak4'];?>
            <?php
            if($data_identitas['tgl_lahir_anak4'] == NULL || $data_identitas['tgl_lahir_anak4'] == ""){
              echo "";
            }else{
              echo ", ".date('d M Y', strtotime($data_identitas['tgl_lahir_anak4']));
            }
            ?>
          </td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Keterangan</td>
          <td width="60%">: <?=$data_identitas['keterangan_anak4'];?></td>
        </tr>

        <tr>
          <td colspan="3" scope="col">Anak 5</td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Nama Lengkap</td>
          <td width="60%">: <?=$data_identitas['nama_anak5'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Tempat, Tanggal Lahir</td>
          <td>: <?=$data_identitas['tempat_lahir_anak5'];?>
            <?php
            if($data_identitas['tgl_lahir_anak5'] == NULL || $data_identitas['tgl_lahir_anak5'] == ""){
              echo "";
            }else{
              echo ", ".date('d M Y', strtotime($data_identitas['tgl_lahir_anak5']));
            }
            ?>
          </td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Keterangan</td>
          <td width="60%">: <?=$data_identitas['keterangan_anak5'];?></td>
        </tr>

        <tr>
          <td colspan="3" scope="col">Anak 6</td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Nama Lengkap</td>
          <td width="60%">: <?=$data_identitas['nama_anak6'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Tempat, Tanggal Lahir</td>
          <td>: <?=$data_identitas['tempat_lahir_anak6'];?>
            <?php
            if($data_identitas['tgl_lahir_anak6'] == NULL || $data_identitas['tgl_lahir_anak6'] == ""){
              echo "";
            }else{
              echo ", ".date('d M Y', strtotime($data_identitas['tgl_lahir_anak6']));
            }
            ?>
          </td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Keterangan</td>
          <td width="60%">: <?=$data_identitas['keterangan_anak6'];?></td>
        </tr>

    

        <tr>
          <th colspan="3" scope="col">Yang bertanggung jawab dalam menanggung biaya hidup bagi keluarga (isteri dan anak-anak), jika saudara diterima di STTE</th>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Nama Lengkap</td>
          <td width="60%">: <?=$data_identitas['nama_pembiaya'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Umur</td>
          <td width="60%">: <?=$data_identitas['umur_pembiaya'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Pekerjaan</td>
          <td width="60%">: <?=$data_identitas['pekerjaan'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Alamat Lengkap</td>
          <td width="60%">: <?=$data_identitas['alamat_pembiaya'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Agama/Gereja</td>
          <td width="60%">: <?=$data_identitas['agama_pembiaya'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Persetujuan Masuk STTE</td>
          <td>: <?php
          if($data_identitas['status_persetujuan_masuk_pembiaya'] == 1){
            echo "Ya";  
          } else if ($data_identitas['status_persetujuan_masuk_pembiaya'] == 2){
            echo "Tidak";
          } else {
            echo "";
          }
          ?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td width="35%">Keterangan</td>
          <td width="60%">: <?=$data_identitas['alasan_pembiaya'];?></td>
        </tr>
        ****/ ?>
      </tbody>
    </table>
    <a type="button" class="btn btn-info btn-edit-done" href="<?= site_url();?>data_identitas"><i class="far fa-edit"></i> Edit</a>
  </section>


  <!-- data_pp -->
  <section class="bg-light row padding-data flag none">
    <table class="table table-striped">
      <pre>
      <?php //print_r($data_pp);?>
      </pre>
      <thead>
        <tr>
          <th colspan="3" scope="col" class="text-center">No. Pendaftaran : <?=$data_identitas['no_pendaftaran'];?></th>
        </tr>
      </thead>
      <tbody>
        <tr class="justify-content-center">
          <td scope="row" width="5%"><?php echo $i++; ?></td>
          <td colspan="2">Riwayat Pendidikan<br>
          <em>Pendidikan Dasar dan Menengah</em>
          <table class="table table-bordered table-sm">
              <tr class="table-active">
                <th width="5%">No.</th>
                <th width="35%">Pendidikan Formal</th>
                <th width="45%">Nama Tempat Pendidikan</th>
                <th width="15%">Tahun Ijazah</th>
              </tr>
              <tr>
                <td>1</td>
                <td>Taman Kanak-Kanak</td>
                <td><?=$data_pp['tk'];?></td>
                <td><?=$data_pp['tk_thn'];?></td>
              </tr>
              <tr>
                <td>2</td>
                <td>SD</td>
                <td><?=$data_pp['sd'];?></td>
                <td><?=$data_pp['sd_thn'];?></td>
              </tr>
              <tr>
                <td>3</td>
                <td>SMP</td>
                <td><?=$data_pp['smp'];?></td>
                <td><?=$data_pp['smp_thn'];?></td>
              </tr>
              <tr>
                <td>4</td>
                <td>SMA/SMK/STM</td>
                <td><?=$data_pp['sma'];?></td>
                <td><?=$data_pp['sma_thn'];?></td>
              </tr>
          </table><br>

          <em>Pendidikan Tinggi</em>          
          <table class="table table-bordered table-sm">
              <tr class="table-active">
                <th width="5%">No.</th>
                <th width="35%">Pendidikan Formal</th>
                <th width="45%">Nama Tempat Pendidikan</th>
                <th width="15%">Tahun Ijazah</th>
              </tr>
              <tr>
                <td>1</td>
                <td><?=$data_pp['pendidikantinggi1_jenjang'];?></td>
                <td><?=$data_pp['pendidikantinggi1_nama'];?></td>
                <td><?=$data_pp['pendidikantinggi1_tahun_ijazah'];?></td>
              </tr>
              <tr>
                <td>2</td>
                <td><?=$data_pp['pendidikantinggi2_jenjang'];?></td>
                <td><?=$data_pp['pendidikantinggi2_nama'];?></td>
                <td><?=$data_pp['pendidikantinggi2_tahun_ijazah'];?></td>
              </tr>
              <tr>
                <td>3</td>
                <td><?=$data_pp['pendidikantinggi3_jenjang'];?></td>
                <td><?=$data_pp['pendidikantinggi3_nama'];?></td>
                <td><?=$data_pp['pendidikantinggi3_tahun_ijazah'];?></td>
              </tr>
          </table>
          </td>
        </tr>


        <tr class="justify-content-center">
          <td scope="row" width="5%"><?php echo $i++; ?></td>
          <td colspan="2">Kursus-Kursus<br>
          <table class="table table-bordered table-sm">
              <tr class="table-active">
                <th width="5%">No.</th>
                <th width="30%">Kursus</th>
                <th width="50%">Nama Lembaga Penyelenggara</th>
                <th width="15%">Tahun Ijazah</th>
              </tr>
              <tr>
                <td>1</td>
                <td><?=$data_pp['kursus1'];?></td>
                <td><?=$data_pp['kursus1_nama'];?></td>
                <td><?=$data_pp['kursus1_ijazah'];?></td>
              </tr>
              <tr>
                <td>2</td>
                <td><?=$data_pp['kursus2'];?></td>
                <td><?=$data_pp['kursus2_nama'];?></td>
                <td><?=$data_pp['kursus2_ijazah'];?></td>
              </tr>
              <tr>
                <td>3</td>
                <td><?=$data_pp['kursus3'];?></td>
                <td><?=$data_pp['kursus3_nama'];?></td>
                <td><?=$data_pp['kursus3_ijazah'];?></td>
              </tr>
          </table>
          </td>
        </tr>



        <tr class="justify-content-center">
          <td scope="row" width="5%"><?php echo $i++; ?></td>
          <td colspan="2">Bakat/Keterampilan Yang Dimiliki<br>
          <table class="table table-bordered table-sm">
              <tr class="table-active">
                <th width="5%">No.</th>
                <th width="30%">Bakat dan Karunia</th>
                <th width="15%">Keterampilan</th>
              </tr>
              <tr>
                <td>1</td>
                <td><?=$data_pp['bakat1'];?></td>
                <td><?=$data_pp['keterampilan1'];?></td>
              </tr>
              <tr>
                <td>2</td>
                <td><?=$data_pp['bakat2'];?></td>
                <td><?=$data_pp['keterampilan2'];?></td>
              </tr>
              <tr>
                <td>3</td>
                <td><?=$data_pp['bakat3'];?></td>
                <td><?=$data_pp['keterampilan3'];?></td>
              </tr>
          </table>
          </td>
        </tr>

        <tr class="justify-content-center">
          <td scope="row" width="5%"><?php echo $i++; ?></td>
          <td colspan="2">Riwayat Pekerjaan<br>
          <table class="table table-bordered table-sm">
              <tr class="table-active">
                <th width="5%">No.</th>
                <th width="50%">Jenis Pekerjaan</th>
                <th width="30%">Jabatan</th>                
                <th width="15%">Lamanya</th>
              </tr>
              <tr>
                <td>1</td>
                <td><?=$data_pp['jenis_kerja1'];?></td>
                <td><?=$data_pp['jabatan_kerja1'];?></td>
                <td><?=$data_pp['thn_kerja1'];?></td>
              </tr>
              <tr>
                <td>2</td>
                <td><?=$data_pp['jenis_kerja2'];?></td>
                <td><?=$data_pp['jabatan_kerja2'];?></td>
                <td><?=$data_pp['thn_kerja2'];?></td>
              </tr>
              <tr>
                <td>3</td>
                <td><?=$data_pp['jenis_kerja3'];?></td>
                <td><?=$data_pp['jabatan_kerja3'];?></td>
                <td><?=$data_pp['thn_kerja3'];?></td>
              </tr>
          </table>
          </td>
        </tr>

        <tr class="justify-content-center">
          <td scope="row" width="5%"><?php echo $i++; ?></td>
          <td colspan="2">Kesaksian Pertobatan<br>
          <table class="table table-bordered table-sm">
            <tr>
                <td>
                <?php 
                if($data_pp['kesaksian_pertobatan'] == null || $data_pp['kesaksian_pertobatan'] == "") {
                  echo "-";
                } else {
                  echo $data_pp['kesaksian_pertobatan'];
                }
                ?>
                </td>
              </tr>
          </table>
          </td>
        </tr>

        <tr class="justify-content-center">
          <td scope="row" width="5%"><?php echo $i++; ?></td>
          <td colspan="2">Tempat Pertobatan<br>
          <table class="table table-bordered table-sm">
            <tr>
                <td>
                <?php 
                if($data_pp['tempat_pertobatan'] == null || $data_pp['tempat_pertobatan'] == "") {
                  echo "-";
                } else {
                  echo $data_pp['tempat_pertobatan'];
                }
                ?>
                </td>
              </tr>
          </table>
          </td>
        </tr>

        <tr class="justify-content-center">
          <td scope="row" width="5%"><?php echo $i++; ?></td>
          <td colspan="2">Deskripsi Pertobatan<br>
          <table class="table table-bordered table-sm">
            <tr>
                <td>
                <?php 
                if($data_pp['deskripsi_pertobatan'] == null || $data_pp['deskripsi_pertobatan'] == "") {
                  echo "-";
                } else {
                  echo $data_pp['deskripsi_pertobatan'];
                }
                ?>
                </td>
              </tr>
          </table>
          </td>
        </tr>

        <tr class="justify-content-center">
          <td scope="row" width="5%"><?php echo $i++; ?></td>
          <td colspan="2">Perkembangan Rohani<br>
          <table class="table table-bordered table-sm">
            <tr>
                <td>
                <?php 
                if($data_pp['perkembangan_rohani'] == null || $data_pp['perkembangan_rohani'] == "") {
                  echo "-";
                } else {
                  echo $data_pp['perkembangan_rohani'];
                }
                ?>
                </td>
              </tr>
          </table>
          </td>
        </tr>

        <tr class="justify-content-center">
          <td scope="row" width="5%"><?php echo $i++; ?></td>
          <td colspan="2">Setelah bertobat saudara membaca Alkitab dan berdoa setiap hari<br>
          <table class="table table-bordered table-sm">
            <tr>
                <td>
                <?php 
                if($data_pp['berdoa'] == null || $data_pp['berdoa'] == "") {
                  echo "-";
                } else {
                  echo $data_pp['berdoa'];
                }
                ?>
                </td>
              </tr>
          </table>
          </td>
        </tr>

        <tr class="justify-content-center">
          <td scope="row" width="5%"><?php echo $i++; ?></td>
          <td colspan="2">Kesaksian menerima panggilan melayani Tuhan<br>
          <table class="table table-bordered table-sm">
            <tr>
                <td>
                <?php 
                if($data_pp['kesaksian_menerima_panggilan_melayani_tuhan'] == null || $data_pp['kesaksian_menerima_panggilan_melayani_tuhan'] == "") {
                  echo "-";
                } else {
                  echo $data_pp['kesaksian_menerima_panggilan_melayani_tuhan'];
                }
                ?>
                </td>
              </tr>
          </table>
          </td>
        </tr>

        <tr class="justify-content-center">
          <td scope="row" width="5%"><?php echo $i++; ?></td>
          <td colspan="2">Dasar Panggilan<br>
          <table class="table table-bordered table-sm">
            <tr>
                <td>
                <?php 
                if($data_pp['dasar_panggilan'] == null || $data_pp['dasar_panggilan'] == "") {
                  echo "-";
                } else {
                  echo $data_pp['dasar_panggilan'];
                }
                ?>
                </td>
              </tr>
          </table>
          </td>
        </tr>

        <tr class="justify-content-center">
          <td scope="row" width="5%"><?php echo $i++; ?></td>
          <td colspan="2">Pembimbing<br>
          <table class="table table-bordered table-sm">
            <tr>
                <td>
                <?php 
                if($data_pp['pembimbing'] == null || $data_pp['pembimbing'] == "") {
                  echo "-";
                } else {
                  echo $data_pp['pembimbing'];
                }
                ?>
                </td>
              </tr>
          </table>
          </td>
        </tr>

        <tr class="justify-content-center">
          <td scope="row" width="5%"><?php echo $i++; ?></td>
          <td colspan="2">Tempat Pelayanan<br>
          <table class="table table-bordered table-sm">
            <tr>
                <td>
                <?php 
                if($data_pp['tempat_pelayanan'] == null || $data_pp['tempat_pelayanan'] == "") {
                  echo "-";
                } else {
                  echo $data_pp['tempat_pelayanan'];
                }
                ?>
                </td>
              </tr>
          </table>
          </td>
        </tr>

        <tr class="justify-content-center">
          <td scope="row" width="5%"><?php echo $i++; ?></td>
          <td colspan="2">Penceritaan Injil<br>
          <table class="table table-bordered table-sm">
            <tr>
                <td>
                <?php 
                if($data_pp['penceritaan_injil'] == null || $data_pp['penceritaan_injil'] == "") {
                  echo "-";
                } else {
                  echo $data_pp['penceritaan_injil'];
                }
                ?>
                </td>
              </tr>
          </table>
          </td>
        </tr>

      </tbody>
    </table>
    <a type="button" class="btn btn-info btn-edit-done" href="<?= site_url();?>data_pp"><i class="far fa-edit"></i> Edit</a>
  </section>




  <!-- data rekomendasi -->
  <section class="bg-light row padding-data flag none">
    <table class="table table-striped">
      <pre>
      <?php //print_r($data_lain);?>
      </pre>
      <thead>
        <tr>
          <th colspan="3" scope="col" class="text-center">No. Pendaftaran : <?=$data_identitas['no_pendaftaran'];?></th>
        </tr>
      </thead>
      <tbody>
      
        <tr>
          <th colspan="3">Diutus secara resmi</th>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Oleh Gembala Sidang?</td>
          <td>: <?=$data_rekomendasi['utusan_majelis_jemaat'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Oleh Sponsor?</td>
          <td>: <?=$data_rekomendasi['utusan_majelis_klasis'];?></td>
        </tr>
        <!--<tr>-->
        <!--  <td scope="row"><?php echo $i++; ?></td>-->
        <!--  <td>Oleh Saudara Sendiri?</td>-->
        <!--  <td>: <?=$data_rekomendasi['utusan_majelis_sinode'];?></td>-->
        <!--</tr>-->
        <!--<tr>-->
        <!--  <td scope="row"><?php echo $i++; ?></td>-->
        <!--  <td>Oleh yayasan P.I</td>-->
        <!--  <td>: <?=$data_rekomendasi['utusan_yayasan_pi'];?></td>-->
        <!--</tr>-->
        <!--<tr>-->
        <!--  <td scope="row"><?php echo $i++; ?></td>-->
        <!--  <td>Oleh persekutuan doa</td>-->
        <!--  <td>: <?=$data_rekomendasi['utusan_persekutuan_doa'];?></td>-->
        <!--</tr>-->
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Oleh saudara pribadi</td>
          <td>: <?=$data_rekomendasi['utusan_pribadi'];?></td>
        </tr>


        <tr>
          <th colspan="3">Pemberi Rekomendasi</th>
        </tr>
        <tr>
          <td colspan="3">Orang 1</td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Nama</td>
          <td>: <?=$data_rekomendasi['nama_pemberi_rekomendasi1'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Umur</td>
          <td>: <?=$data_rekomendasi['umur_pemberi_rekomendasi1'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Pekerjaan</td>
          <td>: <?=$data_rekomendasi['pekerjaan_pemberi_rekomendasi1'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Jabatan Gerejawi</td>
          <td>: <?=$data_rekomendasi['jabatan_gerejawi_permberi_rekomendasi1'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Alamat</td>
          <td>: <?=$data_rekomendasi['alamat_pemberi_rekomendasi1'];?></td>
        </tr>

        <tr>
          <td colspan="3">Orang 2</td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Nama</td>
          <td>: <?=$data_rekomendasi['nama_pemberi_rekomendasi2'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Umur</td>
          <td>: <?=$data_rekomendasi['umur_pemberi_rekomendasi2'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Pekerjaan</td>
          <td>: <?=$data_rekomendasi['pekerjaan_pemberi_rekomendasi2'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Jabatan Gerejawi</td>
          <td>: <?=$data_rekomendasi['jabatan_gerejawi_permberi_rekomendasi2'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Alamat</td>
          <td>: <?=$data_rekomendasi['alamat_pemberi_rekomendasi2'];?></td>
        </tr>




        <tr>
          <th colspan="3">Yang membiayai saudara selama mengikuti pendidikan di STTE</th>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Nama Badan</td>
          <td>: <?=$data_rekomendasi['nama_badan'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Nama Pimpinan</td>
          <td>: <?=$data_rekomendasi['nama_pimpinan'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Umur</td>
          <td>: <?=$data_rekomendasi['umur'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Pekerjaan</td>
          <td>: <?=$data_rekomendasi['pekerjaan'];?></td>
        </tr>
        <tr>
          <td scope="row"><?php echo $i++; ?></td>
          <td>Alamat</td>
          <td>: <?=$data_rekomendasi['alamat'];?></td>
        </tr>

      </tbody>
    </table>
    <a type="button" class="btn btn-info btn-edit-done" href="<?= site_url();?>data_rekomendasi"><i class="far fa-edit"></i> Edit</a>
  </section>




  <!-- data_tekad_dan_keputusan -->
  <section class="bg-light row padding-data flag none">
    <pre>
    <?php //print_r($data_keluarga);?>
    </pre>
    <table class="table table-striped">
      <thead>
        <tr>
          <th colspan="3" scope="col" class="text-center">No. Pendaftaran : <?=$data_tekad_dan_keputusan['no_pendaftaran'];?></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th colspan="3">Bersedia mentaati peraturan-peraturan STTE</th>
        </tr>
        <tr>
          <td scope="row" width="5%"><?php echo $i++; ?></td>
          <td width="35%">Bersedia mentaati peraturan-peraturan STTE</td>
          <td width="60%">: <?=$data_tekad_dan_keputusan['ketersediaan_mentaati_peraturan'];?></td>
        </tr>
        <tr>
          <td scope="row" width="5%"><?php echo $i++; ?></td>
          <td width="35%">Penjelasan</td>
          <td width="60%">: <?=$data_tekad_dan_keputusan['penjelasan_ketersediaan_mentaati_peraturan'];?></td>
        </tr>

        <tr>
          <th colspan="3">Bersedia menerima pengarahan/bimbingan dan teguran dari pimpinan/staf dan mahasiswa STTE</th>
        </tr>
        <tr>
          <td scope="row" width="5%"><?php echo $i++; ?></td>
          <td width="35%">Bersedia menerima pengarahan/bimbingan dan teguran</td>
          <td width="60%">: <?=$data_tekad_dan_keputusan['ketersediaan_menerima_pengarahan'];?></td>
        </tr>
        <tr>
          <td scope="row" width="5%"><?php echo $i++; ?></td>
          <td width="35%">Penjelasan</td>
          <td width="60%">: <?=$data_tekad_dan_keputusan['penjelasan_ketersediaan_menerima_pengarahan'];?></td>
        </tr>

        <tr>
          <th colspan="3">Bersedia menerima dan menjalankan praktek pelayanan yang diatur dan ditetapkan oleh staf STTE</th>
        </tr>
        <tr>
          <td scope="row" width="5%"><?php echo $i++; ?></td>
          <td width="35%">Bersedia menerima dan menjalankan praktek pelayanan</td>
          <td width="60%">: <?=$data_tekad_dan_keputusan['ketersediaan_menjalankan_praktek'];?></td>
        </tr>
        <tr>
          <td scope="row" width="5%"><?php echo $i++; ?></td>
          <td width="35%">Penjelasan</td>
          <td width="60%">: <?=$data_tekad_dan_keputusan['penjelasan_ketersediaan_menjalankan_praktek'];?></td>
        </tr>

      </tbody>
    </table>
    <a type="button" class="btn btn-info btn-edit-done" href="<?= site_url();?>data_tekad_dan_keputusan"><i class="far fa-edit"></i> Edit</a>
  </section>


  <!-- Data Lain-Lain -->
  <section class="bg-light row padding-data flag none">
    <table class="table table-striped">
      <pre>
      <?php //print_r($data_rohani);?>
      </pre>
      <thead>
        <tr>
          <th colspan="3" scope="col" class="text-center">No. Pendaftaran : <?=$data_identitas['no_pendaftaran'];?></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td scope="row" width="5%"><?php echo $i++; ?></td>
          <td width="35%">Merupakan seorang perokok</td>
          <td width="60%">: <?=$data_lain['perokok'];?></td>
        </tr>

        <?php if ($data_lain['perokok'] == "Tidak"): ?>
          <tr>
            <td scope="row" width="5%"><?php echo $i++; ?></td>
            <td width="35%">Berhenti merokok dari</td>
            <td width="60%">: <?=$data_lain['waktu_berhenti_merokok'];?></td>
          </tr>

          <tr>
            <td scope="row" width="5%"><?php echo $i++; ?></td>
            <td width="35%">Alasan berhenti merokok</td>
            <td width="60%">: <?=$data_lain['alasan_berhenti_merokok'];?></td>
          </tr>
        <?php endif; ?>


        <tr>
          <td scope="row" width="5%"><?php echo $i++; ?></td>
          <td width="35%">Pernah melamar menjadi mahasiswa theologia di suatu lembaga pendidikan theologia selain STTE</td>
          <td width="60%">: <?=$data_lain['pernah_menjadi_mhs_theologia'];?></td>
        </tr>

        <?php if ($data_lain['pernah_menjadi_mhs_theologia'] == "Ya"): ?>
        <tr>
            <td scope="row" width="5%"><?php echo $i++; ?></td>
            <td width="35%">Alasan tidak meneruskan untuk menyelasaikan studi saudara di pendidikan theologia</td>
            <td width="60%">: <?=$data_lain['alasan_pernah_menjadi_mhs_theologia'];?></td>
          </tr>
        <?php endif; ?>

      </tbody>
    </table>
    <a type="button" class="btn btn-info btn-edit-done" href="<?= site_url();?>data_lain"><i class="far fa-edit"></i> Edit</a>
  </section>


  
  <!-- <section class="bg-light row padding-data flag none">
    <table class="table table-striped">
      <pre>
      <?php //print_r($data_upload);?>
      </pre>
      <thead>
        <tr>
          <th colspan="2" scope="col" class="text-center">No. Pendaftaran : <?=$data_identitas['no_pendaftaran'];?></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td width="25%">File Foto</td>
          <td width="75%">: 
            <?php if($data_upload['file_foto']) : ?>
              <a href="<?=base_url()?>assets/userfiles/<?=$_SESSION['no_pendaftaran']?>/<?=$data_upload['file_foto']?>" target="_blank"> <?=$data_upload['file_foto']?></a>
            <?php endif;?>
          </td>
        </tr>
        <tr>
          <td>File KTP</td>
          <td>: 
            <?php if($data_upload['file_ktp']) : ?>
              <a href="<?=base_url()?>assets/userfiles/<?=$_SESSION['no_pendaftaran']?>/<?=$data_upload['file_ktp']?>" target="_blank"> <?=$data_upload['file_ktp']?></a>
            <?php endif;?>
          </td>
        </tr>
        <tr>
          <td>File Ijazah</td>
          <td>: 
            <?php if($data_upload['file_ijazah']) : ?>
              <a href="<?=base_url()?>assets/userfiles/<?=$_SESSION['no_pendaftaran']?>/<?=$data_upload['file_ijazah']?>" target="_blank"> <?=$data_upload['file_ijazah']?></a>
            <?php endif;?>
          </td>
        </tr>
        <tr>
          <td>File KK</td>
          <td>: 
            <?php if($data_upload['file_kk']) : ?>
              <a href="<?=base_url()?>assets/userfiles/<?=$_SESSION['no_pendaftaran']?>/<?=$data_upload['file_kk']?>" target="_blank"> <?=$data_upload['file_kk']?></a>
            <?php endif;?>
          </td>
        </tr>
      </tbody>
    </table>
    <a type="button" class="btn btn-info btn-edit-done" href="<?= site_url();?>data_upload"><i class="far fa-edit"></i> Edit</a>
  </section> -->

</section>
</main>


<script>
  document.getElementById("data_identitas").addEventListener("click", function(event){
    event.preventDefault();
    const e = document.querySelectorAll('section.flag');
    const id = document.querySelectorAll('nav div a');
    // console.log(e[0]);
    if(e[0].classList.contains('none')){
      e[0].classList.toggle('none');
      this.classList.toggle('active');
      e.forEach((element) => {
        if (element != e[0]){
          element.classList.add('none');
          id[1].classList.remove('active');
          id[2].classList.remove('active');
          id[3].classList.remove('active');
          id[4].classList.remove('active');
          // id[5].classList.remove('active');
          // id[6].classList.remove('active');
        }
      });
    }
  }); 


  document.getElementById("data_pp").addEventListener("click", function(event){
    event.preventDefault();
    const e = document.querySelectorAll('section.flag');
    const id = document.querySelectorAll('nav div a');
    // console.log(id[6]);
    if(e[1].classList.contains('none')){
      e[1].classList.toggle('none');
      this.classList.toggle('active');
      e.forEach((element) => {
        if (element != e[1]){
          element.classList.add('none');
          // this.classList.remove('active');
          id[0].classList.remove('active');
          id[2].classList.remove('active');
          id[3].classList.remove('active');
          id[4].classList.remove('active');
          // id[5].classList.remove('active');
          // id[6].classList.remove('active');
        }
      });
    }
  });


  document.getElementById("data_rekomendasi").addEventListener("click", function(event){
    event.preventDefault();
    const e = document.querySelectorAll('section.flag');
    const id = document.querySelectorAll('nav div a');
    // console.log(e[1]);
    if(e[2].classList.contains('none')){
      e[2].classList.toggle('none');
      this.classList.toggle('active');
      e.forEach((element) => {
        if (element != e[2]){
          element.classList.add('none');
          // this.classList.remove('active');
          id[0].classList.remove('active');
          id[1].classList.remove('active');
          id[3].classList.remove('active');
          id[4].classList.remove('active');
          // id[5].classList.remove('active');
          // id[6].classList.remove('active');
        }
      });
    }
  });

  document.getElementById("data_tekad_dan_keputusan").addEventListener("click", function(event){
    event.preventDefault();
    const e = document.querySelectorAll('section.flag');
    const id = document.querySelectorAll('nav div a');
    // console.log(e[1]);
    if(e[3].classList.contains('none')){
      e[3].classList.toggle('none');
      this.classList.toggle('active');
      e.forEach((element) => {
        if (element != e[3]){
          element.classList.add('none');
          // this.classList.remove('active');
          id[0].classList.remove('active');
          id[1].classList.remove('active');
          id[2].classList.remove('active');
          id[4].classList.remove('active');
          // id[5].classList.remove('active');
          // id[6].classList.remove('active');
        }
      });
    }
    
  });  

  document.getElementById("data_lain").addEventListener("click", function(event){
    event.preventDefault();
    const e = document.querySelectorAll('section.flag');
    const id = document.querySelectorAll('nav div a');
    // console.log(e[1]);
    if(e[4].classList.contains('none')){
      e[4].classList.toggle('none');
      this.classList.toggle('active');
      e.forEach((element) => {
        if (element != e[4]){
          element.classList.add('none');
          // this.classList.remove('active');
          id[0].classList.remove('active');
          id[1].classList.remove('active');
          id[2].classList.remove('active');
          id[3].classList.remove('active');
          // id[5].classList.remove('active');
          // id[6].classList.remove('active');
        }
      });
    }
  }); 


  // document.getElementById("data_upload").addEventListener("click", function(event){
  //   event.preventDefault();
  //   const e = document.querySelectorAll('section.flag');
  //   const id = document.querySelectorAll('nav div a');
  //   // console.log(e[1]);
  //   if(e[5].classList.contains('none')){
  //     e[5].classList.toggle('none');
  //     this.classList.toggle('active');
  //     e.forEach((element) => {
  //       if (element != e[5]){
  //         element.classList.add('none');
  //         // this.classList.remove('active');
  //         id[0].classList.remove('active');
  //         id[1].classList.remove('active');
  //         id[2].classList.remove('active');
  //         id[3].classList.remove('active');
  //         id[4].classList.remove('active');
  //         id[6].classList.remove('active');
  //       }
  //     });
  //   }
  // }); 
</script>