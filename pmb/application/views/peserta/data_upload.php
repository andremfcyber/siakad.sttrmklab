<section class="col-sm-9">
      <form action="<?php echo site_url();?>savedata/saveDataUpload" id="my-form" name="my-form" method="POST" enctype="multipart/form-data">
        <table class="table table-striped table-bordered table-sm table-rounded">
          <tbody>
            <tr>
              <th colspan = "2">F. UPLOAD FILE (Ukuran Maksimal 2Mb)</th>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="file_foto" class="col-form-label-sm">Upload Foto</label></td>
              <td class="col-8">
                <input type="file" class="form-control-sm" id="file_foto" name="file_foto"><br/>
                <?php if($data_upload['file_foto']) : ?>
                  <small><a href="<?=base_url()?>assets/userfiles/<?=$_SESSION['no_pendaftaran']?>/<?=$data_upload['file_foto']?>" target="_blank"> &nbsp&nbsp<?=$data_upload['file_foto']?></a></small>
                <?php endif;?>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="file_ktp" class="col-form-label-sm">Upload Foto KTP</label></td>
              <td class="col-8">
                <input type="file" class="form-control-sm" id="file_ktp" name="file_ktp"><br/>
                <?php if($data_upload['file_ktp']) : ?>
                  <small><a href="<?=base_url()?>assets/userfiles/<?=$_SESSION['no_pendaftaran']?>/<?=$data_upload['file_ktp']?>" target="_blank"> &nbsp&nbsp<?=$data_upload['file_ktp']?></a></small>
                <?php endif;?>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="file_ijazah" class="col-form-label-sm">Upload File Ijazah</label></td>
              <td class="col-8">
                <input type="file" class="form-control-sm" id="file_ijazah" name="file_ijazah"><br/>
                <?php if($data_upload['file_ijazah']) : ?>
                  <small><a href="<?=base_url()?>assets/userfiles/<?=$_SESSION['no_pendaftaran']?>/<?=$data_upload['file_ijazah']?>" target="_blank"> &nbsp&nbsp<?=$data_upload['file_ijazah']?></a></small>
                <?php endif;?>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="file_kk" class="col-form-label-sm">Upload File KK</label></td>
              <td class="col-8">
                <input type="file" class="form-control-sm" id="file_kk" name="file_kk"><br/>
                <?php if($data_upload['file_kk']) : ?>
                  <small><a href="<?=base_url()?>assets/userfiles/<?=$_SESSION['no_pendaftaran']?>/<?=$data_upload['file_kk']?>" target="_blank"> &nbsp&nbsp<?=$data_upload['file_kk']?></a></small>
                <?php endif;?>
              </td>
            </tr>
              <td colspan="2">
                <center>
                  <?php
                    // if(!empty($data_identitas)&&!empty($data_pp)&&!empty($data_rekomendasi)&&!empty($data_tekad_dan_keputusan)&&!empty($data_lain)&&!empty($data_upload)){
                    if(!empty($data_identitas)&&!empty($data_pp)&&!empty($data_rekomendasi)&&!empty($data_tekad_dan_keputusan)&&!empty($data_lain)){
                  ?>
                    <input type="text" name="done" value="1" hidden>
                    <button type="submit" class="btn btn-sm btn-success"><i class="far fa-file-alt"></i> Save dan Lihat Semua Data</a>
                  <?php
                    }else{
                  ?>
                    <a href="<?php echo site_url();?>data_lain" class="btn btn-sm btn-info"><i class="fas fa-arrow-alt-circle-left"></i> Kembali</a>
                    <button type="submit" id="simpan" class="btn btn-sm btn-primary"><i class="far fa-save"></i> Simpan</button>
                  <?php
                    }
                  ?>
                </center>
              </td>
            </tr>
          </tbody>
        </table>
        <input type="text" id="noPendaftaran" value="<?=@$this->session->userdata('no_pendaftaran');?>" hidden>
      </form>
    </section>
  </div>
</main>

<script>
document.addEventListener('DOMContentLoaded', getData);

function getData(){
  let noPendaftaran = document.getElementById('noPendaftaran').value;
  let params = "no_pendaftaran="+noPendaftaran;

  let xhr = new XMLHttpRequest();
  xhr.open('POST','<?=site_url();?>getdata/get_data_upload',true);
  xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
  xhr.onload = function(){
    if(this.status == 200){
      // console.log(this.responseText);
      if(this.responseText){
        let r = JSON.parse(this.responseText);

        // Check utusan_stat ada/engga, trus input disabled nya on atau off berdasarkan itu
        document.querySelector('#file_foto').value = r.file_foto;
        document.querySelector('#file_ktp').value = r.file_ktp;
        document.querySelector('#file_ijazah').value = r.file_ijazah;
        document.querySelector('#file_kk').value = r.file_kk;
      }
    }
  }
  xhr.send(params);
}  

</script>