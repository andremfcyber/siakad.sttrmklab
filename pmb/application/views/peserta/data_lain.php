    <section class="col-sm-9">
      <form action="<?php echo site_url();?>savedata/saveDataLain" id="my-form" name="my-form" method="POST">
        <table class="table table-striped table-bordered table-sm table-rounded">
          <tbody>
            <tr>
              <th colspan = "2">E. LAIN-LAIN</th>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="perokok" class="col-form-label-sm">Apakah saudara seorang perokok sampai sekarang?</label></td>
              <td class="col-8">
                <select name="perokok" id="perokok" class="custom-select custom-select-sm col-3" onchange="assignTo(this.value);" required>
                  <option value="">-Pilih-</option>
                  <option value="Ya">Ya</option>
                  <option value="Tidak">Tidak</option>
                </select>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="waktu_berhenti_merokok" class="col-form-label-sm">Sejak kapan berhenti merokok?</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-12" id="waktu_berhenti_merokok" name="waktu_berhenti_merokok" disabled>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="alasan_berhenti_merokok" class="col-form-label-sm">Mengapa berhenti merokok?</label></td>
              <td class="col-8">
                <textarea class="form-control form-control-sm col-12" id="alasan_berhenti_merokok" name="alasan_berhenti_merokok" rows="3" disabled></textarea>
              </td>
            </tr>
            
            <tr class="row">
              <td class="col-4"><label for="pernah_menjadi_mhs_theologia" class="col-form-label-sm">Apakah Saudara pernah melamar menjadi mahasiswa theologia di suatu lembaga pendidikan theologia
selain STTRMK?</label></td>
              <td class="col-8">
                <select name="pernah_menjadi_mhs_theologia" id="pernah_menjadi_mhs_theologia" class="custom-select custom-select-sm col-3" onchange="assignTo2(this.value);" required>
                  <option value="">-Pilih-</option>
                  <option value="Ya">Ya</option>
                  <option value="Tidak">Tidak</option>
                </select>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="alasan_pernah_menjadi_mhs_theologia" class="col-form-label-sm">Jelaskan mengapa saudara tidak meneruskan untuk menyelasaikan studi
saudara di pendidikan theologia tersebut?</label></td>
              <td class="col-8">
                <textarea class="form-control form-control-sm col-12" id="alasan_pernah_menjadi_mhs_theologia" name="alasan_pernah_menjadi_mhs_theologia" rows="3"></textarea>
              </td>
            </tr>


            <tr>
              <td colspan="2">
                <center>
                  <?php
                    // if(!empty($data_identitas)&&!empty($data_pp)&&!empty($data_rekomendasi)&&!empty($data_tekad_dan_keputusan)&&!empty($data_lain)&&!empty($data_upload)){
                    if(!empty($data_identitas)&&!empty($data_pp)&&!empty($data_rekomendasi)&&!empty($data_tekad_dan_keputusan)&&!empty($data_lain)){
                  ?>
                    <input type="text" name="done" value="1" hidden>
                    <button type="submit" class="btn btn-sm btn-success"><i class="far fa-file-alt"></i> Save dan Lihat Semua Data</a>
                  <?php
                    }else{
                  ?>
                    <a href="<?php echo site_url();?>data_tekad_dan_keputusan" class="btn btn-sm btn-info"><i class="fas fa-arrow-alt-circle-left"></i> Kembali</a>
                    <button type="submit" id="simpan" class="btn btn-sm btn-primary"><i class="fas fa-arrow-alt-circle-right"></i> Simpan</button>
                  <?php
                    }
                  ?>
                </center>
              </td>
            </tr>
          </tbody>
        </table>
        <input type="text" id="noPendaftaran" value="<?=@$this->session->userdata('no_pendaftaran');?>" hidden>
      </form>
    </section>
  </div>
</main>

<script>
document.addEventListener('DOMContentLoaded', getData);

function getData(){
  let noPendaftaran = document.getElementById('noPendaftaran').value;
  let params = "no_pendaftaran="+noPendaftaran;

  let xhr = new XMLHttpRequest();
  xhr.open('POST','<?=site_url();?>getdata/get_data_lain',true);
  xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
  xhr.onload = function(){
    if(this.status == 200){
      // console.log(this.responseText);
      if(this.responseText){
        let r = JSON.parse(this.responseText);

        // Check perokok ada/engga, trus input disabled nya on atau off berdasarkan itu
        if(r.perokok == "Ya"){
          disabledOn('#waktu_berhenti_merokok');
          disabledOn('#alasan_berhenti_merokok');
        }

        if(r.pernah_menjadi_mhs_theologia == "Tidak"){
          disabledOn('#alasan_pernah_menjadi_mhs_theologia');
        }

        document.querySelector('#perokok').value = r.perokok;
        document.querySelector('#waktu_berhenti_merokok').value = r.waktu_berhenti_merokok;
        document.querySelector('#alasan_berhenti_merokok').value = r.alasan_berhenti_merokok;
        document.querySelector('#pernah_menjadi_mhs_theologia').value = r.pernah_menjadi_mhs_theologia;
        document.querySelector('#alasan_pernah_menjadi_mhs_theologia').value = r.alasan_pernah_menjadi_mhs_theologia;
      }
    }
  }
  xhr.send(params);
}

function assignTo(r){
  if(r == "Tidak" ){
    disabledOff(['#waktu_berhenti_merokok','#alasan_berhenti_merokok']);
  }else{
    disabledOn(['#waktu_berhenti_merokok','#alasan_berhenti_merokok']);
  }
}

function assignTo2(r){
  if(r == "Ya" ){
    disabledOff(['#alasan_pernah_menjadi_mhs_theologia']);
  }else{
    disabledOn(['#alasan_pernah_menjadi_mhs_theologia']);
  }
}

function disabledOn(kelas){
  //console.log(kelas);
  if(Array.isArray(kelas)){
    //console.log(kelas);
    kelas.forEach(function(kelas_single){
      const e = document.querySelector(kelas_single);
      e.setAttribute('disabled',true);
      e.value = "";
    });
  }else{
    const e = document.querySelector(kelas);
    e.setAttribute('disabled',true);
    e.value = "";
  }
}

function disabledOff(kelas){
  //console.log(kelas);
  if(Array.isArray(kelas)){
    //console.log(kelas);
    kelas.forEach(function(kelas_single){
      const e = document.querySelector(kelas_single);
      e.removeAttribute('disabled');
    });
  }else{
    const e = document.querySelector(kelas);
    e.removeAttribute('disabled');
  }
}
  

</script>