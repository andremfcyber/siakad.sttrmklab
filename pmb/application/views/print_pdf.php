<html>
  <head>
    <style>
      .border td{
        border: 1px solid black;
      }
      .border th{
        border: 1px solid black;
      }
      .coba{
        width:100%;
      }
      *{
        padding:5px;
        font-size:13.5px;
      }
      .page-break{
        page-break-before: always;
      }
      header {
        position: fixed;
        top: -50px;
        left: 0px;
        right: 0px;
        height: 70px;

        /** Extra personal styles **/

        /** Border Bottom **/
        border-bottom: 3px double black;
        /* background-image: linear-gradient(black, black);
        background-repeat: no-repeat;
        background-size: 100% 3px;
        background-position: left bottom 3px; */
      }
      body{
        margin-top: 30px;
      }
      td{
        vertical-align : top;
      }
      h5{
        font-weight:none;
        margin:0;
      }
      h2{
        margin-top:-10px;
        font-size:16px;
      }
      h1{
        font-size:18px;
      }
      .img-logo{
        margin-top:5px;
        width:10%;
        float:left;
      }
      .text-header{
        font-size:12px;
        text-align:right;
      }
      /* footer {
        position: fixed; 
        bottom: -60px; 
        left: 0px; 
        right: 0px;
        height: 50px; 

        background-color: #03a9f4;
        color: white;
        text-align: center;
        line-height: 35px;
      } */
      /* footer{ 
        position: fixed; 
        left: 0px; 
        bottom: -180px; 
        right: 0px; 
        height: 150px; 
        background-color: lightblue; 
      }
      footer:after { 
        content: counter(page, "."); 
      } */
      .border-on{
        border: 1px solid black;
      }
      .img-pasfoto{
        width: 125px;
        float:right
      }
      .surat{
        padding:45px;
      }
      .no-padding{
        padding: 0;
      }
      .no-margin{
        margin: 0;
      }
      .padding-li{
        padding: 0 0 0 20px;
      }
    </style>
  </head>
  <body>
    <header>
      <img src="<?php echo "/assets/app_setting_upload/img/".app_setting()['data_setting'][0]->logo ?>" class="img-logo">
      <p class="text-header" style="font-weight:bold;margin-top:10px; margin-bottom:-10px;"><?php echo app_setting()['data_setting'][0]->nama_pendek ?> &nbsp; <?php echo app_setting()['data_setting'][0]->nama_instansi ?></p>
      <p class="text-header" style="margin-top:0px; margin-bottom:-10px;"><?php echo app_setting()['data_setting'][0]->alamat_instansi ?></p>
      <p class="text-header" style="margin-top:0px; margin-bottom:-10px;">Website: <a href="<?php echo app_setting()['data_setting'][0]->website ?>"><?php echo app_setting()['data_setting'][0]->website ?></a></p>
      <p class="text-header" style="margin-top:0px; margin-bottom:-10px;">E-mail: <?php echo app_setting()['data_setting'][0]->email ?></p>
    </header>
    <main>
      No. Pendaftaran : <?=$data_identitas['no_pendaftaran'];?>
      <table style="width:100%;">
        <tbody>
          <tr>
            <th colspan="3" scope="col"><h2 style="text-align:center;">DATA PRIBADI</h2></th>
          </tr>
          <tr>
            <td scope="row" width="5%">1</td>
            <td width="35%">Nama Lengkap</td>
            <td width="60%">: <?=$data_identitas['nama'];?></td>
          </tr>
          <tr>
            <td scope="row">2</td>
            <td>Jenis Kelamin</td>
            <td>: <?php
            if($data_identitas['jk']=='P'){
              echo "Perempuan";  
            }else{
              echo "Laki-Laki";
            }
            ?></td>
          </tr>
          <tr>
            <td scope="row">3</td>
            <td>Program Studi Pilihan</td>
            <td>: <?=$data_identitas['prodi'];?></td>
          </tr>
          <tr>
            <td scope="row">4</td>
            <td>Tempat, Tanggal Lahir</td>
            <td>: <?=$data_identitas['tempat_lahir'];?>, <?= date('d M Y', strtotime($data_identitas['tgl_lahir']));?></td>
          </tr>
          <tr>
            <td scope="row">5</td>
            <td>Kewarganegaraan</td>
            <td>: <?=$data_identitas['kewarganegaraan'];?></td>
          </tr>
          <tr>
            <td scope="row">6</td>
            <td>Alamat Lengkap</td>
            <td>: <?=$data_identitas['alamat'];?>, Telp. <?=$data_identitas['tlp']?></td>
          </tr>
          <tr>
            <td scope="row">7</td>
            <td>Alamat Lengkap Saudara/Kenalan di Jabotabekban</td>
            <td>: <?=$data_identitas['alamat_saudara'];?>, Telp. <?=$data_identitas['tlp_saudara'];?></td>
          </tr>
          <tr>
            <td scope="row">8</td>
            <td>Pendidikan Formal Terakhir</td>
            <td>: <?=$data_identitas['pend_terakhir'];?></td>
          </tr>
          <tr>
            <td scope="row">9</td>
            <td>Nama Sekolah Terakhir</td>
            <td>: <?=$data_identitas['asal_sekolah'];?>, Lulus Tahun: <?=$data_identitas['thn_lulus'];?></td>
          </tr>
          <tr>
            <td scope="row">10</td>
            <td>Alamat Sekolah Terakhir</td>
            <td>: <?=$data_identitas['alamat_sekolah'];?></td>
          </tr>
          <tr>
            <td scope="row">11</td>
            <td>Terakhir Tinggal di Rumah</td>
            <td>: <?=$data_identitas['tinggal_dirumah'];?></td>
          </tr>
          <tr>
            <td scope="row">12</td>
            <td>Status Marital</td>
            <td>: <?=$data_identitas['marital_stat'];?></td>
          </tr>
          <tr>
            <td scope="row">13</td>
            <td>Nama dan Tempat Tanggal Lahir Pasangan</td>
            <td>: <?=@$data_identitas['nama_pasangan'];?>, TTL: <?=@$data_identitas['tempat_lahir_pasangan'];?>, <?= date('d M Y', strtotime($data_identitas['tgl_lahir_pasangan']));?></td>
          </tr>
          <tr>
            <td scope="row">14</td>
            <td>Jumlah Anak</td>
            <td>: Laki-Laki <?=$data_identitas['jml_anak_L'];?> orang, Perempuan <?=$data_identitas['jml_anak_P'];?> orang</td>
          </tr>
          <tr>
            <td scope="row">15</td>
            <td>Anggota Gereja</td>
            <td>: <?=$data_identitas['gereja_anggota'];?></td>
          </tr>
          <tr>
            <td scope="row">16</td>
            <td>Alamat Gereja</td>
            <td>: <?=$data_identitas['alamat_gereja_anggota'];?></td>
          </tr>
          <tr>
            <td scope="row">17</td>
            <td>Beribadah dan Melayani di Gereja yang Sama</td>
            <td>: <?=$data_identitas['ibadah_melayani_gereja'] == 1 ? "Ya" : "Tidak";?></td>
          </tr>
          <tr>
            <td scope="row">18</td>
            <td>Nama Gereja Tempat Beribadah</td>
            <td>: <?=$data_identitas['gereja_ibadah'];?></td>
          </tr>
          <tr>
            <td scope="row">19</td>
            <td>Alamat Gereja Tempat Beribadah</td>
            <td>: <?=$data_identitas['alamat_gereja_ibadah'];?></td>
          </tr>
          <tr>
            <td scope="row">20</td>
            <td>Pelayanan Utama di Gereja</td>
            <td>: <?=$data_identitas['gereja_pelayanan'];?></td>
          </tr>
          <tr>
            <td scope="row">21</td>
            <td>Pekerjaan</td>
            <td>: <?=$data_identitas['pekerjaan'];?></td>
          </tr>
          <tr>
            <td scope="row">22</td>
            <td>Alamat Perusahaan</td>
            <td>: <?=$data_identitas['alamat_perusahaan'];?></td>
          </tr>
          <tr>
            <td scope="row">23</td>
            <td>Perusahaan Begerak di Bidang</td>
            <td>: <?=$data_identitas['bidang_perusahaan'];?></td>
          </tr>
          <tr>
            <td scope="row">24</td>
            <td>Minat Utama</td>
            <td>: <?=$data_identitas['minat'];?></td>
          </tr>
          <tr>
            <td scope="row">25</td>
            <td>Kemampuan Penguasaan Bahasa Inggris</td>
            <td>: 
              <?php 
                echo $data_identitas['inggris'];
                if($data_identitas['inggris'] == "A"){
                  echo " (Sangat Baik)";
                }elseif($data_identitas['inggris'] == "B"){
                  echo " (Baik)";
                }elseif($data_identitas['inggris'] == "C"){
                  echo " (Kurang Menguasai)";
                }else{
                  echo " (Tidak Bisa Sama Sekali)";
                }
              ?>
            </td>
          </tr>
          <tr>
            <td scope="row">26</td>
            <td>Kemampuan Bahasa Asing</td>
            <td>: <?=$data_identitas['bahasa_asing'];?></td>
          </tr>
          <tr>
            <td scope="row">27</td>
            <td>Bakat dan Talenta</td>
            <td>: <?=$data_identitas['bakat'];?></td>
          </tr>
        </tbody>
      </table>
  
      <table class="page-break" style="width:100%;">
        <tbody>
          <tr>
            <th colspan="3" scope="col"><h2 style="text-align:center;">DATA KELUARGA</h2></th>
          </tr>
          <tr>
            <td scope="row" width="5%">28</td>
            <td width="35%">Nama Ayah</td>
            <td width="60%">: <?=$data_keluarga['nama_ayah'];?> (<?=$data_keluarga['umur_ayah'];?> th)</td>
          </tr>
          <tr>
            <td scope="row" width="5%"></td>
            <td width="35%">Nama Ibu</td>
            <td width="60%">: <?=$data_keluarga['nama_ibu'];?> (<?=$data_keluarga['umur_ibu'];?> th)</td>
          </tr>
          <tr>
            <td scope="row">29</td>
            <td>Alamat Orang Tua</td>
            <td>: <?=$data_keluarga['alamat_ortu'];?>, Tlp. <?=$data_keluarga['tlp_ortu'];?></td>
          </tr>
          <tr>
            <td scope="row">30</td>
            <td>Pendidikan Terakhir Ayah</td>
            <td>: <?=$data_keluarga['pend_ayah'];?></td>
          </tr>
          <tr>
            <td scope="row">31</td>
            <td>Pendidikan Terakhir Ibu</td>
            <td>: <?=$data_keluarga['pend_ibu'];?></td>
          </tr>
          <tr>
            <td scope="row">32</td>
            <td>Pekerjaan Ayah</td>
            <td>: <?=$data_keluarga['pekerjaan_ayah'];?></td>
          </tr>
          <tr>
            <td scope="row"></td>
            <td>Pekerjaan Ibu</td>
            <td>: <?=$data_keluarga['pekerjaan_ibu'];?></td>
          </tr>
          <tr>
            <td scope="row">33</td>
            <td>Agama Ayah</td>
            <td>: <?=$data_keluarga['agama_ayah'];?></td>
          </tr>
          <tr>
            <td scope="row"></td>
            <td>Agama Ibu</td>
            <td>: <?=$data_keluarga['agama_ibu'];?></td>
          </tr>
          <tr>
            <td scope="row">34</td>
            <td>Nama Wali</td>
            <td>: <?=$data_keluarga['nama_wali'];?></td>
          </tr>
          <tr>
            <td scope="row">35</td>
            <td>Alamat Wali</td>
            <td>: <?=$data_keluarga['alamat_wali'];?></td>
          </tr>
          <tr>
            <td scope="row">36</td>
            <td>Pekerjaan Wali</td>
            <td>: <?=$data_keluarga['pekerjaan_wali'];?></td>
          </tr>
          <tr>
            <td scope="row"></td>
            <td>Hubungan dengan Wali</td>
            <td>: <?=$data_keluarga['hubungan_wali'];?></td>
          </tr>
          <tr>
            <td scope="row">37</td>
            <td>Agama Wali</td>
            <td>: <?=$data_keluarga['agama_wali'];?></td>
          </tr>
          <tr>
            <td scope="row">38</td>
            <td colspan="2">Nama Saudara Kandung (termasuk Anda)
            <table class="border" cellspacing="0">
                <tr>
                  <th width="5%">No.</th>
                  <th width="35%">Nama Saudara Kandung</th>
                  <th width="10%">Kelamin</th>
                  <th width="10%">Umur</th>
                  <th width="30%">Pendidikan Terakhir</th>
                  <th width="10%">Agama</th>
                </tr>
                <?php
                  $namaSK = explode("|", $data_keluarga['nama_sk']);
                  $jkSK = explode("|", $data_keluarga['jk_sk']);
                  $umurSK = explode("|", $data_keluarga['umur_sk']);
                  $pendSK = explode("|", $data_keluarga['pend_sk']);
                  $agamaSK = explode("|", $data_keluarga['agama_sk']);
                  $length = count($namaSK);
                  // echo $length;
                  for($i=0; $i<$length; $i++){
                ?>
                <tr>
                  <td><?=$i+1;?></td>
                  <td><?=$namaSK[$i];?></td>
                  <td><?=$jkSK[$i];?></td>
                  <td><?=$umurSK[$i];?></td>
                  <td><?=$pendSK[$i];?></td>
                  <td><?=$agamaSK[$i];?></td>
                </tr>
                <?php
                  }
                  ?>
            </table>
            </td>
          </tr>
          <tr>
            <td scope="row" width="5%">39</td>
            <td width="40%">Apakah Anda Berpacaran/Bertunangan?</td>
            <td width="55%">: <?= ($data_keluarga['thn_pcr']) ? "Ya, tahun ".$data_keluarga['thn_pcr'] : "Tidak" ;?></td>
          </tr>
          <tr>
            <td scope="row">40</td>
            <td>Nama Pacar/Tunangan</td>
            <td>: <?=$data_keluarga['nama_pcr'];?></td>
          </tr>
          <tr>
            <td scope="row">41</td>
            <td>Agama Pacar/Tunangan</td>
            <td>: <?=$data_keluarga['agama_pcr'];?></td>
          </tr>
          <tr>
            <td scope="row">42</td>
            <td>Hubungan dengan Pacar/Tunangan Saat ini</td>
            <td>: <?=$data_keluarga['hub_pcr'];?></td>
          </tr>
          <tr>
            <td scope="row">43</td>
            <td>Tanggapan Pacar/Tunangan/Pasangan atas Keputusan Anda masuk <?php echo app_setting()['data_setting'][0]->nama_pendek ?> &nbsp; <?php echo app_setting()['data_setting'][0]->nama_instansi ?></td>
            <td>: <?=$data_keluarga['tanggapan_pcr'];?></td>
          </tr>
          <tr>
            <td scope="row">44</td>
            <td>Apakah Anda Pernah Bercerai?</td>
            <td>: <?= ($data_keluarga['thn_div']) ? "Ya, tahun ".$data_keluarga['thn_div'] : "Tidak" ;?></td>
          </tr>
          <tr>
            <td scope="row">45</td>
            <td>Alasan Bercerai</td>
            <td>: <?=$data_keluarga['alasan_div'];?></td>
          </tr>
          <tr>
            <td scope="row">46</td>
            <td>Apakah Anda Menjadi Penanggung Ekonomi Keluarga?</td>
            <td>: <?= ($data_keluarga['stat_tp'] == 1) ? "Ya" : "Tidak" ;?></td>
          </tr>
          <tr>
            <td scope="row">47</td>
            <td>Jika Masuk <?php echo app_setting()['data_setting'][0]->nama_pendek ?> &nbsp; <?php echo app_setting()['data_setting'][0]->nama_instansi ?>, Siapakah yang Akan Menjadi Penanggung Ekonomi Keluarga?</td>
            <td>: <?=$data_keluarga['nama_tp'];?></td>
          </tr>
        </tbody>
      </table>
    
      <table class="coba page-break" style="width:100%;">
        <tbody>
          <tr>
            <th colspan="3" scope="col"><h2 style="text-align:center;">DATA PENDIDIKAN DAN RIWAYAT PEKERJAAN</h2></th>
          </tr>
          <tr>
            <td scope="row" width="5%">48</td>
            <td colspan="2" width="95%">Riwayat Pendidikan<br>
            <em>Pendidikan Formal</em>
            <table class="border" cellspacing="0">
                <tr>
                  <th width="5%">No.</th>
                  <th width="35%">Pendidikan Formal</th>
                  <th width="45%">Nama Sekloah</th>
                  <th width="15%">Tahun</th>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Taman Kanak-Kanak</td>
                  <td><?=$data_pp['tk'];?></td>
                  <td><?=$data_pp['tk_thn'];?></td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>SD</td>
                  <td><?=$data_pp['sd'];?></td>
                  <td><?=$data_pp['sd_thn'];?></td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>SMP</td>
                  <td><?=$data_pp['smp'];?></td>
                  <td><?=$data_pp['smp_thn'];?></td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>SMA/SMK/STM</td>
                  <td><?=$data_pp['sma'];?></td>
                  <td><?=$data_pp['sma_thn'];?></td>
                </tr>
                <tr>
                  <td>5</td>
                  <td><?=$data_pp['lain1_nm'];?></td>
                  <td><?=$data_pp['lain1'];?></td>
                  <td><?=$data_pp['lain1_thn'];?></td>
                </tr>
                <tr>
                  <td>6</td>
                  <td><?=$data_pp['lain2_nm'];?></td>
                  <td><?=$data_pp['lain2'];?></td>
                  <td><?=$data_pp['lain2_thn'];?></td>
                </tr>
            </table><br>
            <em>Pendidikan Non-Formal</em>          
            <table class="border" cellspacing="0">
                <tr>
                  <th width="5%">No.</th>
                  <th width="35%">Pendidikan Formal</th>
                  <th width="45%">Nama Sekloah</th>
                  <th width="15%">Tahun</th>
                </tr>
                <?php
                  $nonfNM = explode("|", $data_pp['nonf_nm']);
                  $nonfLembaga = explode("|", $data_pp['nonf_lembaga']);
                  $nonfThn = explode("|", $data_pp['nonf_thn']);
                  $length = count($nonfNM);
                  // echo $length;
                  for($i=0; $i<$length; $i++){
                ?>
                <tr>
                  <td><?=$i+1;?></td>
                  <td><?=$nonfNM[$i];?></td>
                  <td><?=$nonfLembaga[$i];?></td>
                  <td><?=$nonfThn[$i];?></td>
                </tr>
                <?php
                  }
                  ?>
            </table>
            </td>
          </tr>
          <tr>
            <td scope="row" width="5%">49</td>
            <td colspan="2">Riwayat Pekerjaan<br>
            <table class="border" cellspacing="0">
                <tr>
                  <th width="5%">No.</th>
                  <th width="30%">Jabatan</th>
                  <th width="50%">Perusahaan dan Bidangnya</th>
                  <th width="15%">Tahun</th>
                </tr>
                <?php
                  $nmKerja = explode("|", $data_pp['nm_kerja']);
                  $jabKerja = explode("|", $data_pp['jab_kerja']);
                  $thnKerja = explode("|", $data_pp['thn_kerja']);
                  $length = count($nonfNM);
                  // echo $length;
                  for($i=0; $i<$length; $i++){
                ?>
                <tr>
                  <td><?=$i+1;?></td>
                  <td><?=$jabKerja[$i];?></td>
                  <td><?=$nmKerja[$i];?></td>
                  <td><?=$thnKerja[$i];?></td>
                </tr>
                <?php
                  }
                  ?>
            </table>
            </td>
          </tr>
        </tbody>
      </table>
      
      
      <table class="page-break" style="width:100%;">
        <tbody>
          <tr>
            <th colspan="3" scope="col"><h2 style="text-align:center;">DATA KEROHANIAN DAN KARAKTER</h2></th>
          </tr>
          <tr>
            <td scope="row" width="5%">50</td>
            <td width="35%">Sejak Tahun erapa Anda Menjadi Anggota Gereja?</td>
            <td width="60%">: <?=$data_rohani['anggota_thn'];?></td>
          </tr>
          <tr>
            <td scope="row">51</td>
            <td>Sudah Dibaptis Anak?</td>
            <td>: <?= ($data_rohani['baptisA_thn']) ? "Ya, tahun ".$data_rohani['baptisA_thn'] : "Tidak" ;?></td>
          </tr>
          <tr>
            <td scope="row">52</td>
            <td>Sudah Disidi?</td>
            <td>: <?= ($data_rohani['sidi_thn']) ? "Ya, tahun ".$data_rohani['sidi_thn'] : "Tidak" ;?></td>
          </tr>
          <tr>
            <td scope="row">53</td>
            <td>Sudah Dibaptis Dewasa?</td>
            <td>: <?= ($data_rohani['baptisD_thn']) ? "Ya, tahun ".$data_rohani['baptisD_thn'] : "Tidak" ;?></td>
          </tr>
          <tr>
            <td scope="row">54</td>
            <td>Baptisan Dewasa/Sidi Dilayankan Oleh</td>
            <td>: <?= $data_rohani['sidi_by'];?></td>
          </tr>
          <tr>
            <td scope="row" width="5%">55</td>
            <td colspan="2">Pengalaman Pelayanan<br>
            <table class="border" cellspacing="0">
                <tr>
                  <th width="5%">No.</th>
                  <th width="30%">Jenis Pelayanan</th>
                  <th width="50%">Tempat Pelayanan</th>
                  <th width="15%">Tahun</th>
                </tr>
                <?php
                  $jenisP = explode("|", $data_rohani['jenis_pelayanan']);
                  $tempatP = explode("|", $data_rohani['tempat_pelayanan']);
                  $thnP = explode("|", $data_rohani['thn_pelayanan']);
                  $length = count($jenisP);
                  // echo $length;
                  for($i=0; $i<$length; $i++){
                ?>
                <tr>
                  <td><?=$i+1;?></td>
                  <td><?=$jenisP[$i];?></td>
                  <td><?=$tempatP[$i];?></td>
                  <td><?=$thnP[$i];?></td>
                </tr>
                <?php
                  }
                  ?>
            </table>
            </td>
          </tr>
          <tr>
            <td scope="row">56</td>
            <td>Hambatan Utama dalam Pertumbuhan Rohani</td>
            <td>: <?= $data_rohani['hambatan_pertumbuhan'];?></td>
          </tr>
          <tr>
            <td scope="row">57</td>
            <td>Hambatan Utama dalam Pelayanan</td>
            <td>: <?= $data_rohani['hambatan_pelayanan'];?></td>
          </tr>
          <tr>
            <td scope="row">58</td>
            <td>Hambatan Utama untuk Masuk <?php echo app_setting()['data_setting'][0]->nama_pendek ?> &nbsp; <?php echo app_setting()['data_setting'][0]->nama_instansi ?></td>
            <td>: <?= $data_rohani['hambatan_masuk'];?></td>
          </tr>
          <tr>
            <td scope="row">59</td>
            <td>Masalah yang Akan Menghambat Kelancaran Studi</td>
            <td>: <?= $data_rohani['masalah'];?></td>
          </tr>
          <tr>
            <td scope="row">60</td>
            <td>Deskripsi Masalah No.59</td>
            <td>: <?= $data_rohani['masalah_detail'];?></td>
          </tr>
          <tr>
            <td scope="row">61</td>
            <td>Rutin Membaca Alkitab?</td>
            <td>: <?= ($data_rohani['jml_baca']) ? "Ya, Sudah Selesai ".$data_rohani['baptisA_thn']." kali" : "Tidak" ;?></td>
          </tr>
          <tr>
            <td scope="row">62</td>
            <td>Rutin Membrikan Persembahan Perpuluhan?</td>
            <td>: <?= ($data_rohani['persembahan_stat'] == 1) ? "Ya" : "Tidak" ;?></td>
          </tr>
          <tr>
            <td scope="row">63</td>
            <td>Alasan No.62</td>
            <td>: <?=$data_rohani['persembahan_alasan'];?></td>
          </tr>
          <tr>
            <td scope="row">64</td>
            <td>Senang Meminjam Uang atau Barang Pada Orang Lain?</td>
            <td>: <?= ($data_rohani['pinjam_stat'] == 1) ? "Ya" : "Tidak" ;?></td>
          </tr>
          <tr>
            <td scope="row">65</td>
            <td>Sedang Menanggung Beban Hutang?</td>
            <td>: <?= ($data_rohani['hutang']) ? "Ya, Sebesar ".$data_rohani['hutang'] : "Tidak" ;?></td>
          </tr>
          <tr>
            <td scope="row">66</td>
            <td>Buku yang Pernah Dibaca dan Sangat Mempengaruhi Hidup</td>
            <td>: <?=$data_rohani['buku'];?></td>
          </tr>
          <tr>
            <td scope="row">67</td>
            <td>Hamba Tuhan yang Paling Berpengaruh dalam Pertumbuhan Rohani</td>
            <td>: <?=$data_rohani['ht_pengaruh'];?></td>
          </tr>
          <tr>
            <td scope="row">68</td>
            <td>Hobby dan Kegemaran</td>
            <td>: <?=$data_rohani['hobby'];?></td>
          </tr>
          <tr>
            <td scope="row">69</td>
            <td>Jenis Olahraga yang Digemari dan Dikuasai</td>
            <td>: <?=$data_rohani['olahraga'];?></td>
          </tr>
        </tbody>
      </table>

      <table class="page-break" style="width:100%;">
        <tbody>
          <tr>
            <th colspan="3" scope="col"><h2 style="text-align:center;">DATA LAIN-LAIN</h2></th>
          </tr>
          <tr>
            <td scope="row">70</td>
            <td>Harapan untuk <?php echo app_setting()['data_setting'][0]->nama_pendek ?> &nbsp; <?php echo app_setting()['data_setting'][0]->nama_instansi ?></td>
            <td>: <?=$data_lain['harapan'];?></td>
          </tr>
          <tr>
            <td scope="row">71</td>
            <td>Mendapatkan Informasi tentang <?php echo app_setting()['data_setting'][0]->nama_pendek ?> &nbsp; <?php echo app_setting()['data_setting'][0]->nama_instansi ?> dari</td>
            <td>: <?=$data_lain['darimana'];?></td>
          </tr>
          <tr>
            <td scope="row">72</td>
            <td>Pernah Mendaftar di <?php echo app_setting()['data_setting'][0]->nama_pendek ?> &nbsp; <?php echo app_setting()['data_setting'][0]->nama_instansi ?> Sebelumnya?</td>
            <td>: <?= ($data_lain['thn_daftar_bef']) ? "Ya, Tahun ".$data_lain['thn_daftar_bef'] : "Tidak" ;?></td>          
          </tr>
          <tr>
            <td scope="row">73</td>
            <td>Merupakan Utusan Gereja</td>
            <td>: <?= ($data_lain['utusan_stat'] == 1) ? "Ya": "Tidak" ;?></td>
          </tr>
          <tr>
            <td scope="row">74</td>
            <td>Nama Gereja Pengutus</td>
            <td>: <?= $data_lain['nama_utus'];?></td>
          </tr>
          <tr>
            <td scope="row">75</td>
            <td>Alamat Gereja Pengutus</td>
            <td>: <?= $data_lain['alamat_utus']." Telp. ".$data_lain['tlp_utus'];?></td>
          </tr>
          <tr>
            <td scope="row">76</td>
            <td>Terikat Janji dengan Gerja Pengutus</td>
            <td>: <?= ($data_lain['utus_stat'] == 1) ? "Ya": "Tidak" ;?></td>
          </tr>
          <tr>
            <td scope="row">77</td>
            <td>Di <?php echo app_setting()['data_setting'][0]->nama_pendek ?> &nbsp; <?php echo app_setting()['data_setting'][0]->nama_instansi ?> Menanggung Biaya Kuliah Sendiri?</td>
            <td>: <?= ($data_lain['mandiri_stat'] == 1) ? "Ya": "Tidak" ;?></td>
          </tr>
          <tr>
            <td scope="row">78</td>
            <td>Orang yang Mendukung Biaya Kuliah</td>
            <td>: <?= $data_lain['nama_biaya'];?></td>
          </tr>
          <tr>
            <td scope="row">79</td>
            <td>Alamat Rumah Penanggung Biaya</td>
            <td>: <?= $data_lain['alamat_biaya']." Tlp. ".$data_lain['tlp_biaya'];?></td>
          </tr>
        </tbody>
      </table>
      <table class="coba">
        <tbody>
          <tr>
            <td colspan="3" height="20"> </td>   
          </tr>
          <tr>
            <td colspan="2"  width="65%">Diisi di ___________________</td>
            <td rowspan="4" width="35%">
              <img src="<?php echo base_url('assets/img/Pasfoto-01.png');?>" class="img-pasfoto">
            </td>
          </tr>
          <tr>
            <td colspan="2" >Tanggal ___________________</td>
          </tr>
          <tr>
            <td colspan="2"  height="50">Saya yang telah mengisi formulir ini dengan sebenar-benarnya</td>
          </tr>
          <tr>
            <td colspan="2" >(__________________________)</td>
          </tr>
        </tbody>
      </table>
      
      <div class="page-break surat">
        <h1 style="text-align:center;">SURAT PERNYATAAN PENANGGUNG</h1>
        <br>
        <br>
        Yang bertanda tangan di bawah ini:
        <table style="width:80%">
          <tr>
            <td width="30%" style="padding-left:-6px;">Nama</td>
            <td width="70%">: ___________________________________________</td>
          </tr>
          <tr>
            <td width="30%" style="padding-left:-6px;">Alamat</td>
            <td width="70%">: ___________________________________________</td>
          </tr>
        </table>
        <br>
        Dengan ini menyatakan kesediaan menanggung Saudara: ___________________________ secara finansial untuk semua biaya studi dan kesehatan selama belajar di <?php echo app_setting()['data_setting'][0]->nama_pendek ?> &nbsp; <?php echo app_setting()['data_setting'][0]->nama_instansi ?>.
        <br>
        <br>
        Surat Pernyataan Penangung ini dibuat sebagai bukti dan ditujukam kepada <?php echo app_setting()['data_setting'][0]->nama_pendek ?> &nbsp; <?php echo app_setting()['data_setting'][0]->nama_instansi ?> Jakarta.
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <h5 style="text-align:right;">Diisi di _____________________</h5>
        <h5 style="text-align:right;">Tanggal _____________________</h5>
        <br>
        <br>
        <br>
        <br>
        <table width="100%">
          <tr>
            <td width="50%" style="text-align:center">
              Tanda tangan Tertanggung,
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              (________________________)
            </td>
            <td width="50%" style="text-align:center">
              Tanda tangan Penanggung,
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              (________________________)
            </td>
          </tr>
          <tr>
            <td colspan="2" width="50%" style="text-align:center">
              <br>
              <br>    
              Diterima Oleh: <?php echo app_setting()['data_setting'][0]->nama_pendek ?> &nbsp; <?php echo app_setting()['data_setting'][0]->nama_instansi ?> Jakarta
              <br>
              Tanggal: ___________________
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              (________________________)
            </td>
          </tr>
        </table>
      </div>

      <div class="page-break surat">
        <h1 style="text-align:center;">SURAT KETERANGAN</h1>
        <br>
        <br>
        Dengan ini Panitia Penerimaan Mahasiswa Baru <?php echo app_setting()['data_setting'][0]->nama_pendek ?> &nbsp; <?php echo app_setting()['data_setting'][0]->nama_instansi ?> telah menerima yang bertanda tangan di bawah ini:
        <table style="width:80%">
          <tr>
            <td width="30%" style="padding-left:-6px;">Nama</td>
            <td width="70%">: ___________________________________________</td>
          </tr>
          <tr>
            <td width="30%" style="padding-left:-6px;">T.T.L</td>
            <td width="70%">: ___________________________________________</td>
          </tr>
          <tr>
            <td width="30%" style="padding-left:-6px;">Jurusan</td>
            <td width="70%">: ___________________________________________</td>
          </tr>
        </table>
        <br>
        Surat keterangan ini dibuat sebagai bukti dan ditujukan kepada orangtua/wali yang bersangkutan bahwa anak tersebut telah diterima oleh pihak <?php echo app_setting()['data_setting'][0]->nama_pendek ?> &nbsp; <?php echo app_setting()['data_setting'][0]->nama_instansi ?> Jakarta.
        <br>
        <br>
        Bersamaan dengan surat ini kami menyertakan beberapa lampiran untuk melengkapi.
        <ul class="no-padding no-margin">
          <li class="padding-li">Lembar Pembayaran Administrasi</li>
          <li class="padding-li">Lembar Pernyataan Penanggung</li>
          <li class="padding-li">Lembar Balasan kepada PPMB</li>
        </ul>
        <br>
        <br>
        <br>
        <h5 style="text-align:right;">Diisi di _____________________</h5>
        <h5 style="text-align:right;">Tanggal _____________________</h5>
        <br>
        <br>
        <br>
        <br>
        <table width="100%">
          <tr>
            <td width="50%" style="text-align:center">
              Tanda tangan Tertanggung,
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              (________________________)
            </td>
            <td width="50%" style="text-align:center">
              Tanda tangan Penanggung,
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              (________________________)
            </td>
          </tr>
          <tr>
            <td colspan="2" width="50%" style="text-align:center">
              <br>
              <br>    
              Diterima Oleh: <?php echo app_setting()['data_setting'][0]->nama_pendek ?> &nbsp; <?php echo app_setting()['data_setting'][0]->nama_instansi ?> Jakarta
              <br>
              Tanggal: ___________________
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              (________________________)
            </td>
          </tr>
        </table>
      </div>

    </main>
    <footer>
    </footer>
  </body>
</html>