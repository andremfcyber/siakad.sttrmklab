<?php
class Admin extends CI_Controller{
  /*
   * Programmer : Fitria Wahyuni.S.Pd
   * Keterangan : Controller khusus untuk Admin's page
   */

  public function showAll(){
    $logged_in = $this->session->userdata('logged_in');
    $level = $this->session->userdata('level');

    if(!empty($logged_in) && $level=='admin'){
      $data = "";
      $this->load->view('templates/header_admin');
      $this->load->view('admin/show',$data);
      $this->load->view('templates/footer');
    }else{
      redirect('login','refresh');
    }
  }

  public function getUsersAll(){
    $logged_in = $this->session->userdata('logged_in');
    $level = $this->session->userdata('level');

    if(!empty($logged_in) && $level=='admin'){
      $q = $this->model_data->users_all();
      $no = 0;
      $data = array();
      $recordsTotal = 0;
      if($q!=null){
        foreach ($q->result() as $r){
          $no++;
          $row = array();
          $row['no'] = $no;
          $row['no_pendaftaran'] = $r->no_pendaftaran;
          $row['nama'] = $r->nama;
          $row['prodi'] = substr($r->nama_prodi,3);
          $row['jenjang'] = substr($r->nama_prodi,0,2);
          $row['ket'] = '<center><a href="getUserDetail/'.$r->no_pendaftaran.'"><i class="far fa-file-alt" style="text-align: center;"></i></a>&nbsp&nbsp&nbsp&nbsp&nbsp<a href="deleteUser/'.$r->no_pendaftaran.'" onclick="return confirm(\'Anda yakin ingin menghapus data ini?\')"><i class="fas fa-trash" style="text-align: center; color:red;"></i></a></center>';

          $data[] = $row;
        }
        $recordsTotal = $q->num_rows();      
      }
      $output = array(
        "recordsTotal" => $recordsTotal,
        // "recordsFiltered" => $q->num_rows(),
        "data" => $data
      );
      echo json_encode($output);
      // echo json_encode($q);
      // Jangan Pake yang langsung soalnya so nomer gabisa dimanipulasi langsung di Javascript nya.. mungkin ada caranya tapi udah nyari susah nemu :(
    }else{
      redirect('login', 'refresh');
    }
  }

  public function getUserDetail($np){
    $logged_in = $this->session->userdata('logged_in');
    $level = $this->session->userdata('level');

    if(!empty($logged_in) && $level=='admin'){
      $data['np'] = $np;
      $data['result'] = $this->model_data->data_all($np);
      $data['upload'] = $this->model_data->get_data_upload($np);
      // echo "<pre>";
      // print_r($data);
      // echo "</pre>";exit;
      $this->load->view('templates/header_admin');
      $this->load->view('admin/show_detail',$data);
      $this->load->view('templates/footer');
    }else{
      redirect('login', 'refresh');
    }
  }

  public function deleteUser($np){
    $logged_in = $this->session->userdata('logged_in');
    $level = $this->session->userdata('level');

    if(!empty($logged_in) && $level=='admin'){
      $path = './assets/userfiles/'.$np;
      if (is_dir($path)) {
        $this->load->helper("file");
        delete_files($path);
        // unlink($path);
        rmdir($path);
      }
      $this->model_user->delete($np);
      redirect('admin/showAll');
    }else{
      redirect('login', 'refresh');
    }
  }  

  public function print_pdf($noPendaftaran){
    // $logged_in = $this->session->userdata('logged_in');
    // $noPendaftaran = $this->session->userdata('no_pendaftaran');

    //if(!empty($logged_in)){
    $logged_in = $this->session->userdata('logged_in');
    $level = $this->session->userdata('level');

    if(!empty($logged_in) && $level=='admin'){
      $data['data_identitas'] = $this->model_data->data_identitas($noPendaftaran);
      $data['data_keluarga'] = $this->model_data->data_keluarga($noPendaftaran);
      $data['data_pp'] = $this->model_data->data_pp($noPendaftaran);
      $data['data_rohani'] = $this->model_data->data_rohani($noPendaftaran);
      $data['data_lain'] = $this->model_data->data_lain($noPendaftaran);

      $this->load->library('pdf');
      $this->pdf->set_option('isRemoteEnabled', TRUE);
      $this->pdf->setPaper('A4', 'potrait');
    
      $this->pdf->filename = "Formulir_Pendaftaran.pdf";
      $this->pdf->load_view('print_pdf', $data);
    // }else{
    //   redirect('login','refresh');
    // }
    }else{
      redirect('login', 'refresh');
    }

  }

  public function adminList(){
    // $q = $this->model_data->admins_all();
    // $no = 0;
    // $data = array();
    // $recordsTotal = 0;
    // if($q!=null){
    //   foreach ($q->result() as $r){
    //     $no++;
    //     $row = array();
    //     $row['no'] = $no;
    //     $row['username'] = $r->username;
    //     $row['email'] = $r->email;
    //     $row['ket'] = '<button data-toggle="modal" data-target="#modalEdit" data-username="'.$r->username.'"><i class="far fa-edit" style=" display: block; text-align: center;"></i></button>';
  
    //     $data[] = $row;
    //   }
    //   $recordsTotal = $q->num_rows();
    // }
    $logged_in = $this->session->userdata('logged_in');
    $level = $this->session->userdata('level');

    if(!empty($logged_in) && $level=='admin'){
      $data['data'] = "";
      $this->load->view('templates/header_admin');
      $this->load->view('admin/admin_list',$data);
      $this->load->view('templates/footer');
    }else{
      redirect('login','refresh');
    }
  }

  public function getAdminsAll(){
    $logged_in = $this->session->userdata('logged_in');
    $level = $this->session->userdata('level');

    if(!empty($logged_in) && $level=='admin'){
      $q = $this->model_data->admins_all();
      $no = 0;
      $data = array();
      $recordsTotal = 0;
      if($q!=null){
        foreach ($q->result() as $r){
          $no++;
          $row = array();
          $row['no'] = $no;
          $row['username'] = $r->username;
          $row['email'] = $r->email;
          $row['ket'] = '<button data-toggle="modal" data-target="#modalEdit" data-username="'.$r->username.'" id="btn-edit'.$r->username.'" onclick="coba(\''.$r->username.'\');" class="btn btn-primary"><i class="far fa-edit" style=" display: block; text-align: center;"></i></button> &nbsp <a href="deleteAdmin/'.$r->username.'" type="button" onclick="return confirm(\'Anda yakin ingin menghapus admin ini?\')" class="btn"><i class="fas fa-trash text-danger" style=" display: block; text-align: center;"></i></button>';
    
          $data[] = $row;
        }
        $recordsTotal = $q->num_rows();
      }

      $output = array(
        "recordsTotal" => $recordsTotal,
        // "recordsFiltered" => $q->num_rows(),
        "data" => $data
      );
      echo json_encode($output);
      // echo json_encode($q);
      // Jangan Pake yang langsung soalnya so nomer gabisa dimanipulasi langsung di Javascript nya.. mungkin ada caranya tapi udah nyari susah nemu :(
    }else{
      redirect('login', 'refresh');
    }
  }

  public function getAdminDetail($username){
    $logged_in = $this->session->userdata('logged_in');
    $level = $this->session->userdata('level');

    if(!empty($logged_in) && $level=='admin'){
      $dt = $this->model_data->admin_detail($username);
      if($dt != null){
        $res = array(
          "rc" => "00",
          "data" => $dt
        );

        echo json_encode($res);
      }else{
        $res = array(
          "rc" => "01",
          "data" => $dt
        );

        echo json_encode($res);
      }
    }else{
      redirect('login', 'refresh');
    }
  }

  public function cekUsername(){
    $id['username'] = $this->input->post('username');
    $q = $this->db->get_where('pmb.admins', $id);
    $row = $q->num_rows();
    if($row>0){
      echo "ada";
    }else{
      echo "kosong";
    }
  }

  public function cekPassword(){
    $id['username'] = $this->input->post('usernameEd');
    $password = md5($this->input->post('passwordEd'));
    $data = $this->db->get_where('pmb.admins', $id)->result();
    // print_r($data[0]->password);
    if($data[0]->password == $password){
      echo "00";
    }else{
      echo "Maaf, Password yang anda masukkan salah..";
      // echo $data->password;
    }
  }

  public function saveData(){
    $logged_in = $this->session->userdata('logged_in');
    $level = $this->session->userdata('level');

    if(!empty($logged_in) && $level=='admin'){
      if($this->input->post('username')!=null){
        $id['username'] = str_replace(' ', '', $this->input->post('username'));
      }else{
        $id['username'] = str_replace(' ', '', $this->input->post('usernameEd'));
      }
      $q = $this->db->get_where('pmb.admins', $id);
      $row = $q->num_rows();
      if($row>0){
        $dt['username'] = $id['username'];
        $dt['email'] = $this->input->post('emailEd');
        //$dt['password'] = md5($this->input->post('passwordEd'));
        $dt['update_time'] = date('Y-m-d H:i:s');
        $this->db->update("pmb.admins",$dt,$id);
        echo "Data Sukses diUpdate";
      }else{
        $dt['username'] = $id['username'];
        $dt['email'] = $this->input->post('email');
        $dt['password'] = md5($this->input->post('password'));
        $dt['insert_time'] = date('Y-m-d H:i:s');
        $this->db->insert("pmb.admins",$dt);
        echo "Data Sukses diSimpan";
      }
    }else{
      redirect('login', 'refresh');
    }
  }

  public function deleteAdmin($username){
    // echo $username;
    $logged_in = $this->session->userdata('logged_in');
    $level = $this->session->userdata('level');

    if(!empty($logged_in) && $level=='admin'){
      $q = $this->db->get_where("pmb.admins", array("username" => $username));
      $row = $q->num_rows();
      if($row>0){
        $this->db->delete("pmb.admins", array("username" => $username));
      }
      redirect("admin/adminList", "refresh");
    }else{
      redirect('login', 'refresh');
    }
  }
}
?>