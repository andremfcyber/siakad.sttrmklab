<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>PMB ver.1</title>
  <!-- CSS BOOTSTRAP -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
  <!-- CSS CUSTOM -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/custom2.css');?>">

  <!-- FONT AWESOME -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

  <!-- DATA TABLE CSS -->
  <link rel="stylesheet" href="<?php echo base_url('assets/DataTables/datatables.css');?>">

  <!-- JQUERY UNTUK DROPDOWN SIDEBAR -->
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/DataTables/datatables.js');?>"></script>
  <style>
    .header-adm {
      background-color: <?php echo app_setting()['data_setting'][0]->warna_header_1 ?> !important;
    }
/*
    .menu-set {
      background-image: url(<?php echo "/assets/app_setting_upload/img/".app_setting()['data_setting'][0]->bg_admin ?>) !important;
    }*/

    .menu-set {
        background-image: url(https://siakad.sttrmk.ac.id/pmb/assets/sfddds.jpg);

    }
    .header-adm {
        background-image: url(https://siakad.sttrmk.ac.id/pmb/assets/sfddds.jpg)!important;
        background-size: cover;
} 
  </style>
</head>
<body>
  <!-- NAV FOR MENU SIDEBAR -->
  <nav class="navbar navbar-expand-sm flex-column no-padding float-left" id="sidebar" style="height: 100vh">
    <button class="navbar-toggler btn-expand-custom" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="toogler-icon-custom"></span>
    </button>
    <div class="collapse navbar-collapse menu-set shadowing padding-25 style-overflow" id="navbarNav">
      <ul class="list-unstyled components" style="width:100%">
      <li class="icon-u">
            <img src="<?php echo "/assets/app_setting_upload/img/".app_setting()['data_setting'][0]->logo ?>" alt="user" width="100%">
        </li>
        <br>
        <li class="text-white text-center">
          <p class="font-10"><?php echo app_setting()['data_setting'][0]->nama_pendek ?> &nbsp; <?php echo app_setting()['data_setting'][0]->nama_instansi ?></p>
        </li>
        <hr>
          <a href="<?= site_url();?>admin/showAll"><h6 style="color:white;text-align:center;">Admin's Page</h6></a>
        <hr>
        <li class="active">
        <div class="dropdown">
          <button type="button" class="btn link-adm dropdown-toggle" style="padding-left: 0px" data-toggle="dropdown">
            <i class="fas fa-user"></i>&nbsp&nbsp&nbspLIST PENDAFTAR
          </button>
          <div class="dropdown-menu" style="background: #320f49;">
            <?php if(count(prodi_list()['prodi_list']) != 0): ?>
              <?php for ($i=0; $i < count(prodi_list()['prodi_list']); $i++): ?>
                <a class="dropdown-item"  style="color: white; font-size : 11px;" class="link-adm" href="<?= site_url();?>admin/showAll?kd_prodi=<?php echo prodi_list()['prodi_list'][$i]->kd_prodi ?>"><?php echo prodi_list()['prodi_list'][$i]->prodi ?></a>
              <?php endfor; ?>
            <?php else: ?>
              <a class="dropdown-item"  style="color: white; font-size : 11px;" class="link-adm">Tidak Ada Prodi</a>
            <?php endif; ?>
          </div>
        </div>
            <!-- <a href="<?= site_url();?>admin/showAll" class="link-adm"><i class="fas fa-user"></i> &nbsp&nbsp&nbspLIST PENDAFTAR</a> -->
        </li>
        <hr>
        <li class="active">
            <a href="<?= site_url();?>admin/adminList" class="link-adm"><i class="fas fa-user-lock"></i> &nbsp&nbsp&nbspLIST ADMIN</a>
        </li>
        
        <hr>
        <!-- <li>
            <a href="#Mariana" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle text-white">Mariana Bay Sands</a>
            <ul class="collapse list-unstyled text-fav" id="Mariana">
                <li>
                    <a href="#">ArtScience Museum</a>
                </li>
                <li>
                    <a href="#">Mariana Bay Sands Skypark</a>
                </li>
                <li>
                    <a href="#">Double Helix Bridge</a>
                </li>
            </ul>
        </li>
        <hr> -->
      </ul>
    </div>
	</nav>
  
