<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>PMB ver.1</title>
  <!-- CSS BOOTSTRAP -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
  <!-- CSS CUSTOM -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css');?>">
  <!-- FONT AWESOME -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  <style>
    .h-peserta {
      background-image: url(<?php echo "/assets/app_setting_upload/img/".app_setting()['data_setting'][0]->bg_login ?>) !important;
    }

    .navbar-custom {
      /*background-image: url(<?php echo "/assets/app_setting_upload/img/".app_setting()['data_setting'][0]->bg_login ?>) !important;*/
      background-image: url('https://siakad.sttrmk.ac.id/pmb/assets/perpuss.jpg')!important;
    }

    .h-pesertas{ 
     background-image: url('https://siakad.sttrmk.ac.id/pmb/assets/perpuss.jpg')!important;
      width: 100%;
      color: white;
      padding: 10px;
      text-align: center;
      background-position: bottom left;
      background-size: cover;
    }
  </style>
</head>
<body>
  <header class="h-pesertas">
    <!-- <img src="<?php echo "/assets/app_setting_upload/img/".app_setting()['data_setting'][0]->logo ?>" class="logo-header"> -->
  <!--   <p class="text-white copyright-header"><?php echo app_setting()['data_setting'][0]->nama_pendek ?> &nbsp;<?php echo app_setting()['data_setting'][0]->nama_instansi ?></p> -->

        <img src="https://siakad.sttrmk.ac.id/pmb/assets/loggo.png" class="logo-header">
        <h5 class="nama-kampus-login"> 
          SEKOLAH TINGGI THEOLOGIA  <br> Rumah Murid Kristus 
        </h5>

  </header>
  <main class="container">
  <h2 class="loginTitle">Formulir Pendaftaran</h2>
  <div class="row padding-tb-30">
    <section class = "col-sm-3 ">
      <nav class="navbar navbar-expand-sm navbar-custom shadowing " id="sidebar">
        <button class="navbar-toggler btn-expand-custom" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="toogler-icon-custom"></span>
        </button>
        <div class="collapse navbar-collapse style-overflow" id="navbarNav">
          <ul class="list-unstyled components ul-custom">
            <li>
                <img src="<?php echo base_url('assets/img/user.jpg');?>" alt="user">
            </li>
            <br>
            <li>
                <h5 class="text-white text-center">Welcome <?=$_SESSION['username'];?></h5>
            </li>
            <hr>
            <li class="active">
                <a href="<?= site_url();?>home/logout">Logout</a>
            </li>
            <hr>
            <!-- <li>
                <a href="#Mariana" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle text-white">Mariana Bay Sands</a>
                <ul class="collapse list-unstyled text-fav" id="Mariana">
                    <li>
                        <a href="#">ArtScience Museum</a>
                    </li>
                    <li>
                        <a href="#">Mariana Bay Sands Skypark</a>
                    </li>
                    <li>
                        <a href="#">Double Helix Bridge</a>
                    </li>
                </ul>
            </li>
            <hr> -->
          </ul>
        </div>
      </nav>
    </section>