<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>PMB ver.1</title>
  <!-- CSS BOOTSTRAP -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
  <!-- CSS CUSTOM -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css');?>">
  <!-- FONT AWESOME -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  <style>
    
    .containerLoginIcon, .bg-login {
      /*background-image: url(<?php echo "/assets/app_setting_upload/img/".app_setting()['data_setting'][0]->bg_login ?>) !important;*/
      background-image: url(https://siakad.sttrmk.ac.id/pmb/assets/sfddds.jpg);
    }

    .title-login {
      text-align: center;
      color: white;
      margin-top: 30px;
      font-size: 16px;
    }

    .nama-kampus-login {
      color: white;
      text-align: center;
      margin-top: 100px;
      font-size: 15px;
      margin-top: 140px;
    }

    .containerLoginIcon img {
      margin-top: -80px;
    }

  </style>
</head>
<body class="bg-login" >
  <!-- <header class="h-peserta clearfix">
    <img src="<?= base_url();?>assets/img/logo1.png" class="logo-header">
    <p class="text-white copyright-header">SEKOLAH TINGGI THEOLOGIA INJILI ARASTAMAR (SETIA) JAKARTA</p>
  </header> -->
  
  <?php if($this->session->flashdata('user_registered')):?>
    <p class="alert alert-success text-center"><?= $this->session->flashdata('user_registered'); ?></p>
  <?php endif;?>

  <?php if($this->session->flashdata('login_failed')):?>
    <p class="alert alert-danger text-center"><?= $this->session->flashdata('login_failed'); ?></p>
  <?php endif;?>

  <?php if($this->session->flashdata('user_loggedout')):?>
    <p class="alert alert-danger text-center"><?= $this->session->flashdata('user_loggedout'); ?></p>
  <?php endif;?>
  
  <main class="containerLogin">
  <div class="containerLoginIcon">
        <h4 class="title-login">Formulir Pendaftaran Online</h4>
        <img src="<?php echo "/assets/app_setting_upload/img/".app_setting()['data_setting'][0]->logo ?>" class="imgLoginLogo">
        <h5 class="nama-kampus-login"><?php echo app_setting()['data_setting'][0]->nama_pendek ?> &nbsp; 
								<?php echo app_setting()['data_setting'][0]->nama_instansi ?></h5><br>
    </div>
    <section class="row justify-content-center login">
      <div class="login-text">
        <h3>Member's Log in</h3>
        <p>Isi Username dan Password Anda</p>
      </div>
      <form action="<?=site_url();?>home/login" class="col-sm-10" method="POST">
        <div class="input-group input-group-sm mb-2 mr-sm-2">
          <div class="input-group-prepend">
            <div class="input-group-text"><i class="fas fa-user"></i></div>
          </div>
          <input type="text" class="form-control" id="username" name="username" placeholder="Username" required autofocus>
        </div>
        
        <div class="input-group input-group-sm mb-2 mr-sm-2">
          <div class="input-group-prepend">
            <div class="input-group-text"><i class="fas fa-unlock-alt"></i></div>
          </div>
          <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
        </div>
        <br>
        <button type="submit" class="btn btn-login btn-sm" >Log in</button>
        <p class="text-center margin-t-10">
          Don't have an account? <a href="<?=site_url();?>register" class="linkRegister">Register Here</a>
        </p>
      </form>
      <div class="login-text">
        <p class="text-center">Jika anda lupa password/mengalami masalah silahkan hubungi admin Akademik/PPMB</p>
      </div>
    </section>
  </main>
</body>
</html>
