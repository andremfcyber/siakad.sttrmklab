<?php
  class Home extends CI_Controller
  {
    /*
   * Programmer : Fitria Wahyuni.S.Pd
   * Keterangan : Controller utama dalam perpindahan halaman, login, rgister, dan pencetakan laporan. dapat diakses oleh peserta
   */
    public function index($page='login'){
      // phpinfo();
			if($page=='login' || $page=='register'){
				$data['title'] = ucfirst($page);
				$this->load->view('users/'.$page, $data); //ini file di folder view si pages (view/pages)
				
			}else{
        //$data['title'] = ucfirst($page);
        // print_r($this->session->userdata());exit;
        $logged_in = $this->session->userdata('logged_in');
        $level = $this->session->userdata('level');
        $noPendaftaran = $this->session->userdata('no_pendaftaran');
        if(!file_exists(APPPATH.'views/peserta/'.$page.'.php')){
          show_404();
        }
        if(!empty($logged_in) && $level=='peserta'){
          $data['data_identitas'] = $this->model_data->data_identitas($noPendaftaran);
          $data['data_keluarga'] = $this->model_data->data_keluarga($noPendaftaran);
          $data['data_pp'] = $this->model_data->data_pp($noPendaftaran);
          $data['data_rekomendasi'] = $this->model_data->data_rekomendasi($noPendaftaran);
          $data['data_tekad_dan_keputusan'] = $this->model_data->data_tekad_dan_keputusan($noPendaftaran);
          $data['data_rohani'] = $this->model_data->data_rohani($noPendaftaran);
          $data['data_lain'] = $this->model_data->data_lain($noPendaftaran);
          $data['data_upload'] = $this->model_data->data_upload($noPendaftaran);

          $this->load->view('templates/header');
          $this->load->view('peserta/'.$page, $data);
          $this->load->view('templates/footer');
        }else{
          redirect('login','refresh');
        }

			}
    }

    public function login(){
      $data['title'] = "Login";
      $this->form_validation->set_rules('username', 'Username', 'required');
      $this->form_validation->set_rules('password', 'Password', 'required');
      
      if($this->form_validation->run() === FALSE){
				$this->load->view('users/login', $data);
			}else{
				// Get Username
				$username = $this->input->post('username');
				// Get and encrypt the password
				$password = md5($this->input->post('password'));

				// Login user
				$id_pmbU = $this->model_user->login($username, $password);

				if($id_pmbU){
          $noPendaftaran = $this->model_user->getNoPendaftaran($id_pmbU);
					// Create session
					$user_data = array(
						'id_pmbU' => $id_pmbU,
						'username' => $username,
            'logged_in' => true,
            'no_pendaftaran' => $noPendaftaran,
            'level' => 'peserta'
					);

					$this->session->set_userdata($user_data);

          // echo "<pre>";
          // print_r($this->session->userdata());
          // echo "</pre>";
          // exit;

					// Set message
          $this->session->set_flashdata('user_loggedin','You are now logged in');
          $data['data_identitas'] = $this->model_data->data_identitas($noPendaftaran);
          $data['data_pp'] = $this->model_data->data_pp($noPendaftaran);
          $data['data_rekomendasi'] = $this->model_data->data_rekomendasi($noPendaftaran);
          $data['data_tekad_dan_keputusan'] = $this->model_data->data_tekad_dan_keputusan($noPendaftaran);
          $data['data_lain'] = $this->model_data->data_lain($noPendaftaran);
          $data['data_keluarga'] = $this->model_data->data_keluarga($noPendaftaran);
          $data['data_rohani'] = $this->model_data->data_rohani($noPendaftaran);
          
          if(!empty($data['data_identitas'])&&!empty($data['data_pp'])&&!empty($data['data_rekomendasi'])&&!empty($data['data_tekad_dan_keputusan'])&&!empty($data['data_lain'])){
  					redirect('done');
          }else{
            redirect('data_identitas');
          }
				}else{
				  $usernameAdm = $this->model_user->loginAdmin($username, $password);
          
          if($usernameAdm){
            // Create session
            $user_data = array(
              'username' => $usernameAdm,
              'logged_in' => true,
              'level' => 'admin'
            );
  
            $this->session->set_userdata($user_data);
  
            // echo "<pre>";
            // print_r($this->session->userdata());
            // echo "</pre>";
            // exit;
  
            // Set message
            $this->session->set_flashdata('user_loggedin','You are now logged in');
              redirect('admin/showAll?kd_prodi=all');
          }

          // Set message
					$this->session->set_flashdata('login_failed','Wrong Username or Password');

					redirect('login');
				}

				// Set Message
				
			}
    }

    public function register(){
			$data['title'] = 'Register';
      $this->form_validation->set_rules('username','Username', 'required');
      $this->form_validation->set_rules('email','Email', 'required|valid_email|callback_check_email_exists');
      $this->form_validation->set_rules('password', 'Password', 'required');
      $this->form_validation->set_rules('password2', 'Confirm Password', 'matches[password]');
      
      if($this->form_validation->run() === FALSE){
				$this->load->view('users/register', $data);
			}else{
				//encrypt password
				$encPassword = md5($this->input->post('password'));

				$data = $this->model_user->register($encPassword);

				// set message
        $this->session->set_flashdata('user_registered','You are now registered and can log in');
        // echo "<pre>";
        // print_r ($data);
        // echo "</pre>";
        // $date = date("y");
        // $date_ = (string)((int)$date+1);
        // $noPendaftaran = $date.$date_.sprintf("%05s",$id);
        // echo $noPendaftaran;


				redirect('login');
			}

    }

    public function check_email_exists($email){
      $this->form_validation->set_message('check_email_exists', 'That email is taken, please choose a different one');
      if($this->model_user->check_email_exists($email)){
				return true;
			}else{
				return false;	
			}
    }

    public function logout(){
      $this->session->unset_userdata('user_id');
			$this->session->unset_userdata('username');
			$this->session->unset_userdata('logged_in');

			// Set message
			$this->session->set_flashdata('user_loggedout','You are now logged out');

			redirect('login');
    }

    public function print_pdf(){
      $logged_in = $this->session->userdata('logged_in');
      $noPendaftaran = $this->session->userdata('no_pendaftaran');
      $level = $this->session->userdata('level');

      if(!empty($logged_in) && $level=='peserta'){
        $data['data_identitas'] = $this->model_data->data_identitas($noPendaftaran);
        $data['data_keluarga'] = $this->model_data->data_keluarga($noPendaftaran);
        $data['data_pp'] = $this->model_data->data_pp($noPendaftaran);
        $data['data_rohani'] = $this->model_data->data_rohani($noPendaftaran);
        $data['data_lain'] = $this->model_data->data_lain($noPendaftaran);

        $this->load->library('pdf');
        $this->pdf->set_option('isRemoteEnabled', TRUE);
        $this->pdf->setPaper('A4', 'potrait');
      
        $this->pdf->filename = "Formulir_Pendaftaran.pdf";
        $this->pdf->load_view('print_pdf', $data);
      }else{
        redirect('login','refresh');
      }

    }
  }
  

?>