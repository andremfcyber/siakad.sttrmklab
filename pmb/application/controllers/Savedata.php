<?php
class Savedata extends CI_Controller{
  /*
   * Programmer : Fitria Wahyuni.S.Pd
   * Keterangan : Controller khusus untuk transakasi penyimpanan data
   */

  public function saveDataPribadi(){
    // echo "<pre>";
    // print_r($this->input->post());
    // echo "</pre>";
    $logged_in = $this->session->userdata('logged_in');
    $noPendaftaran = $this->session->userdata('no_pendaftaran');
    
    if(!empty($logged_in)){
      $minat = '';
      if(empty($this->input->post('minat'))){
        $minat = $this->input->post('minat11');
      }else{
        $minat = $this->input->post('minat');
      }

      $tgl_lahir_pasangan = $this->input->post('tgl_lahir_pasangan');
      if ($tgl_lahir_pasangan == "") {
        $tgl_lahir_pasangan = NULL;
      }

      $tgl_lahir_anak1 = $this->input->post('tgl_lahir_anak1');
      if ($tgl_lahir_anak1 == "") {
        $tgl_lahir_anak1 = NULL;
      }

      $tgl_lahir_anak2 = $this->input->post('tgl_lahir_anak2');
      if ($tgl_lahir_anak2 == "") {
        $tgl_lahir_anak2 = NULL;
      }

      $tgl_lahir_anak3 = $this->input->post('tgl_lahir_anak3');
      if ($tgl_lahir_anak3 == "") {
        $tgl_lahir_anak3 = NULL;
      }

      $tgl_lahir_anak4 = $this->input->post('tgl_lahir_anak4');
      if ($tgl_lahir_anak4 == "") {
        $tgl_lahir_anak4 = NULL;
      }

      $tgl_lahir_anak5 = $this->input->post('tgl_lahir_anak5');
      if ($tgl_lahir_anak5 == "") {
        $tgl_lahir_anak5 = NULL;
      }

      $tgl_lahir_anak6 = $this->input->post('tgl_lahir_anak6');
      if ($tgl_lahir_anak6 == "") {
        $tgl_lahir_anak6 = NULL;
      }

      $umur_pembiaya = $this->input->post('umur_pembiaya');
      if ($umur_pembiaya == "") {
        $umur_pembiaya = NULL;
      }

      $data = array(
        'no_pendaftaran' => $noPendaftaran,
        'nama' => $this->input->post('nama'),
        'tempat_lahir' => $this->input->post('tempat_lahir'),
        'tgl_lahir' => $this->input->post('tgl_lahir'),
        'jk' => $this->input->post('jk'),
        'pekerjaan' => $this->input->post('pekerjaan'),
        'alamat' => $this->input->post('alamat'),
        'kewarganegaraan' => $this->input->post('kewarganegaraan'),
        'gereja_anggota' => $this->input->post('gereja_anggota'),
        'status_baptis' => $this->input->post('status_baptis'),
        'tgl_baptis' => $this->input->post('tgl_baptis'),
        'status_anggota_sidi' => $this->input->post('status_anggota_sidi'),
        'gereja_sidi' => $this->input->post('gereja_sidi'),
        'status_anggota_sidi_penuh' => $this->input->post('status_anggota_sidi_penuh'),


        
        // DATA ORTU
        
        'nama_ortu' => $this->input->post('nama_ortu'),
        'tmptgl_ortu' => $this->input->post('tmptgl_ortu'),
        'nik_ortu' => $this->input->post('nik_ortu'),
        'jeniskelamin_ortu' => $this->input->post('jeniskelamin_ortu'),
        'dusun_ortu' => $this->input->post('dusun_ortu'),
        'kecamatan_ortu' => $this->input->post('kecamatan_ortu'),
        'kota_ortu' => $this->input->post('kota_ortu'),
        'profvinsi_ortu' => $this->input->post('profvinsi_ortu'),
        'pekerjaan_ortu' => $this->input->post('pekerjaan_ortu'),
        'notlp_ortu' => $this->input->post('notlp_ortu'),
        'email_ortu' => $this->input->post('email_ortu'),
        'sudah_berkeluarga_ortu' => $this->input->post('sudah_berkeluarga_ortu'),
        'jaminan_kesehatan_ortu' => $this->input->post('jaminan_kesehatan_ortu'),
        'kewarganegaraan_ortu' => $this->input->post('kewarganegaraan_ortu'),
        'pendidikan_terakhir_ortu' => $this->input->post('pendidikan_terakhir_ortu'),
        'nisn_ortu' => $this->input->post('nisn_ortu'),
        'namasekolah_ortu' => $this->input->post('namasekolah_ortu'),
        'kelastingkat_ortu' => $this->input->post('kelastingkat_ortu'),
        'tahunsa_ortu' => $this->input->post('tahunsa_ortu'),
        'asalgereja_ortu' => $this->input->post('asalgereja_ortu'),
        'pemimpinjemaat_ortu' => $this->input->post('pemimpinjemaat_ortu'),
        'apakabisamelayani_ortu' => $this->input->post('apakabisamelayani_ortu'),
        'programstudi_ortu' => $this->input->post('programstudi_ortu'),
        'namaayah_ortuwali' => $this->input->post('namaayah_ortuwali'),
        'nikayah_ortuwali' => $this->input->post('nikayah_ortuwali'),
        'tglayah_ortuwali' => $this->input->post('tglayah_ortuwali'),
        'namaibu_ortuwali' => $this->input->post('namaibu_ortuwali'),
        'nikibu_ortuwali' => $this->input->post('nikibu_ortuwali'),
        'tglibu_ortuwali' => $this->input->post('tglibu_ortuwali'),
        'namawali_ortuwali' => $this->input->post('namawali_ortuwali'),
        'nikwali_ortuwali' => $this->input->post('nikwali_ortuwali'),
        'tglwali_ortuwali' => $this->input->post('tglwali_ortuwali'),
        'mengetahuisttrmk' => $this->input->post('mengetahuisttrmk'), 

  
        // DATA PACAR

        'nama_pacar' => $this->input->post('nama_pacar'),
        'pekerjaan_pacar' => $this->input->post('pekerjaan_pacar'),
        'agama_pacar' => $this->input->post('agama_pacar'),
        'sudah_menerima_tuhan_yesus' => $this->input->post('sudah_menerima_tuhan_yesus'),
        'hubungan_dimulai' => $this->input->post('hubungan_dimulai'),
        'nama_pasangan' => $this->input->post('nama_pasangan'),
        'tempat_lahir_pasangan' => $this->input->post('tempat_lahir_pasangan'),
        'tgl_lahir_pasangan' => $tgl_lahir_pasangan,
        'pekerjaan_pasangan' => $this->input->post('pekerjaan_pasangan'),
        'pend_terakhir_pasangan' => $this->input->post('pend_terakhir_pasangan'),
        'agama_pasangan' => $this->input->post('agama_pasangan'),
        'pasangan_sudah_menerima_tuhan_yesus' => $this->input->post('pasangan_sudah_menerima_tuhan_yesus'),

        'nama_anak1' => $this->input->post('nama_anak1'),
        'tempat_lahir_anak1' => $this->input->post('tempat_lahir_anak1'),
        'tgl_lahir_anak1' => $tgl_lahir_anak1,
        'keterangan_anak1' => $this->input->post('keterangan_anak1'),

        'nama_anak2' => $this->input->post('nama_anak2'),
        'tempat_lahir_anak2' => $this->input->post('tempat_lahir_anak2'),
        'tgl_lahir_anak2' => $tgl_lahir_anak2,
        'keterangan_anak2' => $this->input->post('keterangan_anak2'),

        'nama_anak3' => $this->input->post('nama_anak3'),
        'tempat_lahir_anak3' => $this->input->post('tempat_lahir_anak3'),
        'tgl_lahir_anak3' => $tgl_lahir_anak3,
        'keterangan_anak3' => $this->input->post('keterangan_anak3'),

        'nama_anak4' => $this->input->post('nama_anak4'),
        'tempat_lahir_anak4' => $this->input->post('tempat_lahir_anak4'),
        'tgl_lahir_anak4' => $tgl_lahir_anak4,
        'keterangan_anak4' => $this->input->post('keterangan_anak4'),

        'nama_anak5' => $this->input->post('nama_anak5'),
        'tempat_lahir_anak5' => $this->input->post('tempat_lahir_anak5'),
        'tgl_lahir_anak5' => $tgl_lahir_anak5,
        'keterangan_anak5' => $this->input->post('keterangan_anak5'),

        'nama_anak6' => $this->input->post('nama_anak6'),
        'tempat_lahir_anak6' => $this->input->post('tempat_lahir_anak6'),
        'tgl_lahir_anak6' => $tgl_lahir_anak6,
        'keterangan_anak6' => $this->input->post('keterangan_anak6'),

        'nama_pembiaya' => $this->input->post('nama_pembiaya'),
        'umur_pembiaya' => $umur_pembiaya,
        'pekerjaan_pembiaya' => $this->input->post('pekerjaan_pembiaya'),
        'alamat_pembiaya' => $this->input->post('alamat_pembiaya'),
        'agama_pembiaya' => $this->input->post('agama_pembiaya'),
        'status_persetujuan_masuk_pembiaya' => $this->input->post('status_persetujuan_masuk_pembiaya'),
        'alasan_pembiaya' => $this->input->post('alasan_pembiaya'),

      );

      // var_dump($data);die();
      //Check udah ada datanya atau belum
      $q = $this->db->get_where('pmb.data_identitas', array('no_pendaftaran' => $noPendaftaran));
      $row = $q->num_rows();
      if($row > 0){
        $data['tgl_update'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->update_data_identitas($data);      
      }else{
        $data['tgl_insert'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->save_data_identitas($data);
      }
      //var_dump($result);die();
      if($result){
        if(($this->input->post('done')!== null)){
          redirect('done');
        }else{
          redirect('data_pp');
        }
      }else{
        $message = "Error! Terjadi Kesalahan Saat Input Data";
        redirect('data_identitas');
        echo "<script type='text/javascript'>alert('$message');</script>";
      }
    }else{
      redirect('login');
    }

  }
  
  public function saveDataKeluarga(){
    // echo "<pre>";
    // print_r($this->input->post());
    // echo "</pre>";exit;
    $logged_in = $this->session->userdata('logged_in');
    $noPendaftaran = $this->session->userdata('no_pendaftaran');
    if(!empty($logged_in)){
      $namaSK = [];
      $jkSK = [];
      $umurSK = [];
      $pendSK = [];
      $agamaSK = [];
      $jmlSaudara = $this->input->post('jml_saudara');
      $index = 0;
      for($i=0; $i<$jmlSaudara; $i++){
        $n = ($this->input->post('nama_sk'.($i+1)) ?: '-');
        $j = ($this->input->post('jk_sk'.($i+1)) ?: '-');
        $u = ($this->input->post('umur_sk'.($i+1)) ?: '-');
        $p = ($this->input->post('pend_sk'.($i+1)) ?: '-');
        $a = ($this->input->post('agama_sk'.($i+1)) ?: '-');
        // echo $n."|".$j."|".$u."|".$p."|".$a."<br>";
        if($n != '-'){
          $namaSK[$index] = $n;
          $jkSK[$index] = $j;
          $umurSK[$index] = $u;
          $pendSK[$index] = $p;
          $agamaSK[$index] = $a;
          $index++;
        }
      }
      // print_r($namaSK);
      // exit;
      $data = array(
        'no_pendaftaran' => $noPendaftaran,
        'nama_ayah' => $this->input->post('nama_ayah'),
        'umur_ayah' => ($this->input->post('umur_ayah')? $this->input->post('umur_ayah') : 0 ),
        'nama_ibu' => $this->input->post('nama_ibu'),
        'umur_ibu' => ($this->input->post('umur_ibu') ? $this->input->post('umur_ibu'):0),
        'alamat_ortu' => $this->input->post('alamat_ortu'),
        'tlp_ortu' => $this->input->post('tlp_ortu'),
        'pend_ayah' => $this->input->post('pend_ayah'),
        'pend_ibu' => $this->input->post('pend_ibu'),
        'pekerjaan_ayah' => $this->input->post('pekerjaan_ayah'),
        'pekerjaan_ibu' => $this->input->post('pekerjaan_ibu'),
        'agama_ayah' => $this->input->post('agama_ayah'),
        'agama_ibu' => $this->input->post('agama_ibu'),
        'nama_wali' => $this->input->post('nama_wali'),
        'alamat_wali' => $this->input->post('alamat_wali'),
        'tlp_wali' => $this->input->post('tlp_wali'),
        'pekerjaan_wali' => $this->input->post('pekerjaan_wali'),
        'hubungan_wali' => $this->input->post('hubungan_wali'),
        'agama_wali' => $this->input->post('agama_wali'),
        'nama_sk' => ($namaSK ? implode('|',$namaSK) : ""),
        'jk_sk' => ($namaSK ? implode('|',$jkSK) : ""),
        'umur_sk' => ($namaSK ? implode('|',$umurSK) : ""),
        'pend_sk' => ($namaSK ? implode('|',$pendSK) : ""),
        'agama_sk' => ($namaSK ? implode('|',$agamaSK) : ""),
        'thn_pcr' => $this->input->post('thn_pcr'),
        'nama_pcr' => $this->input->post('nama_pcr'),
        'agama_pcr' => $this->input->post('agama_pcr'),
        'hub_pcr' => $this->input->post('hub_pcr'),
        'tanggapan_pcr' => $this->input->post('tanggapan_pcr'),
        'thn_div' => $this->input->post('thn_div'),
        'alasan_div' => $this->input->post('alasan_div'),
        'stat_tp' => $this->input->post('stat_tp'),
        'nama_tp' => @$this->input->post('nama_tp')
      );
      // echo "<pre>";
      // print_r($data);
      // echo "</pre>";exit;
      //Check udah ada datanya atau belom
      $q = $this->db->get_where('pmb.data_keluarga', array('no_pendaftaran' => $noPendaftaran));
      $row = $q->num_rows();
      if($row > 0){
        $data['tgl_update'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->update_data_keluarga($data);      
      }else{
        $data['tgl_insert'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->save_data_keluarga($data);
      }

      if($result){
        if(($this->input->post('done')!== null)){
          redirect('done');
        }else{
          redirect('data_pp');
        }
      }else{
        $message = "Error! Terjadi Kesalahan Saat Input Data";
        redirect('data_keluarga');
        echo "<script type='text/javascript'>alert('$message');</script>";
      }
    }else{
      redirect('login');
    }

  }

  public function saveDataPp(){
    $logged_in = $this->session->userdata('logged_in');
    $noPendaftaran = $this->session->userdata('no_pendaftaran');
    if(!empty($logged_in)){

      $data = array(
        'no_pendaftaran' => $noPendaftaran,
        'tk' => $this->input->post('tk'),
        'tk_thn' => $this->input->post('tk_thn'),
        'sd' => $this->input->post('sd'),
        'sd_thn' => $this->input->post('sd_thn'),
        'smp' => $this->input->post('smp'),
        'smp_thn' => $this->input->post('smp_thn'),
        'sma' => $this->input->post('sma'),
        'sma_thn' => $this->input->post('sma_thn'),
        'pendidikantinggi1_jenjang' => $this->input->post('pendidikantinggi1_jenjang'),
        'pendidikantinggi1_nama' => $this->input->post('pendidikantinggi1_nama'),
        'pendidikantinggi1_tahun_ijazah' => $this->input->post('pendidikantinggi1_tahun_ijazah'),

        'pendidikantinggi2_jenjang' => $this->input->post('pendidikantinggi2_jenjang'),
        'pendidikantinggi2_nama' => $this->input->post('pendidikantinggi2_nama'),
        'pendidikantinggi2_tahun_ijazah' => $this->input->post('pendidikantinggi2_tahun_ijazah'),

        'pendidikantinggi3_jenjang' => $this->input->post('pendidikantinggi3_jenjang'),
        'pendidikantinggi3_nama' => $this->input->post('pendidikantinggi3_nama'),
        'pendidikantinggi3_tahun_ijazah' => $this->input->post('pendidikantinggi3_tahun_ijazah'),

        'kursus1' => $this->input->post('kursus1'),
        'kursus1_nama' => $this->input->post('kursus1_nama'),
        'kursus1_ijazah' => $this->input->post('kursus1_ijazah'),

        'kursus2' => $this->input->post('kursus2'),
        'kursus2_nama' => $this->input->post('kursus2_nama'),
        'kursus2_ijazah' => $this->input->post('kursus2_ijazah'),

        'kursus3' => $this->input->post('kursus3'),
        'kursus3_nama' => $this->input->post('kursus3_nama'),
        'kursus3_ijazah' => $this->input->post('kursus3_ijazah'),

        'bakat1' => $this->input->post('bakat1'),
        'keterampilan1' => $this->input->post('keterampilan1'),

        'bakat2' => $this->input->post('bakat2'),
        'keterampilan2' => $this->input->post('keterampilan2'),

        'bakat3' => $this->input->post('bakat3'),
        'keterampilan3' => $this->input->post('keterampilan3'),

        'jenis_kerja1' => $this->input->post('jenis_kerja1'),
        'jabatan_kerja1' => $this->input->post('jabatan_kerja1'),
        'thn_kerja1' => $this->input->post('thn_kerja1'),

        'jenis_kerja2' => $this->input->post('jenis_kerja2'),
        'jabatan_kerja2' => $this->input->post('jabatan_kerja2'),
        'thn_kerja2' => $this->input->post('thn_kerja2'),

        'jenis_kerja3' => $this->input->post('jenis_kerja3'),
        'jabatan_kerja3' => $this->input->post('jabatan_kerja3'),
        'thn_kerja3' => $this->input->post('thn_kerja3'),

        'kesaksian_pertobatan' => $this->input->post('kesaksian_pertobatan'),
        'tempat_pertobatan' => $this->input->post('tempat_pertobatan'),
        'deskripsi_pertobatan' => $this->input->post('deskripsi_pertobatan'),
        'perkembangan_rohani' => $this->input->post('perkembangan_rohani'),
        'berdoa' => $this->input->post('berdoa'),
        'kesaksian_menerima_panggilan_melayani_tuhan' => $this->input->post('kesaksian_menerima_panggilan_melayani_tuhan'),
        'dasar_panggilan' => $this->input->post('dasar_panggilan'),
        'pembimbing' => $this->input->post('pembimbing'),
        'tempat_pelayanan' => $this->input->post('tempat_pelayanan'),
        'penceritaan_injil' => $this->input->post('penceritaan_injil'),

        'jenis_pelayanan1' => $this->input->post('jenis_pelayanan1'),
        'jabatan_pelayanan1' => $this->input->post('jabatan_pelayanan1'),
        'thn_pelayanan1' => $this->input->post('thn_pelayanan1'),

        'jenis_pelayanan2' => $this->input->post('jenis_pelayanan2'),
        'jabatan_pelayanan2' => $this->input->post('jabatan_pelayanan2'),
        'thn_pelayanan2' => $this->input->post('thn_pelayanan2'),

        'jenis_pelayanan3' => $this->input->post('jenis_pelayanan3'),
        'jabatan_pelayanan3' => $this->input->post('jabatan_pelayanan3'),
        'thn_pelayanan3' => $this->input->post('thn_pelayanan3'),
        
      );

      //Check udah ada datanya atau belom
      $q = $this->db->get_where('pmb.data_pp', array('no_pendaftaran' => $noPendaftaran));
      $row = $q->num_rows();
      if($row > 0){
        $data['tgl_update'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->update_data_pp($data);      
      }else{
        $data['tgl_insert'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->save_data_pp($data);
      }

      if($result){
        if(($this->input->post('done')!== null)){
          redirect('done');
        }else{
          redirect('data_rekomendasi');
        }
      }else{
        $message = "Error! Terjadi Kesalahan Saat Input Data";
        redirect('data_pp');
        echo "<script type='text/javascript'>alert('$message');</script>";
      }
    }else{
      redirect('login');
    }

  }

  public function saveDataRohani(){
    // echo "<pre>";
    // print_r($this->input->post());
    // echo "</pre>";
    $logged_in = $this->session->userdata('logged_in');
    $noPendaftaran = $this->session->userdata('no_pendaftaran');
    if(!empty($logged_in)){
      $jenisP = [];
      $tempatP = [];
      $thnP = [];
      $index = 0;
      $jmlPelayanan = $this->input->post('jml_pelayanan');
      for($i=0; $i<$jmlPelayanan; $i++){
        $jp = ($this->input->post('jenis_pelayanan'.($i+1)) ?: '-');
        $tmp = ($this->input->post('tempat_pelayanan'.($i+1)) ?: '-');
        $thp = ($this->input->post('thn_pelayanan'.($i+1)) ?: '-');
        // echo $jp."|".$tmp."|".$thp."<br>";exit;

        if($jp != '-'){
          $jenisP[$index] = $jp;
          $tempatP[$index] = $tmp;
          $thnP[$index] = $thp;
        
          $index++;
        }
      }
      // print_r($jenisP);
      // exit;
      $data = array(
        'no_pendaftaran' => $noPendaftaran,
        'anggota_thn' => $this->input->post('anggota_thn'),
        'baptisA_thn' => $this->input->post('baptisA_thn'),
        'sidi_thn' => $this->input->post('sidi_thn'),
        'baptisD_thn' => $this->input->post('baptisD_thn'),
        'sidi_by' => $this->input->post('sidi_by'),
        'jenis_pelayanan' => ($jenisP ? implode('|',$jenisP) : ""),
        'tempat_pelayanan' => ($jenisP ? implode('|',$tempatP) : ""),
        'thn_pelayanan' => ($jenisP ? implode('|',$thnP) : ""),
        'hambatan_pertumbuhan' => $this->input->post('hambatan_pertumbuhan'),
        'hambatan_pelayanan' => $this->input->post('hambatan_pelayanan'),
        'hambatan_masuk' => $this->input->post('hambatan_masuk'),
        'masalah' => $this->input->post('masalah'),
        'masalah_detail' => $this->input->post('masalah_detail'),
        'jml_baca' => ($this->input->post('jml_baca')?$this->input->post('jml_baca'):0),
        'persembahan_stat' => ($this->input->post('persembahan_stat')?$this->input->post('persembahan_stat'):0),
        'persembahan_alasan' => $this->input->post('persembahan_alasan'),
        'pinjam_stat' => ($this->input->post('pinjam_stat')? $this->input->post('pinjam_stat'):0),
        'hutang' => ($this->input->post('hutang') ? $this->input->post('hutang') : 0),
        'buku' => $this->input->post('buku'),
        'ht_pengaruh' => $this->input->post('ht_pengaruh'),
        'hobby' => $this->input->post('hobby'),
        'olahraga' => $this->input->post('olahraga')
      );
      // echo "<pre>";
      // print_r($data);
      // echo "</pre>";exit;

      // Check udah ada datanya atau belom
      $q = $this->db->get_where('pmb.data_rohani', array('no_pendaftaran' => $noPendaftaran));
      $row = $q->num_rows();
      if($row > 0){
        $data['tgl_update'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->update_data_rohani($data);      
      }else{
        $data['tgl_insert'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->save_data_rohani($data);
      }

      if($result){
        if(($this->input->post('done')!== null)){
          redirect('done');
        }else{
          redirect('data_lain');
        }
      }else{
        $message = "Error! Terjadi Kesalahan Saat Input Data";
        redirect('data_rohani');
        echo "<script type='text/javascript'>alert('$message');</script>";
      }
    }else{
      redirect('login');
    }
    
  }

  public function saveDataLain(){
    // echo "<pre>";
    // print_r($this->input->post());
    // echo "</pre>";
    $logged_in = $this->session->userdata('logged_in');
    $noPendaftaran = $this->session->userdata('no_pendaftaran');
    if(!empty($logged_in)){
      $data = array(
        'no_pendaftaran' => $noPendaftaran,
        'perokok' => $this->input->post('perokok'),
        'waktu_berhenti_merokok' => $this->input->post('waktu_berhenti_merokok'),
        'alasan_berhenti_merokok' => $this->input->post('alasan_berhenti_merokok'),
        'pernah_menjadi_mhs_theologia' => $this->input->post('pernah_menjadi_mhs_theologia'),
        'alasan_pernah_menjadi_mhs_theologia' => $this->input->post('alasan_pernah_menjadi_mhs_theologia'),
      );

      // Check udah ada datanya atau belom
      $q = $this->db->get_where('pmb.data_lain', array('no_pendaftaran' => $noPendaftaran));
      $row = $q->num_rows();
      if($row > 0){
        $data['tgl_update'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->update_data_lain($data);      
      }else{
        $data['tgl_insert'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->save_data_lain($data);
      }

      // Update status done di user biar bisa diliat admin
      // $this->model_saveData->update_done($noPendaftaran);

      if($result){
        if(($this->input->post('done')!== null)){
          redirect('done');
        }else{
          redirect('data_upload');
        }
      }else{
        $message = "Error! Terjadi Kesalahan Saat Input Data";
        redirect('data_upload');
        echo "<script type='text/javascript'>alert('$message');</script>";
      }
    }else{
      redirect('login');
    }
    
  }


  public function saveDataRekomendasi(){
    $logged_in = $this->session->userdata('logged_in');
    $noPendaftaran = $this->session->userdata('no_pendaftaran');
    if(!empty($logged_in)){
      $data = array(
        'no_pendaftaran' => $noPendaftaran,
        'utusan_majelis_jemaat' => $this->input->post('utusan_majelis_jemaat'),
        'utusan_majelis_klasis' => $this->input->post('utusan_majelis_klasis'),
        'utusan_majelis_sinode' => $this->input->post('utusan_majelis_sinode'),
        'utusan_yayasan_pi' => $this->input->post('utusan_yayasan_pi'),
        'utusan_persekutuan_doa' => $this->input->post('utusan_persekutuan_doa'),
        'utusan_pribadi' => $this->input->post('utusan_pribadi'),

        'nama_pemberi_rekomendasi1' => $this->input->post('nama_pemberi_rekomendasi1'),
        'umur_pemberi_rekomendasi1' => $this->input->post('umur_pemberi_rekomendasi1'),
        'pekerjaan_pemberi_rekomendasi1' => $this->input->post('pekerjaan_pemberi_rekomendasi1'),
        'jabatan_gerejawi_permberi_rekomendasi1' => $this->input->post('jabatan_gerejawi_permberi_rekomendasi1'),
        'alamat_pemberi_rekomendasi1' => $this->input->post('alamat_pemberi_rekomendasi1'),

        'nama_pemberi_rekomendasi2' => $this->input->post('nama_pemberi_rekomendasi2'),
        'umur_pemberi_rekomendasi2' => $this->input->post('umur_pemberi_rekomendasi2'),
        'pekerjaan_pemberi_rekomendasi2' => $this->input->post('pekerjaan_pemberi_rekomendasi2'),
        'jabatan_gerejawi_permberi_rekomendasi2' => $this->input->post('jabatan_gerejawi_permberi_rekomendasi2'),
        'alamat_pemberi_rekomendasi2' => $this->input->post('alamat_pemberi_rekomendasi2'),

        'nama_badan' => $this->input->post('nama_badan'),
        'nama_pimpinan' => $this->input->post('nama_pimpinan'),
        'umur' => $this->input->post('umur'),
        'pekerjaan' => $this->input->post('pekerjaan'),
        'alamat' => $this->input->post('alamat'),
      );

      // Check udah ada datanya atau belom
      $q = $this->db->get_where('pmb.data_rekomendasi', array('no_pendaftaran' => $noPendaftaran));
      $row = $q->num_rows();
      if($row > 0){
        $data['tgl_update'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->update_data_rekomendasi($data);      
      }else{
        $data['tgl_insert'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->save_data_rekomendasi($data);
      }

      // Update status done di user biar bisa diliat admin
      // $this->model_saveData->update_done($noPendaftaran);

      if($result){
        if(($this->input->post('done')!== null)){
          redirect('done');
        }else{
          redirect('data_tekad_dan_keputusan');
        }
      }else{
        $message = "Error! Terjadi Kesalahan Saat Input Data";
        redirect('data_upload');
        echo "<script type='text/javascript'>alert('$message');</script>";
      }
    }else{
      redirect('login');
    }
    
  }

  public function saveDataTekadDanKeputusan(){
    $logged_in = $this->session->userdata('logged_in');
    $noPendaftaran = $this->session->userdata('no_pendaftaran');
    if(!empty($logged_in)){
      $data = array(
        'no_pendaftaran' => $noPendaftaran,
        'ketersediaan_mentaati_peraturan' => $this->input->post('ketersediaan_mentaati_peraturan'),
        'penjelasan_ketersediaan_mentaati_peraturan' => $this->input->post('penjelasan_ketersediaan_mentaati_peraturan'),
        'ketersediaan_menerima_pengarahan' => $this->input->post('ketersediaan_menerima_pengarahan'),
        'penjelasan_ketersediaan_menerima_pengarahan' => $this->input->post('penjelasan_ketersediaan_menerima_pengarahan'),
        'ketersediaan_menjalankan_praktek' => $this->input->post('ketersediaan_menjalankan_praktek'),
        'penjelasan_ketersediaan_menjalankan_praktek' => $this->input->post('penjelasan_ketersediaan_menjalankan_praktek'),
      );

      // Check udah ada datanya atau belom
      $q = $this->db->get_where('pmb.data_tekad_dan_keputusan', array('no_pendaftaran' => $noPendaftaran));
      $row = $q->num_rows();
      if($row > 0){
        $data['tgl_update'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->update_data_tekad_dan_keputusan($data);      
      }else{
        $data['tgl_insert'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->save_data_tekad_dan_keputusan($data);
      }

      // Update status done di user biar bisa diliat admin
      // $this->model_saveData->update_done($noPendaftaran);

      if($result){
        if(($this->input->post('done')!== null)){
          redirect('done');
        }else{
          redirect('data_lain');
        }
      }else{
        $message = "Error! Terjadi Kesalahan Saat Input Data";
        redirect('data_upload');
        echo "<script type='text/javascript'>alert('$message');</script>";
      }
    }else{
      redirect('login');
    }
    
  }


  public function saveDataUpload(){
    // echo "<pre>";
    // print_r($this->input->post());
    // echo "</pre>";
    $logged_in = $this->session->userdata('logged_in');
    $noPendaftaran = $this->session->userdata('no_pendaftaran');
    if(!empty($logged_in)){
      if (!is_dir('./assets/userfiles/'.$noPendaftaran)) {
        mkdir('./assets/userfiles/'.$noPendaftaran, 0777, true);
      }
      $config['upload_path'] = './assets/userfiles/'.$noPendaftaran;//configurasi upload path si image yg dipost di create
      $config['allowed_types'] = 'gif|jpg|png|pdf';
      $config['max_size'] = '2048000';
      // $config['max_width'] = '2000';
      // $config['max_height'] = '2000';
      // echo '<pre>';
      // print_r($_FILES);
      // echo '</pre>';exit;
      // echo $config['upload_path'];exit;
      $file_exist = $this->model_data->data_upload($noPendaftaran);
      if(isset($_FILES['file_foto']['name']) && $_FILES['file_foto']['error']==0){
          $file_foto_temp = $_FILES['file_foto']['name'];
          $file_foto = str_replace(' ', '_', $file_foto_temp); 
          $config['file_name'] = $file_foto;

          // $data['file_foto'] = $file_foto;
          $this->load->library('upload');
          $this->upload->initialize($config);
          if(!$this->upload->do_upload('file_foto')){
            // echo $this->upload->display_errors();exit;
            if(!empty($file_exist['file_foto']) || ($file_exist['file_foto'])!=null){
              $data['file_foto'] = $file_exist['file_foto'];
            }else{
              $data['file_foto'] = NULL;
            }
          }else{
            // $data = array('upload_data' => $this->upload->data());
            $data['file_foto'] = $file_foto;
            // $this->upload->display_errors();exit;

          }
        }
        
        if(isset($_FILES['file_ktp']['name']) && $_FILES['file_ktp']['error']==0){
          $file_ktp_temp = $_FILES['file_ktp']['name'];
          $file_ktp = str_replace(' ', '_', $file_ktp_temp); 
          $config['file_name'] = $file_ktp;

          // $data['file_ktp'] = $file_ktp;
          $this->load->library('upload');
          $this->upload->initialize($config);
          if(!$this->upload->do_upload('file_ktp')){
            if(!empty($file_exist['file_ktp']) || ($file_exist['file_ktp'])!=null){
              $data['file_ktp'] = $file_exist['file_ktp'];
            }else{
              $data['file_ktp'] = NULL;
            }
          }else{
            // $data = array('upload_data' => $this->upload->data());
            $data['file_ktp'] = $file_ktp;
          }
        }

        if(isset($_FILES['file_ijazah']['name']) && $_FILES['file_ijazah']['error']==0){
          $file_ijazah_temp = $_FILES['file_ijazah']['name'];
          $file_ijazah = str_replace(' ', '_', $file_ijazah_temp); 
          $config['file_name'] = $file_ijazah;

          // $data['file_ijazah'] = $file_ijazah;
          $this->load->library('upload');
          $this->upload->initialize($config);
          if(!$this->upload->do_upload('file_ijazah')){
            if(!empty($file_exist['file_ijazah']) || ($file_exist['file_ijazah'])!=null){
              $data['file_ijazah'] = $file_exist['file_ijazah'];
            }else{
              $data['file_ijazah'] = NULL;
            }
          }else{
            // $data = array('upload_data' => $this->upload->data());
            $data['file_ijazah'] = $file_ijazah;
          }
        }

        if(isset($_FILES['file_kk']['name']) && $_FILES['file_kk']['error']==0){
          $file_kk_temp = $_FILES['file_kk']['name'];
          $file_kk = str_replace(' ', '_', $file_kk_temp); 
          $config['file_name'] = $file_kk;

          // $data['file_kk'] = $file_kk;
          $this->load->library('upload');
          $this->upload->initialize($config);
          if(!$this->upload->do_upload('file_kk')){
            if(!empty($file_exist['file_kk']) || ($file_exist['file_kk'])!=null){
              $data['file_kk'] = $file_exist['file_kk'];
            }else{
              $data['file_kk'] = NULL;
            }
          }else{
            // $data = array('upload_data' => $this->upload->data());
            $data['file_kk'] = $file_kk;
          }
        }
        $data['no_pendaftaran'] = $noPendaftaran;
      // echo "<pre>";
      // print_r($data);
      // echo "</pre>";exit;

      //Check udah ada datanya atau belom
      $q = $this->db->get_where('pmb.data_upload', array('no_pendaftaran' => $noPendaftaran));
      $row = $q->num_rows();
      if($row > 0){
        $data['tgl_update'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->update_data_upload($data);      
      }else{
        $data['tgl_insert'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->save_data_upload($data);
      }

      // Update status done di user biar bisa diliat admin
      $this->model_saveData->update_done($noPendaftaran);

    //   if($result){
    //     if(($this->input->post('done')!== null)){
    //       redirect('done');
    //     }else{
    //       redirect('data_upload');
    //     }
    //   }else{
    //     $message = "Error! Terjadi Kesalahan Saat Input Data";
    //     redirect('data_lain');
    //     echo "<script type='text/javascript'>alert('$message');</script>";
    //   }
    // }else{
    //   redirect('login');
    }

      redirect('done');
    
  }
}

?>