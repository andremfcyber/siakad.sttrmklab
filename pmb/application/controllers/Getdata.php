<?php
class Getdata extends CI_Controller{
  /*
   * Programmer : Fitria Wahyuni.S.Pd
   * Keterangan : Controller khusus untuk penghubung dengan model untuk pengambilan data
   */

  public function get_data_identitas(){
    $noPendaftaran = $_POST['no_pendaftaran'];
    $data_identitas = $this->model_data->data_identitas($noPendaftaran);
    
    if(!empty($data_identitas)){
      echo json_encode($data_identitas);
    }
  }
  
  public function get_data_keluarga(){
    $noPendaftaran = $_POST['no_pendaftaran'];
    $data_keluarga = $this->model_data->data_keluarga($noPendaftaran);

    if(!empty($data_keluarga)){
      echo json_encode($data_keluarga);
    }
  }

  public function get_data_pp(){
    $noPendaftaran = $_POST['no_pendaftaran'];
    $data_pp = $this->model_data->data_pp($noPendaftaran);

    if(!empty($data_pp)){
      echo json_encode($data_pp);
    }
  }

  public function get_data_rohani(){
    $noPendaftaran = $_POST['no_pendaftaran'];
    $data_rohani = $this->model_data->data_rohani($noPendaftaran);

    if(!empty($data_rohani)){
      echo json_encode($data_rohani);
    }
  }

  public function get_data_lain(){
    $noPendaftaran = $_POST['no_pendaftaran'];
    $data_lain = $this->model_data->data_lain($noPendaftaran);

    if(!empty($data_lain)){
      echo json_encode($data_lain);
    }
  }


  public function get_data_rekomendasi(){
    $noPendaftaran = $_POST['no_pendaftaran'];
    $data_rekomendasi = $this->model_data->data_rekomendasi($noPendaftaran);

    if(!empty($data_rekomendasi)){
      echo json_encode($data_rekomendasi);
    }
  }

  public function get_data_tekad_dan_keputusan(){
    $noPendaftaran = $_POST['no_pendaftaran'];
    $data_tekad_dan_keputusan = $this->model_data->data_tekad_dan_keputusan($noPendaftaran);

    if(!empty($data_tekad_dan_keputusan)){
      echo json_encode($data_tekad_dan_keputusan);
    }
  }

  public function get_data_upload(){
    $noPendaftaran = $_POST['no_pendaftaran'];
    $data_lain = $this->model_data->data_upload($noPendaftaran);

    if(!empty($data_lain)){
      echo json_encode($data_lain);
    }
  }
}
?>